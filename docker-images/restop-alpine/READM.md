build cocoon/restop image
=========================

    docker build -t cocoon/restop .
    docker push cocoon/restop
    

build cocoon/restop:rpi image for raspberry pi
==============================================

    cp Dockerfile arm-Dockerfile
    
edit arm-Dockerfile

    #FROM alpine:3.4
    FROM armhf/alpine:3.4
    

build it 

    docker build -f arm-Dockerfile -t cocoon/restop:rpi .
    docker push cocoon/restop:rpi



