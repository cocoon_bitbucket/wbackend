"""

    queues are a multi-channel communication system

    channels are
        stdin , stdout stdlog

        and behave like fifo pipes

    the api is compatible with python io

    main methods of Queue Api:

        write(line) : to write on stdin channel (fifo)
        readline(): to read a line from stdout  (fifo)

        log.info(msg) : to write to stdlog channel
        readline(channel_name='stdlog') to read from stdlog channel)

    main methods of Channel api

        write(line)
        readline()
        size()
        is_empty()
        clear()



"""

from restop_queues.client import QueueClient
from restop_queues.connectors import EchoConnector
from restop_queues.models import Queue,QueueHub
from restop_queues.controllers import QueueServerBase
from restop_queues.adapters import GenericAdapter,EchoAdapter