"""

    a Hub server

    sample usage

    in master:

        hub= QueueServer()
        hub. start()

    in remote
        # create hub client
        hub_queue_key = 'queue:id:default'
        hub_client= QueueClient(hub_queue_key,redis_db=db)

        # ask hub to start echo server
        hub_client.send('start_adapter', dict(name='echo',run_mode='thread'))
        time.sleep(2)
        queue_key= hub_client.receive()

        # start echo queue client
        queue_client= QueueClient(queue_key)

        # test it
        queue_client.open()
        queue_client.write('hello\n')
        time.sleep(2)
        line= queue_client.readline()
        assert line == 'hello'
        queue_client.close()

        # shutdown echo server
        queue_client.exit()

        time.sleep(2)


"""
import time

from wbackend.model import Database
from restop_queues.apps.queue_server import QueueServer


from config import QUEUES, REDIS

redis_db = Database(
    host= REDIS.get('host', 'localhost'),
    port= REDIS.get('port', 6379),
    db= REDIS.get('db', 0)
)
redis_db.flushdb()

if __name__== '__main__':

    print("start queue server with queues: %s" % QUEUES)
    hub = QueueServer(config=QUEUES,redis_db=redis_db)
    hub.start()

    while 1:
        time.sleep(1)