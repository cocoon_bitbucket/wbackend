import time


from wbackend.model import Database

from wbackend.model import Database
from restop_queues.apps.queue_shell import  QueueShell


from restop_queues import QueueClient
from restop_queues.apps.queue_server import QueueServer

from config import MOCK_QUEUE_SERVER_KEY, REDIS


import logging


redis_db = Database(
    host= REDIS.get('host', 'localhost'),
    port= REDIS.get('port', 6379),
    db= REDIS.get('db', 0)
)


def test_echo_server():
    """

    test local echo server

    :return:
    """


    # create hub client
    hub_client= QueueClient(MOCK_QUEUE_SERVER_KEY, redis_db=redis_db)

    # ask hub to start echo server
    hub_client.send('start_adapter', dict(name='echo', run_mode='thread'))
    time.sleep(2)
    status,queue_key = hub_client.receive()

    # start echo queue client
    queue_client = QueueClient(queue_key)

    # test it
    queue_client.open()
    queue_client.write('hello\n')
    time.sleep(2)
    line = queue_client.readline()
    assert line == 'hello'
    queue_client.close()

    # shutdown echo server
    queue_client.exit()

    time.sleep(2)


def test_usb_echo():
    """

        test pyserial loop SerialConnector

    :return:
    """

    redis_db = Database('localhost', 6379, 0)
    config= {
        'usb0': {
        'class': 'SerialConnector',
        # a special device from pyserial library acting like an echo server
        'device': "loop://?logging=debug",
        'bauds': 115200
        }
    }

    # create hub server
    server= QueueServer(config,redis_db=redis_db)
    server.start()


    # create hub client
    hub_client= QueueClient(MOCK_QUEUE_SERVER_KEY, redis_db=redis_db)

    # ask hub to start echo server
    hub_client.send('start_adapter', dict(name='usb0', run_mode='thread'))
    time.sleep(2)
    status,queue_key = hub_client.receive()

    # start echo queue client
    queue_client = QueueClient(queue_key,redis_db=redis_db)

    # test it
    queue_client.open()

    #flush
    line= queue_client.readline()
    print(line)
    while line:
        line = queue_client.readline()
        print(line)

    queue_client.write('ls -l /tmp\n')
    time.sleep(2)
    line = queue_client.readline()
    print(line)
    assert line=='ls -l /tmp'
    while line:
        line = queue_client.readline()
        print(line)

    #assert line == 'hello'
    queue_client.close()

    # shutdown echo server
    queue_client.exit()

    time.sleep(2)

    return



def test_usb_server():

    redis_db= Database('192.168.1.21')

    # create hub client
    hub_client= QueueClient(MOCK_QUEUE_SERVER_KEY, redis_db=redis_db)

    # ask hub to start echo server
    hub_client.send('start_adapter', dict(name='usb1', run_mode='thread'))
    time.sleep(2)
    status,queue_key = hub_client.receive()

    # start echo queue client
    queue_client = QueueClient(queue_key,redis_db=redis_db)

    # test it
    queue_client.open()

    #flush
    line= queue_client.readline()
    print(line)
    while line:
        line = queue_client.readline()
        print(line)

    queue_client.write('ls -l /tmp"\n')
    time.sleep(2)
    line = queue_client.readline()
    print(line)
    while line:
        line = queue_client.readline()
        print(line)

    #assert line == 'hello'
    queue_client.close()

    # shutdown echo server
    queue_client.exit()

    time.sleep(2)


def test_usb_with_auth():
    """

        test pyserial loop SerialConnector

    :return:
    """
    redis_db = Database('localhost', 6379, 0)
    redis_db.flushdb()
    #device= "loop://?logging=debug"
    device= "/dev/ttyUSB2"


    config= {
        'usb': {
            'class': 'SerialAuthConnector',
            'device': device,
            'bauds': 115200,
            'encoding': 'UTF-8',
            'prompt_login': 'login:',
            'prompt_password': 'assword:',
            'login': 'root',
            'password': 'sah'
        }
    }

    # create hub server
    server= QueueServer(config,redis_db=redis_db)
    server.start()


    # create hub client
    hub_client= QueueClient(MOCK_QUEUE_SERVER_KEY, redis_db=redis_db)

    # ask hub to start echo server
    hub_client.send('start_adapter', dict(name='usb', run_mode='thread'))
    time.sleep(2)
    status,queue_key = hub_client.receive()

    # start echo queue client
    queue_client = QueueClient(queue_key,redis_db=redis_db)

    # test it
    queue_client.open()

    #flush
    # line= queue_client.readline()
    # print(line)
    # while line:
    #     line = queue_client.readline()
    #     print(line)


    time.sleep(1200)
    # queue_client.write('ls -l /tmp\n')
    # time.sleep(2)
    # line = queue_client.readline()
    # print(line)
    # assert line=='ls -l /tmp'
    # while line:
    #     line = queue_client.readline()
    #     print(line)

    #assert line == 'hello'
    queue_client.close()

    # shutdown echo server
    queue_client.exit()

    time.sleep(2)

    return


def test_mock_queue():
    """


    :return:
    """
    # create hub client
    hub_client = QueueClient(MOCK_QUEUE_SERVER_KEY, redis_db=redis_db)

    # ask hub to start echo server
    hub_client.send('start_adapter', dict(name='mock', run_mode='thread'))
    time.sleep(2)
    status,queue_key = hub_client.receive()

    # start echo queue client
    queue_client = QueueClient(queue_key, redis_db=redis_db)

    # test it
    queue_client.open()

    queue_client.write("ping")


    # implements the mock device
    mock= QueueClient(queue_key, redis_db=redis_db)
    # read the input channel
    line= mock.readline(channel_name='stdin')
    assert line.strip() == "ping"
    # simulate the pong response
    mock.write("pong\n",channel_name='stdout')


    # back to the client to read the pong response
    line= queue_client.readline()
    assert line.strip() == "pong"

    # close
    queue_client.close()
    mock.close()


    return



def queue_shell():
    """


    :return:
    """
    prompt = QueueShell(host=REDIS.get('host',REDIS.get('host','localhost')))
    prompt.cmdloop('Starting queue_shell...')


if __name__== '__main__':

    logging.basicConfig(level=logging.DEBUG)


    #test_echo_server()
    #test_usb_echo()
    #test_usb_server()
    #test_usb_with_auth()
    #test_mock_queue()
    queue_shell()

    print "Done"