


# How to: usb queues

#overview

usb queues allow access via a redis connection to a remote usb (serial) port 

we neeed 2 components

##a server 

with usb or serial plug connected to the real devices

the server open the serial ports and create queues to access it


## a client

who connects via redis to the pseudo-usb queues


# install 

## install wbackend (restop core)

python 2.7

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/wbackend.git restop
    cd restop
    pip install -r requirements.txt
    python setup.py install
    
## testing

    cd  restop_queues/tutorial/usb_queues
    python client.py




