

REDIS= {
    #'host': 'localhost',
    'host': '192.168.1.21',
    'port': 6379,
    'db': 0
}

MOCK_QUEUE_SERVER_KEY= "queuehub:id:default"


loop_serial= "loop://?logging=debug"

QUEUES= {

    'usb0': {
        'class': 'SerialConnector',
        'device': '/dev/ttyUSB0',
        'bauds': 115200
    },
    'usb1': {
        'class': 'SerialAuthConnector',
        #'device': '/dev/ttyUSB1',
        'device': loop_serial,
        'encoding': 'UTF-8',
        'prompt': "/cfg/system/root #",
        'prompt_login': 'login:',
        'prompt_password': 'assword:',
        'login': 'admin',
        'password': 'admin1234'
    },
    'usb2': {
        'class': 'SerialAuthConnector',
        'device': '/dev/ttyUSB2',
        'encoding': 'UTF-8',
        'prompt': "/cfg/system/root #",
        'prompt_login': 'login:',
        'prompt_password': 'assword:',
        'login': 'admin',
        'password': 'admin1234'
    },
    'usb3': {
        'class': 'SerialConnector',
        'device': '/dev/ttyUSB3',
        'bauds': 115200
    },

    'echo': {
        'class':'EchoConnector'
        },

    '_loop': {
        'class':'SerialConnector',
        'device':loop_serial,
        'bauds':115200
    },

    'shell': {
        'class': 'ProcessConnector',
        'command_line': 'bash\n'
    },

    'mock': {
        'class': 'QueueConnector',
        'queue_key': 'queue:id:mock',
        'host': REDIS['host']

    }
}

