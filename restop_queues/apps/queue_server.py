"""

    a Hub server

    sample usage

    in master:

        hub= QueueServer()
        hub. start()

    in remote
        # create hub client
        hub_queue_key = 'queue:id:default'
        hub_client= QueueClient(hub_queue_key,redis_db=db)

        # ask hub to start echo server
        hub_client.send('start_adapter', dict(name='echo',run_mode='thread'))
        time.sleep(2)
        queue_key= hub_client.receive()

        # start echo queue client
        queue_client= QueueClient(queue_key)

        # test it
        queue_client.open()
        queue_client.write('hello\n')
        time.sleep(2)
        line= queue_client.readline()
        assert line == 'hello'
        queue_client.close()

        # shutdown echo server
        queue_client.exit()

        time.sleep(2)



    sample config:

    config= {

        'usb0': {
            'class': 'SerialConnector',
            'device': '/dev/ttyUSB0',
            'bauds': 115200
        },
        'usb1': {
            'class': 'SerialConnector',
            'device': '/dev/ttyUSB1',
            'bauds': 115200,
            'encoding': 'UTF-8'
        },
        'usb2': {
            'class': 'SerialConnector',
            'device': '/dev/ttyUSB2',
            'bauds': 115200
        },
        'usb3': {
            'class': 'SerialConnector',
            'device': '/dev/ttyUSB3',
            'bauds': 115200
        },

        'echo': {
            'class':'EchoConnector'
            },

        '_loop': {
            'class':'SerialConnector',
            'device':loop_serial,
            'bauds':115200
        },

        'shell': {
            'class': 'ProcessConnector',
            'command_line': 'bash\n'
        },


"""
import time

from wbackend.model import Model, Database
from restop_queues import Queue,QueueHub
from restop_queues.controllers.hub import ProcessQueueHubServer

# import connectors plugins (do not remove the following imports)
from restop_queues.connectors import EchoConnector
from restop_queues.connectors.process_connector import ProcessConnector
from restop_queues.connectors.queue_connector import QueueConnector
from restop_queues.connectors.telnet_connector import TelnetConnector
from restop_queues.connectors.serial_connector import SerialConnector,SerialAuthConnector
#from restop_usb.connectors import SerialConnector


class QueueServer(object):
    """
        create and start a hub server : default

        create queues from a given config

    """
    name= "default"


    def __init__(self,config,redis_db=None):
        """

        """
        self.config= config
        if not redis_db:
            redis_db = Database('localhost',6379,0)
        Model.bind(database=redis_db, namespace=None)
        self.redis_db= redis_db
        self.create_queues()
        return


    def create_queues(self):
        """

        :return:
        """
        # create queues
        for name, kwargs in self.config.iteritems():
            queue = Queue(name=name, parameters=kwargs)
            queue.save()

        # create hub queue
        QueueHub.create(name=self.name)
        return


    def start(self):
        """
            start hub server
        :return:
        """
        # create hub
        hub_model = QueueHub.load(self.name)
        hub = ProcessQueueHubServer(hub_model)
        hub.start()
        return


if __name__== '__main__':
    """


    """

    redis_db= Database('localhost',6379,0)
    config= {
        'usb0': {
            'class': 'SerialConnector',
            'device': '/dev/ttyUSB0',
            'bauds': 115200,
            'encoding': 'UTF-8'
        }
    }

    hub = QueueServer(config,redis_db=redis_db)
    hub.start()

    while 1:
        time.sleep(1)