
import time

from walrus import Database

from restop_queues import QueueClient

from cmd import Cmd


redis_db=Database('localhost')
#redis_db.flushdb()


class QueueShell(Cmd):
    """
        open a queue from a queue server , send command and receive responses

    """

    hub_key= 'queuehub:id:default'
    hub_host= 'localhost'

    default_prompt = '> '
    use_rawinput = 1

    def __init__(self, host=None,hub_key=None,completekey='tab', stdin=None, stdout=None):
        """

        :param completekey:
        :param stdin:
        :param stdout:
        """
        Cmd.__init__(self,completekey=completekey,stdin=stdin,stdout=stdout)

        if host:
            self.hub_host=host
        if hub_key:
            self.hub_key= hub_key

        self.prompt= self.default_prompt
        self.current_queue= None
        self.current_queue_name= ''
        self.current_queue_key= ''

        self.redis_db= Database(self.hub_host)
        self.hub_client= QueueClient(self.hub_key,redis_db=self.redis_db)
        # test redis connection
        assert self.redis_db.ping() == True ,"cannot reach redis at %s" % self.hub_host
        return


    def precmd(self, line):
        """Hook method executed just before the command line is
        interpreted, but after the input prompt is generated and issued.

        """
        return line

    def postcmd(self, stop, line):
        """Hook method executed just after a command dispatch is finished."""
        return stop

    def preloop(self):
        """Hook method executed once when the cmdloop() method is called."""
        pass

    def postloop(self):
        """Hook method executed once when the cmdloop() method is about to
        return.

        """
        pass

    def emptyline(self):
        """Called when an empty line is entered in response to the prompt.

        If this method is not overridden, it repeats the last nonempty
        command entered.

        """
        if self.current_queue:
            # queue opened:  read queue
            self.do_read()

        else:
            # no queue opened
            if self.lastcmd:
                # repeat last command
                return self.onecmd(self.lastcmd)


    def default(self, line):
        """Called on an input line when the command prefix is not recognized.

        If this method is not overridden, it prints an error message and
        returns.

        """
        if self.current_queue:
            # send line to queue
            self.current_queue.write(line)
        else:
            # no queue opened
            self.stdout.write('*** Unknown syntax: %s\n'%line)


    def do_open(self,args):
        """
            open a named queue
        """
        queue_name= args.strip()
        self.stdout.write("opening: %s\n" % queue_name)
        # ask hub to start echo server
        self.hub_client.send('start_adapter', dict(name= queue_name, run_mode='thread'))
        time.sleep(4)
        status,key = self.hub_client.receive()
        if not status:
            #if key and not ':' in key:
            self.stdout.write('fail to open queue %s : %s\n' % (queue_name,key))
            return
        # start echo queue client
        self.current_queue_key= key
        self.current_queue = QueueClient(self.current_queue_key,redis_db=self.redis_db)
        self.current_queue_name= queue_name

        self.prompt= "%s> " % queue_name
        return

    def do_close(self,args):
        """
            close the current opened queue
        """
        if self.current_queue:
            self.stdout.write("closing: %s\n" % self.current_queue_name)
            self.current_queue.close()
            time.sleep(0.5)
            self.current_queue.exit()
            time.sleep(0.5)
            self.current_queue=None
            self.current_queue_name = ''
            self.current_queue_key= ''

            self.prompt = self.default_prompt
        else:
            print('no current queue\n')


    def do_send(self,args):
        """
            send command to an opened queue
        """
        if self.current_queue:
            self.stdout.write("sending [%s] ...\n" % args)
            self.current_queue.write("%s\n" % args)
        else:
            self.stdout.write('no current queue\n')
        return


    def do_read(self,args=None):
        """
            read content of an opened queue
        """
        if self.current_queue:
            #print('read queue: %s' % current_queue_name)
            line= self.current_queue.readline()
            while line:
                self.stdout.write("%s\n" % line)
                line= self.current_queue.readline()
        else:
            self.stdout.write('no current queue\n')


    def do_quit(self, args):
        """Quits the program."""
        self.stdout.write("Quitting.\n")
        if self.current_queue:
            self.do_close(args)
        raise SystemExit


    def do_list(self,args):
        """

        :param args:
        :return:
        """
        self.stdout.write("list queues ...\n" )
        # ask hub to start echo server
        self.hub_client.send('list_queues')
        time.sleep(4)
        status, queues = self.hub_client.receive()
        if not status:
            # if key and not ':' in key:
            self.stdout.write('fail to list queues\n')
        else:
            self.stdout.write("queues: %s\n" % str(queues))

    def do_inspect(self,name):
        """

        :param name:
        :return:
        """
        self.stdout.write("inspect queue: %s ...\n" % name)
        # ask hub to start echo server
        self.hub_client.send('inspect_queue',dict(name=name))
        time.sleep(4)
        status, data = self.hub_client.receive()
        if not status:
            # if key and not ':' in key:
            self.stdout.write('fail to inspect queue: %s\n' % name)
        else:
            self.stdout.write("configuration: %s\n" % str(data))


if __name__ == '__main__':

    import logging
    logging.basicConfig(level=logging.DEBUG)


    prompt = QueueShell()
    prompt.cmdloop('Starting queue__shell...')


#
# projects
#
import sys
import select

class Stdin(object):
    """
        a non blocking stdin (unix only )

    """
    def __init__(self):
        """


        """
        self.stdin= sys.stdin

    def readline(self,size=-1):
        """

        :param size:
        :return:
        """
        if sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
            # if line ready
            line = self.stdin.readline()
            if line:
                return line
            else:  # an empty line means stdin has been closed
                print('EOF')
                return 'EOF'
        else:
            # nothing to read
            return ''

    def __getattr__(self, item):
        """
        :param item:
        :return:
        """
        return getattr(self.stdin,item)