import time
import subprocess
import os
import fcntl

from walrus import Database

from . import AbstractConnector



class ProcessConnector(AbstractConnector):
    """

        a connector to write on a process

    """
    def __init__(self, name ,log=None,command_line='bash\n',**kwargs):
        """

        :param name: string  the queue_key eg queue:id:destination
        :param log:
        :param redis_db:
        """
        queue_key= name
        #redis_db= redis_db or Database(host='localhost',port=6379,db=0)
        super(ProcessConnector,self).__init__(queue_key,log=log,**kwargs)
        self.shell= True
        self.command_line= command_line
        self._buffer= []
        self._remain=""

    def open(self, **kwargs):
        """ start shell process """
        self.log.debug(("Popen " + self.command_line))
        self._driver = subprocess.Popen(
            self.command_line,
            shell=self.shell,
            bufsize=0,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            universal_newlines=False
        )
        time.sleep(2)
        started = self._driver.poll()
        self.log.debug("shell connector started=%s" % started)
        return True

    def close(self):
        """ close shell process """
        self._driver.terminate()


    def write(self, line,**kwargs):
        """
            write line to serial port

        :param line:
        :return:
        """
        if not line.endswith('\n'):
            line= line + b'\n'
        self._driver.stdin.writelines(line + "\n")
        self._driver.stdin.flush()


    def readline(self, wait= False,**kwargs):
        """

        :param wait:
        :param kwargs:
        :return:
        """
        if wait:
            # blocking read
            line = self._driver.stdout.readline()
            return line
        else:
            if self._buffer:
                # still things in buffer pop first
                return self._buffer.pop(0)
            else:
                # buffer is empty get some more
                #complete= False
                all= self._read()

                if all == '':
                    # empty
                    return None

                self._buffer= all.split('\n')
                if self._remain:
                    self._buffer[0]= self._remain + self._buffer[0]
                self.remain=""
                if self._buffer[-1] == '':
                    # complete: remote last blank line
                    self._buffer.pop(-1)
                else:
                    # last line was not complete: remember it
                    self._remain= self._buffer.pop(-1)
                return self._buffer.pop(0)

    def _read(self):
        """
            non blocking read
        :param wait:
        :return:
        """
        output= self._driver.stdout
        fd= output.fileno()
        fl = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
        try:
            return output.read()
        except:
            return ""
