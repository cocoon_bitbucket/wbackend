import time
from telnetlib import Telnet

from . import AbstractConnector, logger



class TelnetConnector(AbstractConnector):
    """




    """
    def __init__(self,name ,log=None,
                 host='localhost',
                 user=None,
                 password=None,port=23,
                 user_prompt= 'sername: ',
                 password_prompt= 'assword: ', **kwargs):
        """

        :param device:
        :param bauds:
        """
        super(TelnetConnector,self).__init__(name,log=log,**kwargs)
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.user_prompt= user_prompt
        self.password_prompt= password_prompt


    def setup(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        return True, "setup ok"


    def open(self, **kwargs):
        """
            start serial port

        """
        self._driver = Telnet(self.host, port=self.port, timeout=5)

        if self.user:
            rc = self._driver.read_until(self.user_prompt, timeout=5)
            self.write(self.user + "\n")
            if self.password:
                rc = self._driver.read_until(self.password_prompt, timeout=5)
                self.write(self.password + "\n")
                time.sleep(1)

        flush = self._driver.read_until('\n', timeout=5)

        return True


    def close(self):
        """
            kill slave process
        """
        self._driver.close()


    def write(self, line,**kwargs):
        """
            write line to serial port

        :param line:
        :return:
        """
        buffer = line.encode('ascii')
        self._driver.write(buffer)


    def readline(self, **kwargs):
        """

        :param channel_name:
        :return:
        """
        line = self._driver.read_until('\n', timeout=1)
        line= line.strip('\n').strip('\r')
        return line
