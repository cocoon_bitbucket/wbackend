import time
from walrus import Database
from restop_queues.client import QueueClient

from . import AbstractConnector, logger



class QueueConnector(AbstractConnector):
    """

        a connector to write on a queue via a queue client

    """
    def __init__(self, name ,log=None,queue_key=None,
                 redis_db=None,
                 host= 'localhost',port=6379,db=0,
                 **kwargs):
        """

        :param name: string  the queue_key eg queue:id:destination
        :param log:
        :param redis_db:
        """
        queue_key= queue_key or name
        redis_db= redis_db or Database(host=host,port=port,db=db)
        super(QueueConnector,self).__init__(queue_key,log=log,**kwargs)
        self._driver= QueueClient(queue_key,log=log,redis_db=redis_db,**kwargs)


    def open(self, **kwargs):
        """
            start serial port

        """
        return True


    def close(self):
        """
            kill slave process
        """
        return True


    def write(self, line,channel_name='stdin',**kwargs):
        """
            write line to serial port

        :param line:
        :return:
        """
        self._driver.write(line,channel_name=channel_name)


    def readline(self, channel_name='stdout',**kwargs):
        """

        :param channel_name:
        :return:
        """
        return self._driver.readline(channel_name=channel_name)

