import time
import serial
import codecs
#from serial.serialutil import to_bytes
import logging


from . import AbstractConnector

loop_serial= "loop://?logging=debug"
mock_serial= "mock://?logging=debug"



class SerialConnector(AbstractConnector):
    """


    """
    def __init__(self,name, device=loop_serial,bauds=115200,log=None,**kwargs):
        """

        :param device:
        :param bauds:
        """
        self.name= name
        # check parameters
        status, msg = self.setup(**kwargs)
        # init
        super(SerialConnector,self).__init__(name,log=log,**kwargs)
        self.device= device
        self.bauds= bauds
        self._driver=None
        # check setup
        if not status:
            self.log.error(msg)
            raise RuntimeError(msg)
        else:
            self.log.debug(msg)


    def setup(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        return True, "setup ok"

    def open(self, **kwargs):
        """
            start serial port

        """

        device= self.device
        bauds= self.bauds
        # create serial port
        if device.startswith('loop://'):
            # dummy loop serial
            self.log.debug('serial connector: start pyserial loop(%s)' % (device))
            self._driver=serial.serial_for_url(device)
        # elif device.startswith('mock://'):
        #     # dummy mock serial
        #     self.log.debug('serial server: start mock adapter(%s)' % (device))
        #     self.proc=MockProcess(device[7:],redis_db=self.database)
        else:
            self.log.debug('serial connector: opening serial port with  pyserial (%s|%s)' % (device,bauds))
            self._driver=serial.Serial(
                port=device,baudrate=bauds,bytesize=serial.EIGHTBITS,parity= serial.PARITY_NONE, stopbits=1,
                timeout=1
            )
            self.log.debug('serial connector: port open')

        self._driver._timeout=0
        #self._driver._timeout = 1


        return True


    def close(self):
        """
            kill slave process
        """
        self.log.debug('serial connector: closing port for %s' % str(self._driver))
        if self._driver:
            self._driver.close()
            del self._driver
            self._driver=None
        self.log.debug("serial connector: port closed")
        return
    close=close

    def write(self, line,**kwargs):
        """
            write line to serial port

        :param line:
        :return:
        """
        #line= to_bytes(line)
        if self._driver:
            # if not line.endswith('\n'):
            #     line= line + '\n'
            line= bytearray(line,encoding='utf-8')
            self._driver.write(line)
            self._driver.flush()
        else:
            self.log.error('serial connector: no driver')


    # def readline(self,size=-1, **kwargs):
    #     """
    #
    #     :param size:
    #     :param kwargs:
    #     :return:
    #     """
    #     if self._driver:
    #         line= self._driver.readline()
    #         return line
    #     else:
    #         self.log.error('serial connector: no connector')
    #     return None


    def readline(self,size=-1, **kwargs):
        """

        :param size:
        :param kwargs:
        :return:
        """
        if self._driver:
            line= self._driver.readline()
            return self.filter_out(line)
        else:
            self.log.error('serial connector: no connector')
        return None

    def filter_out(self,line):
        """
        filter stdout of the device
        :param line:
        :return:
        """
        return line


class SerialAuthConnector(SerialConnector):
    """

        a serial connector who handles a login/password


    """
    required_model_parameters = ['prompt_login', 'prompt_password','prompt' ,'login', 'password']


    # def __init__(self,name, device=loop_serial,bauds=115200,log=None,**kwargs):
    #     """
    #
    #     :param device:
    #     :param bauds:
    #     """
    #     self.name= name
    #
    #     # check parameters
    #     status,msg= self.setup(**kwargs)
    #
    #     # # check parameters
    #     # ok= True
    #     # missing= []
    #     # self.auth_parameters= {}
    #     # for p in self.required_model_parameters:
    #     #     try:
    #     #         self.auth_parameters[p]=  kwargs.pop(p)
    #     #     except KeyError:
    #     #         ok= False
    #     #         missing.append(p)
    #     # init ( setting logger)
    #     super(SerialAuthConnector,self).__init__(name,device=device,bauds=bauds,log=log,**kwargs)
    #     if not status:
    #         self.log.error(msg)
    #         raise RuntimeError(msg)
    #     else:
    #         self.log.debug(msg)


    def setup(self,**kwargs):
        """
        setup and check auth parameters
        :param kwargs:
        :return: status, msg
        """
        self.connected= False
        status = True
        msg= "setup auth parameters ok"
        missing = []
        self.auth_parameters = {}
        for p in self.required_model_parameters:
            try:
                value= kwargs.pop(p)
                #value= bytearray(value,encoding='utf-8')
                self.auth_parameters[p] = value
            except KeyError:
                status = False
                missing.append(p)
                msg = 'missing parameters for SerialAuthConnector %s : %s' % (self.name, str(missing))
        return status,msg


    # def readline(self,size=-1, **kwargs):
    #     """
    #
    #     :param size:
    #     :param kwargs:
    #     :return:
    #     """
    #     if self._driver:
    #         line= self._driver.readline()
    #         return self.filter_out(line)
    #     else:
    #         self.log.error('serial connector: no connector')
    #     return None

    def filter_out(self, line):
        """
            filter the stdout of the device

        :param line:
        :return:
        """
        if not self.connected:

            if line:
                dline= codecs.decode(line,'utf-8')
                if self.auth_parameters['prompt_login'] in dline:
                    # intercept login prompt
                    self.log.debug('intercept login prompt: %s' % self.auth_parameters['prompt_login'])
                    self.log.debug('inject login: %s' % self.auth_parameters['login'])
                    self.write("%s\n" % self.auth_parameters['login'])
                    time.sleep(1)
                    self.log.debug('inject password')
                    self.write(b"%s\n" % self.auth_parameters['password'])
                    time.sleep(0.5)
                    self.write(b"\n\n")
                    self.connected= True

                elif line.startswith(self.auth_parameters['prompt']):
                    # intercept prompt  eg /cfg/system/root #
                    self.log.debug('intercept prompt: %s' % self.auth_parameters['prompt'])
                    self.connected= True

                # elif self.auth_parameters['prompt_password'] in line:
                #     # intercept password prompt:
                #     self.log.debug('intercept password prompt: %s' % self.auth_parameters['prompt_password'])
                #     self.log.debug('inject password')
                #     self.write(b"%s\n" % self.auth_parameters['password'])
                #     time.sleep(0.5)
            else:
                # empty line try to get the prompt login
                self.log.debug("empty line")
                self.write(b"\n\n")
                time.sleep(1)


        return line
