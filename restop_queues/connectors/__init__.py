import logging

logger= logging.getLogger('connectors')


def ConnectorFactory(ConnectorClass, name='generic', log=None, *args,**kwargs):
    """
        a Factory to add a connector to an adapter

        sample usage:
            from restop_queues.adapters import GenericAdapter
            from restop_queues.connectors import ConnectorFactory, EchoConnector

            adapter = GenericAdapter(queue, run_mode='thread')
            factory= ConnectorFactory(EchoConnector)
            adapter.bind_connector(factory)
            adapter.start()

    :param ConnectorClass:  a class derived from AbastractConnector
    :param name: string
    :param log: a logger
    :param kwargs:
    :return:
    """
    def wrapper():
        """

        :return:
        """
        connector = ConnectorClass(name, log=log, *args, **kwargs)
        return connector

    return wrapper


class PluginConnectorMount(type):
    """
        The base of the plugin system
    """
    def __init__(cls, name, bases, attrs):
        """

        :param name:
        :param bases:
        :param attrs:
        """
        super(PluginConnectorMount,cls).__init__(name,bases,attrs)
        if not hasattr(cls, 'plugins'):
            # This branch only executes when processing the mount point itself.
            # So, since this is a new plugin type, not an implementation, this
            # class shouldn't be registered as a plugin. Instead, it sets up a
            # list where plugins can be registered later.
            cls.plugins = []
        else:
            # This must be a plugin implementation, which should be registered.
            # Simply appending it to the list is all that's needed to keep
            # track of it later.
            cls.plugins.append(cls)

    def get_plugins(cls, *args, **kwargs):
        return sorted([p(*args, **kwargs) for p in cls.plugins])

    def select_plugin(cls,classname):
        """
            name of the class
        """
        for item in reversed(cls.plugins):
            if item.__name__ == classname:
                # warning in case of duplicates: return only the last one
                return item

        # not found: return None
        raise KeyError('no such connector plugin: %s' % classname)



class AbstractConnector(object):
    """
        A connector is a io api compatible object
        provides at least write() and readline() methods

    """
    __metaclass__ = PluginConnectorMount

    def __init__(self,name ,log=None,**kwargs):
        """

        :param device:
        :param bauds:
        """
        self._name= name
        self.log= log or logger
        self._driver= None

        for key,value in kwargs.iteritems():
            setattr(self,key,value)

    def open(self, **kwargs):
        """
            start serial port

        """
        return True

    def close(self,**kwargs):
        """
            kill slave process
        """
        return True


    def write(self, line,**kwargs):
        """
            write line to serial port

        :param line:
        :return:
        """
        raise NotImplementedError

    def write_lines(self, lines, **kwargs):
        """

        :param lines:
        :param kwargs:
        :return:
        """
        if isinstance(lines, (list, tuple)):
            for line in lines:
                self.write(line)
        else:
            self.write(lines)


    def read(self,size=-1, **kwargs):
        """

        :param channel_name:
        :return:
        """
        raise NotImplementedError

    def readline(self,size=-1, **kwargs):
        """

        :param channel_name:
        :return:
        """
        raise NotImplementedError


    def readlines(self,hint=-1,**kwargs):
        """

        :param kwargs:
        :return:
        """
        lines=[]
        while 1:
            line= self.readline()
            if line:
                lines.append(line)
            else: break
        return lines



class EchoConnector(AbstractConnector):
    """


    """
    def __init__(self,name,log=logger,**kwargs):
        """

        :param name:
        :param log:
        :param kwargs:
        """
        super(EchoConnector,self).__init__(name,log=log, **kwargs)
        self._buffer= []


    def write(self, line,**kwargs):
        """
            write line to serial port

        :param line:
        :return:
        """
        self._buffer.append(line)


    def readline(self,size=-1,**kwargs):
        """

        :param size:
        :param kwargs:
        :return:
        """
        try:
            line= self._buffer.pop(0)
        except IndexError:
            line= None
        return line

