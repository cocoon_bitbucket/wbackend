"""

    adapters are queues to interface redis channels (stdin,stdoout) to a connector

"""
import time
import threading
from controllers import QueueServerBase
from .controllers.hub import QueueHubServerBase
from .connectors import EchoConnector,ConnectorFactory,AbstractConnector



class AbstractAdapter(QueueHubServerBase):
    """
        adapter

         relay redis channel stdin to a connector stdin (write)
         relay redis channel stdout to a connector stdout (read)

    """
    def open_connector(self):
        """
            start serial port

        """
        #self.connector=EchoConnector('echo',log=self.log)
        #return True
        if self.connector:
            self.connector.open()


    def close_connector(self):
        """
            kill slave process
        """
        if self.connector:
            self.connector.close()

    def write_connector(self, line):
        """
            write line to serial port

        :param line:
        :return:
        """
        if self.connector:
            self.connector.write(line)
        else:
            self.log.error('no connector')


    def read_connector(self):
        """

        :param channel_name:
        :return:
        """
        if self.connector:
            return self.connector.readline()
        else:
            return ""

    def run(self):
        """
            main loop , entry point for thread or process
        :return:
        """
        # start
        try:
            self.log.debug('open connector')
            self.open_connector()
        except Exception as e:
            self.log.error("fail to start connector: %s" % str(e))
            raise

        # start transfer_out thread
        if self._threaded_transfer_out:
            self.__task_out = threading.Thread(target=self.transfer_out())
            self.__task_out.daemon = True
            self.__task_out.start()

        try:
            while not self.Terminated:
                self.run_once()
                time.sleep(0.1)

            self.log.debug('run ended')
            self.keep_alive(clear=True)

        finally:
            # stop connector
            self.log.debug('close connector')
            try:
                self.close_connector()
            except:
                pass

        # sys.exit(0)
        time.sleep(1)
        return 0


    def run_once(self):
        """

        :return:
        """
        self.keep_alive()

        # transfer slave stdout input to redis <stdout> ( if any )
        if not self._threaded_transfer_out:
            self.transfer_out()
        #  transfer redis <stdin> input to slave stdin ( if any )
        self.transfer_in()

        self.handle_signal()

    def transfer_in(self):
        """

            transfer redis stdin input to slave stdin ( if any )

        :return:
        """
        #size= len(self._stdin)
        size= self.model.size('stdin')
        if size > 0:
            #incoming_cmd= self._stdin.popleft()
            incoming_cmd= self.readline(channel_name='stdin')
            # add newline if necessary
            if not incoming_cmd.endswith('\n'):
                incoming_cmd= incoming_cmd + '\n'
            # send to connector
            self.connector.write(incoming_cmd)

    def transfer_out(self,group=False):
        """

            transfer slave stdout input to redis stdout ( if any )

        :return:
        """
        # read process output
        count= 0
        slave_out= self.connector.readline()
        if slave_out:
            #self.q_out.put(slave_out)
            if group == True:
                # write to queue as block
                #self._stdout.append(slave_out)
                self.write(slave_out, channel_name='stdout')
                count= 1
            else:
                # write each line to queue
                # if '\n' in slave_out:
                #     lines= slave_out.split("\n")
                # else:
                #     lines=[slave_out]
                #count= len(lines)

                lines= slave_out.split("\n")
                count=len(lines)
                if count > 1:
                    del lines[-1]
                    count= count -1
                for line in lines:
                    # add line to queue stdout
                    self.write(line, channel_name='stdout')

        return count

    def transfer_out_thread(self):
        """

        :return:
        """
        while not self.Terminated:
            self.transfer_out()
            time.sleep(0.01)


    def exit(self):
        """

        :return:
        """
        # close the connector
        self.log.debug('close connector')
        time.sleep(1)

        return super(AbstractAdapter,self).exit()



class EchoAdapter(AbstractAdapter):
    """

    """

    def open_connector(self):
        """
            start serial port

        """
        self.connector=EchoConnector('echo',log=self.log)



class GenericAdapter(AbstractAdapter):
    """
        an adapter where we can plug a connector

    """
    _connector_factory= None

    def start(self):
        """

        :return:
        """
        # check the connector
        if not self.connector:
            # no connector: try to build it from factory
            if not self._connector_factory:
                raise RuntimeError('Adapter has no Connector Factory')
        # start connector
        self.open_connector()
        rc= super(GenericAdapter,self).start()
        return rc

    def bind_connector(self,connector,run_mode=None):
        """

        :param connector_class:
        :return:
        """
        if callable(connector):
            #if connector.func_name=='wrapper':
            self._connector_factory= connector
            self.connector= None
        else:
            # assume it is a connector instance
            self.connector= connector
            self._connector_factory= None
        # else:
        #     raise RuntimeError('bind connector: must be calllable')
        # set run mode
        if run_mode:
            assert run_mode in (None,'thread','process','passive')
            self._run_mode= run_mode

    def open_connector(self):
        """
            start serial port

        """
        if not self.connector:
            self.connector= self._connector_factory()
        self.connector.open()
        return


class GenericAuthAdapter(GenericAdapter):
    """

        a generic adapter who filters connector output to intercept login and password prompts

        required model parameters: ['prompt_login','prompt_password','login','password']


    """
    required_model_parameters= ['prompt_login','prompt_password','login','password']

    def __init__(self, model, log=None, run_mode=None, **kwargs):
        """

        :param model:
        :param log:
        :param run_mode:
        :param kwargs:
        """
        super(GenericAdapter, self).__init__(model, log=log, run_mode=run_mode, **kwargs)
        self.log.debug('setup AuthAdapter')
        # check parameters
        ok= True
        missing= []
        for p in self.required_model_parameters:
            try:
                self.model.parameters[p]
            except KeyError:
                self.log.error('missing parameter: %s ' % p)
                ok= False
                missing.append(p)
        if not ok:
            raise RuntimeError('missing parameters for GenericAuthAdapter %s : %s' % (self.model.name,missing))

    def transfer_out(self, group=False):
        """

            transfer slave stdout input to redis stdout ( if any )

        :return:
        """
        # read process output
        count = 0
        slave_out = self.connector.readline()
        if slave_out:
            # self.q_out.put(slave_out)
            if group == True:
                # write to queue as block
                count = 1
                raise NotImplementedError('group transfer_out not implemented')
            else:
                # write each line to queue
                lines = slave_out.split("\n")
                count = len(lines)
                if count > 1:
                    del lines[-1]
                    count = count - 1
                for line in lines:
                    # add line to queue stdout
                    self.auth_filter(line)
                    #self.write(line, channel_name='stdout')
        return count

    def auth_filter(self,line):
        """

        :param line:
        :return:
        """
        if self.model.parameters['prompt_login'] in line:
            # intercept login prompt
            self.log.debug('intercept login prompt: %s' % self.model.parameters['prompt_login'])
            self.log.debug('inject login: %s' % self.model.parameters['login'])
            self.connector.write("%s\n" % self.model.parameters['login'])
            time.sleep(0.5)

        elif self.model.parameters['prompt_password'] in line:
            # intercept password prompt:
            self.log.debug('intercept password prompt: %s' % self.model.parameters['prompt_password'])
            self.log.debug('inject password')
            self.connector.write("%s\n" % self.model.parameters['password'])
            time.sleep(0.5)

        self.write(line, channel_name='stdout')
        return


def adapter_factory(queue, run_mode='thread'):
    """

        build an adapter from the queue model

        parameters must have a 'class' parameter representing tuhe connector class name
            eg EchoConnector

        sample usage:

        queue= Queue('shell', 'parameters': { 'class':'ProcessConnector','command_line': 'bash\n}
        queue.save()

         adapter= adapter_factory(queue,run_mode='thread')
         adapter.start()


    :param queue: instance of restop_queue.models.Queue
    :param run_mode:
    :return:
    """

    def adapter_wrapper():
        # build an adapter
        name = queue.name
        parameters = queue.parameters.copy()

        assert 'class' in parameters, "queue must have a connector 'class' parameter, like EchoConnector"
        classname = parameters.pop('class')

        cls = AbstractConnector.select_plugin(classname)
        factory = ConnectorFactory(cls, name, **parameters)

        adapter = GenericAdapter(queue, run_mode=run_mode)
        adapter.bind_connector(factory)
        return adapter

    return adapter_wrapper