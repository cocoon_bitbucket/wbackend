from walrus import *
from wbackend.model import Model
from wbackend.graph import Graph

# dashboard keys for keep_alive , pid ,status
DASHBOARD_KEEP_ALIVE= "dashboard:keepalive"
DASHBOARD_PID= "dashboard:pid"
DASHBOARD_STATUS= "dashboard:status"

KEEP_ALIVE_DONE= 'Done'


class Channel(object):
    """
        a handler for walrus List compatible with StringIo

    """

    def __init__(self, wlist):
        """

        :param wlist: an instance of walrus List
        """
        self._buffer= wlist

    @property
    def key(self):
        return self._buffer.key

    def write(self, data):
        """
            FIFO by default
        :param data:
        :return:
        """
        self._buffer.append(data)

    def readline(self):
        """
            FIFO behaviour
        :return:
        """
        line= None
        if len(self._buffer):
            line= self._buffer.popleft()
        return line

    def is_empty(self):
        """

        :return:
        """
        if len(self._buffer):
            return False
        return True

    def size(self):
        """

        :param queue_name:
        :return:
        """
        return len(self._buffer)

    def clear(self):
        """

        :return:
        """
        self._buffer.database.delete(self.key)


    def truncate(self,size=0):
        """

        :param size:
        :return:
        """
        if size > 0:
            try:
                self._buffer= self._buffer[:size]
            except Exception as e:
                del self._buffer
        else:
            del self._buffer
        return self.size()


    def peek(self,size=1):
        """

        :param size:
        :return:
        """
        return self._buffer[-size]

    def tell(self):
        """

        :return:
        """
        return self.size()

    def seek(self,offset=0):
        """

        :param offset:
        :return:
        """
        raise NotImplementedError


class Fifo(Channel):
    """
        a handler for walrus List compatible with StringIo

    """

    def write(self, data):
        """
            FIFO by default
        :param data:
        :return:
        """
        self._buffer.append(data)



class Lifo(Channel):
    """
        a handler for walrus List compatible with StringIo

    """
    def readline(self):
        """

        :return:
        """
        line= None
        if len(self._buffer):
            line= self._buffer.popright()
        return line



class Queue(Model):
    """


    """
    collection= 'queue'

    name= TextField(primary_key=True)

    # channels
    stdlog = ListField()
    stdin = ListField()
    stdout = ListField()

    log_offset = IntegerField(default=1)

    status = TextField(default='created')
    parameters = JSONField()


    def channel_key(self,queue_name):
        """
            _queue:container:<name>:_queue:id:<id>

        :param name: string (short name of queue : ( stdin , stdout)
        :return:
        """
        return "%s:container:%s:%s" % (self.collection,queue_name,self.get_hash_id())


    def channel(self, name):
        """
            return a channel
        :param name:
        :return:
        """
        ch = getattr(self, name)
        assert isinstance(ch, List)
        return Fifo(ch)

    def write(self,value,channel_name='stdin'):
        """


        :param channel: string short name of the channel ( stdin/stdout)
        :return:
        """
        return self.channel(channel_name).write(value)


    def readline(self, channel_name='stdout'):
        """

        :param queue_name: string short name of the queue ( stdin/stdout)
        :return:
        """
        return self.channel(channel_name).readline()

    def is_empty(self,channel_name):
        """

        :return:
        """
        return self.channel(channel_name).is_empty()


    def size(self,channel_name):
        """

        :param queue_name:
        :return:
        """
        return self.channel(channel_name).size()


    def peek(self, channel_name,size=1):
        """

        :param channel_name:
        :return:
        """
        return self.channel(channel_name).peek(size)

    def truncate(self,channel_name,size=0):
        """

        :param channel_name:
        :param size:
        :return:
        """
        return self.channel(channel_name).truncate(size)


    def clear(self, channel_name=None):
        """
            clear channel(s)  stdin,stdout,stdlog
        :param channel_name:
        :return:
        """
        if not channel_name:
            channels = ['stdin', 'stdout', 'stdlog']
        else:
            channels = [channel_name]
        for name in channels:
            self.channel(name).clear()
            #self.database.delete(channel_key)






class QueueHub(Queue):
    """
        an Usb queue controller

    """
    collection= 'queuehub'
    _default_name = 'default'

    queue_member= 'queue'

    # a list of queue hash_ids
    queues = HashField()

    @classmethod
    def create(cls, name=None,parameters=None,**kwargs):
        """
        Create a new local session
        """
        #members = []
        #if 'members' in kwargs:
        #    members = kwargs.pop('members')

        name= name or  cls._default_name
        parameters= parameters or {}
        instance = cls(name=name, parameters=parameters,**kwargs)
        instance.save()
        instance.stdlog.append('Queue Hub model created')
        #instance.create_queues()
        return instance

    @classmethod
    def load(cls, primary_key=None, convert_key=True):
        primary_key= primary_key or cls._default_name
        return super(QueueHub,cls).load(primary_key,convert_key=convert_key)



    def add_queue(self,queue):
        """

        :param queue:
        :return:
        """
        if queue.get_hash_id() not in self.queues:
            self.queues[queue.get_hash_id()]= "created"
            return True
        else:
            return False

    def del_queue(self, queue):
        """

        :param queue:
        :return:
        """
        if queue.get_hash_id() in self.queues:
            del self.queues[queue.get_hash_id()]
            return True
        else:
            return False


    # @classmethod
    # def create(cls, data):
    #     """
    #     Create a new local session
    #     """
    #     #members = []
    #     #if 'members' in kwargs:
    #     #    members = kwargs.pop('members')
    #     if 'name' in data:
    #         name= data.pop('name')
    #     else:
    #         name= 'default'
    #     instance = cls(name=name, data=data)
    #     instance.save()
    #     instance.logs.append('Usb Controller model created')
    #     instance.create_queues(data)
    #     return instance
    #
    #     # create queues
    # def create_queues(self,data):
    #     """
    #     :param data: dictinaries
    #     :return:
    #     """
    #     for name,value in data.iteritems():
    #         q= UsbQueue(name=name,**value)
    #         self.logs.append('create_queue %s with %s' % (name,str(value)))
    #         q.save()
    #     return



