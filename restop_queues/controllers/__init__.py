
import os
import signal
import time
import threading
import multiprocessing
import json

from walrus import Hash
from wbackend.logger import SimpleLogger as Logger
from restop_queues.models import DASHBOARD_KEEP_ALIVE, DASHBOARD_PID, DASHBOARD_STATUS,KEEP_ALIVE_DONE


class QueueServerBase(object):
    """



    """
    # run mode: one of None,thread,process
    _run_mode= None

    def __init__(self,model,log=None,run_mode=None,connector=None,**kwargs):
        """

        :param name:
        """
        self.model= model
        self.agent_id= self.model.get_hash_id()
        self._signal_key= self.agent_id + ':signal'
        self._keep_alive= Hash(self.model.database,DASHBOARD_KEEP_ALIVE)
        self._status = Hash(self.model.database, DASHBOARD_STATUS)
        self.log = log or Logger(self.model.channel_key('stdlog'), self.model.database)
        self.connector= connector or None
        self.__task=None
        self.__task__out= None
        self._threaded_transfer_out= False
        self.Terminated= False
        self.set_status('created')
        if run_mode:
            assert run_mode in ('thread','process','passive')
            self._run_mode= run_mode


    def keep_alive(self, clear=False):
        """

            write time to /agents[agent_id]
        :return:
        """
        if clear:
            # clear keep alive for this agent
            self._keep_alive[self.agent_id] = KEEP_ALIVE_DONE
        else:
            # set keep alive to current time for this agent
            self._keep_alive[self.agent_id] = time.time()


    def run_once(self):
        """
                echo mode
        :return:
        """
        self.keep_alive()


        # read stdin and transfer to stdout
        if len(self.model.stdin):
            line= self.model.stdin.popleft()
            self.model.stdout.append(line)

        self.handle_signal()
        return


    def run(self):
        """

        :return:
        """
        while not self.Terminated :

            self.run_once()
            time.sleep(0.1)

        self.log.debug('run ended')
        self.keep_alive(clear=True)

        #sys.exit(0)
        time.sleep(1)
        return 0


    # def is_running(self):
    #     """
    #
    #     :return:
    #     """
    #     if self.Terminated == True:
    #         return False
    #     else:
    #         return True

    def write(self,line,channel_name='stdin'):
        """

        :param line:
        :param channel_name:
        :return:
        """
        self.model.write(line,channel_name=channel_name)


    def readline(self,channel_name='stdout'):
        """

        :param channel_name:
        :return:
        """
        return self.model.readline(channel_name)


    def set_signal(self,value='abort'):
        """

        :param value:
        :return:
        """
        self.model.database.set(self._signal_key,value)

    def get_signal(self):
        """

        :return:
        """
        signal= self.model.database.get(self._signal_key)
        if signal:
            # remove it
            self.log.debug('clear signal %s' % signal)
            self.model.database.delete(self._signal_key)
        return signal


    def handle_signal(self):
        """

        :return:
        """
        signal = self.get_signal()
        if signal:
            self.log.info("receive signal: %s" % signal)
            if signal == 'clear':
                self.model.clear()
            else:
                # signal is quit
                self.Terminated= True
                self.exit()
        return

    def exit(self):
        """

        :return:
        """
        self.log.debug('exit')
        self.Terminated= True

        if self._run_mode == 'process':
            if self.__task:
                self.keep_alive(clear=True)
                os.kill(int(self.__task.pid), signal.SIGKILL)
        self.__task= None
        self.set_status('terminated')
        time.sleep(1)
        return


    def kill(self,timeout=2):
        """
            to avoid launching two servers on the same queue
        :param timeout:
        :return:
        """
        # position abort signal in case another process run
        self.set_signal()
        # leave time to eventual process to kill itself
        time.sleep(timeout)
        # clear the signal in case no process was running
        self.get_signal()



    def start(self):
        """

        :return:
        """
        if self._keep_alive[self.agent_id] != KEEP_ALIVE_DONE:
            self.kill()
        if self._run_mode == 'thread':
            if not self.__task:
                # only one task at a time
                self.__task = threading.Thread(target=self.run)
                self.__task.daemon = True
                self.__task.start()
                self.set_status('running')
        elif self._run_mode == 'process':
            if not self.__task:
                self.__task = multiprocessing.Process(target=self.run)
                self.__task.daemon = True
                self.__task.start()
                self.set_status('running')
        else:
            # run mode is None or passive
            pass

    def is_alive(self):
        if self.__task:
            return self.__task.is_alive()
        return False


    def set_status(self,status):
        """

        :param status:
        :return:
        """
        self._status[self.agent_id]=status

    def get_status(self):
        return self._status[self.agent_id]

    def wait_for_status(self, status, timeout=10):
        """
        :param status:
        :return:
        """
        t1 = 0
        while timeout > 0:
            if self.get_status == status:
                # OK
                return True
            t1 = self.keep_alive()
            if t1 == KEEP_ALIVE_DONE:
                # adapter has finished ( and not status)
                return False
            # continue
            time.sleep(1)
            timeout = timeout - 1
        # timeout
        return False




class ThreadedQueueServer(QueueServerBase):
    """

    """
    _run_mode= 'thread'

class ProcessQueueServer(QueueServerBase):
    """

    """
    _run_mode= 'process'

