
import json

from restop_queues import Queue

from . import QueueServerBase
#from .factories import adapter_factory
#from restop_queues.adapters import adapter_factory
import restop_queues.adapters

class QueueHubServerBase(QueueServerBase):
    """
        A command queue
        a queue which receive commands from stdin
        and respond to stdout

    """

    def __init__(self, model, log=None, run_mode=None, **kwargs):
        """

        :param model:
        :param log:
        :param run_mode:
        :param kwargs:
        """
        super(QueueHubServerBase,self).__init__(model,log=log,run_mode=run_mode,**kwargs)
        # init queue list
        self._adapters={}

    def run_once(self):
        """
            wait commands ( name, kwargs ) from stdin
        :return:
        """
        self.keep_alive()

        # read stdin and transfer to stdout
        if len(self.model.stdin):
            line = self.model.stdin.popleft()
            # interpret command   cmd <json arguments>
            if ' ' in line:
                command, arguments = line.split(' ', 1)
            else:
                command = line
                arguments = ""
            status,response = self.execute_command(command, arguments=arguments)
            self.model.stdout.append(json.dumps([status,response]))

        self.handle_signal()
        return


    def execute_command(self, command, arguments=None):
        """

        :param command: string,   q:tv ,
        :param content:
        :return:
        """
        command = command.strip('\n').strip()
        self.log.debug('received command: %s %s' % (command, arguments))
        response= ""
        if arguments:
            try:
                arguments = json.loads(arguments)
            except Exception  as e:
                response= 'cannot convert argument %s to json %s' % (arguments,e)
                self.log.debug(response)
                return False, response
        else:
            arguments = {}

        try:
            func = getattr(self, command)
        except AttributeError:
            # not implemented
            response = "command [%s] not implemented" % command
            self.log.debug(response)
            return False,response

        status= True
        try:
            response = func(** arguments)
        except Exception as e:
            response = "execution raised: %s" % e
            self.log.error(response)
            status= False
        #response = json.dumps(response)
        self.log.debug('response is %s' % response)

        return status,response


    def start_adapter(self,name,run_mode= 'thread'):
        """
            start a thread to handle a queue

            usbqueue:id:<name>

        :name: string , name of the queue ( tv, _loop) etc...
        :return:
        """
        queue = Queue.load(name)
        queue_key = queue.get_hash_id()

        adapter= restop_queues.adapters.adapter_factory(queue,run_mode=run_mode)
        adapter_obj= adapter()
        adapter_obj.start()

        return queue_key

    def list_queues(self):
        """

        :return:
        """
        rq= Queue.all()
        queues= [ q.name for q in rq]
        return queues

    def inspect_queue(self,name):
        """

        :param name:
        :return:
        """
        rq= Queue.get(Queue.name==name)
        data= rq._data
        return data


class ThreadedQueueHubServer(QueueHubServerBase):
    """

    """
    _run_mode= 'thread'


class ProcessQueueHubServer(QueueHubServerBase):
    """

    """
    _run_mode= 'process'



