from restop_queues.connectors import AbstractConnector, ConnectorFactory
from restop_queues.adapters import GenericAdapter

def adapter_factory(queue, run_mode='thread'):
    """

        build an adapter from the queue model

        parameters must have a 'class' parameter representing tuhe connector class name
            eg EchoConnector

        sample usage:

        queue= Queue('shell', 'parameters': { 'class':'ProcessConnector','command_line': 'bash\n}
        queue.save()

         adapter= adapter_factory(queue,run_mode='thread')
         adapter.start()


    :param queue: instance of restop_queue.models.Queue
    :param run_mode:
    :return:
    """

    def adapter_wrapper():
        # build an adapter
        name = queue.name
        parameters = queue.parameters.copy()

        assert 'class' in parameters, "queue must have a connector 'class' parameter, like EchoConnector"
        classname = parameters.pop('class')

        cls = AbstractConnector.select_plugin(classname)
        factory = ConnectorFactory(cls, name, **parameters)

        adapter = GenericAdapter(queue, run_mode=run_mode)
        adapter.bind_connector(factory)
        return adapter

    return adapter_wrapper