

import re
import time
import json
import os
import signal

from walrus import Database, List,Hash
from wbackend.logger import SimpleLogger as Logger
import logging

default_logger= logging.getLogger(__name__)


#default_redis_url="redis://localhost:6379/0"

# dashboard keys for keep_alive , pid ,status
dashboard_keep_alive= "dashboard:keepalive"
dashboard_pid= "dashboard:pid"
dashboard_status= "dashboard:status"


def channel_key(collection, item, queue_name):
    """
        <collection>:container:stdin:<collection>:id:<item>

    :param collection:
    :param item:
    :return:
    """
    return "%s:container:%s:%s:id:%s" % (collection, queue_name, collection, str(item))




class QueueClient(object):
    """
           a client to send data to queues

    """
    def __init__(self,agent_id,log=None,redis_db=None,**kwargs):
        """

        :param agent_id: string of form <collection>:id:<item> eg  agent:id:1
        :param log:

        """
        self.agent_id= agent_id
        assert ':' in agent_id , "queue agent id must be <collection>:id:<item>, got %s" % agent_id
        self.collection,dummy,self.item= self.agent_id.split(':')
        # for api compatibility with Connector
        self.name= self.item
        self._signal_key= self.agent_id + ':signal'


        self.database= redis_db or Database()
        # hash key for keep alive
        self._keep_alive=Hash(self.database,dashboard_keep_alive)
        self._status = Hash(self.database, dashboard_status)

        self.log= log or Logger(self.channel_key('stdlog'), self.database)


    def open(self, **kwargs):
        """ for Connector Aii compatibility """
        return True

    def close(self):
        """ for Connector Aii compatibility """
        return True

    def channel_key(self, name):
        """
            rqueues:container:stdin:rqueues:id:1

        :param name: string (short name of queue : ( stdin , stdout)
        :return:
        """
        return channel_key(self.collection, self.item, name)


    def write(self,value,channel_name='stdin'):
        """
        :param queue_name: string short name of the queue ( stdin/stdout)
        :return:
        """
        queue= List(self.database, self.channel_key(channel_name))
        queue.append(value)


    def readline(self, channel_name='stdout', timeout=1):
        """
        :param channel_name: string short name of the queue ( stdin/stdout)
        :return:
        """
        queue = List(self.database, self.channel_key(channel_name))
        if len(queue):
            return queue.popleft()

        value= None
        end= time.time() + timeout
        while time.time() <= end:
            if len( queue ):
                value= queue.popleft()
                break
            time.sleep(0.1)
        return value

    def clear(self):
        """
            ask server to clear all channels
        :param channel_name:
        :return:
        """
        self.log.debug('client: ask for clear')
        self.signal('clear')


    def send(self,operation, arguments=None,channel_name='stdin'):
        """
            send a command to redis <cmd> queue

        :param operation:
        :param arguments:
        :return:
        """
        arguments= arguments or {}
        json_arguments= json.dumps(arguments)
        command= '%s %s\n' % (operation , json_arguments)
        self.write(command,channel_name=channel_name)

    def receive(self):
        """
            get a response to a command
        :return:
        """
        line= self.readline()
        if line:
            try:
                response= json.loads(line)
                status,message= response
                return status,message
            except Exception as e:
                self.log('error in receive: %s' % e)
                pass
        return None

    def signal(self,value='abort'):
        """
            set signal for queue server
        :param value:
        :return:
        """
        self.database.set(self._signal_key,value)


    def keep_alive(self):
        """
            return the timestamp of agent keep alive
        """
        return self._keep_alive[self.agent_id]


    ###
    def exit(self,wait=True):
        """
            send exit to agent adapter
        """
        rc= True
        self.log.debug('client: ask for exit')
        self.signal('abort')
        if wait:
            self.log.debug('client: wait for exit')
            rc= self.wait(counter=5)
            if rc:
                self.log.debug('client: exit ok')
            else:
                self.log.debug('client: stop waitting for exit')
        return rc


    def wait(self,counter=15):
        """
            wait until adapter is done (eg keep alive is None)
        """
        t1= 0
        t2=0
        while counter > 0:
            t2=t1
            t1 = self.keep_alive()
            if t1 == 'Done':
                # OK adapter is done
                return True
            time.sleep(1)
            counter = counter -1
        # still alive
        if t1==t2:
            self.log.error("client: adapter is freezed, last timestamp: %s" % t1)
        else:
            self.log.debug("adapter is still alive, last timestamp: %s" % t1)

        return False


    def expect(self,pattern='*' ,timeout= 3 , cancel_on=None, regex=True,**kwargs):
        """
            read agent input until pattern is match or timeout reached or one of cancel_on pattern matches

            returns a list: buffer

            The last entry of buffer shows the result

            pattern found: "================ found %s" % pattern
            timeout:       "================ timeout reached
            cancel:        "================ canceled %s" % cancel_on[index]

        :param item:
        :param data: dict   ( pattern="*" , timeout=3, cancel_on=[] )
        :param kwargs:   pattern,timeout=3,cancel_on=None
        :return:
        """

        trace_enabled= True

        pattern= pattern or '*'
        timeout= int(timeout) or 3
        cancel_on= cancel_on or []
        if isinstance(cancel_on,basestring):
            cancel_on= [cancel_on]

        # compute search pattern with ignore case
        if regex is False:
            pattern=re.escape(pattern)
        search_pattern= re.compile(pattern,re.I)
        # compute optional cancel patterns
        cancel_patterns= []
        for p in cancel_on:
            r= re.compile(p)
            cancel_patterns.append(r)

        buffer=[]
        buffer.append(self.trace("expect pattern= [%s]" % pattern))
        # take timestamp
        start_time = time.strftime("%X") + ".000"
        t0= time.time()
        limit= t0 + timeout
        t= t0
        while True:

            # check timeout
            t= time.time()
            if t > limit:
                buffer.append(self.trace("timeout reached"))
                break

            # read the line
            line = self.readline()
            if line is None:
                time.sleep(0.4)
                continue
            line= line.decode('utf-8','ignore').encode('utf-8')
            buffer.append(line)

            # Search for expected text
            if search_pattern.search(line) != None:
                # we found the expected line , return it
                buffer.append(self.trace("found %s" % pattern))
                # end the read loop
                break

            # search for cancel patterns
            found_cancel = False
            if cancel_patterns:
                for index,search_cancel in enumerate(cancel_patterns):
                    if search_cancel.search(line):
                        # we found a cancel pattern
                        buffer.append(self.trace('canceled %s' % cancel_on[index] ))
                        found_cancel= True
                        break
            if found_cancel:
                break
        return buffer

    def watch(self, timeout=5):
        """
            watch serial log for a duration
        :param timeout:
        :return:
        """
        lines = self.expect('Never Catch This', timeout=timeout)
        # add lines to log
        self.log.debug(lines)
        return lines



    def trace(self, s):
        """

        :param s: string , message to trace
        :return:
        """
        now = time.time()
        fmt = "================ " + s + " ==================" + " [at t=%(now)03d]" % { 'now':now }
        return fmt


    def sync(self,timeout=10):
        """

        send an echo SYNCHRO on serial stdin and wait for SYNCHRO on stdout

        :param timeout:
        :return:
        """
        t0=time.time()

        self.write('\n')
        sync_message= "echo SYNCHRO TAG %s" % str(t0)
        self.write(sync_message)
        time.sleep(2)
        r= self.expect("^SYNCHRO TAG %s" % str(t0),timeout=timeout)
        return r


    @property
    def status(self):
        """


        :return:
        """
        return self._status[self.agent_id]


    def wait_for_status(self,status,timeout=10):
        """

        :param status:
        :return:
        """
        t1= 0
        while timeout > 0:
            if self.status == status:
                # OK
                return True
            t1 = self.keep_alive()
            if t1 is None:
                # adapter has finished ( and not status)
                    return False
            # continue
            time.sleep(1)
            timeout = timeout -1
        # timeout
        return False

