
import pytest

import sys
import os

sys.path.append('..')


# backend
from restop_client import Pilot



RESTOP_SERVER_URL= 'http://localhost:5000/restop/api/v1'
RESTOP_SERVER_PORT= 5000



REDIS_URL= "redis://localhost:6379/0"
PLATFORM= "platform.yml"
LOCAL_PLATFORM= "platform_local.yml"
LEGACY_PLATFORM= "platform.json"



@pytest.fixture
def conftest_dir():
    """
        return directory of conftest
    :return:
    """

    import os
    path= os.path.dirname(os.path.realpath(__file__))
    return path


@pytest.fixture
def local_pilot():
    """
        return an instance of restop_client.rf_baseplugin.Pilot

    :return:
    """

    pilot= Pilot()
    pilot.setup_pilot('demo','demo',RESTOP_SERVER_URL)

    return pilot

