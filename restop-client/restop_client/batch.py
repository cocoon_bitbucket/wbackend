#!/usr/bin/env python
import sys
import scheduler
from restop_client.scheduler import Scheduler,default_campaign

import logging




logging.basicConfig(level=logging.DEBUG)


if len(sys.argv) > 1:
    campaign = sys.argv[1]
else:
    campaign= default_campaign

if len(sys.argv) > 2:
    name = sys.argv[1]
else:
    name= campaign



rs= Scheduler()
rs.run(campaign, name)


