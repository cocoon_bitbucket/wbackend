#!/usr/bin/env python
import os
import time
import subprocess
from ConfigParser import SafeConfigParser

from restop_client.restop_api import RestopApi
import logging

log= logging.getLogger('scheduler')


os.environ['NO_PROXY'] = 'localhost'


default_campaign= "campaign"
default_base= "./"


class MyConfigParser(SafeConfigParser):
    def getlist(self,section,option):
        value = self.get(section,option)
        return list(filter(None, (x.strip() for x in value.splitlines())))

    def getlistint(self,section,option):
        return [int(x) for x in self.getlist(section,option)]


class Job(object):
    """
        represent a job entry

        [job_name]
        repeat: 1
        delay: 2


    """
    def __init__(self,job_data,iteration,workspace,**context):
        """

        :param job_data:
        """
        self.data= job_data
        self.iteration= iteration
        self.workspace=workspace
        self.context=context

    @property
    def repeat(self):
        return int(self.data.get('repeat',1))

    @property
    def delay(self):
        return int(self.data.get('delay', 0 ))


    @classmethod
    def get_job(cls,job_data,iteration,workspace,**context):
        """

        :param data:
        :return:
        """
        if 'restop' in job_data:
            return RestopJob(job_data,iteration,workspace,**context)
        elif 'pybot' in job_data:
            return PybotJob(job_data,iteration,workspace,**context)
        elif 'exe' in job_data:
            return ShellJob(job_data, iteration, workspace, **context)

        else:
            raise ValueError("cannot handle this job: %s" % str(job_data))

    def _make_workspace(self,workspace):
        try:
            os.mkdir(workspace)
        except OSError, e:
            if e.errno == 17:
                log.warning('workspace directory already exists: %s' % workspace)
                pass
            else:
                log.error('cannot create workspace: %s' % str(e))
        except  Exception, e:
            log.error("aborted with error: %s" % str(e))


class PybotJob(Job):
    """
        run a pybot command  :

        pybot -o <_workspace_> <pybot> <script_dir>/<script>

        eg  pybot -o ./robot/1 -v dummy:dummy  ./script.robot

        the pybot result files are stored in the workspace

    """

    @property
    def script_dir(self):
        return self.data.get('script_dir','')

    @property
    def script(self):
        return self.data['script']

    @property
    def pybot(self):
        return self.data.get('pybot','')

    def run(self):

        log.debug("run pybot job %s in directory %s" % (self.script,self.workspace))

        self._make_workspace(self.workspace)

        cmd= "pybot -d %s %s %s" % ( self.workspace,self.pybot,self.script)
        log.info("command: %s" % cmd)

        p= subprocess.call(cmd,shell=True)

        return

class RestopJob(PybotJob):
    """

        special pybot job where we fetch interface from server

    """
    platform_url= "http://localhost:5000/restop/api/v1"
    platform_ini="restop_client.ini"


    def run(self):
        log.debug("run pybot restop_client job %s in directory %s" % (self.script, self.workspace))

        self._make_workspace(self.workspace)

        # fetch interface
        platform_url= self.data['restop']
        r= RestopApi.fetch(platform_url,filepath=self.platform_ini)


        cmd = "pybot -d %s %s %s" % (self.workspace, self.pybot, self.script)
        log.info("command: %s" % cmd)

        p = subprocess.call(cmd, shell=True)

        return




class ShellJob(Job):
    """

    """

    @property
    def exe(self):
        return self.data['exe']


    def run(self):

        log.debug("run job %s in directory %s" % (self.exe,self.workspace))

        self._make_workspace(self.workspace)

        p= subprocess.call(self.exe,shell=True,cwd=self.workspace)

        return




class Scheduler(object):
    """

    """
    filepath = './scheduler.ini'


    def __init__(self, root= './' ,  filepath=None):
        """
        :param filepath:
        """
        filepath = filepath or self.filepath
        self.data = {}
        self.parser = self.load(filepath)

        self.base= self.parser.get('main','base')
        self.iteration= 0


    def load(self, filenames=None):
        """

        :param filepath:
        :return:
        """
        filenames = filenames or self.filepath

        if isinstance(filenames, basestring):
            filenames = [filenames]

        if not filenames:
            filenames.append(self.filepath)

        cp = MyConfigParser()
        cp.read(filenames)

        return cp


    def run(self,campaign=default_campaign,name=None):
        """


        :param campaign:
        :return:
        """
        self.campaign= campaign
        self.name= name or self.campaign

        log.info("start campaign: %s" % self.campaign)

        sections= self.parser.sections()

        try:

            message= "unknown job: [%s] in file [%s]" % (self.campaign,self.filepath)
            assert self.campaign in sections , message

            self.campaign_data= self.parser._sections[self.campaign]

            # make the session directory   ./
            session_directory = os.path.join(self.base, self.name)

            try:
                os.mkdir(session_directory)
            except OSError, e:
                if e.errno == 17:
                    log.warning('campaign directory already exists')
                    pass
                else:
                    raise
            except  Exception ,e:
                pass

            repeat= int(self.campaign_data.get('repeat',1))
            #job= self.campaign_data['exe']
            for iteration in xrange(0,repeat):

                # create first iteration directory (workspace )
                #self.log.info("")
                workspace_id= str(iteration+1)
                workspace= os.path.join(session_directory,workspace_id)


                # run first iteration

                job= Job.get_job(self.campaign_data,iteration,workspace)

                #log.info("run job, iteration=%s" % iteration + 1)

                #self.run_job(job,workspace)
                job.run()


                # wait for delay
                if iteration < repeat-1:
                    delay= int(self.campaign_data['delay'])
                    log.debug("wait for delay: %d" % delay)
                    time.sleep(delay)
                else:
                    # last job : skip delay
                    log.debug('last job: skip delay')


        except KeyboardInterrupt ,e:
            log.error('interrupted by user')
            pass

        except Exception ,e:
            log.error("aborted on error: %s" % e )
            raise

        finally:
            log.info('campaign %s Done.' % self.campaign)



if __name__=="__main__":


    import sys

    logging.basicConfig(level=logging.DEBUG)


    if len(sys.argv) > 1:
        campaign = sys.argv[1]
    else:
        campaign= default_campaign

    if len(sys.argv) > 2:
        name = sys.argv[1]
    else:
        name= campaign



    rs= Scheduler()

    r= rs.run('test_stb')
    rs.run(campaign,name)


