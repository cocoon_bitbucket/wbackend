__author__ = 'cocoon'
"""


    a module to handle Session configuration

    a session configuration is a list of dict

    [
        { identifier, category , parameters },
        { identifier, category , parameters },


    ]

    identifier: string , unique identifier of an agent
    category:   string , category of agent  , like android_device, pjsip_device ..)
    parameters : dict , parameters of the agent , the schema depends on the agent category


"""



class SessionConfiguration(object):
    """
    a session configuration is a list of dict describing agents configuration within a session

    [
        { identifier, category , parameters },
        { identifier, category , parameters },


    ]

    identifier: string , unique identifier of an agent
    category:   string , category of agent  , like android_device, pjsip_device ..)
    parameters : dict , parameters of the agent , the schema depends on the agent category



    """
    def __init__(self,configuration):
        """

        :param configuration:
        :return:
        """
        assert isinstance(configuration,list)

        # store original configuration
        self._configuration= configuration


        # a dict of agents configuration, the key is the agent identifier
        self._agents= {}

        # a list of unique categories
        self._categories= set()

        self._agent_list=[]

        # analyse
        for agent_configuration in self._configuration:
            assert len(agent_configuration) == 3  , "bad agent configuration %s" % str(agent_configuration)

            # parse agent configuration
            identifier,category,parameters= agent_configuration

            # check agent configuration
            try:
                assert isinstance(category,basestring)
                assert isinstance(parameters,dict)

            except AssertionError:
                raise RuntimeError("bad configuration format for agent: %s" % str(identifier))

            # check unicity
            if identifier in self._agents:
                raise RuntimeError("agent identifier [%s]  is not unique within the session" % str(identifier))

            self._agent_list.append(identifier)
            self._agents[identifier]= {
                'identifier': identifier,
                'category': category,
                'parameters': parameters
            }

            self._categories.add(category)


    def category_list(self):
        """

            return an ordered list of unique categories
        :return:
        """
        return sorted(list(self._categories))

    def agent_list(self):
        """
            return list of agents in order of configuration

        :return:
        """
        return self._agent_list

    def agent(self,identifier):
        """
            get the agent dictionary

        :param identifier:
        :return:
        """
        return self._agents[identifier]



    def set_agent_parameter(self,identifier,key,value):
        """

        :param agent:
        :param key:
        :param value:
        :return:
        """
        self._agents[identifier]['parameters'][key]= value

    def get_agent_parameter(self,identifier,key):
        """

        :param identifier:
        :return:
        """
        return self._agents[identifier]['parameters'][key]


    def configuration(self,agent_name=None,category=None):
        """
            query the up to date configuration , filters are agent_name or category

        :return: the up to date configuration
        """
        cnf= []
        for name in self.agent_list():
            if agent_name is not None:
                # filter on agent name
                if name != agent_name:
                    # skip it
                    continue
            # add agent configuration
            agent=self.agent(name)
            if category is not None:
                # filter on category
                if category != agent['category']:
                    # skip the it
                    continue
            cnf.append([agent['identifier'],agent['category'],agent['parameters']])

        if agent_name is None:
            # return all configuration
            return cnf
        else:
            # return single agent configuration
            assert len(cnf) == 1
            return cnf[0]


if __name__ == "__main__":

    Alice= 'Alice'
    Bob= '0a9b2e63'

    #Charlie= '127.0.0.1:5555'

    Charlie= 'Charlie'

    cnf= [
            [ Alice,    'pjsip_device',    {'command_line':""}         ],
            [ Bob  ,    'android_device', {}         ],
            [ Charlie  ,    'pjsip_device',  {'command_line':""}          ],
        ]

    s =SessionConfiguration(cnf)

    categories= s.category_list()
    print categories
    assert categories == ['android_device', 'pjsip_device']


    agents= s.agent_list()
    print agents
    assert agents== ['Alice', '0a9b2e63', 'Charlie']


    alice= s.agent(Alice)
    assert alice == {'category': 'pjsip_device', 'identifier': 'Alice', 'parameters': {'command_line': ''}}


    alice['parameters']['add_on']="add_on"
    s.set_agent_parameter(Alice,'add_on_2',"add_on_2")

    config= s.configuration()
    print config
    assert config == [['Alice', 'pjsip_device', {'add_on': 'add_on', 'add_on_2': 'add_on_2', 'command_line': ''}], ['0a9b2e63', 'android_device', {}], ['Charlie', 'pjsip_device', {'command_line': ''}]]

    p1= s.get_agent_parameter(Alice,'add_on_2')
    print p1
    assert p1== 'add_on_2'

    alice_configuration= s.configuration(agent_name=Alice)
    print alice_configuration
    assert alice_configuration == ['Alice', 'pjsip_device', {'add_on': 'add_on', 'add_on_2': 'add_on_2', 'command_line': ''}]

    pjsip_configuration= s.configuration(category='pjsip_device')
    print pjsip_configuration
    assert pjsip_configuration==  [['Alice', 'pjsip_device', {'add_on': 'add_on', 'add_on_2': 'add_on_2', 'command_line': ''}], ['Charlie', 'pjsip_device', {'command_line': ''}]]


    android_configuration= s.configuration(category='android_device')
    print android_configuration
    assert android_configuration == [['0a9b2e63', 'android_device', {}]]


    print