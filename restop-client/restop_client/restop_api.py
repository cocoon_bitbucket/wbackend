"""
    restop_api


    fetch client api from remote restop server
    store it localy in a restop_client.ini

    retreive api info from restop_client.ini



    restop_client.ini sample

    ; restop_client.ini
    [interface]
    operations =
       kw1
       kw2
    collections =
      root
      oauth2
    collection_operations =
      adb_devices
    session_collection=  phone_sessions



"""
import os
from ConfigParser import SafeConfigParser

from http_proxy import SimpleProxy


os.environ['NO_PROXY'] = 'localhost'

class MyConfigParser(SafeConfigParser):
    def getlist(self,section,option):
        value = self.get(section,option)
        return list(filter(None, (x.strip() for x in value.splitlines())))

    def getlistint(self,section,option):
        return [int(x) for x in self.getlist(section,option)]


class RestopApi(object):
    """



    """

    filepath= "./restop_client.ini"

    @classmethod
    def fetch(cls,url,filepath=None):
        """
            fetch remote configuration

        :param url: string , server base url eg localhost:5000/restop/api/v1
        :param filepath: string , file path to store the congig
        :return:
        """
        filepath= filepath or cls.filepath

        # request to /apidoc
        print "fetch client interface from server at %s" % url
        data = None
        proxy = SimpleProxy(url)
        url = proxy.url_for(collection='apidoc')
        r = proxy.client.get(url)
        if r.status_code == 200:
            # ok
            data = r.json()
            if data:
                cls.store_ini_file(data['message'],name=filepath)
        else:
            # cannot fetch interface
            data= {}
        return data

    @classmethod
    def store_ini_file(cls, data, name=None):
        """
            store restop_client.ini localy
        :param data:
        :param name:
        :return:
        """
        name= name or cls.filepath

        with open(name, 'wt') as fh:
            fh.write("; restop_client.ini\n")
            fh.write("[interface]\n")
            if 'operations' in data:
                fh.write("operations =\n")
                for op in data['operations']:
                    fh.write("  %s\n" % op)
            if 'collections' in data:
                fh.write("collections =\n")
                for op in data['collections']:
                    fh.write("  %s\n" % op)
            if 'collection_operations' in data:
                fh.write("collection_operations =\n")
                for op in data['collection_operations']:
                    fh.write("  %s\n" % op)
            if 'session_collection' in data:
                fh.write("session_collection=  %s\n" % data['session_collection'])

    def __init__(self,filepath=None):
        """
        :param filepath:
        """
        filepath=filepath or self.filepath
        self.data = {}
        self.parser= self.load(filepath)

    def load(self,filenames=None):
        """

        :param filepath:
        :return:
        """
        filenames= filenames or self.filepath

        if isinstance(filenames, basestring):
            filenames = [filenames]

        if not filenames:
            filenames.append(self.filepath)

        cp = MyConfigParser()
        cp.read(filenames)

        return cp

    @property
    def operations(self):
        """

        :return:
        """
        return self.parser.getlist('interface', 'operations')
    keywords= operations

    @property
    def collections(self):
        """

        :return:
        """
        return self.parser.getlist('interface', 'collections')

    @property
    def collection_operations(self):
        """

        :return:
        """
        return self.parser.getlist('interface', 'collection_operations')

    @property
    def session_collection(self):
        """

        :return:
        """
        return self.parser.get('interface','session_collection')





def get_keywords(filenames=None):
    """

    :return:
    """
    filenames= filenames or []
    if isinstance(filenames,basestring):
        filenames= [filenames]

    if not filenames:
        filenames.append('./restop_client.ini')

    cp= MyConfigParser()
    cp.read(filenames)

    keywords= cp.getlist('interface','operations')

    return keywords

if __name__ == "__main__":


    base_url= "http://localhost:5001/restop/api/v1"

    # fetch remote api to restop_client.ini

    # GET http://localhost:5001/restop/api/v1/apidoc
    r= RestopApi.fetch(base_url, filepath="./restop_client.ini")


    cnf= RestopApi(filepath='./restop_client.ini')

    print cnf.collections
    print cnf.keywords




