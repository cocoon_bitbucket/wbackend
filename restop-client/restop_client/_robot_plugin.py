"""


    syp_plugin to interface DeviceProxy server




"""

import inspect
from pprint import pprint

from http_proxy import SimpleProxy
from restop_api import RestopApi


base_url= "http://localhost:5000/restop/api/v1"


#import robot
#from robot.libraries.BuiltIn import BuiltIn

syp_ptf= dict(

    # here comes the platform configuration
)

# set droyrunner keywords
KEYWORDS= set(['click_by_index', 'native_drag_to', 'ui_object', 'dump', 'fling_backward_horizontally', 'get_object', 'click_on_object', 'scroll_forward_vertically', 'clear_text', 'pinch_in', 'long_click_by_index', 'pinch_out', 'register_press_watcher', 'swipe_right', 'native_gesture', 'fling_forward_horizontally', 'execute_adb_shell_command', 'set_text', 'scroll_to_end_horizontally', 'click_linear_by_text', 'scroll_to_horizontally', 'click', 'swipe_down', 'native_fling', 'open_notification', 'scroll_to_horizontal', 'start_test_agent', 'execute_adb_command', 'native_operation', 'swipe_left', 'scroll_backward_vertically', 'flash', 'fling_forward_vertically', 'unfreeze_screen_rotation', 'wait_for_object_exists', 'type', 'call', 'wait_until_object_gone', 'get_count', 'get_info_of_object', 'scroll_to_vertically', 'scroll_to_vertical', 'set_screen_orientation', 'get_object_data', 'scroll_to_beginning_vertically', 'get_device_info', 'screenshot', 'adb_devices', 'turn_off_screen', 'click_tab_by_text', 'long_click', 'scroll_to_end_vertically', 'open_quick_settings', 'freeze_screen_rotation', 'register_click_watcher', 'swipe_by_coordinates', 'drag_by_coordinates', 'press', 'scroll_backward_horizontally', 'get_child', 'fling_backward_vertically', 'press_home', 'scroll_to_beginning_horizontally', 'wait_until_gone', 'get_count_of_object', 'flatten_ui_objet', 'wait_for_exists', 'list_all_watchers', 'native_scroll', 'click_at_coordinates', 'wait_idle', 'turn_on_screen', 'set_object_text', 'get_info_by_index', 'remove_watchers', 'snapshot', 'scroll_forward_horizontally', 'install', 'stop_test_agent', 'get_sibling', 'swipe_up', 'wait_update', 'get_screen_orientation', 'uninstall'])



class RobotFrameworkListener(object):
    """



    """
    ROBOT_LISTENER_API_VERSION = 2
    ROBOT_LIBRARY_SCOPE = 'Global'

    def __init__(self):
        """
            init pilot
        """
        self.ROBOT_LIBRARY_LISTENER = self

    def  start_suite(self,name, attributes): pass
    def  end_suite(self,name, attributes): pass
    def  start_test(self,name, attributes): pass
    def  end_test(self,name, attributes): pass
    def  start_keyword(self,name, attributes): pass
    def  end_keyword(self,name, attributes): pass
    def  log_message(self,message): pass
    def  message(self,message): pass
    def  output_file(self,path): pass
    def  log_file(self,path): pass
    def  report_file(self,path): pass
    def  debugfile(self,path): pass
    def  close(self): pass



    # def _end_test(self, name, attrs):
    #         print 'Suite %s (%s) ending.' % (name, attrs['id'])
    #         self.test_status[name.lower()] = attrs["status"]



class Pilot(RobotFrameworkListener):
    """
        syp_plugin to interface syprunner

 
    """

    _platform = syp_ptf
    _keywords = KEYWORDS
    #_dry = False

    def __init__(self):
        """
            init pilot
        """
        super(Pilot,self).__init__()
        self.test_status = {}

        self._result = ''
        self._users = {}
        self.conf = {}      # configuration of a platform version
        self.ptf = None     # ref to a SypPlatform instance
        #self.ptf = SypPlatform(self._platform)
        self._session = None
        self._dry = False

        # add own dynamic keywords
        for name, func in inspect.getmembers(Pilot, predicate=inspect.ismethod):
            if not name.startswith('_'):
                self._keywords.add(name)

    #get_lib = BuiltIn().get_library_instance('Selenium2Library')


    def get_keyword_names(self):
        """

        :return:
        """
        return list(self._keywords)


    #     # add self keywords
    #     #return [name for name in dir(self) if hasattr(getattr(self, name), 'robot_name')]
    #     #return ['serial_synch', 'stb_status', 'send_key']
    #     # add droydrunner keywords
    #     #for name,func in inspect.getmembers(self._api._mobile_factory, predicate=inspect.ismethod  ):
    #     #    if not name.startswith('_'):
    #     #        self._keywords.append(name)
    #     # add session keywords
    #     #self._keywords.extend(['open_session','close_session','adb_devices','scan_devices'])
    #     return self._keywords
    #


    def set_pilot_dry_mode(self):
        """
            set pilot to dry mode ( no action will be taken towards devices)
        """
        print "pilot: warning: you enter in dry mode"
        self._dry = True

    def setup_pilot(self,platform_name,platform_version,platform_url=None):
        """
            initialize pilot with platform configuration file
        """
        self.platform_name= platform_name
        self.platform_version= platform_version
        self.platform_url= platform_url or base_url

        print "pilot: setting up platform pilot for platform=%s , version=%s ,url=%s" % (
            platform_name,platform_version,platform_url)

        collection_operations= ['adb_devices','get_platform_configuration','ptf_get_user_data','get_sip_address']
        self.proxy= SimpleProxy(
                self.platform_url,
                collection= 'phone_sessions',
                collection_operations= collection_operations
                )

        return

    def _dry_return(self,*args,**kwargs):
        """
            return with dry mode : always OK
        """
        print "pilot: warning you are in dry mode, no operation was transmitted to syprunner"
        self._result = "OK"
        return self._result

    def open_Session(self,*users):
        """
            open a session : start session with a terminal per user specified

            Open Session Alice Bob

            -    start a session with userA=Alice , userB=Bob
        """
        print "pilot: initialisation conf = %s " % str(self.conf)
        print "pilot: Open Session with users: %s" % str(users)

        if self._dry:
            return self._dry_return()

        result= self.proxy.open(users)
        # {u'agents_count': 2, u'agent_started': 2}
        pprint(result)
        # check all agents are started
        if result.has_key('code') and result['code'] >=300:
            raise RuntimeError('cannot open session')
        if not result['agent_started'] == result['agents_count']:
            raise RuntimeError("cant start all agents")


    def get_platform_configuration(self):
        """
            Get platform configuration ( a platform.json)
        """
        result=self.proxy.get_platform_configuration()
        print "pilot: platform configuration is %s" % str(result)
        return result

    def dummy_operation(self):
        """
            A dummy operation:  do nothing
        """
        print "pilot: dummy operation :platform configuration is %s" % str(self.conf)



    def close_session(self,result=0,error=0):
        """
            close session , unregister all users and quit
        """
        print "pilot: Close Session"
        #self.proxy.close()


        #self._users = {}

        if self._dry:
            #self._session = None
            return self._dry_return()

        r= self.proxy.close()
        print r['logs']
        #
        # try :
        #     self._session.close_session(result,error)
        # except AttributeError:
        #     pass
        # self._session=None


    ### HELPERS
    def get_sip_address(self,user=None,format=None,fac=None):
        """
            Compute and return a sip destination from user , format and eventualy fac

            *@fac*: +CFA      (*71)

            *@user*:   Charlie

            *@format*: sid_ext  (12 2515)

            return a destination like : sip:*71122515@sip.osp.com

        """
        print "pilot: get_sip_address(user=%s,format=%s,fac=%s)" % (str(user),str(format),str(fac))
        result= self.proxy.get_sip_address(user=user,format=format,fac=fac)
        return result

    # primary interface
    def _execute(self,user,command,*args,**kwargs):
        """
           the transmission belt with syp runner plugin

           all calls to syprunner plugin should pass here
        """
        if not self._dry:
            #return self._session.execute(user,command,*args,**kwargs)
            res= self.proxy._send_operation(command,user=user,**kwargs)
            return self._handle_result(res)

        else:
            return self._dry_return()

    def _get_url(self,user,command,*args,**kwargs):
        """
           the transmission belt with syp runner plugin

           all calls to syprunner plugin should pass here
        """
        if not self._dry:
            #return self._session.execute(user,command,*args,**kwargs)
            res= self.proxy._get(command,user=user,**kwargs)
            return self._handle_result(res)

        else:
            return self._dry_return()



    def _relay(self,command,*args,**kwargs):
        """
            relay a command to syprelay
        """
        if not self._dry:
            #sip_relay_func = getattr(self._session,command)
            #return sip_relay_func(*args,**kwargs)
            raise NotImplementedError
        else:
            return self._dry_return()


    def _handle_result(self,result,raise_on_error=True):
        """


        :param result: dict   { result: 200 , logs: [] , message: ""}
        :return:
        """
        status= result.get('result',200)
        message= result.get('message',None)
        print '=== status:%s' % str(status)
        print '=== message: %s' % result.get('message','No message')
        print '=== logs:'
        pprint( result.get('logs',[]))
        # check status
        if raise_on_error:
            if isinstance(status,int):
                if status >= 400:
                    raise RuntimeError("code: %s , %s" % (str(status),str(message)))
        return message


    ###  base telephony functions
    def _call(self,user,sip_address):
        """
            @user: eg Alice Bob
            @sip_address
        """
        return self._execute(user,"call" , destination=sip_address)
        #return self._handle_result(res)


    def _answer_call(self,user,code,wait_incoming = False):
        """
            answer call , by default assumes a pending call is present, don t wait for it

            @user  eg Alice , bob ...
            @code : 180 , 200 ...


        """
        return self._execute(user,"answer_call",code=code, wait_incoming=wait_incoming)


    def _check_call(self,user,state="CONFIRMED"):
        #
        return self._execute(user,"check_call",state=state)

    def _expect(self,user,pattern,raise_on_error=True,title="",ignore_timeout=False):
        return self._execute(user,'expect', pattern=pattern, raise_on_error=True, title="",ignore_timeout=False)


    def unregister(self,user):
        """
            unregister a user
        """
        print "pilot: user %s unregister" % user
        return self._execute(user,'unregister')

    def wait_timed_out(self,user):
        """
            wait for a time out
        """
        print "pilot: user %s wait for time out" % user
        return self._execute(user,'wait_timed_out')

    def watch_log(self,user,duration=2):
        """
            just listen for a while to catch the log
        """
        clock = int(duration)
        print "pilot: user %s watch log for a duration of %d " % (user,clock)
        return self._execute(user,'watch_log',duration=clock)


    def wait_incoming_call(self,user):
        """
            wait an incoming call  (INVITE)


            wait for aproximatively 30  seconds for an incoming call

            - if ok return the FROM line of the incoming call sip INVITE message

            - if not : cancel script with a timeout

        """
        print "pilot: user %s wait for incoming call" % (user)
        #
        return self._execute(user,'wait_incoming_call')


    def wait_incoming_response(self,user,code,sip_method,cseq=''):
        """
            wait an incoming sip response  ( SIP/2.0 <code> for an operation

            *@code*: str  , 200, 202 ...

            *@sip_method*: str  , INVITE , REGISTER ...

        """
        print "pilot: user %s wait for a sip response of type %s  (%s)" % (user ,str(code), sip_method)
        return self._execute(user,'wait_incoming_response', code=code,sip_method=sip_method,cseq=cseq)

    def wait_incoming_request(self,user,sip_method,cseq=''):
        """
            Wait an incoming sip request  ( INVITE , CANCEL )

            *@user*: Alice ,Bob

            *@sip_method*: str  , INVITE , REGISTER ...

            *@cseq*: optional: sequence identifier

        """
        print "pilot: user %s wait for a sip request of type %s " % (user , sip_method)
        return self._execute(user,'wait_incoming_request', sip_method=sip_method,cseq=cseq)



    def hangup(self,user,call_indice=None):
        """
            hangup the call with the specified indice

            by default indice is None and we hangup the current call
        """
        print "pilot: user %s hangup the call with indice: %s" % (user,str(call_indice))
        return self._execute(user,"hangup",call_indice=call_indice)

    def wait_hangup(self,user):
        """
            wait for hangup
        """
        print "pilot: user %s wait for hangup" % user
        #
        return self._execute(user,"wait_hangup")

    def sleep(self,seconds=1):
        """
            sleep n seconds
        """
        self._session.sleep(seconds)

    def _check_not_received(self,user,pattern,refresh_count=1):
        """
            check user dont receive pattern

        """
        if not self._dry:
            #return self._session.execute(user,command,*args,**kwargs)
            res= self.proxy._send_operation('check_not_received',user=user,pattern=pattern,refresh_count=refresh_count)
            return self._handle_result(res,raise_on_error=False)

        else:
            return self._dry_return()


        #return self._execute(user,'check_not_received',pattern=pattern,refresh_count=refresh_count)


    def _transfer(self,user,sip_address):
        """
            transfer a call to a sip address
            @user: eg Alice
            @sip_address
        """
        return self._execute(user,"transfer" , destination=sip_address)

    def hold_call(self,user,call_index=None):
        """
            put the specified call on hold

            if no call_index specified: put the current call on hold

            *@user*: eg Alice , Bob

            *@call_index*: the index of the call ( 0: first call , 1: second call ...)

        """
        return self._execute(user,"hold",call_index=call_index)

    def unhold_call(self,user,call_index=None):
        """
            cancel call on hold ( same parmeters as hold_call
        """
        return self._execute(user,"unhold",call_index=call_index)

    def start_player(self,user):
        """
            set the --auto-play option on the terminal
        """
        return self._execute(user,"start_player")

    def stop_player(self,user):
        """
            cancel the --auto-play option on the terminal
        """
        return self._execute(user,"stop_player")

    def start_recorder(self,user):
        """
            set the --aut-rec parameter on the terminal
        """
        return self._execute(user,"start_recorder")

    def stop_recorder(self,user):
        """
            cancel the --auto-rec on the terminal
        """
        return self._execute(user,"stop_recorder")


    def select_previous_call(self,user):
        """
            make the previous call the current call

        *@user*: Alice , Bob ...

        """
        return self._execute(user,"select_previous_call")

    def select_next_call(self,user):
        """

            make the next call the current call

        *@user*: Alice , Bob ...
        """
        return self._execute(user,"select_next_call")


    def dump_call_quality_status(self,user,call_indice=None):
        """
            ask the terminal to dump the media status into log trace , for the user and the call_indice specified

            if call_indice is None : take the current call

            *@user*: Alice Bob ..
            *@call_indice*: DEPRECATED indice of the call ( 0 , 1 ... or None for current call )

        """
        media_log = self._execute(user,'dump_call_quality_status',call_indice=call_indice)
        return media_log

    # def  check_call_quality(self, user,min_packets= 100 , max_loss_rate = 0.1, slot = '0', channel = 'TX', call=None):
    #     """
    #         analyse last dump media status
    #     """
    #     self.dump_call_quality_status(user)
    #     stats = self._execute(user,'check_call_quality',min_packets=min_packets,max_loss_rate=max_loss_rate,channel=channel,slot=slot,call=call)
    #     if stats['packets'] < int(min_packets):
    #         print 'not enough packets'
    #         # find a clean way to break the test
    #         raise Exception('check call quality failed, not enough packets')
    #     if stats['loss_percent'] > float(max_loss_rate):
    #         print "too many loss packets"
    #         raise Exception('check call quality failed, too many loss packets')
    #         # find a clean way to break the test
    #     return stats


    #
    # high level functions
    #

    def wait_sip_response(self,user , code):
        """
            wait for a sip response of type  'SIP/2.0 <code>'

            *DEPRECATED* : use wait_incoming_response instead

            this version catch the first SIP/2.0 response which can be from an other sequence
        """
        expected = "SIP/2.0 %s" % str(code)
        print "pilot: user %s wait for a sip response of type %s  (%s)" % (user ,str(code), expected)
        res= self.proxy._send_operation('expect',user=user,pattern=expected, raise_on_error=True, title="", ignore_timeout=False)
        return res
        #return self._expect(user, pattern=expected, raise_on_error=True, title="", ignore_timeout=False)


    def call_feature_access_code(self,user,call_feature_access_code,userX=None,format=None):
        """
            call Application server with a feature access code

            eg  -CFA , -CFU
        """
        sip_address=self.get_sip_address(user=userX,format=format,fac=call_feature_access_code)

        print "pilot: user %s calls AS with Feature Access code  (%s)" % (user, sip_address)
        #fac = self.ptf.fac(call_feature_access_code)
        #sip_address= self.ptf.sip_address_for(fac)
        return self._call(user,sip_address)


    def call_number(self,user,destination):
        """
            call an arbitrary number or sip address

            *@user*: eg Alice , Bob

            *@destination*:  sip address or number

            - *WARNING*:  using literal sip address in test may make your Test non portable"

            - use 'Call User' or 'Call Destination' instead
        """
        print "pilot: user %s call destination %s" % (user,destination)
        print "pilot: WARNING:  using literal sip address in test may make your Test non portable "
        return self._call(user,sip_address=destination)


    def call_destination(self,user,destination):
        """
            call a predefined destination (defined in platform.json)

            *@user*: eg Alice , Bob

            *@destination*:  the literal name of a destination defined in platform configuration
        """
        res= self.proxy.call_destination(user,destination=destination)
        return self._handle_result(res)
        # # retrieve destination from configuration
        # resolved_destination= self.ptf.destination(destination)
        # sip_address= self.ptf.sip_address_for(resolved_destination)
        # print "pilot: user %s call destination %s  (%s)" % (user,destination,sip_address)
        # #
        # return self._call(user,sip_address=sip_address)


    def call_user(self,userA,userX,format="universal"):
        """
             userA call userX with format (universal,national ...)

             *@userA*: The user who is calling , eg Alice

             *@userX*: The user to call , eg Bob

             *@format*: the number format  ( in ext , sit_ext , national , international , universal ... )

        """
        res= self.proxy.call_user(userA,userX=userX,format=format)
        return self._handle_result(res)
        # # compute sip address for userX with format
        # sip_address = self.ptf.user(userX).sip_address_to_user(format)
        # print  "pilot: user %s call user %s with format %s  (%s)" % ( userA, userX, format ,sip_address)
        # return self._call(userA,sip_address=sip_address)

    def call_sd(self,user,sd):
        """
             user call with direct sd8 key sd ( 1..8 )

             @user: eg Alice Bob
             @sd: the sd8 direct key on the terminal ( eg 1 , 2  ... 8)
        """
        return self.proxy.call_sd(user,sd=sd)
        # target_user = self.ptf.sd8(user,sd)
        # sip_address= self.ptf.sip_address_for(sd)
        # print "pilot: user %s call direct sd8 key %s  (%s)" % (user,sd,sip_address)
        # #
        # return self._call(user,sip_address=sip_address)

    def transfer_to_user(self,userA,userX,format="universal"):
        """
            Unatented transfer call

            *userA* transfer call to *userX* with *format* (universal,national ...)
        """
        return self.proxy.transfer_to_user(userA,userX=userX,format=format)

        # # compute sip address for userX with format
        # sip_address = self.ptf.user(userX).sip_address_to_user(format)
        # print  "pilot: user %s transfer call to user %s with format %s  (%s)" % ( userA, userX, format ,sip_address)
        # return self._transfer(userA,sip_address=sip_address)

    def transfer_to_destination(self,user,destination):
        """
            transfer call (unatended) to a predefined destination (defined in platform.json)

            *@user*: eg Alice , Bob

            *@destination*:  the literal name of a destination defined in platform conf
        """
        return self.transfer_to_destination(user,destination=destination)

        # # retrieve destination from configuration
        # resolved_destination= self.ptf.destination(destination)
        # sip_address= self.ptf.sip_address_for(resolved_destination)
        # print "pilot: user %s transfer call to destination %s  (%s)" % (user,destination,sip_address)
        # #
        # return self._transfer(user,sip_address=sip_address)
        # #return self._call(user,sip_address=sip_address)


    def transfer_attended(self,user,call_indice=0):
        """
            attented transfer call

            userA transfer current call to previously established call number 'call_indice' eg 0
        """
        print  "pilot: user %s transfer current call to previously established call %s" % ( user, str(call_indice))
        return self._execute(user,"transfer_attended" , call_indice = str(call_indice))


    def wait_incoming_call_and_answer_ok(self,user):
        """
            wait for an incoming call and send 200 OK
        """
        self.wait_incoming_call(user)
        return self.answer_call_ok(user)


    def answer_call(self,user,code=200):
        """
            Answer a pending call with code

            warning: check there is a pending call using 'Wait Incoming Call'
        """
        print "pilot: user %s sends code %s OK" % ( user , str(code))
        return self._answer_call(user,code=code,wait_incoming=False)

    def answer_call_ringing(self,user):
        """
           Answer a pending call with code 180 (ringing)

           warning: check there is a pending call using 'Wait Incoming Call'
        """
        print "pilot: user %s sends 180 ringing" %  user
        return self._answer_call(user,code=180,wait_incoming=False)

    def answer_call_ok(self,user):
        """
           Answer a pending call with code 200 (OK)

            warning: check there is a pending call using 'Wait Incoming Call'
        """
        print "pilot: user %s sends 200 OK" %  user
        return self._answer_call(user,code=200,wait_incoming=False)


    def wait_ringing(self,user):
        """
            wait for sip response  'SIP/2.0 180)
        """
        print "pilot: user %s wait for ringing" %  user
        #TODO: replace with wait_incoming_response
        res= self.wait_sip_response(user,180)
        #res= self.wait_incoming_response(user,180,'INVITE')
        return res

    def wait_call_confirmed(self,user):
        """
            wait for call initiated by user to be confirmed

            The call is confirmed by the pjsua stack when it receives the ACK from the remote party
        """
        print "pilot: user %s wait for call to be confirmed" % user
        return self._check_call(user,state="CONFIRMED")

    def wait_call_disconnected(self,user):
        """
            wait for call to be disctonnected


        """
        print "pilot: user %s wait for call to be disconnected" % user
        #
        return self._check_call(user,state="DISCONNECTED")



    def bad_call(self):
        """
            a dummy function to get a failed call
        """
        self._result = "KO"
        raise RuntimeError('Bad Call')
        #return self._result

    def activate_redirection_to_user(self,userA,userB,redirect_kind,call_format):
        """
            call Application Server using a Feature Access Code to redirect userA to userB

            *@userA*: the user to be redirected

            *@userB*: target of the redirection

            *@redirect_kind*: CFA,CFB,CFNA,CFNR  (Always,Busy,NotAnswered,NotReachable)

            *@call_format*: site,national,international,universal

            eg: activate redirection to user  Alice  Bob  CFA  national


        """
        return self._execute(userA,'activate_redirection_to_user',userB=userB,redirect_kind=redirect_kind,call_format=call_format)
        #return self.proxy.activate_redirection_to_user(userA,userB=userB,redirect_kind=redirect_kind,call_format=call_format)

        # fac_name = "+" + redirect_kind
        # fac_code = self.ptf.fac(fac_name)
        # destination = self.ptf.sip_address_for( fac_code + self.ptf.user(userB).to_user(format_=call_format))
        # print "pilot: user %s ask a call forward via feature access code %s to user %s with format %s (%s) " % (
        #     userA,redirect_kind,userB,call_format,destination)
        # # place the call
        # return self._call(userA,sip_address=destination)


    def cancel_redirection(self,user,redirect_kind):
        """

            Convenience function to send a FAC sequence to AS to cancel a redirection

            *@userA*: the user to be redirected

            *@redirect_kind*: CFA,CFB,CFNA,CFNR  (Always,Busy,NotAnswered,NotReachable)
        """
        return self._execute(user,'cancel_redirection',redirect_kind=redirect_kind)
        #return self.proxy.cancel_redirection(user,redirect_kind=redirect_kind)

        # fac_name = "-" + redirect_kind
        # fac_code = self.ptf.fac(fac_name)
        # destination = self.ptf.sip_address_for(fac_code)
        # print "pilot: call AS to cancel a redirection of type %s for user %s (%s)" %(redirect_kind,user,destination)
        # # place the call
        # return self._call(user,sip_address=destination)


    def check_no_incoming_call_received(self,user,refresh_count=1):
        """
            Check that user dont receive an incoming call within a given time

            *@user*: the user eg Bob

            *@refresh_count*: the number of refresh count to wait ( a refresh count represents aproximatively 5 seconds )

        """
        self._check_not_received(user,pattern=">*> Incoming call",refresh_count=refresh_count)



    #
    # functions using siprelay directly
    #
    def  check_call_quality(self, user,min_packets= 100 , max_loss_rate = 0.1, channel = 'RX',slot = '0' , call_indice=None):
        """
            Analyse last dump media status on the channel specified ( RX received or TX transmit )

            - control that a minimum packet has been received or transmit

            - control that loss rate is under a certain threeshold

            *@user*: eg Alice , Bob

            *@min_packets*: the number of minimum packets eg ( 100 : a minimal of 100 packets has been transmit or received)

            *@max_loss_rate*: the maximum percentage of lost packets ( eg: 0.1 : lost packets are under 10 %)

            *@channel*: the channel to check: can be RX for received TX for transmit


        """
        #print "%s: check call quailty" % user
        #validation_func =  SypRelay.check_loss_rate(min_packets,max_loss_rate)
        #return self._relay('check_media',role=user,channel=channel,slot=slot,call_indice=call_indice,validation = validation_func)

        return self._execute(user,'check_call_quality',
            min_packets=min_packets,max_loss_rate=max_loss_rate,channel=channel,slot=slot,call_indice=call_indice)



    def check_incoming_call(self,user,display_name):
        """
            checks display name on FROM header of the incoming INVITE message

            *@user*: str , the user receiving the call (eg Bob)

            *@display_name*: the display name to compare with ( or unkwown )


        """

        # *@from_user*: str , the user from we wait the call
        #
        # *@checks*:str , comma separated tags : the kind of check to perform ( "display,number"  )
        #
        #     if from_user is an account: (eg Alice )
        #         check this call is coming from this user
        #             if display in checks: check display_name ( CLIP Call Line Identity Pressentation
        #             if number in checks: checks number       ( CNIP Call Number Identity presentation
        #     if from_user is 'unkwown' :
        #         we check the identity restrictions
        #            if display in checks : check display is unkwown  ( CLIR call line Identity Restriction )
        #            if number in checks : check number is Unkwon     ( CNIR Call NumberIdentity Restriction

        # if from_user:
        #     # check a call is from a specific user
        #     if display_name is None:
        #         try:
        #             display_name = self.ptf.user(from_user).user_display
        #         except KeyError:
        #             # not an account
        #             if from_user == 'unkwown':
        #                 display_name = 'unkwown'
        #             else:
        #                 raise ValueError('Not a  valid from_user account: %s' % from_user)
        # perform the check
        #validation = SypRelay.check_caller(user,display_name)
        #r = self._relay('check_incoming_call',user,validation)
        #validation = self.proxy.check_caller(user,display= display_name)
        #res= self.proxy._send_operation('check_incoming_call',user=user,display_name=display_name)
        return self._execute(user,'check_incoming_call',display_name=display_name)




    def  get_last_received_sip_request(self, user, sip_method="INVITE"):
        """
             Return the last received sip request message for the specified operation

        *@user*: Alice, Bob

        *@operation*: INVITE ...

        return: the sip message as a string
        """
        return self._execute(user,'get_last_received_sip_request',sip_method=sip_method)
        #return self._session.get_last_received_sip_request(user,sip_method=sip_method)


    # new keywords for v.0.5
    def ptf_get_user_data(self,role):
        """
            return info of a user role

        *@role*: Alice, Bob

        return: dictionary

        available keys are:

            username like +33146502514@sip.osp.com

            display like "Alice Btelu"

            format_X where X can be : universal, national ...


        """
        result= self.proxy.ptf_get_user_data(role=role)
        return result

        # user= self.ptf.user(role)
        #
        # data={}
        # data.update(user.data)
        #
        # # override username with public username
        # data['username'] = user.username
        #
        # data['sda']= user.user_sda()
        # data['contact']= user.user_contact()
        # data['domain']= data['registrar']
        #
        # for format_ in user.number_formats:
        #     data['format_%s' % format_] = user.to_user(format_)
        #
        # return data

    def send_dtmf(self, user, digits):
        """

        send a dtmf sequence

        :param digits:
        :return:
        """
        return self._execute(user, 'send_dtmf', digits=digits)

    def check_dtmf(self, user, digits):
        """

        check an incoming dtmf sequence

        :param digits:
        :return:
        """
        return self._execute(user, 'check_dtmf', digits=digits)


    def check_no_dtmf(self, user,refresh_count=1):
        """

        send a dtmf sequence

        :param digits:
        :return:
        """
        return self._execute(user, 'check_no_dtmf', refresh_count=refresh_count)

    #mapping for the agent method ( click , wait_for_exists , press_home ... )
    def __getattr__(self, function_name):
        """
            wrapper to wild methods

        :param function_name:
        :return:
        """
        def wrapper(agent_id,**kwargs):
            """

            """
            return self._execute(agent_id,function_name,**kwargs)
        return wrapper



    def apidoc(self,collection='-',item='-'):
        """

        :param collection:
        :return:
        """
        return self._get_url('-', 'apidoc', collection=collection,item=item)


    #
    #   stb keywords
    #

    def serial_synch(self,user,timeout=10):
        """

        :param user:string: device alias
        :param timeout:int: timeout in seconds
        :return:
        """
        return self._execute(user,'serial_synch',timeout=timeout)

    def serial_watch(self,user,timeout=5):
        """
            watch serial log for a duration of timeout in seconds
        :param user:string: device alias
        :param timeout:int timeout in second
        :return:
        """
        return self._execute(user, '_serial_watch', timeout=timeout)


    def stb_send_dump(self, user):
        """
            send signal to stb to dump the overlay
        :param user: device alias
        :return:
        """
        return self._execute(user, 'stb_send_dump')


    def send_key(self,user,key='',timeout=5,wait=True):
        """
            send key to device
        :param user: device alias
        :param key: key to send
        :param timeout:int  timeout in seconds
        :param wait: boolean , wait for feed back
        :return:
        """
        return self._execute(user,'send_key',key= key,timeout=timeout,wait=wait)

    def stb_zap(self,user, channel):
        """

            zap to a channel
        :param user:string: device alias
        :param channel: string eg 'tv1'
        :return: True
        """

        return self._execute(user, 'stb_zap',channel=channel)



    def stb_status(self,user):
        return self._execute(user,'stb_status')



    def wording_get(self,user, tag):
        """
            get a wording
        :param user:string: device alias
        :param tag:string: tag to read
        :return:string:
        """
        return self._execute(user, 'wording_get',tag=tag)

    def tv_data_get(self, user,tag):
        """
            get stb TVData attribute

        :param user:string: device alias
        :param tag:
        :return:string:
        """
        return self._execute(user, 'tv_data_get')


    def stb_get_env(self,user, key=None):
        """
        stb get Env Data

        :param user:string: device alias
        :param key:string: name of the key , or None
        :return:string: value of the Key , or a Env dictionary if key is None
        """
        return self._execute(user, 'stb_get_env',key=key)


    def scheduler_start(self,user):
        """

        :return:
        """
        print "start stat scheduler"
        return self._execute(user, 'scheduler_start')

    def scheduler_read(self, user):
        """

        :return:
        """
        print "read scheduler trace"
        return self._execute(user, 'scheduler_read')

    def page_get_info_from_live_banner(self,user):
        """
            get the program info from the last stb dump
        :param user:string device alias
        :return:
        """
        return self._execute(user, 'page_get_info_from_live_banner')

    def page_get_info_from_mosaic_focus(self, user):
        """
            get the program info from the last stb dump
        :param user:string device alias
        :return:
        """
        return self._execute(user, 'page_get_info_from_mosaic_focus')


    def page_action_select(self,user, t, partial=False):
        """
        :param user:string device alias
        :param t: string eg epgNOW
        :param partial:boolean
        :return:
        """
        return self._execute(user, 'page_action_select',t=t,partial=partial)


    def page_get_info_from_epg_focus(self,user):
        """

        :param user:string device alias
        :return:
        """
        return self._execute(user, 'page_get_info_from_epg_focus')


    def tv_get_lcn(self, user,channel):
        """
        :param user:string device alias
        :param channel:
        :return:
        """
        return self._execute(user, 'tv_get_lcn', channel=channel)

    def tv_get_first_channel_Dtt_off(self,user, channel):
        """
        :param user:string device alias
        :param channel: string eg tv2 )
        :return:string
        """
        return self._execute(user, 'tv_get_first_channel_Dtt_off', channel=channel)



    def tv_get_first_channel_Dtt_on(self, user,channel):
        """
        :param user:string device alias
        :param channel: string eg tv2 )
        :return:
        """
        return self._execute(user, 'tv_get_first_channel_Dtt_on', channel=channel)


robot_plugin = Pilot



if __name__=="__main__":

    import inspect

    p= Pilot()

    # for name, func in inspect.getmembers(Pilot, predicate=inspect.ismethod):
    #     if not name.startswith('_'):
    #         Pilot._keywords.append(name)

    p.setup_pilot("demo","qualif")

    ks= p.get_keyword_names()


    print "Done"