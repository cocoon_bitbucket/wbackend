#!/usr/bin/env bash


# build local
echo "build local images"
docker-compose build

## create volumes
#echo "create volumes"
#docker volume create --name alpine_static
#
#
## copy ide static files to alpine_static volume
#echo "copy data to volumes"
#docker run -v alpine_static:/usr/share/nginx/html/static --name helper alpine_ide cp -rf /usr/local/lib/restop/restop_ide/app/static/ /usr/share/nginx/html/static/
#docker rm helper
