

list restop bundle images
-------------------------
docker images | grep bundle


remove iamges
-------------

docker rmi -f dockerbundle_master

docker rmi -f dockerbundle_samples

docker rmi -f cocoon/restop:bundle


rebuild
--------
docker build --force-rm -t cocoon/restop:bundle .

docker-compose build

launch
------
docker-compose up


test
----

cd restop-client/tests/draft

run test_via_robot_plugin.py



adb stuff
=========


start server
------------

#docker run -d --privileged --device /dev/bus/usb -p 5037:5037  --name adbd sorccu/adb
docker run -d --privileged -v /dev/bus/usb:/dev/bus/usb -p 5037:5037  --name adbd sorccu/adb

run adb devices
---------------

docker run --rm -ti --net container:adbd sorccu/adb adb devices

or 
 
 docker run --rm -ti --net container:dockerbundle_nginx_1 sorccu/adb adb devices
