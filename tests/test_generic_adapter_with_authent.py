import time

from wbackend.model import Database,Model
from restop_queues.models import Queue

from restop_queues.adapters import GenericAdapter,GenericAuthAdapter
from restop_queues.client import QueueClient

from restop_queues.connectors import ConnectorFactory, EchoConnector
from restop_queues.connectors.queue_connector import QueueConnector

from restop_queues.connectors.serial_connector import SerialConnector, SerialAuthConnector ,loop_serial

LATENCE=2


def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushall()
    Model.bind(database=db,namespace=None)
    return db


def get_queue():
    """


    :return:
    """

    name= 'auth'
    parameters= {"login":"admin","password":"admin1234","prompt_login":"login:","prompt_password":'assword:'}

    q= AuthQueue(name=name,parameters=parameters)
    q.save()

    return q


def test_generic_echo_adapter_passive():
    """



    :return:
    """

    db= get_new_db()

    # set target queue
    name= 'target'
    parameters= {"k1":"k1"}

    target_queue= Queue(name=name,parameters=parameters)
    target_queue.save()
    target_queue_key = target_queue.get_hash_id()

    target_connector = QueueConnector(target_queue_key, redis_db=db)



    queue = get_queue()


    #passive
    adapter= GenericAuthAdapter(queue,run_mode= 'passive',log=None,connector=target_connector)
    # try:
    #     adapter.start()
    # except RuntimeError,e:
    #     # AttributeError: 'NoneType' object has no attribute 'read'
    #     # no connector assigned
    #     pass
    #
    #
    #
    #
    # factory= ConnectorFactory(EchoConnector)
    # adapter.bind_connector(factory)
    adapter.start()

    adapter.open_connector()

    adapter.write("hello world\n")

    adapter.run_once()
    adapter.run_once()


    # transfer target output to auth adapter
    line= target_connector.readline(channel_name='stdin')
    target_connector.write(line,channel_name='stdout')


    # read
    adapter.run_once()
    line= adapter.readline()
    assert line == "hello world"


    # inject prompt login to target
    target_connector.write("login:\n",channel_name='stdout')

    # read
    adapter.run_once()
    line= adapter.readline()
    assert line == "login:"

    # inject prompt login to target
    target_connector.write("password:\n",channel_name='stdout')


    # read
    adapter.run_once()
    line= adapter.readline()
    assert line == "password:"


    adapter.close_connector()

    adapter.exit()



def test_SerialAuthConnector():
    """

    :return:
    """
    parameters= {
        'prompt_login': 'login:',
        'prompt_password': 'assword:',
        'login': 'me',
        'password': 'myPassword'
    }

    c= SerialAuthConnector('myusb',device=loop_serial,  **parameters)
    c.open()
    c.write('hello\n')
    c.write('bye\n')

    time.sleep(LATENCE)
    line= c.readline()
    assert line == "hello\n"
    line= c.readline()
    assert line == "bye\n"

    line= c.readline()
    assert line == ''


    # test authenticate ( device is loop )

    # simulate prompt login
    c.write('login:\n')

    # simulate prompt password
    c.write('password:\n')

    time.sleep(LATENCE)
    line= c.readline()
    assert line.strip() == 'login:'
    line= c.readline()
    assert line.strip() == 'password:'
    line = c.readline()
    assert line.strip() == 'me'
    line = c.readline()
    assert line.strip() == 'myPassword'


    return


#
#  dev
#

from walrus import TextField
class AuthQueue(Queue):
    """


    """
    collection= 'authqueue'

    prompt_login= TextField(default='login:')
    prompt_password= TextField(default='assword:')

    login= TextField()
    password= TextField()



if __name__=="__main__":

    import logging
    logging.basicConfig(level=logging.DEBUG)


    #test_generic_echo_adapter_passive()

    test_SerialAuthConnector()

    print "Done"

