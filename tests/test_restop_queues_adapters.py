import time

from wbackend.model import Database,Model

from restop_queues.models import Queue

from restop_queues.adapters import GenericAdapter
from restop_queues.client import QueueClient

from restop_queues.connectors import ConnectorFactory, EchoConnector
from restop_queues.connectors.queue_connector import QueueConnector
from restop_queues.connectors.process_connector import ProcessConnector

# def ConnectorFactory(ConnectorClass, name='generic', log=None, **kwargs):
#     def wrapper():
#         """
#
#         :return:
#         """
#         connector = ConnectorClass(name, log=log, **kwargs)
#         return connector
#
#     return wrapper

LATENCE=2


def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushall()
    Model.bind(database=db,namespace=None)
    return db


def get_queue():
    """


    :return:
    """

    name= 'generic'
    parameters= {"k1":"v1"}

    q= Queue(name=name,parameters=parameters)
    q.save()

    return q


def test_echo_connector():
    """

    :return:
    """
    c= EchoConnector('echo')
    c.open()
    c.write('hello')
    c.write('bye')
    line= c.readline()
    assert line == "hello"
    line= c.readline()
    assert line == "bye"
    line= c.readline()
    assert line is None
    return

def test_process_connector():
    """

    :return:
    """
    db=get_new_db()


    c= ProcessConnector('process',command_line='bash',redis_db=db)
    c.open()
    c.write('ls -l')
    time.sleep(2)
    line= c.readline()
    while line is not None:
        line=c.readline()
    c.close()
    return




def test_queue_connector():
    """

    :return:
    """
    db= get_new_db()
    queue= get_queue()
    queue_key= queue.get_hash_id()

    c= QueueConnector(queue_key,redis_db=db)
    c.open()
    c.write('hello')
    c.write('bye')
    line= c.readline(channel_name='stdin')
    assert line == "hello"
    line= c.readline(channel_name='stdin')
    assert line == "bye"
    line= c.readline(channel_name='stdin')
    assert line is None
    return



def test_generic_echo_adapter_passive():
    """



    :return:
    """



    db= get_new_db()
    queue = get_queue()


    #passive
    adapter= GenericAdapter(queue,run_mode= 'passive',log=None)
    try:
        adapter.start()
    except RuntimeError as e:
        # AttributeError: 'NoneType' object has no attribute 'read'
        # no connector assigned
        pass

    factory= ConnectorFactory(EchoConnector)
    adapter.bind_connector(factory)
    adapter.start()

    adapter.open_connector()

    adapter.write("hello world\n")

    adapter.run_once()
    adapter.run_once()

    line= adapter.readline()
    assert line == "hello world"

    adapter.close_connector()

    adapter.exit()

def test_generic_echo_adapter_threaded():
    """



    :return:
    """

    db = get_new_db()
    queue = get_queue()

    # thread
    adapter = GenericAdapter(queue, run_mode='thread')

    # check error becaise no boud connector
    try:
        adapter.start()
    except RuntimeError as e:
        # AttributeError: 'NoneType' object has no attribute 'read'
        # no connector assigned
        pass


    factory= ConnectorFactory(EchoConnector)
    adapter.bind_connector(factory)
    adapter.start()

    adapter.write("hello\n")
    time.sleep(LATENCE)

    line= adapter.readline()
    assert line == "hello"


    adapter.exit()



    return







if __name__=="__main__":

    import logging
    logging.basicConfig(level=logging.DEBUG)

    test_echo_connector()
    test_process_connector()
    test_queue_connector()

    test_generic_echo_adapter_passive()
    test_generic_echo_adapter_threaded()

    print("Done")

