from restop_graph import TraceDriver,TraceParser

import logging

logging.basicConfig(level=logging.DEBUG)

temp_file = "/tmp/trace"



def test_local_commands():
    """

    :return:
    """
    #
    # generate df metric to /tmp/trace
    #
    driver = TraceDriver(filename=temp_file)

    begin_commands = driver.gen_start_trace()
    shell_cmd = driver.get_collector_command('df')
    df_commands = driver.gen_command_lines('df', shell_cmd)

    print begin_commands
    print df_commands

    #
    # run the command localy
    #
    driver.run_local(begin_commands)
    driver.run_local(df_commands)

    #
    # parse trace from /tmp/trace and inject into graphite
    #
    parser = TraceParser.from_file(filename=temp_file, device='tv', configfile='./collectors.ini')
    parser.push_all_to_graphite()

    # parser.push_all_to_influxdb()
    return

if __name__=="__main__":

    test_local_commands()

    print "Done."