"""


    test acces to usbhub server on master via the usbhub client


    prerequisite
        a usbhub server must be started


"""
import time

from walrus import Database
from wbackend.model import Model
from restop_queues.models import Queue,QueueHub

from restop_queues.client import QueueClient


from restop_usb.models import UsbQueueHub,UsbQueue
from restop_usb.controllers import ProcessUsbQueueHubServer

from restop_usb.client  import UsbControllerClient

from restop_queues.connectors.queue_connector import QueueConnector

from restop_stats.models import StatSchedulerQueue
from restop_stats.controllers import StatScheduler

from restop_usb.controllers import UsbQueueServerBase


#REDIS_HOST= '192.168.99.100'
REDIS_HOST= '192.168.1.21'
#REDIS_HOST= 'localhost'
REDIS_PORT= 6379
REDIS_DB= 0


redis_db= Database(host=REDIS_HOST,port=REDIS_PORT,db=REDIS_DB)
#redis_db.flushdb()
Model.bind(redis_db)

# hub_model= UsbQueueHub.create()
# hub_server= ProcessUsbQueueHubServer(hub_model)
# hub_server.create_queue_server('_loop',device="loop://?logging=debug")
# hub_server.create_queue_server('usb0',device='/dev/ttyUSB0')
# #hub_server.start()

def start_queue_server(name='shell'):
    """


    :return:
    """
    # create client to hub server
    hub_queue_key = 'queuehub:id:default'
    hub_client = QueueClient(hub_queue_key, redis_db=redis_db)

    # ask hub server to start  server
    hub_client.send('start_adapter', dict(name=name, run_mode='thread'))
    time.sleep(2)
    status,queue_key = hub_client.receive()
    return queue_key


def test_mock_device_shell():
    """

        send command to a Process server at queue:id:shell

    :return:
    """

    # create client to hub server
    # create hub client
    hub_queue_key = 'queuehub:id:default'
    hub_client = QueueClient(hub_queue_key, redis_db=redis_db)

    # ask hub to start shell server
    hub_client.send('start_adapter', dict(name='usb0', run_mode='thread'))
    time.sleep(2)
    status,queue_key = hub_client.receive()

    # start echo queue client
    client = QueueClient(queue_key,redis_db=redis_db)

    client.write('ls -l /tmp\n')

    time.sleep(2)

    line= client.readline()
    while line:
        line= client.readline()

    cmd= 'ash /tmp/trace-scheduler >> /tmp/trace-scheduler.txt &\n'
    client.write(cmd)

    time.sleep(2)

    line= client.readline()
    while line:
        line= client.readline()

    client.write('ls -l /tmp\n')
    line = client.readline()
    while line:
        line = client.readline()

    client.write('killall ash\n')
    line = client.readline()
    while line:
        line = client.readline()

    client.write('ps\n')
    line = client.readline()
    while line:
        line = client.readline()


    # shutdown shell server
    client.exit()
    time.sleep(3)

    return


def test_stat_scheduler():
    """



    :return:
    """

    # start server queue
    destination_queue_key= start_queue_server('usb0')


    # create stats scheduler
    qs = StatSchedulerQueue(name='statscheduler')
    qs.save()
    adapter= StatScheduler(qs,run_mode= 'thread',log=None,
                           feedback_url= "http://%s:8080/callback_statscheduler" % REDIS_HOST)

    # create destination connector
    connector= QueueConnector(name=destination_queue_key,redis_db=redis_db,log=adapter.log)
    # bind it to stats scheduler
    adapter.bind_connector(connector)
    adapter.start()


    adapter.job_init(metrics='df')


    time.sleep(1)

    adapter.job_sync()

    time.sleep(1)

    adapter.job_stop()

    time.sleep(1)

    adapter.close_connector()

    adapter.exit()


    return




def test_usbqueuehub_step_by_step():
    """



    :return:
    """
    device= '_loop'

    usb_model= UsbQueue.load(device)
    usbserver= UsbQueueServerBase(usb_model)

    usbserver.write('\n')
    usbserver.write('ls\n')

    usbserver.open_connector()

    for i in xrange(0,5):

        usbserver.run_once()

        time.sleep(1)

    time.sleep(1)

    usbserver.close_connector()
    return





def test_loop():
    """

    :return:
    """

    client= UsbControllerClient(redis_db=redis_db)

    usb= client.start_queue_server('_loop')

    time.sleep(1)
    usb.clear()

    usb.write('\n')
    usb.write('ls -l /tmp\n')

    for i in xrange(0,10):
        res = usb.readline()
        print res
        time.sleep(1)
    usb.exit()

    return




def test_usb0():
    """

    :return:
    """

    client = UsbControllerClient(redis_db=redis_db)

    usb = client.start_queue_server('usb0')

    time.sleep(1)
    usb.clear()
    time.sleep(3)
    usb.write('\n')
    usb.write('ls -l /tmp\n')

    time.sleep(5)

    for i in xrange(0, 30):
        res = usb.readline()
        print res
        #time.sleep(2)
    usb.exit()

    return


if __name__=="__main__":


    #test_mock_device_shell()
    #test_stat_scheduler()

    #test_usbqueuehub_step_by_step()
    #test_loop()
    #test_usb0()

    print "Done"


