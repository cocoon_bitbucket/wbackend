import time
from walrus import Database
from wbackend.model import Model

from restop_queues import Queue,QueueHub, QueueClient
from restop_queues.connectors import AbstractConnector,ConnectorFactory
from restop_queues.controllers.hub import ProcessQueueHubServer
#from restop_queues.adapters import GenericAdapter
from restop_queues.adapters import GenericAdapter

from restop_queues.adapters import adapter_factory

#from restop_queues.controllers.factories import adapter_factory

from restop_queues.connectors import EchoConnector
from restop_queues.connectors.process_connector import ProcessConnector
from restop_queues.connectors.queue_connector import QueueConnector
from restop_queues.connectors.telnet_connector import TelnetConnector
from restop_queues.connectors.serial_connector import SerialConnector,SerialAuthConnector
#from restop_usb.connectors import SerialConnector


loop_serial= "loop://?logging=debug"
mock_serial= "mock://?logging=debug"

config= {
    'echo': {
        'class':'EchoConnector'
        },
    'shell': {
        'class':'ProcessConnector',
        'command_line': 'bash\n'
        },
    'serial': {
        'class':'SerialConnector',
        'device':loop_serial,
        'bauds':115200
    },
    'telnet': {
        'class': 'TelnetConnector',
        'host': 'localhost',
    },
    'queue':{
        'class': "QueueConnector",
        'queue_key': 'queue:id:destination',
        'host': 'localhost',
        'port': 6379,
        'db':0
    }

}


def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushall()
    Model.bind(database=db,namespace=None)
    return db

def create_queues():
    """


    :return:
    """
    # create queues
    for name, kwargs in config.iteritems():
        queue = Queue(name=name, parameters=kwargs)
        queue.save()
    return



def adapter_factory2(queue,run_mode='thread'):

    def adapter_wrapper():
        # build an adapter
        name= queue.name
        parameters= queue.parameters.copy()

        assert 'class' in parameters, "queue must have a connector 'class' parameter"
        classname = parameters.pop('class')

        cls = AbstractConnector.select_plugin(classname)
        factory = ConnectorFactory(cls, name, **parameters)

        adapter = GenericAdapter(queue, run_mode='thread')
        adapter.bind_connector(factory)
        return adapter

    return adapter_wrapper




def test_select_connector():
    """

    :return:
    """

    cls= AbstractConnector.select_plugin('EchoConnector')

    c= cls('echo')

    c.open()
    c.write('hello')
    c.write('bye')
    line= c.readline()
    assert line == "hello"
    line= c.readline()
    assert line == "bye"
    line= c.readline()
    assert line is None
    return

def test_select_connectors():
    """

    :return:
    """
    for name,kwargs in config.iteritems():

        kwargs= kwargs.copy()
        classname= kwargs.pop('class')
        cls= AbstractConnector.select_plugin(classname)

        connector= cls(name,**kwargs)
        factory= ConnectorFactory(cls,name,**kwargs)

    return



def test_adapter_factory():
    """

    :return:
    """
    db= get_new_db()

    adapters= {}


    # create queues
    for name,kwargs in config.iteritems():
        queue = Queue(name=name, parameters=kwargs)
        queue.save()

    # create adapters
    for queue in Queue.all():
        if 'class' in queue.parameters:
            adapters[queue.name]= adapter_factory(queue)

    # start adapters
    for name in sorted(config.keys()):
        adapter= adapters[name]
        adapter().start()

    return



def test_hub_start_server():
    """

    :return:
    """
    db= get_new_db()

    adapters= {}


    # create queues
    for name,kwargs in config.iteritems():
        queue = Queue(name=name, parameters=kwargs)
        queue.save()

    # create adapters
    hub_model= QueueHub.create(name='default')
    hub= ProcessQueueHubServer(hub_model)
    hub.start()

    for queue in Queue.all():
        if 'class' in queue.parameters:

            q= hub.start_adapter(queue.name,run_mode='thread')

            continue


    return


def test_hub_client():
    """

    :return:
    """
    db= get_new_db()
    create_queues()

    # create hub
    hub_model= QueueHub.create(name='default')
    hub= ProcessQueueHubServer(hub_model)
    hub.start()

    # create hub client
    hub_queue_id = hub_model.get_hash_id()
    hub_client= QueueClient(hub_queue_id,redis_db=db)


    hub_client.send('list_queues')
    time.sleep(3)
    status,queues= hub_client.receive()
    assert queues == [u'echo', u'serial', u'telnet', u'shell', u'queue']

    hub_client.send('inspect_queue', dict(name='echo'))
    time.sleep(2)
    status, data = hub_client.receive()
    assert data['name'] == 'echo'

    hub_client.send('start_adapter', dict(name='echo',run_mode='thread'))
    time.sleep(2)
    status,queue_key= hub_client.receive()

    queue_client= QueueClient(queue_key)

    queue_client.open()
    queue_client.write('hello\n')
    time.sleep(2)
    line= queue_client.readline()
    assert line == 'hello'
    return



def test_serial_connector():
    """


    :return:
    """
    name= 'usb'
    parameters= {
        'class': 'SerialConnector',
        # a special device from pyserial library acting like an echo server
        'device': "loop://?logging=debug",
        'bauds': 115200
    }
    c= SerialConnector(name,**parameters)
    c.open()
    c.write("hello\n")
    time.sleep(2)
    line= c.readline()
    assert line == "hello\n"
    c.close()

    return

def test_serial_auth_connector():
    """


    :return:
    """
    name= 'usb'
    parameters= {
            'class': 'SerialAuthConnector',
            # a special device from pyserial library acting like an echo server
            'device': "loop://?logging=debug",
            'bauds': 115200,
            'encoding': 'UTF-8',
            'prompt_login': 'login:',
            'prompt_password': 'assword:',
            'login': 'admin',
            'password': 'admin1234'
    }
    c= SerialAuthConnector(name,**parameters)
    c.open()
    c.write("hello\n")
    time.sleep(2)
    line= c.readline()
    assert line == "hello\n"
    # inject login prompt
    c.write(c.auth_parameters['prompt_login'])
    time.sleep(2)
    line= c.readline()
    assert line==  c.auth_parameters['prompt_login']

    line= c.readline()
    assert line.strip() == c.auth_parameters['login']

    # inject password prompt
    c.write(c.auth_parameters['prompt_password'])
    time.sleep(2)
    line= c.readline()
    assert line==  c.auth_parameters['prompt_password']

    time.sleep(2)
    line= c.readline()

    c.close()

    return


def test_telnet_connector():
    """


    :return:
    """
    name= 'telnet'
    parameters= {
        'class': 'TelnetConnector',
        'host' : '192.168.1.1',
        'port': 23,
        'user' : "root",
        'password' : "sah",
        'user_prompt' :'login: ',
        'password_prompt' : 'assword: '
    }
    c= TelnetConnector(name,**parameters)
    c.open()


    c.write('echo "hello"\n')
    time.sleep(2)
    line= c.readline()
    assert "hello" in line
    c.close()

    return





if __name__=="__main__":

    import logging
    logging.basicConfig(level=logging.DEBUG)

    test_serial_connector()
    #test_serial_auth_connector()

    #test_telnet_connector()


    test_select_connector()
    test_select_connectors()

    #test_adapter_factory()

    #test_hub_start_server()

    test_hub_client()

    print "Done"

