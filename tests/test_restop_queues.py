import time
import threading
import multiprocessing

from wbackend.model import Database,Model

from restop_queues.models import Queue,QueueHub
from restop_queues.controllers import QueueServerBase, ThreadedQueueServer, ProcessQueueServer

from restop_queues.adapters import AbstractAdapter, EchoAdapter
from restop_queues.client import QueueClient

from restop_queues.connectors import EchoConnector



def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushall()
    Model.bind(database=db,namespace=None)
    return db


def get_queue():
    """


    :return:
    """

    name= 'q1'
    parameters= {"k1":"v1"}

    q= Queue(name=name,parameters=parameters)
    q.save()

    return q


def test_echo_connector():
    """

    :return:
    """
    c= EchoConnector('echo')
    c.open()
    c.write('hello')
    c.write('bye')
    line= c.readline()
    assert line == "hello"
    line= c.readline()
    assert line == "bye"
    line= c.readline()
    assert line is None
    return



def test_queue_model():
    """

    :return:
    """
    db = get_new_db()

    name= 'q1'
    parameters= {"k1":"v1"}

    q= Queue(name=name)
    q.save()

    q= Queue.load(name)
    assert q.name== name

    q.parameters= parameters
    q.save()
    assert q.parameters == parameters

    assert q.is_empty('stdin')

    q.write('some data','stdin')
    assert q.is_empty('stdin') == False
    assert q.size('stdin') == 1

    data= q.readline('stdin')
    assert data== 'some data'

    assert q.is_empty('stdin') == True
    assert q.size('stdin') == 0

    text= 'fill'
    for i in xrange(0,3):
        t= "fill%d" % (i+1)
        q.write(t,channel_name='stdin')
        q.write(t,channel_name='stdout')
        q.write(t, channel_name='stdlog')

    r= q.truncate(channel_name='stdlog',size=2)

    q.clear()
    assert q.size('stdin') == 0
    assert q.size('stdout') == 0
    assert q.size('stdlog') == 0


    return





def test_queue_hub_model():
    """

    :return:
    """
    db = get_new_db()

    name= 'q1'
    parameters= {"k1":"v1"}

    q= QueueHub(name=name,parameters=parameters)
    q.save()

    q= QueueHub.load(name)
    assert q.name== name

    q.parameters= parameters
    q.save()
    assert q.parameters == parameters

    assert q.is_empty('stdin')

    q.write('some data','stdin')
    assert q.is_empty('stdin') == False
    assert q.size('stdin') == 1

    data= q.readline('stdin')
    assert data== 'some data'

    assert q.is_empty('stdin') == True
    assert q.size('stdin') == 0

    q2= Queue(name='q2')
    q2.save()
    q3= Queue(name='q3')
    q3.save()

    q.add_queue(q2)
    q.add_queue(q3)

    q.del_queue(q2)

    return



def test_queue_server_base():
    """


    :return:
    """

    db = get_new_db()

    name= 'q1'
    parameters= {"k1":"v1"}

    q= Queue(name=name)
    q.save()

    q= Queue.load(name)
    assert q.name== name

    server= QueueServerBase(q)


    text= 'some data'
    server.write(text)
    server.write(text,channel_name='stdout')

    assert server.model.size('stdin') == 1
    assert server.model.size('stdout') == 1

    rc= server.readline()
    assert rc == text

    rc= server.readline(channel_name='stdin')
    assert rc == text

    assert server.model.size('stdin') == 0
    assert server.model.size('stdout') == 0


    server.write(text)
    assert server.model.size('stdin') == 1

    server.log.info("about to run once")
    server.run_once()
    assert server.model.size('stdin') == 0
    assert server.model.size('stdout') == 1
    server.log.info("run once done")

    line= server.readline()
    assert server.model.size('stdout') == 0
    assert line == text

    line = server.readline()
    assert line is None

    return



def test_threaded_queue_server():
    """


    :return:
    """

    db = get_new_db()

    name= 'q1'
    parameters= {"k1":"v1"}

    q= Queue(name=name,parameters=parameters)
    q.save()

    q= Queue.load(name)
    assert q.name== name

    server= ThreadedQueueServer(q)
    server.start()

    assert server.is_alive() == True


    text= 'some data'
    server.write(text)

    time.sleep(1)

    rc= server.readline()
    assert rc == text


    server.set_signal('abort')

    #server.exit()
    time.sleep(2)

    assert server.is_alive() == False

    return

def test_process_queue_server():
    """


    :return:
    """

    db = get_new_db()

    name= 'process'
    parameters= {"k1":"v1"}

    q= Queue(name=name,parameters=parameters)
    q.save()

    q= Queue.load(name)
    assert q.name== name

    server= ProcessQueueServer(q)
    server.start()

    assert server.is_alive() == True

    text= 'some data'
    server.write(text)

    time.sleep(1)

    rc= server.readline()
    assert rc == text

    server.exit()

    assert server.is_alive() == False

    return


def test_queue_client():
    """

    :return:
    """
    db= get_new_db()
    queue= get_queue()
    server= ThreadedQueueServer(queue)
    server.start()

    agent_id= queue.get_hash_id()

    client= QueueClient(agent_id,redis_db=db)

    text= "hello there"
    client.write(text)

    client.log.info('text sended')
    time.sleep(1)
    line= client.readline()
    client.log.debug('readline')

    assert line == text

    #client.exit()

    client.exit()


    k= client.keep_alive()
    assert k == 'Done'

    return


def test_client_expect():
    """

    :return:
    """
    db= get_new_db()
    queue= get_queue()
    server= ThreadedQueueServer(queue)
    server.start()

    agent_id= queue.get_hash_id()

    client= QueueClient(agent_id,redis_db=db)

    client.write("hello there\n")

    text= "this what im looking for"
    client.write(text)

    client.write('dont care of this line')

    r= client.expect(text,timeout=120)

    assert 'found' in r[-1]

    client.exit()


    k = client.keep_alive()
    assert k == 'Done'


def test_echo_adapter():
    """

    :return:
    """
    db= get_new_db()
    queue= get_queue()
    server= EchoAdapter(queue)
    server._run_mode= 'thread'
    server.start()

    agent_id= queue.get_hash_id()

    client= QueueClient(agent_id,redis_db=db)

    text= "this what im looking for"
    client.write(text)


    line= client.readline()


    client.exit()


    k = client.keep_alive()
    assert k == 'Done'





def test_client_sync():
    """

    :return:
    """
    db= get_new_db()
    queue= get_queue()
    server= ThreadedQueueServer(queue)
    server.start()

    agent_id= queue.get_hash_id()

    client= QueueClient(agent_id,redis_db=db)

    client.write("hello there\n")

    text= "this what im looking for"
    client.write(text)

    client.write('dont care of this line')

    r= client.sync(timeout=2)



    client.exit()


    k = client.keep_alive()
    assert k == 'Done'




def test_mock_concern():
    """


    :return:
    """

    db = get_new_db()

    name= 'q1'
    parameters= {"k1":"v1"}

    q= Queue(name=name,parameters=parameters)
    q.save()

    q= Queue.load(name)
    assert q.name== name

    agent_id = q.get_hash_id()
    client = QueueClient(agent_id, redis_db=db)
    mock= QueueClient(agent_id, redis_db=db)

    server= QueueServerBase(q)

    # do not start
    server._mode= None
    server.start()
    assert server.is_alive() == False

    text= 'some data'
    # client write some text  ( stdin implicit )
    client.write(text)

    # mock readline it from stdin explicitly
    r= mock.readline(channel_name='stdin')

    # mock transform it and respond
    mock.write(text.upper(), channel_name='stdout')

    # client read it
    #time.sleep(1)
    line= client.readline()

    assert line == text.upper()

    client.exit()


    return


def test_double_queue_server():
    """
        check not 2 usb queue server coexist

    :return:
    """
    db= get_new_db()
    queue_model= get_queue()


    # start a first queue server
    queue_server= ThreadedQueueServer(queue_model)
    queue_server.start()
    time.sleep(1)


    # start another server to the same queue
    queue_server2=  ThreadedQueueServer(queue_model)

    # start now
    queue_server2.start()

    time.sleep(2)
    queue_server2.exit()
    return




if __name__=="__main__":

    import logging
    logging.basicConfig(level=logging.DEBUG)

    test_echo_connector()
    test_queue_model()
    test_queue_hub_model()

    test_queue_server_base()


    test_threaded_queue_server()
    test_process_queue_server()

    test_queue_client()


    test_client_expect()


    test_echo_adapter()

    test_client_sync()

    test_mock_concern()

    test_double_queue_server()

    print("Done")