import time
import json

from wbackend.model import Database,Model

from restop_queues.models import Queue
from restop_queues.client import QueueClient

from restop_stats.models import StatSchedulerQueue
from restop_stats.controllers import StatScheduler

from restop_queues.connectors import ConnectorFactory, EchoConnector
from restop_queues.connectors.telnet_connector import TelnetConnector
from restop_queues.connectors.queue_connector import QueueConnector

from restop_usb.client import UsbControllerClient



LATENCE=2

_default_metrics= ['df', 'cpu','meminfo','loadavg']
default_metrics= 'df cpu meminfo loadavg'



DEVICE_HOST= "192.168.1.21"
DEVICE_QUEUE_NAME= "usb0"


def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushall()
    Model.bind(database=db,namespace=None)
    return db

def get_db1():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=1)
    return db


def get_db_stbplay():
    """

    :return:
    """
    db = Database(host= DEVICE_HOST, port=6379, db=0)
    return db




def get_queue():
    """


    :return:
    """

    name= 'stbplay'
    parameters= {
        "feedback_url": "http://localhost:5000/feedback_url" ,
        "with_curl": True,
    }

    q= StatSchedulerQueue.create(name=name,**parameters)
    q.save()

    return q

def get_connector_queue():
    """

    :return:
    """
    q= Queue(name='destination')
    q.save()
    return q



def test_queue_connector():
    """

    :return:
    """
    db= get_new_db()
    queue= get_connector_queue()
    queue_key= queue.get_hash_id()

    c= QueueConnector(queue_key,redis_db=db)
    c.open()
    c.write('hello')
    c.write('bye')
    line= c.readline(channel_name='stdin')
    assert line == "hello"
    line= c.readline(channel_name='stdin')
    assert line == "bye"
    line= c.readline(channel_name='stdin')
    assert line is None
    return


dummy_operation= 'dummy ' + json.dumps({'option':'hello'})


def test_statscheduler_client():
    """


    :return:
    """

    db = get_new_db()
    queue = get_queue()

    # thread
    adapter = StatScheduler(queue, log=None, feedback_url="http://localhost:5000/feedback_url")

    destination_queue = Queue(name='destination')
    destination_queue_key = destination_queue.get_hash_id()

    factory = ConnectorFactory(QueueConnector,name=destination_queue_key, redis_db=db, log=adapter.log)
    adapter.bind_connector(factory)
    adapter.start()

    # test adapter
    adapter.write(dummy_operation)
    time.sleep(LATENCE)
    line = adapter.readline()
    # receive the json response "hello"
    assert line == '"hello"'


    # create a client to the statsscheduler
    adapter_key= queue.get_hash_id()
    client= QueueClient(adapter_key,log=adapter.log)


    client.send('job_init' , dict(metrics="df") )
    time.sleep(2)
    client.send('job_sync')
    time.sleep(2)

    #client.send('job_close')

    client.exit()


    return




def test_adapter_passive():
    """



    :return:
    """
    db= get_new_db()
    db1= get_db1()
    queue = get_queue()


    #passive
    adapter= StatScheduler(queue,run_mode= 'passive',log=None,feedback_url= "http://localhost:5000/feedback_url")
    try:
        adapter.start()
    except RuntimeError,e:
        # AttributeError: 'NoneType' object has no attribute 'read'
        # no connector assigned
        pass

    # temporary queue just to get a queue id
    destination_queue= Queue(name='destination')
    destination_queue_key= destination_queue.get_hash_id()

    factory= ConnectorFactory(QueueConnector,
                              name=destination_queue_key,redis_db=db1,log=adapter.log)
    adapter.bind_connector(factory)
    adapter.start()

    adapter.open_connector()

    adapter.write(dummy_operation)

    adapter.run_once()

    line= adapter.readline()
    # receive the json response "hello"
    assert line == '"hello"'


    adapter.job_init(metrics=default_metrics)


    time.sleep(1)

    adapter.job_sync()

    time.sleep(1)

    adapter.job_stop()

    time.sleep(1)

    adapter.close_connector()

    adapter.exit()


    return

def test_adapter_threaded():
    """



    :return:
    """
    db = get_new_db()
    queue = get_queue()

    # thread
    adapter = StatScheduler(queue, log=None, feedback_url="http://localhost:5000/feedback_url")

    destination_queue = Queue(name='destination')
    destination_queue_key = destination_queue.get_hash_id()

    factory = ConnectorFactory(QueueConnector,name=destination_queue_key, redis_db=db, log=adapter.log)
    adapter.bind_connector(factory)
    adapter.start()

    adapter.write(dummy_operation)
    time.sleep(LATENCE)
    line = adapter.readline()
    # receive the json response "hello"
    assert line == '"hello"'


    # start the sceduler
    adapter.job_init(metrics=default_metrics)

    time.sleep(11)

    adapter.job_sync()

    time.sleep(12)

    #adapter.job_stop()

    time.sleep(1)

    adapter.exit()

    time.sleep(10)

    return



def test_stbplay():
    """

    :return:
    """
    db = get_new_db()
    queue = get_queue()


    # thread
    adapter = StatScheduler(queue, log=None, feedback_url="http://localhost:5000/feedback_url")


    # get remote hub client
    queue_hub_redis = Database(host=DEVICE_HOST, port=6379, db=0)
    queue_hub_client = QueueClient('queuehub:id:default', redis_db=queue_hub_redis)
    # ask remote hub to start queue usb0
    queue_hub_client.send('start_adapter', dict(name=DEVICE_QUEUE_NAME, run_mode='thread'))

    # get a client to the remote usb0 queue
    usb_client = QueueClient('queue:id:%s' % DEVICE_QUEUE_NAME, redis_db=queue_hub_redis)


    # test remote usb
    usb_client.write('ls -l /tmp\n')
    time.sleep(LATENCE)
    line= usb_client.readline()
    while line:
        line = usb_client.readline()

    # bind usb client to stat scheduler
    adapter.bind_connector(usb_client)
    adapter.connector= usb_client
    adapter.start()




    # start the scheduler
    adapter.job_init(metrics=default_metrics)

    time.sleep(11)

    adapter.job_sync()

    time.sleep(12)

    adapter.job_stop()

    time.sleep(1)

    adapter.exit()

    # empty usb queue and kill it
    line= usb_client.readline()
    while line:
        line = usb_client.readline()
    usb_client.exit()

    return


def test_livebox_telnet_by_console():
    """

    :return:
    """
    db = get_new_db()

    # create statscheduler model
    name = 'livebox'
    parameters = {
        "with_curl": False,
    }
    queue = StatSchedulerQueue.create(name=name, **parameters)
    queue.save()

    # thread
    adapter = StatScheduler(queue, log=None, with_curl= False)

    # create telnet connector
    telnet_connector= TelnetConnector('telnet',
                                      host= '192.168.1.1',
                                      port= 23,
                                      user= 'root',
                                      password='sah'
                                      )
    # test telnet connector
    telnet_connector.open()
    telnet_connector.write('ps | grep ash\n')
    time.sleep(LATENCE)
    line= telnet_connector.readline()
    while line:
        line = telnet_connector.readline()
    telnet_connector.close()

    # bind usb client to stat scheduler
    adapter.bind_connector(telnet_connector)
    adapter.start()


    # start the scheduler
    adapter.job_init(metrics=default_metrics)

    time.sleep(11)

    adapter.job_sync()

    time.sleep(12)

    adapter.job_stop()

    time.sleep(1)

    adapter.exit()

    time.sleep(5)

    return







if __name__=="__main__":

    import logging
    logging.basicConfig(level=logging.DEBUG)

    #test_statscheduler_client()

    #test_stbplay()

    #test_queue_connector()

    #test_adapter_passive()
    #test_adapter_threaded()

    test_livebox_telnet_by_console()

    print "Done"

