import time

from wbackend.model import Database,Model

from restop_usb.connectors import SerialConnector,loop_serial
from restop_usb.models import UsbQueue , UsbQueueHub

from restop_usb.controllers import UsbQueueServerBase, ThreadedUsbQueueServer
from restop_usb.controllers import UsbQueueHubServerBase, ProcessUsbQueueHubServer
from restop_usb.client import UsbControllerClient
from restop_usb.client import QueueClient

from restop_usb.tools import start_usb_hub


LATENCE= 1

def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushall()
    Model.bind(database=db,namespace=None)
    return db


def get_queue():
    """


    :return:
    """

    name= '_loop'
    parameters = {"device": "loop://?logging=debug","bauds":9600}

    q= UsbQueue.create(name=name,parameters=parameters)

    q= UsbQueue.load(name)
    assert q.name == name
    assert q.device == "loop://?logging=debug"
    assert q.bauds == 9600
    return q

def get_hub():
    """


    :return:
    """
    parameters = {"dummy": "hello","bauds":115200}

    q= UsbQueueHub.create(parameters=parameters)

    q= UsbQueueHub.load()
    assert q.name == 'default'
    assert q.parameters['dummy'] == "hello"
    return q


def test_serial_connector():
    """

    :return:
    """
    c= SerialConnector('echo',device=loop_serial)
    c.open()
    c.write('hello\n')
    c.write('bye\n')
    #time.sleep(2)
    t0= time.time()
    line= c.readline()
    t1= time.time()
    print "duration=%d" % (t1-t0)
    assert line == "hello\n"
    line = c.readline()
    assert line == 'bye\n'

    c.write('\n')
    line= c.readline()
    line = c.readline()

    assert line is ''

    c.close()

    return




def test_queue_model():
    """

    :return:
    """
    db = get_new_db()

    q= get_queue()

    # name= '_loop'
    # parameters = {"device": "loop://?logging=debug","bauds":9600}
    #
    # q= UsbQueue.create(name=name,parameters=parameters)
    #
    # q= UsbQueue.load(name)
    # assert q.name == name
    # assert q.device == "loop://?logging=debug"
    # assert q.bauds == 9600

    assert q.is_empty('stdin')

    q.write('some data','stdin')
    assert q.is_empty('stdin') == False
    assert q.size('stdin') == 1

    data= q.readline('stdin')
    assert data== 'some data'

    assert q.is_empty('stdin') == True
    assert q.size('stdin') == 0

    return

def test_queue_hub_model():
    """

    :return:
    """
    db = get_new_db()


    #name= 'usbcontroller'

    q= get_hub()

    # q= UsbQueueHub(name=name)
    # q.save()
    #
    # q= UsbQueueHub.load(name)
    # assert q.name== name

    assert q.is_empty('stdin')

    q.write('some data','stdin')
    assert q.is_empty('stdin') == False
    assert q.size('stdin') == 1

    data= q.readline('stdin')
    assert data== 'some data'

    assert q.is_empty('stdin') == True
    assert q.size('stdin') == 0

    q1= UsbQueue.create(name='tv',  parameters={'device':'/dev/ttyUSB0', 'bauds':9600})
    assert q1.device == '/dev/ttyUSB0'
    assert q1.bauds == 9600
    q2 = UsbQueue.create(name='tv2',parameters={'device': '/dev/ttyUSB1'})
    assert q2.device == '/dev/ttyUSB1'
    assert q2.bauds == 115200

    q.add_queue(q1)
    q.add_queue(q2)

    q.del_queue(q1)

    return



def test_queue_server_base():
    """


    :return:
    """

    db = get_new_db()

    q= get_queue()

    # name= '_loop'
    # parameters= {"device":"loop://?logging=debug"}
    #
    # q= UsbQueue(name=name,parameters=parameters)
    # q.save()
    #
    # q= UsbQueue.load(name)
    # assert q.name== name

    server= UsbQueueServerBase(q)


    text= 'some data'
    server.write(text)
    server.write(text,channel_name='stdout')

    assert server.model.size('stdin') == 1
    assert server.model.size('stdout') == 1

    rc= server.readline()
    assert rc == text

    rc= server.readline(channel_name='stdin')
    assert rc == text

    assert server.model.size('stdin') == 0
    assert server.model.size('stdout') == 0


    server.write(text)
    assert server.model.size('stdin') == 1

    server.log.info("about to run once")

    server.open_connector()

    server.run_once()
    assert server.model.size('stdin') == 0
    server.run_once()

    assert server.model.size('stdout') == 1
    server.log.info("run once done")

    line= server.readline()
    assert server.model.size('stdout') == 0
    assert line == text


    server.write_connector("this is a line\n")
    server.write_connector("and another aline\n")
    time.sleep(2)

    lines= server.read_connector()
    assert lines.startswith("this")


    server.keep_alive(clear=True)
    server.close_connector()

    return



def test_threaded_queue_server():
    """


    :return:
    """

    db = get_new_db()

    q= get_queue()

    server= ThreadedUsbQueueServer(q)
    server.start()

    assert server.is_alive() == True


    text= 'some data'
    server.write(text)

    time.sleep(LATENCE)

    rc= server.readline()
    assert rc == text


    server.set_signal('abort')

    #server.exit()
    time.sleep(2)

    assert server.is_alive() == False

    return


def test_process_queue_hub_server():
    """


    :return:
    """

    db = get_new_db()

    hub= get_hub()

    #queue= get_queue()
    #agent_id= queue.get_hash_id()

    server= ProcessUsbQueueHubServer(hub)
    server.start()
    assert server.is_alive() == True

    # get a client to usb hub
    client = UsbControllerClient(log=server.log,redis_db=db)

    # create loop queue on server
    queue_id= client.create_queue_server('_loop',device="loop://?logging=debug")

    # start loop queue
    client_queue= client.start_queue_server('_loop')
    #client_queue= client.get_adapter_for_queue('_loop')

    text= 'hello\n'
    client_queue.write(text)
    time.sleep(LATENCE)
    line=client_queue.readline()
    assert line== text.strip('\n')

    # close client queue
    client_queue.exit()


    # ask hub to start the _loop server
    #r= client.start_queue_server('_loop')
    time.sleep(1)


    client.exit()
    #server.exit()

    time.sleep(1)

    assert server.is_alive() == False

    return


def test_queue_client():
    """

    :return:
    """
    db= get_new_db()

    # get a loop queue
    queue= get_queue()


    server= ThreadedUsbQueueServer(queue)
    server.start()

    agent_id= queue.get_hash_id()

    client= QueueClient(agent_id,redis_db=db)

    text= "hello there"
    client.write(text)

    client.log.info('text sended')
    time.sleep(LATENCE)
    line= client.readline()
    client.log.debug('readline')

    assert line == text

    #client.exit()

    client.exit()


    k= client.keep_alive()
    assert k == 'Done'

    return


def test_client_expect():
    """

    :return:
    """
    db= get_new_db()
    queue= get_queue()
    server= ThreadedUsbQueueServer(queue)
    server.start()

    agent_id= queue.get_hash_id()

    client= QueueClient(agent_id,redis_db=db)

    client.write("hello there\n")

    text= "this what im looking for"
    client.write(text)

    client.write('dont care of this line')

    r= client.expect(text,timeout=120)

    assert 'found' in r[-1]

    client.exit()


    k = client.keep_alive()
    assert k == 'Done'


def test_client_sync():
    """

    :return:
    """
    db= get_new_db()
    queue= get_queue()
    server= ThreadedUsbQueueServer(queue)
    server.start()

    agent_id= queue.get_hash_id()

    client= QueueClient(agent_id,redis_db=db)

    client.write("hello there\n")

    text= "this what im looking for"
    client.write(text)

    client.write('dont care of this line')

    r= client.sync(timeout=5)

    client.exit()


    k = client.keep_alive()
    assert k == 'Done'


def test_tools():

    db= get_new_db()

    data= {
        '_loop': {
                     'device': "loop://?logging=debug"
                 },
        '_mock': {
                     'device': "loop://?logging=debug"
                 },
        'tv': {
            'device': "/dev/ttyUSB0",
            'bauds': 115200
        },
        'stbplay': {
            'device': "/dev/ttyUSB1"
        }
    }

    hub= start_usb_hub(data)
    hub_id= hub.model.get_hash_id()

    hub_client= UsbControllerClient()

    q1= hub_client.start_queue_server('_loop')
    #q1= client.get_adapter_for_queue('_loop')

    text= 'hello world\nand\nbye\n'
    q1.write(text)
    time.sleep(LATENCE)
    line= q1.readline()
    assert line== 'hello world'
    line = q1.readline()
    assert line == 'and'
    line = q1.readline()
    assert line == 'bye'

    line = q1.readline()
    assert line is None



    text = 'hello world\n'
    q1.write(text,channel_name='stdin')
    q1.write(text,channel_name='stdout')
    q1.write(text,channel_name='stdlog')

    q1.clear()
    #hub_client.clear_queue('_loop')

    q1.exit()

    hub.exit()
    time.sleep(1)

    return


def test_double_queue_server():
    """
        check not 2 usb queue server coexist

    :return:
    """
    db= get_new_db()
    queue_model= get_queue()


    queue_server= ThreadedUsbQueueServer(queue_model)
    queue_server.start()

    time.sleep(1)

    #t0= queue_server.keep_alive()

    # start another server to the same queue
    queue_server2=  ThreadedUsbQueueServer(queue_model)

    # # position abort signal in case another process run
    # queue_server2.set_signal()
    # # leave time to eventual process to kill itself
    # time.sleep(2)
    # # clear the signal in case no process was running
    # queue_server2.get_signal()


    # start now
    queue_server2.start()


    queue_server2.exit()
    return



if __name__=="__main__":

    test_serial_connector()
    test_queue_model()
    test_queue_hub_model()

    test_queue_server_base()

    test_threaded_queue_server()

    test_process_queue_hub_server()
    #
    test_queue_client()
    #
    #
    test_client_expect()
    test_client_sync()

    test_double_queue_server()

    test_tools()
    print "Done"