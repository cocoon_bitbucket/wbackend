
import pytest

import sys
from yaml import load

from walrus import Database


sys.path.append('..')

from wbackend.model import Model


REDIS_HOST= 'localhost'
REDIS_PORT= "6379"
REDIS_DB= 0

PLATFORM= "./platform.yml"


@pytest.fixture
def redis_db():
    """
        return a walrus redis Database
    :return:
    """

    db = Database(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
    db.flushall()
    Model.bind(db)

    return db

