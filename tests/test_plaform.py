import pytest


from restop_platform.models import *


PLATFORM= './platform.yml'


@pytest.fixture
def platform_data():
    """
        load platform content from yml file

    :return:
    """
    platform_file = file(PLATFORM)
    platform_data = load(platform_file)

    return platform_data




if __name__=="__main__":

    from yaml import load
    import time

    import logging
    logging.basicConfig(level='DEBUG')

    # ...
    stream= file('platform.yml')
    data = load(stream)

    db = Database(host='localhost', port=6379, db=0)
    db.flushall()


    Model.bind(database=db,namespace=None)



    def check_platform():

        p= Platform.create(name='default',data=data)
        db.flushdb()

        p.setup()

        p1= Profile.query(Profile.name=='default')
        p1 = p1.next()

        d1= Device.query(Device.name=='tv').next()

        profile= d1.get_profile()


        site= Site.get(Site.name=='site_1_1')
        devices= site.get_devices()


        device=devices[0]

        site= device.get_site()

        e1= site.get_enterprise()

        sites= e1.get_sites()

        return



    def check_session():
        p = Platform.create(name='default', data=data)
        db.flushdb()

        p.setup()


        #members= ['tv','LB1','Mobby','Marylin','Fred','Fanny']
        members= ['sample_1', 'sample_2']
        session= Session.create(platform='default',members=members)
        #session.setup(members)

        session.logs.append('session created')


        token= session.token

        print token.owner
        print token.created
        print token.expired
        print token.has_expired()

        time.sleep(2)
        session.close()

        print session.created
        print session.finished

        token.revoke()



        return

    def check_apidoc():
        """

        :return:
        """

        p = Platform.create(name='default', data=data)
        db.flushdb()

        p.setup()

        collections = ["root", "oauth2", "files", "apidoc", "platforms", "phone_sessions", 'syprunner_agents',
                       'droydrunner_agents',
                       "livebox_agents", "tvbox_agents"]

        collection_operations = ['adb_devices', 'get_platform_configuration', 'ptf_get_user_data',
                                           'get_sip_address']

        operations = set()
        # add droydrunner keywords
        operations.update(
            ['click_by_index', 'native_drag_to', 'ui_object', 'dump', 'fling_backward_horizontally', 'get_object',
             'click_on_object', 'scroll_forward_vertically', 'clear_text', 'pinch_in', 'long_click_by_index',
             'pinch_out', 'register_press_watcher', 'swipe_right', 'native_gesture', 'fling_forward_horizontally',
             'execute_adb_shell_command', 'set_text', 'scroll_to_end_horizontally', 'click_linear_by_text',
             'scroll_to_horizontally', 'click', 'swipe_down', 'native_fling', 'open_notification',
             'scroll_to_horizontal', 'start_test_agent', 'execute_adb_command', 'native_operation', 'swipe_left',
             'scroll_backward_vertically', 'flash', 'fling_forward_vertically', 'unfreeze_screen_rotation',
             'wait_for_object_exists', 'type', 'call', 'wait_until_object_gone', 'get_count', 'get_info_of_object',
             'scroll_to_vertically', 'scroll_to_vertical', 'set_screen_orientation', 'get_object_data',
             'scroll_to_beginning_vertically', 'get_device_info', 'screenshot', 'adb_devices', 'turn_off_screen',
             'click_tab_by_text', 'long_click', 'scroll_to_end_vertically', 'open_quick_settings',
             'freeze_screen_rotation', 'register_click_watcher', 'swipe_by_coordinates', 'drag_by_coordinates', 'press',
             'scroll_backward_horizontally', 'get_child', 'fling_backward_vertically', 'press_home',
             'scroll_to_beginning_horizontally', 'wait_until_gone', 'get_count_of_object', 'flatten_ui_objet',
             'wait_for_exists', 'list_all_watchers', 'native_scroll', 'click_at_coordinates', 'wait_idle',
             'turn_on_screen', 'set_object_text', 'get_info_by_index', 'remove_watchers', 'snapshot',
             'scroll_forward_horizontally', 'install', 'stop_test_agent', 'get_sibling', 'swipe_up', 'wait_update',
             'get_screen_orientation', 'uninstall'])
        # add stb keywords
        operations.update(
            ['serial_synch', 'serial_watch', 'serial_expect', 'send_key', 'stb_send_dump', 'stb_status',
                 'page_get_info_from_live_banner', 'menu_select', 'scheduler_start', 'scheduler_read',
                 'page_action_select', 'page_get_list', 'page_get_label_from_list_focus', 'page_find_in_list',
                 'stb_toolbox_list', 'stb_toolbox_select', 'scheduler_init', 'scheduler_sync', 'scheduler_close',
                 'stb_get_lineup,stb_pcb_cli,stb_info'
                 ])

        operations = list(operations)


        api= Apidoc.create(platform_name= 'default',
                           session_collection= 'sessions',
                           collections=collections,
                           collection_operations=collection_operations,
                           operations= operations
                           )

        apidoc= api.export()

        api.update({'operations':['new_operation'],'collection_operations':['new_collection_operation']})

        apidoc= api.export()
        assert 'new_operation' in apidoc['operations']
        return

    def check_agent():
        """

        """
        db = Database(host='localhost', port=6379, db=0)
        Model.bind(database=db, namespace=None)
        db.flushdb()

        alias= 'agent'
        agent= AgentModel.create(name=alias,parameters={},session=1)

        agent.logs.append('agent created')
        agent_id= agent.get_id()

        #agent=AgentModel.query(AgentModel.name==alias)

        agent= AgentModel.load(agent_id)
        assert agent._initial_log_index == 1

        agent.logs.append("this is a log")

        response= agent.make_response({})
        assert len(response['logs'])==1

        return


    #
    #
    #
    check_agent()
    check_apidoc()
    check_platform()
    check_session()

    print "Done."
