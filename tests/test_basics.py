
from walrus import Database
from walrus import *
from wbackend.model import Model



class RawItem(Model):

    collection= "raw_item"

    data = JSONField()


class Item(Model):

    collection= "item"

    name= TextField(index=True)


class User(Model):

     collection= 'user'
     #index_separator = ':'

     #database = db
     name = TextField(primary_key=True)
     dob = DateField(index=True)
     data = JSONField()


class Device(Model):

    collection= "device"
    namespace = 'ptf1'
    #database = db


    name = TextField(index=True)
    myset= ZSetField()

    params= JSONField()

    #data= JSONField()

class RQueue(Model):
    """

    """
    collection= "queue"
    #database = db

    name= TextField(index=True)

    stdin=  ListField()
    stdout= ListField()


class Session(Model):
    """

    """
    collection = "session"

    platform= TextField(index=True,default='default')
    start = DateField(index=True,default=datetime.datetime.now())
    end = DateField()

    members= ListField()


    data = JSONField()
    log= ListField()


    def get_platform(self):
        return Platform.load(Session.platform)


class Agent(Model):
    """

    """
    name= TextField(index=True)
    session= TextField(index=True)
    device= TextField()

    params= JSONField()


    def get_session(self):
        """

        :return:
        """
        session= Session.load(Agent.session)
        return session

    def get_device(self):
        """

        :return:
        """
        device= Device.load(Agent.device)
        return device



class Platform(Model):
    """

    """
    name= TextField(index=True,default='default')
    data= JSONField()


#
#
#

def test_select_plugin(redis_db):
    """

    :param redis_db:
    :return:
    """

    item_class = Model.select_plugin('item')
    assert item_class is Item


def test_raw_item(redis_db):
    """

    :param reddis_db:
    :return:
    """
    i = RawItem.create(data={'name': 'wild'})
    assert isinstance(i,RawItem)
    assert i._id == 1
    assert i._data== {'_id': 1, 'data': {'name': 'wild'}}



def test_item(redis_db):
    """

    :param reddis_db:
    :return:
    """
    i = Item.create(name= 'item')
    assert isinstance(i,Item)
    assert i._id == 1
    assert i._data== {'_id': 1, 'name': 'item'}


def test_item_with_wild_field(redis_db):
    """

    :param reddis_db:
    :return:
    """
    i = Item.create(name= 'item',wild='wild')
    assert isinstance(i,Item)

    i= Item.load('1')
    assert i._id == 1
    assert i._data== {'_id': 1, 'name': 'item','wild':'wild'}

def test_item_by_name(redis_db):
    """

    :param reddis_db:
    :return:
    """
    i = Item.create(name= 'my_name')
    assert i._id == 1

    i= Item.get(Item.name=='my_name')
    assert i._id ==1
    assert i._data== {'_id': 1, 'name': 'my_name'}


def test_user(redis_db):
    """

    :param reddis_db:
    :return:
    """
    i = User.create(name= 'Alice',dob=datetime.datetime.now())
    assert isinstance(i,User)
    assert i.get_hash_id() == 'user:id:Alice'

    i= User.get(User.name=='Alice')
    assert i.get_id() == "Alice"
    assert i.name == 'Alice'


if __name__=="__main__":

    # # binds models to database
    # for cl in Model.plugins:
    #     cl.database= db

    db = Database(host='localhost', port=6379, db=0)
    db.flushall()


    Model.bind(database=db,namespace=None)

    RawItem.create(data= {'name':'wild'})
    RawItem.create(data= {'name':'wild2'})

    i= Item.create(name='item',wild='wild')





    u=User.create(name='Charlie', dob=datetime.date(1983, 1, 1))
    #<User: Charlie>

    d=Device.create(name='tv',other='wild',params= {'p1':'p1','p2':'p2'})
    d.myset.add('hello',0)

    d2= Device.load('1')

    d2.params={'p0':'p0','p2':'p2'}
    d2.save()

    d3= Device.load('1')

    names_dobs = [
         ('Huey', datetime.date(2011, 6, 1)),
         ('Zaizee', datetime.date(2012, 5, 1)),
         ('Mickey', datetime.date(2007, 8, 1)),
    ]


    User_model= Model.select_plugin('user')

    for name, dob in names_dobs:
         User_model.create(name=name, dob=dob, data={'bidon':"wild"})


    q= RQueue.create( name='queue_1')
    q.save()

    q.stdin.append('hello')

    q1= RQueue.query(RQueue.name=='queue_1')
    q1=q1.next()

    print 'Done'
