
# dashboard keys for keep_alive , pid ,status
dashboard_keep_alive= "dashboard:keepalive"
dashboard_pid= "dashboard:pid"
dashboard_status= "dashboard:status"


exit_command= "exit\n"


# clone of function restop_backend.models.get_queue_key
def get_queue_key(collection, item, queue_name):
    """
        collection:container:stdin:collection:id:1

    :param collection:
    :param item:
    :return:
    """
    return "%s:container:%s:%s:id:%s" % (collection, queue_name, collection, str(item))


class QueueCommon(object):
    """

        convention for adapters queue names and dashboard


    """

    @classmethod
    def queue_key_from_item(cls,collection,item,queue_name):
        """
            collection:container:stdin:collection:id:1

        :param collection:
        :param item:
        :return:
        """
        return "%s:container:%s:%s:id:%s" % (collection, queue_name,collection, str(item))


    @classmethod
    def queue_key_from_item_id(cls, item_id,queue_name):
        """

        :param item_idro: string of form  collection:id:item
        :return:
        """
        collection, id, item = item_id.split(':', 2)
        assert id == 'id', "not a valid item_id"
        return cls.queue_key(collection,item,queue_name)

