
__author__ = 'cocoon'
"""


    adapter adapt a process to receive command lines from a redis queue and relay output to another redis queue


    # create an adapter
    a = ThreadedAdapter($agent_id, "python term.py")
    a.start()

    # use adapter
    d= ClientAdapter($agent_id)
    d.send('cmd\n')
    response= d.read()
    d.exit()

    note: you need a redis-server

    read commands from redis queue /agents/$agent_id/stdin

        and write it to term stdin


    read output from term stdout

        and write it to redis queue /agents/$agent_id/stout



"""
import re
import sys
import fcntl
import os
import time
import subprocess
import threading
import multiprocessing

import redis
from walrus import Hash,List
from wbackend.logger import SimpleLogger as Logger
from restop_queues.models import DASHBOARD_KEEP_ALIVE, DASHBOARD_PID, DASHBOARD_STATUS

from restop_adapters import get_queue_key
from restop_adapters import exit_command

import logging

default_logger= logging.getLogger(__name__)


class Adapter(object):
    """
        adapt a process to be driven via redis queues

        launch a process ( with command line )


        run:
            read lines from queue <stdin> and write it to process stdin

            read lines from process and write to <stdout> queue


    """
    #cmd_exit= 'exit\n'
    cmd_exit= exit_command

    #queue_index= ['stdin','stdout']

    def __init__(self,agent_id,command_line,shell=True,log=None,redis_db=None,**redis_kwargs):
        """

        :param agent_id: string   <collection>:id:<item>  like samples:1 samples:id:1
        :param command_line:
        :return:
        """
        self.agent_id = agent_id
        assert ':' in agent_id, "queue agent id must be <collection>:id:<item>, got %s" % agent_id
        self.collection, dummy, self.item = self.agent_id.split(':', 2)

        self.database= redis_db or redis.Redis(** redis_kwargs)
        self.redis_db=self.database
        self.log= log or Logger(self.queue_key('logs'),self.database)


        self._stdin= List(self.database,self.queue_key('stdin'))
        self._stdout= List(self.database,self.queue_key('stdout'))

        self._keep_alive= Hash(self.database,DASHBOARD_KEEP_ALIVE)
        self._status= Hash(self.database, DASHBOARD_STATUS)
        self._pid= Hash(self.database, DASHBOARD_PID)

        self.redis_kwargs= redis_kwargs


        self.command_line= command_line
        self.shell= shell

        self.proc= None
        self.Terminated= False
        self._logs=[]

    def queue_key(self,name):
        """
            rqueues:container:stdin:rqueues:id:1

        :param name: string (short name of queue : ( stdin , stdout)
        :return:
        """
        return get_queue_key(self.collection, self.item, name)


    def agents_status(self):
        """
            list status of  all declared agents

        :return:
        """
        return self.database.hgetall(DASHBOARD_KEEP_ALIVE)


    #
    # interface with process
    #

    def run_process(self,fullcmd=None,shell=None):
        """
            launch the slave process
        """
        fullcmd= fullcmd or self.command_line
        shell = shell or self.shell
        #self.trace("Popen " + fullcmd  )
        self.proc = subprocess.Popen(fullcmd, shell=shell, bufsize=0, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                     universal_newlines=False)
        if self.proc:
            #d= Hash(self.database,dashboard_pid)
            self._pid[self.agent_id]= self.proc.pid
        time.sleep(1)
        self.started = self.proc.poll()


        return self.started

    def is_running(self):
        """
            return the subprocess status

        """
        if self.proc is None:
            #  proc has not been started
            return False
        status = self.proc.poll()
        if status == None:
            # process is still running
            return True
        else:
            # process has returned
            self._status = status
            return False

    def send(self, cmd):
        """
            send command to process

        :param cmd:
        :return:
        """
        #self.trace("send " + cmd)
        if not cmd.endswith('\n'):
            cmd += '\n'

        self._write_stdin(cmd)
        #self.proc.stdin.writelines(cmd)
        #self.proc.stdin.flush()

    def _write_stdin(self,cmd):
        """
            write to process stdin

        :param cmd: str with nl
        :return:
        """
        self.proc.stdin.writelines(cmd)
        self.proc.stdin.flush()


    def read(self,wait=False,log=True):
        """
            read output from process

        """
        line= self._read_stdout(wait=wait)
        #if log:
        #    self.log(line)
        return line

    def _read_stdout(self,wait=False):
        """
            red from process stdout
        :param wait:
        :return:
        """
        if wait:
            line = self.proc.stdout.readline()
        else:
            output= self.proc.stdout
            fd= output.fileno()
            fl = fcntl.fcntl(fd, fcntl.F_GETFL)
            fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
            try:
                return output.read()
            except:
                return ""
        return line


    # def shutdown_process(self):
    #     """
    #         kill slave process
    #     """
    #     if sys.hexversion >= 0x02060000:
    #         self.proc.terminate()
    #     else:
    #         self.proc.wait()
    #     self._pid[self.agent_id]= 0
    #     return


    def shutdown_process(self):
        """
            kill slave process
        """
        if sys.hexversion >= 0x02060000:
            try:
                self.proc.terminate()
            except OSError as  e:
                # [Errno 3] No such process
                pass

        else:
            self.proc.wait()
        # reset pid
        self._pid[self.agent_id]= 0
        return



    #
    #
    #

    def transfer_in(self):
        """

            transfer redis stdin input to slave stdin ( if any )

        :return:
        """
        # read redis queue in
        #incoming_cmd= self.q_in.get(timeout= 1)

        size= len(self._stdin)
        if size > 0:
            incoming_cmd= self._stdin.popleft()
            #incoming_cmd= self._stdin[0]
            #del self._stdin[0]
            if incoming_cmd == self.cmd_exit:
                self.log.debug('adapter for %s received exit command' % self.agent_id)
                self.Terminated = True
            else:
                self.send(incoming_cmd)

        # incoming_cmd= self.queues.get('stdin')
        # incoming_cmd = self._stdin.get()
        # if incoming_cmd:
        #     if incoming_cmd == self.cmd_exit:
        #         # exit condition, stop slave process and exit main loop
        #         #self.shutdown()
        #         self.Terminated=True
        #     else:
        #         self.send(incoming_cmd)


    def transfer_out(self,group=False):
        """

            transfer slave stdout input to redis stdout ( if any )

        :return:
        """
        # read process output
        count= 0
        slave_out= self.read()
        if slave_out:
            #self.q_out.put(slave_out)
            if group == True:
                # write to queue as block
                #self.queues.put('stdout',slave_out)
                self._stdout.append(slave_out)
            else:
                # write each line to queue
                lines= slave_out.split("\n")
                count=len(lines)
                if count > 1:
                    del lines[-1]
                    count= count -1
                for line in lines:
                    # add line to queue stdout
                    self._stdout.append("%s\n" % line)
                    #self.queues.put('stdout',"%s\n" % line)
        return count


    def reset_queue(self,queue_name):
        """
            empty queue
        :param queue: redis queue, like self.q_in or self.q_out
        :return:
        """
        queue= List(self.database,self.queue_key(queue_name))
        while True:
            if len(queue) == 0:
                break
            dummy= queue[0]
            del queue[0]


    def keep_alive(self,clear=False):
        """

            write time to /agents[agent_id]
        :return:
        """
        if clear:
            # clear keep alive for this agent
            #self.db.hset( self.key_prefix,self.agent_id,None)
            self._keep_alive[self.agent_id] = None
        else:
            # set keep alive to current time for this agent
            #self.db.hset( self.key_prefix,self.agent_id,time.time())
            self._keep_alive[self.agent_id]=time.time()

    def get_keep_alive(self):
        """

        :return:
        """
        #return self.db.hget(self.key_prefix,self.agent_id)
        return  self._keep_alive[self.agent_id]

    def run_once(self):
        """

        :return:
        """
        self.keep_alive()

        # transfer slave stdout input to redis <stdout> ( if any )
        self.transfer_out()
        #  transfer redis <stdin> input to slave stdin ( if any )
        self.transfer_in()


    def run(self):
        """
            main loop

            read from /agents/?/stdin
            if not empty :
                send to slave stdin

            read from slave output
                if not empty :
                    push to /agents/?/stdout

        :return:
        """
        #self.trace("starting process for agent: %s" % self.agent_id)

        # reset queues
        self.keep_alive()


        #for queue in self._queues.values():
        self.reset_queue('stdin')
        self.reset_queue('stdout')

        # launch slave process
        self.run_process()
        self.Terminated= False

        while not self.Terminated:
            # infinite loop  till 'exit' received via command line
            self.run_once()

        # kill slave process and reset keep_alive
        self.shutdown_process()
        self.keep_alive(clear=True)
        sys.exit(0)


    def set_status(self, status):
        """
            set the agent status in the redis store

        :param status:
        :return:
        """
        self._status[self.agent_id]= status


    def get_status(self):
        """
            get the agent status in the redis store

        :param status:
        :return:
        """
        return self._status[self.agent_id]


class ThreadedAdapter(Adapter,threading.Thread):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        Adapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        self.setDaemon(True)


class ProcessAdapter(Adapter,multiprocessing.Process):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        multiprocessing.Process.__init__(self)
        Adapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        #self.setDaemon(True)







