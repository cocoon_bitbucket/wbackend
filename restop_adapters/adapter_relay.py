
from walrus import List
from adapter import Adapter ,get_queue_key




class RelayAdapter(Adapter):
    """
        relay stdin to relay.stdin
        and relay.stdout to stdout


    """
    def __init__(self,agent_id,target_id,log=None,redis_db=None):
        super(RelayAdapter,self).__init__(agent_id,'',log=log,redis_db=redis_db)

        # target queue id eg: mock:id:tvbox
        self.target_id= target_id
        self.target_collection,_dummy,self.target_item= self.target_id.split(':')


    def run_process(self,fullcmd=None,shell=None):
        """
            launch the slave process
        """
        self._pid[self.agent_id]= -1
        self.started = None
        return self.started

    def is_running(self):
        """
            return the subprocess status

        """
        return True

    def _write_stdin(self,cmd):
        """
            write to target stdin

        :param cmd: str with nl
        :return:
        """
        queue = List(self.database, get_queue_key(self.target_collection,self.target_item,'stdin'))
        queue.append(cmd)
        pass

    def _read_stdout(self,wait=False):
        """
            red from process stdout
        :param wait:
        :return:
        """
        value= None
        queue = List(self.database, get_queue_key(self.target_collection,self.target_item,'stdout'))
        if len(queue):
            value = queue.popleft()
        return value
        

    def shutdown_process(self):
        """
            kill slave process
        """
        # reset pid
        self._pid[self.agent_id]= 0
        return


