import threading
import time
import multiprocessing
import serial
from adapter import Adapter
from mock_adapter import MockProcess


import logging
logger= logging.getLogger(__name__)

loop_serial= "loop://?logging=debug"
mock_serial= "mock://?logging=debug"

class SerialPortAdapter(Adapter):
    """


        a loop to manage a serial port


        run:
            read lines from queue <stdin> and write it to serial_port

            read lines from serial_ports and write to <stdout> queue


    """

    def run_process(self,fullcmd=None,shell=None):
        """
            check serial port is available

        :fullcmd :  a string with coma separated elements
            device/baud/parity stop  eg /dev/usb_serial,115200,8,N,1

        """
        fullcmd= fullcmd or self.command_line
        assert fullcmd is not None
        if "," in fullcmd:
            device,parameters= fullcmd.split(",",1)
        else:
            device = fullcmd
            parameters=""

        bauds=115200
        # create serial port
        if device.startswith('loop://'):
            # dummy loop serial
            self.log.debug('serial server: start pyserial loop(%s)' % (device))
            self.proc=serial.serial_for_url(device)
        elif device.startswith('mock://'):
            # dummy mock serial
            self.log.debug('serial server: start mock adapter(%s)' % (device))
            self.proc=MockProcess(device[7:],redis_db=self.database)
        else:
            self.log.debug('serial server: start pyserial (%s|%s)' % (device,bauds))
            self.proc=serial.Serial(
                port=device,baudrate=bauds,bytesize=serial.EIGHTBITS,parity= serial.PARITY_NONE, stopbits=1,
                timeout=1
            )

        self.proc._timeout=1
        #self.proc.writeTimeout = 2

        #self.proc.close()
        #self.proc.open()

        self.started = True
        return self.started

    def is_running(self):
        """
            return the subprocess status

        """
        return True

    def send(self, cmd):
        """
            send command to serial port

        :param cmd:
        :return:
        """
        #self.trace("send " + cmd)
        if not cmd.endswith('\n'):
            cmd += '\n'

        self._write_stdin(cmd)


    def _write_stdin(self,cmd):
        """
            write to serial port

        :param cmd: str with nl
        :return:
        """
        self.proc.write(cmd)



    def read(self,wait=False,log=True):
        """
            read output from serial port

        """
        line= self._read_stdout(wait=wait)
        #if log:
        #    self.log.debug(line)
        return line

    def _read_stdout(self,wait=False):
        """
            read from serial port
        :param wait:
        :return:
        """
        #line = self.proc.readline()
        #return line
        try:
            lines= self.proc.readlines()
            #lines = self.proc.readline()
        except Exception,e:
            self.log.error('serial server: readlines failed', exc_info=True)
            raise e
        lines= self.hook_on_readlines(lines)
        return "".join(lines)

    def hook_on_readlines(self,lines):
        """

            called by _read_stdout , a hook to modify lines read from serial port

        :param lines: list of strings
        :return:
        """
        return lines

    def transfer_in(self):
        """

            transfer redis stdin input to slave stdin ( if any )

        :return:
        """
        # read redis queue in
        #incoming_cmd= self.q_in.get(timeout= 1)

        size= len(self._stdin)
        if size > 0:
            incoming_cmd= self._stdin.popleft()
            #incoming_cmd= self._stdin[0]
            #del self._stdin[0]
            if incoming_cmd == self.cmd_exit:
                self.log.debug('serial adapter for %s received exit command' % self.agent_id)
                # stop myself
                self.Terminated = True
                time.sleep(1)
                # stop the serial handler
                self.shutdown_process()

            else:
                # send command
                self.send(incoming_cmd)

    def shutdown_process(self):
        """
            kill slave process
        """
        self.log.debug('shutdown serial adapter for %s' % str(self.proc))
        if self.proc:
            self.proc.close()
            del self.proc
            self.proc=None
        return



class SerialportThreadedAdapter(SerialPortAdapter,threading.Thread):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        Adapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        self.setDaemon(True)


class SerialPortProcessAdapter(SerialPortAdapter,multiprocessing.Process):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        multiprocessing.Process.__init__(self)
        Adapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        #self.setDaemon(True)






# if __name__=='__main__':
#
#
#     from restop_adapters.client  import ClientAdapter
#     cmd= "/dev/cu.usbserial-FTVE89ZND,115200,8,N,1"
#     cmd= fake_serial
#
#     logging.basicConfig(level=logging.DEBUG)
#
#     def test_raw():
#
#         sp= SerialPortAdapter("tv",command_line=None)
#         sp.run_process(cmd)
#         n= 10
#         while n > 0:
#             sp.run_once()
#             n -= 1
#         return
#
#     def test_thread():
#
#         sp= SerialportThreadedAdapter("tvbox:id:1",command_line=None)
#         sp.run_process(cmd)
#
#         cli = ClientAdapter("tvbox:id:1")
#
#         n= 1000
#         while n > 0:
#
#             line= cli.read()
#             print line
#             n -=1
#
#         cli.exit()
#
#     def test_process():
#
#         sp= SerialPortProcessAdapter("tvbox:id:1",command_line=None,log=logger)
#         sp.run_process(cmd)
#
#
#         cli = ClientAdapter("tvbox:id:1",log=logger)
#
#         # inject some lines in usb
#         sp._write_stdin("hello there\n")
#
#
#         n= 10
#         while n > 0:
#
#             line= cli.read()
#             print line
#             n -= 1
#
#         cli.exit()
#
#
#
#     #test_thread()
#     test_process()
#
# print 'Done'