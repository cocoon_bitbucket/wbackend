"""

    an adapter to run python code


"""



import sys
import ast
import time
#import subprocess
import threading
import multiprocessing

from cStringIO import StringIO


from walrus import Database
from adapter import Adapter



class ExecError(Exception):
    """Thrown when supplied code raises exception during execution."""

    def __init__(self, stdout, stderr, original_error):
        super(ExecError, self).__init__()
        self.stdout = stdout
        self.stderr = stderr
        self.original_error = original_error


def execute_code( code ):
    """

    :param code: string : python source code
    :return:
    """
    tree = ast.parse(code)

    # stdout = StringIO()
    # stderr = StringIO()
    # prev_stdout = sys.stdout
    # prev_stderr = sys.stderr
    # sys.stdout = stdout
    # sys.stderr = stderr


    try:
        exec (compile(tree, filename="<ast>", mode="exec")) in dict()
    except Exception as e:
        raise
        #raise ExecError(stdout.getvalue(), stderr.getvalue(), e)

    finally:
        pass
        #sys.stdout = prev_stdout
        #sys.stderr = prev_stderr



def execute_code_in_thread(code):
    """

    :param code:
    :return:
    """
    t= threading.Thread(target=execute_code,args=(code,))
    t.start()
    return t



class PythonAdapter(Adapter):
    """


        a loop to manage a batch


        run:
            read lines from queue <stdin> and write it to serial_port

            read lines from serial_ports and write to <stdout> queue


    """

    def __init__(self, agent_id, command_line, shell=True, log=None, redis_db=None, **redis_kwargs):
        """

        :param agent_id: string   <collection>:id:<item>  like samples:1 samples:id:1
        :param command_line:
        :return:
        """
        super(PythonAdapter,self).__init__(agent_id,command_line=command_line,redis_db=redis_db)
        return

    #
    # interface with process
    #

        #
        # interface with process
        #

    def run_process(self, fullcmd=None, shell=None):
        """
            launch the slave process
        """

        # get the python source code
        code= fullcmd or self.command_line

        # execute it in a thread
        try:
            self.proc= execute_code_in_thread(code)

            self._pid[self.agent_id]= self.proc.name

            self.started = self.proc.is_alive()
            self.log.debug("adapter: child thread started= %s" % self.started)

        except Exception ,e:
            # failed to compile or exec
            self.log.error('failed to run\n%s' % repr(e))
            # kill ourselve
            self.Terminated= True


        return self.started

    def is_running(self):
        """
            return the subprocess status

        """
        if self.proc:
            return self.proc.is_alive()
        else:
            return False


    def shutdown_process(self):
        """
            kill slave process
        """

        return

    def keep_alive(self,clear=False):
        """

            write time to /agents[agent_id]
        :return:
        """
        # get process status
        status= None
        if self.is_running():
            self._status[self.agent_id] = 'running'
            self._keep_alive[self.agent_id] = time.time()
        else:
            # the child thread is not running
            self._status[self.agent_id] = 'not running'
            self._keep_alive[self.agent_id] = None
            self._pid[self.agent_id]= None
            self.log.debug('adapter: child thread has finished')
            # kill ourself
            self.Terminated= True

        return


    def _read_stdout(self,wait=False):
        """
            read from child thread  stdout
        :param wait:
        :return:
        """
        line= None
        # if wait:
        #     line = self.proc.stdout.readline()
        # else:
        #     output= self.proc.stdout
        #     fd= output.fileno()
        #     fl = fcntl.fcntl(fd, fcntl.F_GETFL)
        #     fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
        #     try:
        #         return output.read()
        #     except:
        #         return ""
        return line

    def _write_stdin(self,cmd):
        """
            write to child thread stdin

        :param cmd: str with nl
        :return:
        """
        # self.proc.stdin.writelines(cmd)
        # self.proc.stdin.flush()

    def send(self, cmd):
        """
            send command to process

        :param cmd:
        :return:
        """
        # self.trace("send " + cmd)
        if not cmd.endswith('\n'):
            cmd += '\n'

        self._write_stdin(cmd)
        # self.proc.stdin.writelines(cmd)
        # self.proc.stdin.flush()

    def run(self):
        """
            main loop

            read from /agents/?/stdin
            if not empty :
                send to slave stdin

            read from slave output
                if not empty :
                    push to /agents/?/stdout

        :return:
        """
        self.log.debug('adapter: starting ')
        # reset queues
        #self.keep_alive()

        # for queue in self._queues.values():
        self.reset_queue('stdin')
        self.reset_queue('stdout')

        # launch slave process
        self.run_process()
        self.Terminated = False

        while not self.Terminated:
            # infinite loop  till 'exit' received via command line
            self.run_once()

        # kill slave process and reset keep_alive
        self.shutdown_process()
        self.keep_alive(clear=True)
        self.log.debug('adapter closed')
        sys.exit(0)




class PythonThreadedAdapter(PythonAdapter, threading.Thread):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        Adapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        self.setDaemon(True)


class PythonProcessAdapter(PythonAdapter, multiprocessing.Process):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        multiprocessing.Process.__init__(self)
        Adapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        #self.setDaemon(True)






if __name__=='__main__':


    from client import ClientAdapter
    cmd=  ""

    code = '''\
import time

for i in xrange(0,60):
  print "."
  time.sleep(1)
print "child Done"
'''

    agent_id= 'tasks:id:1'


    def test_execute_code():
        """

        :return:
        """

        # direct execution
        #execute_code(code)

        # execute code in a thread
        t= execute_code_in_thread(code)
        #time.sleep(2)
        t.join()

        return



    def test_raw():

        db = Database(host='localhost', port=6379, db=0)
        db.flushall()

        sp= PythonAdapter(agent_id, command_line=code)
        sp.run_process(cmd)
        n= 10
        while n > 0:
            sp.run_once()
            n -= 1

        sp.shutdown_process()
        return

    def test_thread():

        sp= PythonThreadedAdapter("tv:id:1", command_line=code)
        sp.start()
        #sp.run_process(cmd)

        cli = ClientAdapter("tv:id:1")

        n= 1000
        while n > 0:

            line= cli.read()
            print line
            n -=1

        cli.exit()

    def test_process():

        sp= PythonProcessAdapter("tv", command_line=None)
        sp.run_process(cmd)


        cli = ClientAdapter("tv")

        # n= 10
        # while n > 0:
        #
        #     line= cli.read()
        #     print line
        #     n -= 1
        #
        # cli.exit()


    #test_execute_code()
    #test_raw()
    test_thread()
    #test_process()

print 'Done'