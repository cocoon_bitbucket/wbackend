import time
import threading
from adapter import Adapter
from asyncproc import Process



class AsyncAdapter(Adapter):
    """

        a process adapter based on asyncproc

    """

    def run_process(self,fullcmd=None,shell=None):
        """
            launch the slave process
        """
        fullcmd= fullcmd or self.command_line
        shell = shell or self.shell
        #self.trace("Popen " + fullcmd  )

        #self.proc = subprocess.Popen(fullcmd, shell=shell, bufsize=0, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
        #                             universal_newlines=False)
        self.proc = Process(fullcmd, shell=shell, bufsize=0, universal_newlines=False)

        if self.proc:
            #d= Hash(self.database,dashboard_pid)
            self._pid[self.agent_id]= self.proc._Process__process.pid
        time.sleep(1)
        self.started = self.proc._Process__process.poll()

        return self.started

    def _write_stdin(self,cmd):
        """
            write to process stdin

        :param cmd: str with nl
        :return:
        """
        self.proc.write(cmd)
        return

    def _read_stdout(self,wait=False):
        """
            red from process stdout
        :param wait:
        :return:
        """
        line= self.proc.read()
        return line

    def shutdown_process(self):
        """
            kill slave process
        """
        self.proc.terminate()
        # reset pid
        self._pid[self.agent_id] = 0
        return



class ThreadedAsyncAdapter(AsyncAdapter,threading.Thread):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        AsyncAdapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        self.setDaemon(True)