#!/usr/bin/env python

"""

    pseudo opiqs command


"""
__author__ = 'cocoon'

import os
import sys
import time

prompt= ''


script= "read_tvdc.pig"
files= [ "output_1.csv" , "output_2.csv"]
result= "task.zip"



def make_output_1():
    """


    :return:
    """
    data= """\
this is
the output_1 result
"""
    with open(files[0],"w") as fh:
        fh.write(data)

def make_output_2():
    """


    :return:
    """
    data= """\
this is
the output_2 result
"""
    with open(files[1],"w") as fh:
        fh.write(data)




while 1:

    print "opiqs batch started in directory: %s" % os.getcwd()
    print "with command line: %s" % " ".join(sys.argv)


    # remove files
    for f in files:
        try:
            os.unlink(f)
        except:
            pass
    try:
        os.unlink(result)
    except:
        pass

    # check presence of script
    #assert os.stat(script)

    print "sleep 10"
    time.sleep(10)


    make_output_1()


    print "the show must go on"

    print "sleep 10"
    time.sleep(10)

    make_output_2()

    print "this is the end"

    break
