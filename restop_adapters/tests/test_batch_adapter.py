import time

# from wbackend.model import Model,Database
# from wbackend.models import RQueues

from walrus import Database
from restop_adapters.batch_adapter import BatchAdapter,BatchThreadedAdapter,BatchProcessAdapter
from restop_adapters.client import ClientAdapter



if __name__ == "__main__":

    cmd= "python fake_opiqs.py"


    def test_raw():

        db = Database(host='localhost', port=6379, db=0)
        db.flushall()

        #Model.bind(database=db, namespace=None)
        #rq = RQueues.create_for_item_id('task:id:1')

        sp = BatchAdapter("task:id:1", command_line=cmd)
        sp.run_process(cmd)
        n = 10
        while n > 0:
            sp.run_once()
            n -= 1
        return


    def test_thread():

        db = Database(host='localhost', port=6379, db=0)
        db.flushall()


        #Model.bind(database=db, namespace=None)
        #rq = RQueues.create_for_item_id('task:id:1')


        sp = BatchThreadedAdapter("task:id:1", command_line=cmd)
        sp.run_process(cmd)
        sp.start()
        time.sleep(2)

        cli = ClientAdapter("task:id:1")

        n = 100
        while n > 0:
            line = cli.read()
            print line
            n -= 1

        cli.exit()


    def test_process():

        db = Database(host='localhost', port=6379, db=0)
        db.flushall()

        #Model.bind(database=db, namespace=None)
        #rq = RQueues.create_for_item_id('task:id:1')


        sp = BatchProcessAdapter("task:id:1", command_line=cmd)
        sp.run_process(cmd)
        sp.start()
        time.sleep(2)

        cli = ClientAdapter("task:id:1")
        time.sleep(2)
        n = 10
        while n > 0:
            line = cli.read()
            print line
            n -= 1

        cli.exit()


    test_raw()
    test_thread()
    test_process()

    print 'Done'