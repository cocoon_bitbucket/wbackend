import time
import signal

from wbackend.model import Database
from restop_adapters.adapter import Adapter, ThreadedAdapter, ProcessAdapter

from restop_adapters.client import ClientAdapter
from restop_adapters.adapter_relay import RelayAdapter
from restop_adapters.mock_adapter import MockAdapter

def test_basic():
    """
        test without redis

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()

    agent_id= 'agent:id:1'
    mock_id= 'mock:1:tvbox'


    client= ClientAdapter(agent_id,redis_db=db)
    relay= RelayAdapter(agent_id, mock_id,redis_db=db)
    mock= MockAdapter(mock_id,'',redis_db=db)

    cmd= "ping"

    # client send a command
    client.send(cmd)

    # relay transfer it to target
    relay.transfer_in()

    # target read command
    command= mock.mock_read_stdin()
    assert command== "%s\n" % cmd
    # target answer pong
    mock.mock_write_stdout('pong')

    # relay transfer resposne
    relay.transfer_out()

    # client read response
    response= client.read()
    assert response== "%s\n" % cmd

    return


if __name__=="__main__":


    test_basic()

    print "Done"