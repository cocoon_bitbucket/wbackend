import time

#from wbackend.model import Model,Database
#from wbackend.models import RQueues

from walrus import Database

from restop_adapters.client  import ClientAdapter
from restop_adapters.mock_adapter import MockAdapter
from restop_adapters.serial_port_adapter import SerialPortAdapter,SerialportThreadedAdapter,SerialPortProcessAdapter


import logging
logger= logging.getLogger(__name__)

fake_serial= "loop://?logging=debug"
mock_serial= "mock://mock:id:serial"

cmd = fake_serial

logging.basicConfig(level=logging.DEBUG)


def test_raw():

    db = Database(host='localhost', port=6379, db=0)
    db.flushall()

    #Model.bind(database=db, namespace=None)
    #rq = RQueues.create_for_item_id('serial:id:1')

    sp = SerialPortAdapter("serial:id:1", command_line=None,log=logger)
    sp.run_process(cmd)

    # inject data

    r= sp.send("hello there\nhello again")
    n = 5
    while n > 0:
        sp.keep_alive()
        nblines= sp.transfer_out()
        n -= 1
    sp.shutdown_process()
    return


def test_thread():

    db = Database(host='localhost', port=6379, db=0)
    db.flushall()

    #Model.bind(database=db, namespace=None)
    #rq = RQueues.create_for_item_id('serial:id:1')

    sp = SerialportThreadedAdapter("serial:id:1", command_line=fake_serial,log=logger,host='localhost', port=6379, db=0)
    sp.run_process(cmd)
    sp.start()

    time.sleep(2)
    cli = ClientAdapter("serial:id:1")


    sp.send("hello there\n")
    time.sleep(3)

    line= None
    n = 10
    while n > 0:
        line = cli.read()
        print line
        if line:
            assert line == "hello there\n"
            break
        time.sleep(1)
        n -= 1
    assert line , "no line received"
    cli.exit()

    time.sleep(2)

def test_process():

    db = Database(host='localhost', port=6379, db=0)
    db.flushall()

    #Model.bind(database=db, namespace=None)
    #rq = RQueues.create_for_item_id('serial:id:1')

    sp = SerialPortProcessAdapter("serial:id:1", command_line=fake_serial, log=logger)
    sp.run_process(cmd)
    sp.start()

    time.sleep(2)
    cli = ClientAdapter("serial:id:1", log=logger)

    cli.send("hello there\n")
    time.sleep(5)
    # inject some lines in usb
    #sp._write_stdin("hello there\n")

    line= None
    n = 10
    while n > 0:
        line = cli.read()
        print line
        if line:
            assert line == "hello there\n"
            break
        time.sleep(1)
        n -= 1
    assert line , "no line received"
    cli.exit()

    time.sleep(2)



def test_mock_serial():

    db = Database(host='localhost', port=6379, db=0)
    db.flushall()

    #Model.bind(database=db, namespace=None)
    #rq = RQueues.create_for_item_id('serial:id:1')

    sp = SerialPortAdapter("serial:id:1", command_line=mock_serial,log=logger)
    sp.run_process()

    # inject data to mock serial

    r= sp.send("hello there\nhello again")
    n = 2
    while n > 0:
        sp.keep_alive()
        nblines= sp.transfer_out()
        assert nblines == 0
        n -= 1

    # mock server simulate a response
    mock= MockAdapter('mock:id:serial','',redis_db=db)
    mock.mock_read_stdin()
    mock.mock_write_stdout('OK')

    sp.keep_alive()
    nblines = sp.transfer_out()
    assert nblines > 0

    sp.shutdown_process()


    return


#
# test_raw()
# test_thread()
# test_process()

test_mock_serial()

print "Done"

