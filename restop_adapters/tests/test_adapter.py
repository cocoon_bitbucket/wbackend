__author__ = 'cocoon'

import time
import signal

from wbackend.model import Model,Database
from wbackend.models import RQueues
from restop_adapters.adapter import Adapter, ThreadedAdapter, ProcessAdapter
from restop_adapters.client import ClientAdapter


def test_basic():
    """
        test without redis

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()

    #Model.bind(database=db, namespace=None)
    #rq = RQueues.create_for_item_id('agent:id:1')

    p= Adapter("agent:id:1", "python term.py")

    started= p.run_process()
    assert started == None

    p.send("ping")
    time.sleep(3)

    r= p.read()
    print r
    assert r == 'pong\n'

    p.send('show')
    time.sleep(3)

    r=p.read()
    print r
    r=p.read()
    print r

    r=p.read()

    p.send('exit')
    time.sleep(3)


    p.shutdown_process()

    time.sleep(3)

    return

def test_basic_command_failed():
    """
        test without redis

    :return:
    """

    p= Adapter("agent:id:1", "python nowhere.py")

    started= p.run_process()

    assert started== 2
    # python: can't open file 'nowhere.py': [Errno 2] No such file or directory

    return



def test_with_redis():
    """

        NEED a redis server

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db, namespace=None)

    rq = RQueues.create_for_item_id('agent:id:1')

    p= Adapter("agent:id:1", "python term.py",host='localhost', port=6379, db=0)

    #p= Adapter("agent:id:1", "python term.py")

    started= p.run_process()
    assert started is None

    #p.reset_queue(p.queue('stdin'))
    #p.reset_queue(p.queue('stdout'))

    r= rq.put('stdin',"ping\n")
    #assert r == 1
    p.transfer_out()
    p.transfer_in()
    p.keep_alive()

    print p.get_keep_alive()

    p.transfer_out()
    p.transfer_in()
    p.keep_alive()

    print p.get_keep_alive()

    p.shutdown_process()

    p.get_keep_alive()

    agents= p.agents_status()


    p.keep_alive(clear=True)


    agents = p.agents_status()

    return

def test_redis_commands():

    p= Adapter("agent:id:1", "python term.py")

    agents= p.agents_status()

    print agents
    return


def test_ThreadedAdapter():

    p= ThreadedAdapter('agent:id:1',"python term.py")


    #p.run_process()

    p.start()
    time.sleep(2)

    # print("send ping")
    # p.q_in.put("ping\n")
    # time.sleep(2)
    #
    # response= p.q_out.get(timeout=1)
    # #response= p.q_out.get_nowait()
    # print "response is %s" % response
    #
    # print("send exit")
    # p.q_in.put("exit\n")
    # time.sleep(2)
    #
    # response= p.q_out.get(timeout=1)
    # #response= p.q_out.get_nowait()
    # print "response is %s" % response


    client= ClientAdapter('agent:id:1')

    print "sending ping"
    client.send('ping\n')

    time.sleep(1)
    response= client.read()
    print "response is %s" % response
    assert response == 'pong\n'



    print "send exit"
    client.exit()
    # client.send('exit\n')
    # #time.sleep(1)

    response= client.read()
    print "response is %s" % response

    #p.stop()


    return

def test_ProcessAdapter():

    p= ProcessAdapter('agent:id:1',"python term.py")


    #p.run_process()

    p.start()
    time.sleep(2)

    # print("send ping")
    # p.q_in.put("ping\n")
    # time.sleep(2)
    #
    # response= p.q_out.get(timeout=1)
    # #response= p.q_out.get_nowait()
    # print "response is %s" % response
    #
    # print("send exit")
    # p.q_in.put("exit\n")
    # time.sleep(2)
    #
    # response= p.q_out.get(timeout=1)
    # #response= p.q_out.get_nowait()
    # print "response is %s" % response


    client= ClientAdapter('agent:id:1')

    print "sending ping"
    client.send('ping\n')

    time.sleep(2)
    response= client.read()
    print "response is %s" % response
    assert response == 'pong\n'


    print "send exit"
    client.exit()
    # client.send('exit\n')
    # #time.sleep(1)

    response= client.read()
    print "response is %s" % response

    #p.stop()


    return

def test_signal():


    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()

    p= ThreadedAdapter('agent:id:1',"python term.py")
    p.start()
    time.sleep(2)


    client= ClientAdapter('agent:id:1')

    print "sending ping"
    client.send('ping\n')

    time.sleep(1)
    response= client.read()
    print "response is %s" % response
    assert response == 'pong\n'


    # send signal to process
    client.signal(signal.SIGUSR1)
    time.sleep(3)

    response = client.read()
    print "response is %s" % response
    assert response == 'Signal handler called with signal 30\n'

    print "send exit"
    client.exit()
    time.sleep(3)

    response= client.read()
    print "response is %s" % response

    #p.stop()


    return

#test_signal()

test_basic()
#test_basic_command_failed()
# test_with_redis()
# test_redis_commands()
# test_ThreadedAdapter()
# test_ProcessAdapter()


print

