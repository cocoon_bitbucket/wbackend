
import re
import time
import redis
import json
import os
import signal

from walrus import Database, List,Hash
from wbackend.logger import SimpleLogger as Logger

from restop_adapters import get_queue_key
from restop_adapters import exit_command
from restop_adapters import dashboard_keep_alive,dashboard_pid,dashboard_status

import logging

default_logger= logging.getLogger(__name__)


class ClientAdapter(object):
    """
        a client to send data to queues


        status

            created
            started

            terminated


    """
    #cmd_exit = 'exit\n'
    cmd_exit= exit_command
    #queue_index = ['stdin','stdout','cmd']

    def __init__(self,agent_id,log=None,redis_db=None,**redis_kwargs):
        """

        :param agent_id: string of form <collection>:id:<item> eg  agent:id:1
        :param key_prefix: hash key for keep_alive
        :param log:
        :param redis_kwargs:
        """
        self.agent_id= agent_id
        assert ':' in agent_id , "queue agent id must be <collection>:id:<item>, got %s" % agent_id
        self.collection,dummy,self.item= self.agent_id.split(':')

        if not redis_db:
            redis_db= redis.Redis(**redis_kwargs)
        self.database= redis_db
        # hash key for keep alive
        self._keep_alive=Hash(self.database,dashboard_keep_alive)
        self._status = Hash(self.database, dashboard_status)

        #self.key_prefix= dashboard_keep_alive
        if not log:
            log= Logger(self.queue_key('logs'),self.database)
        self.log=log

        self._logs=[]


    def queue_key(self,name):
        """
            rqueues:container:stdin:rqueues:id:1

        :param name: string (short name of queue : ( stdin , stdout)
        :return:
        """
        return get_queue_key(self.collection, self.item, name)


    def put(self,queue_name,value):
        """
        :param queue_name: string short name of the queue ( stdin/stdout)
        :return:
        """
        queue= List(self.database,self.queue_key(queue_name))
        queue.append(value)

    def get(self, queue_name, timeout=1):
        """
        :param queue_name: string short name of the queue ( stdin/stdout)
        :return:
        """
        queue = List(self.database, self.queue_key(queue_name))
        if len( queue ):
            value=queue.popleft()
            #value=queue[0]
            #del queue[0]
        else:
            value= None
        return value

    def send(self,cmd):
        """
            send command to redis queue <stdin>
        """
        return self.put('stdin',cmd)

    def read(self,timeout=1):
        """
            read command from redis queue
        """
        return self.get('stdout')


    def cmd(self,operation, arguments):
        """
            send a command to redis <cmd> queue

        :param operation:
        :param arguments:
        :return:
        """
        json_arguments= json.dumps(arguments)
        command= '%s %s\n' % (operation , json_arguments)
        self.put('cmd',command)


    def keep_alive(self):
        """
            return the timestamp of agent keep alive
        """
        return self._keep_alive[self.agent_id]
        #return self.db.hget(self.key_prefix,self.agent_id)

    ###
    def exit(self,wait=True):
        """
            send exit to agent adapter
        """
        rc= True
        self.send(self.cmd_exit)
        if wait:
            rc= self.wait_for_status('terminated')
        return rc


    def wait(self,counter=15):
        """
            wait until adapter is done (eg keep alive is None)
        """
        t1= 0
        t2=0
        while counter > 0:
            t2=t1
            t1 = self.keep_alive()
            if t1 is None:
                # OK adapter is done
                return True
            time.sleep(1)
            counter = counter -1
        # still alive
        if t1==t2:
            print("adapter is freezed, last timestamp: %s" % t1)
        else:
            print("adapter is still alive, last timestamp: %s" % t1)

        return False


    def expect(self,pattern='*' ,timeout= 3 , cancel_on=None, regex=True,**kwargs):
        """
            read agent input until pattern is match or timeout reached or one of cancel_on pattern matches

            returns a list: buffer

            The last entry of buffer shows the result

            pattern found: "================ found %s" % pattern
            timeout:       "================ timeout reached
            cancel:        "================ canceled %s" % cancel_on[index]

        :param item:
        :param data: dict   ( pattern="*" , timeout=3, cancel_on=[] )
        :param kwargs:   pattern,timeout=3,cancel_on=None
        :return:
        """

        trace_enabled= True

        pattern= pattern or '*'
        timeout= int(timeout) or 3
        cancel_on= cancel_on or []
        if isinstance(cancel_on,basestring):
            cancel_on= [cancel_on]

        # compute search pattern with ignore case
        if regex is False:
            pattern=re.escape(pattern)
        search_pattern= re.compile(pattern,re.I)
        # compute optional cancel patterns
        cancel_patterns= []
        for p in cancel_on:
            r= re.compile(p)
            cancel_patterns.append(r)

        buffer=[]
        buffer.append(self.trace("expect pattern= [%s]" % pattern))
        # take timestamp
        start_time = time.strftime("%X") + ".000"
        t0= time.time()
        limit= t0 + timeout
        t= t0
        while True:

            # check timeout
            t= time.time()
            if t > limit:
                buffer.append(self.trace("timeout reached"))
                break

            # read the line
            line = self.read()
            if line is None:
                time.sleep(0.4)
                continue
            line= line.decode('utf-8','ignore').encode('utf-8')
            buffer.append(line)

            # Search for expected text
            if search_pattern.search(line) != None:
                # we found the expected line , return it
                buffer.append(self.trace("found %s" % pattern))
                # end the read loop
                break

            # search for cancel patterns
            found_cancel = False
            if cancel_patterns:
                for index,search_cancel in enumerate(cancel_patterns):
                    if search_cancel.search(line):
                        # we found a cancel pattern
                        buffer.append(self.trace('canceled %s' % cancel_on[index] ))
                        found_cancel= True
                        break
            if found_cancel:
                break
        return buffer

    def watch(self, timeout=5):
        """
            watch serial log for a duration
        :param timeout:
        :return:
        """
        lines = self.expect('Never Catch This', timeout=timeout)
        # add lines to log
        self.log.debug(lines)
        return lines



    def trace(self, s):
        """

        :param s: string , message to trace
        :return:
        """
        now = time.time()
        fmt = "================ " + s + " ==================" + " [at t=%(now)03d]" % { 'now':now }
        return fmt


    def sync(self,timeout=10):
        """

        send an echo SYNCHRO on serial stdin and wait for SYNCHRO on stdout

        :param timeout:
        :return:
        """
        t0=time.time()

        self.send('\n')
        sync_message= "echo SYNCHRO TAG %s" % str(t0)
        self.send(sync_message)
        time.sleep(2)
        r= self.expect("^SYNCHRO TAG %s" % str(t0),timeout=timeout)
        return r

    def signal(self,sig=signal.SIGABRT):
        """
            send a signal to piloted process
        :param sig:
        :return:
        """
        pid= int(self.database.hget(dashboard_pid,self.agent_id))
        if pid:
            self.put('logs', "send signal %s to %s process" % (str(sig), self.agent_id))
            r= os.kill(pid, sig)
            time.sleep(2)
        else:
            self.put('logs', "cannot send signal %s to %s: pid is 0" % (str(sig), self.agent_id))

        return

    @property
    def status(self):
        """


        :return:
        """
        return self._status[self.agent_id]


    def wait_for_status(self,status,timeout=10):
        """

        :param status:
        :return:
        """
        t1= 0
        while timeout > 0:
            if self.status == status:
                # OK
                return True
            t1 = self.keep_alive()
            if t1 is None:
                # adapter has finished ( and not status)
                    return False
            # continue
            time.sleep(1)
            timeout = timeout -1
        # timeout
        return False