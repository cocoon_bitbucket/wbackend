import sys
import threading
import multiprocessing
import time
from telnetlib import Telnet
from adapter import Adapter



import logging
logger= logging.getLogger(__name__)


class TelnetBaseAdapter(Adapter):
    """


        a loop to manage a telnet connection


    """
    def __init__(self,agent_id,host="localhost",user=None,password=None,port=23,log=None,redis_db=None,**kwargs):
        """

        :param agent_id:
        :param command_line:
        :param shell:
        :param log:
        :param redis_db:
        :param redis_kwargs:
        """
        super(TelnetBaseAdapter,self).__init__(agent_id,command_line="",log=log,redis_db=redis_db)
        self.host= host
        self.port=port
        self.user=user
        self.password= password


    def run_process(self,fullcmd=None,shell=None):
        """
            launch the telnet client
        """
        self.proc=Telnet(self.host,port=self.port,timeout=5)

        if self.user:
            #self.proc.read_until("login: ")
            rc= self.proc.read_until("sername: ",timeout=5)
            self._write_stdin(self.user + "\n")
            if self.password:
                rc= self.proc.read_until("assword: ",timeout=5)
                self._write_stdin(self.password + "\n")

        flush= self.proc.read_until('\n',timeout=5)
        self._pid[self.agent_id] = -1
        self.started = True
        return self.started


    def is_running(self):
        """
            return the subprocess status

        """
        if self.proc is None:
            #  proc has not been started
            return False
        else:
            return True

    def shutdown_process(self):
        """
            kill slave process
        """
        #rc= self._write_stdin("exit\n")
        #time.sleep(1)

        self.proc.close()
        self.proc=None
        # reset pid
        self._pid[self.agent_id]= 0
        return


    def _read_stdout(self,wait=False):
        """
            read from process stdout
        :param wait:
        :return:
        """
        line= self.proc.read_until('\n',timeout=1)
        return line


    def _write_stdin(self,cmd):
        """
            write to process stdin

        :param cmd: str with nl
        :return:
        """
        #buffer= b'%s' % cmd
        buffer= cmd.encode('ascii')
        self.proc.write(buffer)




    def run(self):
        """


            start telnet session

            send password

            enter main loop

                read from agent stdin
                if not empty :
                    send to slave stdin

                read from slave output
                    if not empty :
                        push to agent stdout

        :return:
        """
        #self.trace("starting process for agent: %s" % self.agent_id)

        # reset queues
        self.keep_alive()


        #for queue in self._queues.values():
        self.reset_queue('stdin')
        self.reset_queue('stdout')

        # launch slave process
        self.run_process()
        self.Terminated= False

        # # send password
        # time.sleep(3)
        # self.send(self.password)
        # time.sleep(1)


        while not self.Terminated:
            # infinite loop  till 'exit' received via command line
            self.run_once()

        # kill slave process and reset keep_alive
        self.shutdown_process()
        self.keep_alive(clear=True)
        sys.exit(0)




class TelnetThreadedAdapter(TelnetBaseAdapter,threading.Thread):
    """
        a threading version of adapter

    """
    #def __init__(self,agent_id,command_line,password,**kwargs):
    def __init__(self, agent_id, host="localhost", user=None, password=None, port=23,
                 log=None, redis_db=None,**kwargs):

        """

        """
        threading.Thread.__init__(self)
        TelnetBaseAdapter.__init__(self,agent_id,host=host,user=user,password=password,port=port,
                               log=log,redis_db=redis_db,**kwargs)
        self.Terminated=False
        self.setDaemon(True)


class TelnetProcessAdapter(TelnetBaseAdapter,multiprocessing.Process):
    """
        a threading version of adapter

    """

    def __init__(self, agent_id, host="localhost", user=None, password=None, port=23,
                 log=None, redis_db=None, **kwargs):
        """

        """
        multiprocessing.Process.__init__(self)
        TelnetBaseAdapter.__init__(self, agent_id, host=host, user=user, password=password, port=port,
                               log=log, redis_db=redis_db, **kwargs)
        self.Terminated=False
        #self.setDaemon(True)


#TelnetAdapter=TelnetProcessAdapter
TelnetAdapter=TelnetThreadedAdapter