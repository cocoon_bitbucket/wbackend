import sys
import time
import threading
import multiprocessing

from adapter import Adapter


class BatchAdapter(Adapter):
    """


        a loop to manage a batch


        run:
            read lines from queue <stdin> and write it to serial_port

            read lines from serial_ports and write to <stdout> queue


    """

    #
    # interface with process
    #

    # def shutdown_process(self):
    #     """
    #         kill slave process
    #     """
    #     if sys.hexversion >= 0x02060000:
    #         try:
    #             self.proc.terminate()
    #         except OSError ,e:
    #             # [Errno 3] No such process
    #             pass
    #
    #     else:
    #         self.proc.wait()
    #     return

    def keep_alive(self,clear=False):
        """

            write time to /agents[agent_id]
        :return:
        """
        # get process status
        status= None
        if self.proc :
            status= self.proc.poll()
            self._status[self.agent_id] = status
        else:
            self._status[self.agent_id] = 'not running'
        if status == None:
            # process still running update keep alive timestamp
            if clear:
                # clear keep alive for this agent
                self._keep_alive[self.agent_id]= None
                #self.database.hset( self.key_prefix,self.agent_id,None)
            else:
                # set keep alive to current time for this agent
                self._keep_alive[self.agent_id]= time.time()
                #self.database.hset( self.key_prefix,self.agent_id,time.time())

        else:
            # process has exited
            self._status[self.agent_id]= status
            self._keep_alive[self.agent_id]=status
            # kill myself
            self.Terminated=True

        return



class BatchThreadedAdapter(BatchAdapter,threading.Thread):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        Adapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        self.setDaemon(True)


class BatchProcessAdapter(BatchAdapter,multiprocessing.Process):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        multiprocessing.Process.__init__(self)
        Adapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        #self.setDaemon(True)






if __name__=='__main__':




    from adapter import ClientAdapter
    cmd=  ""


    def test_raw():

        db = Database(host='localhost', port=6379, db=0)
        db.flushall()
        Model.bind(database=db, namespace=None)

        rq = RQueues.create_for_item_id('agent:id:1')


        sp= BatchAdapter("opiqs:1",command_line=None)
        sp.run_process(cmd)
        n= 10
        while n > 0:
            sp.run_once()
            n -= 1
        return

    def test_thread():

        sp= BatchThreadedAdapter("tv",command_line=None)
        sp.run_process(cmd)

        cli = ClientAdapter("tv")

        n= 1000
        while n > 0:

            line= cli.read()
            print line
            n -=1

        cli.exit()

    def test_process():

        sp= BatchProcessAdapter("tv",command_line=None)
        sp.run_process(cmd)


        cli = ClientAdapter("tv")

        n= 10
        while n > 0:

            line= cli.read()
            print line
            n -= 1

        cli.exit()



    test_raw()
    test_thread()
    test_process()

print 'Done'