
from walrus import List
from adapter import Adapter, get_queue_key


class MockAdapter(Adapter):
    """


        a dummy adapter

    """

    def mock_write_stdout(self,line):
        """
            put some data in stdout queue

        :param line:
        :return:
        """
        queue = List(self.database, self.queue_key('stdout'))
        queue.append(line)

    def mock_read_stdin(self):
        """

        :return:
        """
        queue = List(self.database, self.queue_key('stdin'))
        if len(queue):
            value = queue.popleft()
        else:
            value = None
        return value

    def show(self,queue_name='stdout'):
        """
         return the content of a queue ( without pop it )
        :param queue_name:
        :return:
        """
        queue = List(self.database, self.queue_key(queue_name))
        return list(queue)



    #
    # override methods
    #

    def run_process(self,fullcmd=None,shell=None):
        """
            launch the slave process
        """
        self._pid[self.agent_id]= -1
        self.started = None
        return self.started

    def is_running(self):
        """
            return the subprocess status

        """
        return True

    def _write_stdin(self,cmd):
        """
            write to process stdin

        :param cmd: str with nl
        :return:
        """
        pass

    def _read_stdout(self,wait=False):
        """
            read from process stdout
        :param wait:
        :return:
        """
        return ""

    def shutdown_process(self):
        """
            kill slave process
        """
        # reset pid
        self._pid[self.agent_id]= 0
        return




class MockProcess(object):
    """
        wirite and read from redis ques
    """
    _timeout=1

    def __init__(self,target_id,redis_db=None):
        """

        :param target_id:
        """
        self.target_id= target_id
        self.database= redis_db
        self.collection,dummy,self.item= self.target_id.split(':')
        return

    def readlines(self):
        """
            read from target stdout
        :return:
        """
        queue = List(self.database, get_queue_key(self.collection,self.item,'stdout'))
        if len(queue):
            value = queue.popleft()
        else:
            value = ""
        return value


    def write(self,cmd):
        """
            write to target stdin
        :param cmd:
        :return:
        """
        queue = List(self.database, get_queue_key(self.collection,self.item,'stdin'))
        queue.append(cmd)
        return True

    def open(self,*args,**kwargs):
        """

        :param kwargs:
        :return:
        """
        pass

    def close(self):
        """

        :return:
        """
        pass