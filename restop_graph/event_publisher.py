from datetime import datetime
from configobj import ConfigObj

import requests

from gdiamond.graphite import GraphiteHandler
from gdiamond.metric import Metric

from tools import TimestampConverter

class EventPublisher(object):
    """



    """
    def __init__(self,config=None,configfile= None):
        """


        :param conf:
        """
        config = config or {}
        conf = ConfigObj(configfile)
        conf.update(config)

        self.handler = GraphiteHandler(config=conf)
        self.conf=conf


    def publish(self,event_name,value,timestamp=None):
        """


        :param event_name:
        :param value:
        :param timestamp:
        :return:
        """
        path= "%s.events.%s" % (self.conf['path_prefix'],event_name)
        if timestamp:
            if isinstance(timestamp,datetime):
                # it is at datetime format
                timestamp=TimestampConverter.datetime_to_epoq(timestamp)
            elif isinstance(timestamp,int) or isinstance(timestamp,float):
                # it is a epoq
                timestamp= int(timestamp)
            else:
                # assume it is a string (like: 2016-09-23T16:55:55.963843)
                if 'T' in timestamp:
                    # iso format
                    timestamp=TimestampConverter.iso_to_epoq(timestamp)
                else:
                    self.handler.log.error('invalid timestamp: %s' % timestamp)
        # create metric
        metric = Metric(path, value, raw_value=value, timestamp=timestamp)

        # send metric to graphite
        if self.handler.socket:
            self.handler._process(metric)
        else:
            self.handler.log.error('no connection to graphite')

    def new_event(self,what,data=None,tags=None,when=None):
        """

        :param what:
        :param tags:
        :param data:
        :param timestamp:
        :return:
        """
        data= data or "restop event"
        tags= tags or ['restop']
        if not when:
            when= datetime.utcnow()
            when= TimestampConverter.datetime_to_epoq(when)

        url= "http://192.168.1.21:8000/events/"
        response= requests.post()

        raise NotImplementedError()