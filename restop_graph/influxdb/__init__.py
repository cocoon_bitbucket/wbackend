"""

    a module to inject events in influxdb


    composed of 2 parts

    a plublisher to build events and publish it to redis pubsub channel influxdb.events

        p= Publisher()
        p.publish(title,text,flags,timestamp)


    an subscriber to inject events in influxdb ( listen to redis pusub channel influxdb.events )


        subscriber= Subscriber(redis_db)
        injector= Injector("http://192.168.99.100:8086",dbname="telegraf")
        subscriber.set_worker(injector)
        subscriber.start()






"""


from publisher import Publisher
from injector import Injector,Subscriber,Listener
