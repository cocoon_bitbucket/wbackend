"""

    Publish an influxdb event to redis pubsub channel influxdb.events


"""
import time
import json

import redis

from core import default_dbname,default_publish_channel



# redis_db= redis.StrictRedis()





class Publisher(object):
    """

        decorator to publish influx db event on
        redis pubsub channel influxdb.events

        message is json of dict

            dict(
                title= title,
                text= text,
                tags=tags or "",
                timestamp=timestamp or time.time()
            )

        usage as a decorator:

        @Publisher( title: "my title",text="my text")
        my_keyword( )


        standalone usage:

        p= Publisher()
        p.publish_event(title="hello",text="world",tags="my,tags",timestamp=time.time())


    """
    log_channel = "Logger"

    def __init__(self, title="", text="", tags="", timestamp=0L,

                 dbname=default_dbname, channel=default_publish_channel, redis_db=None):
        """

        :param channel:
        :param redis_db:
        """
        self.title = title
        self.text = text
        self.tags = tags
        self.timestamp = timestamp or time.time()
        self.dbname = dbname
        self.channel = channel
        self.redis_db = redis_db or redis.StrictRedis()

    def log(self, message):
        """

        :param message:
        :return:
        """
        msg = "publish message %s\n" % json.dumps(message)
        print(msg)

    def __call__(self, original_func):
        decorator_self = self

        def wrappee(*args, **kwargs):
            title = decorator_self.title or original_func.__name__
            text = decorator_self.text or title
            tags = decorator_self.tags
            timestamp = decorator_self.timestamp
            # compose message
            message = decorator_self.event_message(title, text, tags, timestamp)
            # publish the message
            decorator_self.publish_event(message)

            # call original function
            original_func(*args, **kwargs)

        return wrappee

    @classmethod
    def event_message(cls, title="", text="", tags="", timestamp=0L):
        """
            compose an Influxdb.event message

        :param title: string
        :param text: string
        :param tags: string
        :param timestamp: float
        :return:
        """
        return dict(
            title=title,
            text=text,
            tags=tags,
            timestamp=timestamp or time.time()
        )

    def publish_event(self, message=None,channel=default_publish_channel):
        """

        :param message:  dict  see event_message method
        :return:
        """

        if not message:
            message = self.event_message(self.title, self.text, self.tags, self.timestamp)
        msg = json.dumps(message)
        # publish event message on channel influxdb.events
        r=self.redis_db.publish(channel, msg)
        if r < 1 :
            # warning there is no subscriber for the channel
            self.log("WARNING no subscriber for channel: %s" % channel)
        # publish log (on channel "Logger)
        self.log(msg)
        return msg


if __name__ == "__main__":
    # stand alone usage

    redis_db= redis.StrictRedis(host="192.168.1.21")

    p = Publisher(redis_db=redis_db)
    event= Publisher.event_message(title="I am a publiser",text="thos is it")
    p.publish_event(event)


    # as a decorator

    @Publisher(title="the bar")
    def bar(a, b, c):
        print('in bar', a, b, c)


    @Publisher()
    def bar2(a, b, c):
        print('in bar2', a, b, c)


    @Publisher(text="my bar 3")
    def bar3(a, b, c):
        print('in bar3', a, b, c)


    @Publisher(title="my title", text="my text")
    def bar4(a, b, c):
        print('in bar4', a, b, c)


    bar('x', 'y', 'z')
    bar2('x', 'y', 'z')
    bar3('x', 'y', 'z')
    bar4('x', 'y', 'z')

    print("Done")