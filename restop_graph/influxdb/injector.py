"""


    inject influxdb events to an influxdb store


    subscribe to redis pubsub channel: "influxdb.events" and write events received  to influxdb database



"""
import threading
import json
import redis
import requests
from core import default_dbname,default_channels


class Injector(object):
    """

        inject data to influxdb

        event mode : write(msg)

        graphite mode: write_graphite(msg)


    """
    headers = {
        'Content-type': 'application/octet-stream',
        'Accept': 'text/plain'
    }

    def __init__(self,influxdb_host="localhost",dbname='graphite'):
        """

        :param influxdb_host:
        :param dbname:
        :param channel:
        """

        self.influxdb_url= "http://"+ influxdb_host + ":8086"
        self.influxdb_graphite= influxdb_host + ":2003"
        self.dbname= dbname
        self.session= requests.session()
        self.session.headers= self.headers

        # Initialize Data
        self.socket = None

    def make_influxdb_event_line(self,msg):
        """
            compute event line form msg
        :param msg: dict ( title,text,tags,timestamp)  timestamp is a float representing seconds
        :return:  string: payload for send event
        """
        # timestamp with precision = ms
        msg["timestamp"] = str(int(msg["timestamp"] * 1000))
        line = 'events title="%(title)s",text="%(text)s",tags="%(tags)s" %(timestamp)s\n' % msg
        return line


    def send(self,line):
        """
            send event lineto influxdb
        :param line: string eg
        :return:
        """
        url= self.influxdb_url + "/write"
        params = (
            ('db', self.dbname),
            ('precision', "ms")   # precision is milli-second
        )
        response = self.session.post(url, data=line, params=params)
        print(response.text)
        assert response.status_code == 204
        return


    def write(self,msg):
        """
            write an event message to the influxdb url

        :param msg: dict
        :return:
        """
        line = self.make_influxdb_event_line(msg)
        self.send(line)


    def write_graphite(self,msg):
        """

        :param msg: string compatible graphite msg to send to port 2003
        :return:
        """
        raise NotImplemented


# Listener

class Worker(object):
    """
        minimal worker

    """
    def write(self,item,channel):
        """

        :param item:
        :return:
        """
        print("worker handle ", item , "on channel  :", channel )



class Listener(threading.Thread):
    """
        a redis Pusub listener

    """
    def __init__(self, redis_db, channels=default_channels):
        threading.Thread.__init__(self)
        self.redis = redis_db
        self.pubsub = self.redis.pubsub()
        self.pubsub.subscribe(channels)

        self.worker= None

    def set_worker(self, worker):
        """

        :param injector: instance of  class with write(msg) method
        :return:
        """
        self.worker = worker

    def work(self, item,channel):

        if self.worker :
            self.worker.write(item,channel)
            #print(item['channel'], ":", item['data'])
        else:
            print("no worker defined: use listener.set_worker(worker)")

    def run(self):
        for item in self.pubsub.listen():
            if item["type"] == "message" or item["type"] == "pmessage":
                if item['data'] == '"KILL"':
                    self.pubsub.unsubscribe()
                    print(self, "unsubscribed and finished")
                    break
                else:
                    self.work(item["data"],channel=item['channel'])



class Subscriber(Listener):
    """

    """


    def work(self,item,channel):
        """

            send event message to influxdb

        :param item: string: a json representation of a event dict (title,text,tag,timestamp)
        :return:
        """
        if channel == "influxdb.events":
            try:
                msg= json.loads(item)
            except Exception as e:
                # cannot serialize item
                self.log("infludb subscriber: %s" % str(e))
                msg= None

            if msg:
                self.log("subscriber worker write message: %s" % msg)
                try:
                    self.worker.write(msg)
                except Exception as e:
                    self.log(" ... error:%s" % str(e))
        else:
            self.log("cannot handle channel %s" % channel)



    def log(self,message):
        """
            publish message on influxdb.logs
        :param message: string
        :return:
        """
        self.redis.publish("influxdb.logs",message)

        print(message)




if __name__=="__main__":
    """

    """
    import time

    redis_db= redis.StrictRedis(host="192.168.1.21")

    injector= Injector("192.168.1.21",dbname="graphite")

    subscriber= Subscriber(redis_db)
    subscriber.set_worker(injector)


    # publish some message
    for i in range(1):
        #
        msg= dict(title="event %d" %i , text="new sample event", tags="some,tags,T%s" % str(i),timestamp=time.time())
        redis_db.publish("influxdb.events",json.dumps(msg))
        time.sleep(1)

    msg = "graphite metric"
    redis_db.publish("influxdb.graphite_metrics", msg)
    time.sleep(1)

    # invalid msg
    #msg = dict(title="event %d" % 99, text="bad sample event", tags="some,tags,T%s" % str(99))
    #redis_db.publish(default_channel, json.dumps(msg))

    redis_db.publish('influxdb', json.dumps("KILL"))


    subscriber.start()

    # for msg in subscriber.pubsub.listen():
    #     print(msg)



    time.sleep(5)











