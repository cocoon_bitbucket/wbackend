# coding=utf-8

"""
The CPUCollector collects CPU utilization metric using /proc/stat.

#### Dependencies

 * /proc/stat

"""
import collector
from collector import Collector, str_to_bool
from error import DiamondException
from metric import Metric

try:
    import psutil
except ImportError:
    psutil = None


class CPUCollector(Collector):

    PROC = '/proc/stat'
    INTERVAL = 1

    MAX_VALUES = {
        'user': collector.MAX_COUNTER,
        'nice': collector.MAX_COUNTER,
        'system': collector.MAX_COUNTER,
        'idle': collector.MAX_COUNTER,
        'iowait': collector.MAX_COUNTER,
        'irq': collector.MAX_COUNTER,
        'softirq': collector.MAX_COUNTER,
        'steal': collector.MAX_COUNTER,
        'guest': collector.MAX_COUNTER,
        'guest_nice': collector.MAX_COUNTER,
    }

    @classmethod
    def get_command_line(cls,**kwargs):
        return "cat /proc/stat | grep cpu"


    def get_default_config_help(self):
        config_help = super(CPUCollector, self).get_default_config_help()
        config_help.update({
            'percore':  'Collect metrics per cpu core or just total',
            'simple':   'only return aggregate CPU% metric',
            'normalize': 'for cpu totals, divide by the number of CPUs',
        })
        return config_help

    def get_default_config(self):
        """
        Returns the default collector settings
        """
        config = super(CPUCollector, self).get_default_config()
        config.update({
            'path':     'cpu',
            'percore':  'True',
            'xenfix':   None,
            'simple':   'False',
            'normalize': 'False',
        })
        return config

    def collect_metric(self,lines,timestamp):
        """
        Collector cpu stats
        """
        #if os.access(self.PROC, os.R_OK):
        if True:


            results = {}
            # Open file
            #file = open(self.PROC)

            ncpus = -1  # dont want to count the 'cpu'(total) cpu.
            for line in lines:
                if not line.startswith('cpu'):
                    continue

                ncpus += 1
                elements = line.split()

                cpu = elements[0]

                if cpu == 'cpu':
                    cpu = 'total'
                elif not str_to_bool(self.config['percore']):
                    continue

                results[cpu] = {}

                if len(elements) >= 2:
                    results[cpu]['user'] = elements[1]
                if len(elements) >= 3:
                    results[cpu]['nice'] = elements[2]
                if len(elements) >= 4:
                    results[cpu]['system'] = elements[3]
                if len(elements) >= 5:
                    results[cpu]['idle'] = elements[4]
                if len(elements) >= 6:
                    results[cpu]['iowait'] = elements[5]
                if len(elements) >= 7:
                    results[cpu]['irq'] = elements[6]
                if len(elements) >= 8:
                    results[cpu]['softirq'] = elements[7]
                if len(elements) >= 9:
                    results[cpu]['steal'] = elements[8]
                if len(elements) >= 10:
                    results[cpu]['guest'] = elements[9]
                if len(elements) >= 11:
                    results[cpu]['guest_nice'] = elements[10]

            # Close File
            #file.close()

            metrics = {}
            metrics['cpu_count'] = ncpus

            for cpu in results.keys():
                stats = results[cpu]
                for s in stats.keys():
                    # Get Metric Name
                    metric_name = '.'.join([cpu, s])
                    # Get actual data
                    if ((str_to_bool(self.config['normalize']) and
                         cpu == 'total' and
                         ncpus > 0)):


                        metrics[metric_name] = self.derivative(
                            metric_name,
                            long(stats[s]),
                            self.MAX_VALUES[s]) / ncpus
                    else:
                        metrics[metric_name]= long(stats[s])

                        #metrics[metric_name] = self.derivative(
                        #    metric_name,
                        #    long(stats[s]),
                        #    self.MAX_VALUES[s])


            # Publish Metric Derivative
            for metric_name in metrics.keys():
                self.publish(metric_name,
                             metrics[metric_name],
                             precision=2,timestamp=timestamp)
            return True

