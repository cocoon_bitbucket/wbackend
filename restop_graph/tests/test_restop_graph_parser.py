
from restop_graph import TraceParser


#sample= 'stats-tv.txt'
sample= 'stats-livebox.txt'
configfile= '../collectors.ini'

def test_parser_graphite():

    device= 'livebox'

    content= None
    with open(sample,'r') as fh:
        data= fh.read()
        content= data.split('\n')

    config = {'host': '192.168.1.21'}
    #config= { 'host':'10.179.1.246'}

    if content:
        parser = TraceParser(content, device=device, configfile=configfile,config=config)
        parser.push_all_to_graphite()

def test_parser_influxdb():

    device= 'tv'

    content= None
    with open(sample,'r') as fh:
        data= fh.read()
        content= data.split('\n')

    config = {'host': '10.179.1.246', 'port':8086}
    if content:
        parser = TraceParser(content, device=device, configfile=configfile,config=config)
        parser.push_all_to_influxdb()


if __name__=="__main__":


    import logging
    logging.basicConfig(level=logging.DEBUG)

    test_parser_graphite()

    #test_parser_influxdb()

    print "Done."

