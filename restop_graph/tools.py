
import datetime


class TimestampConverter(object):
    """



    """

    @classmethod
    def iso_to_epoq(cls, timestamp):
        """
            convert timestamp to epoq

        :param timestamp:
        :return:
        """
        #  '2016-08-10T10:57:58Z' does not match format '%Y%m%dT%H%M%S.%fZ'
        if 'Z' in timestamp:
            t = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%SZ")
        else:
            #
            if '.' in timestamp:
                # 2016-09-23T16:55:55.963843
                t = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%f")
            else:
                # assume 2016-09-23T16:55:55
                t = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S")
        timestamp = t.strftime('%s')
        return timestamp


    @classmethod
    def epoq_to_datetime(cls,epoq):
        """



        :param epoq:
        :return:
        """
        timestamp= float(epoq)
        dt = datetime.datetime.fromtimestamp(timestamp)
        return dt


    @classmethod
    def datetime_to_epoq(cls, datetime_object):
        """



        :param epoq:
        :return:
        """
        epoq = datetime_object.strftime('%s')
        return float(epoq)
