from walrus.models import *
from walrus import Database
from model import Model


def get_queue_key(cls, collection, item, queue_name):
    """
        collection:container:stdin:collection:id:1

    :param collection:
    :param item:
    :return:
    """
    return "%s:container:%s:%s:id:%s" % (collection, queue_name, collection, str(item))


class RQueues(Model):
    """

        handler for related queues ( stdin, stdout , logs )

    """

    collection= 'rqueues'

    collection= TextField(default='rqueues')
    item =  IntegerField(default=0)

    name= TextField(index=True)

    @classmethod
    def create(cls,**kwargs):
        """

        :param kwargs:
        :return:
        """
        collection= kwargs.get('collection', cls.collection)
        item= int(kwargs.get('item',0))
        name= "%s:id:%s" % (collection,item)
        instance= cls(collection=collection,item=item,name=name)
        instance.save()
        if item == 0:
            instance.item = instance.get_id()
            instance.name = "%s:id:%s" % (collection, instance.item)
            instance.save()
        return instance


    @classmethod
    def create_for_item(cls,collection,item):
        """

        :param kwargs:
        :return:
        """
        return cls.create(collection=collection,item=int(item))
        # collection=collection
        # item= int(item)
        # name= "%s-%s" % (collection,item)
        # instance= cls(collection=collection,item=item,name=name)
        # instance.save()
        # return instance

    @classmethod
    def create_for_item_id(cls,item_id):
        """

        :param item_id: string a hash_id ( <collection>:id:<item> )
        :return:
        """
        collection,id,item = item_id.split(':',2)
        assert id == 'id' , "not a valid item_id"
        return cls.create(collection=collection, item=int(item))

        # item= int(item)
        # name= "%s-%s" % (collection,item)
        # instance= cls()
        # instance.save()
        # return instance

    def queue_key(self,name):
        """
            rqueues:container:stdin:rqueues:id:1

        :param name: string (short name of queue : ( stdin , stdout)
        :return:
        """
        return "%s:container:%s:%s:id:%d" % (self.collection,name,self.collection,self.item)

    def put(self,queue_name,value):
        """


        :param queue_name: string short name of the queue ( stdin/stdout)
        :return:
        """
        queue= List(self.database,self.queue_key(queue_name))
        queue.append(value)

    def get(self, queue_name, timeout=1):
        """


        :param queue_name: string short name of the queue ( stdin/stdout)
        :return:
        """
        queue = List(self.database, self.queue_key(queue_name))
        if len( queue ):
            value=queue[0]
            del queue[0]
        else:
            value= None
        return value

    def empty(self,queue_name):
        """

        :return:
        """
        queue = List(self.database, self.queue_key(queue_name))
        return len(queue) == 0

    def qsize(self,queue_name):
        """

        :param queue_name:
        :return:
        """
        queue = List(self.database, self.queue_key(queue_name))
        return len(queue)


if __name__ == '__main__':


    db = Database(host='localhost', port=6379, db=0)
    db.flushall()


    Model.bind(database=db,namespace=None)


    #rq= RQueues.create_for_item('rqueues',1)
    #rq= RQueues.create()
    #rq = RQueues.create_for_item_id('rqueues:id:1')
    rq = RQueues.create_for_item_id('agents:id:1')


    rq.put('stdin',"hello world")
    rq.put('stdin', "hello here")

    value= rq.get('stdin')
    assert value== 'hello world'
    value= rq.get('stdin')
    assert value== 'hello here'
    value= rq.get('stdin')
    assert value== None




    print "Done"