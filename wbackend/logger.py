import redis
from walrus import List

class SimpleLogger(object):
    """

        write log into a redis queue

    """

    def __init__(self,queue_key,redis_db=None):
        """

        :param queue_key: string ( eg logger:container:logs:logger:id:1 )
        :param redis_db:
        """
        self.queue_key= queue_key
        self.database = redis_db or redis.Redis()
        self.queue= List(self.database,self.queue_key)


    def _write(self,line):
        """

        :param line:
        :return:
        """
        self.queue.append(line)

    def info(self,message,exc_info=False):
        """

        :param message:
        :return:
        """
        self._write(message)
    debug=info
    warning=info

    def error(self,message,exc_info=False):
        self._write("ERROR: %s" % message)

    critical= error