import os
import binascii
import datetime
from datetime import timedelta

from walrus import *
from wbackend.model import Model
from wbackend.graph import Graph

import logging
log= logging.getLogger('platform')



class Trace(Model):

     collection= 'trace'

     session= IntegerField(primary_key=True)
     start = DateField(index=True)
     result = JSONField()


class TraceEntry(Model):
    """


    """
    collection= 'traces'

    #sequence = AutoIncrementField()
    session = IntegerField(index=True)
    hub= TextField()
    agent= IntegerField()

    operation= TextField()
    result = JSONField(default={})
    log_offset= IntegerField(default=1)

    begin= DateTimeField(default=datetime.datetime.utcnow())
    end = DateTimeField(default=datetime.datetime.utcnow())
