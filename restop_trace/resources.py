"""


    handle trace for a session



    POST /trace  session=1

    POST /trace/<session>/  hub=droydrunner_agents  agent_id=1  log_offset=1 result=""

    GET /trace/<session>


"""
from wbackend.model import Model
from restop.resource import Resource, JsonResource


class Resource_trace(JsonResource):
    """

    """
    collection= 'traces'


    def _collection_post(self,session,hub,agent,operation,result,log_offset):
        """

        :param session:
        :param hub:
        :param agent:
        :param operation:
        :param result:
        :param log_offset:
        :return:
        """
        trace_factory= Model.select_plugin(self.collection)
        e = trace_factory.create(session=session, hub=hub, agent=agent,
                              operation=operation, result=result, log_offest=log_offset)
        return e


    def _item_post(self,item,hub,agent,operation,result,log_offset):
        """

            create trace entry

                POST /traces/1
                    body:
                      session=1,hub=

        :param item:
        :param kwargs:
        :return:
        """

        return True