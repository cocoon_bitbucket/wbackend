__author__ = 'cocoon'

from restop.tools import ScanMethods
from restop.plugins.resources import DispatchResource
from restop.application import InvalidUsage,ApplicationError
from restop.client import RestopClient,ConnectionError,ConnectTimeout
from models import Platform
from models import Session
from models import Token
from models import Device,Profile
from models import Hub
from models import Apidoc
from models import AgentModel
from models import LocalSession


from restop.resource import Resource

try:
    import restop_graph
except ImportError:
    restop_graph= None


class Resource_platforms(Resource):
    """

    """
    collection= 'platforms'

    def collection_get(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        return "platforms"

    def item_get(self, item, **kwargs):
        """
            get platform configuration

            GET /platforms/1

        :item: str platform_id
        :return:
        """
        platform = Platform.get(Platform.name == 'default')
        data = platform.data
        return data


    def op_get_config(self,item,**kwargs):
        """

            POST /platforms/1/get_config

        :item: str platform_id
        :return:
        """
        platform= Platform.get(Platform.name=='default')
        data= platform.data
        return data


    def op_get_hub(self,item,**kwargs):
        """
            get data of a hub
        :param kwargs:
        :return:
        """
        data= self.request.json
        hub_name= data['name']
        try:
            hub= Hub.get(Hub.name==hub_name)
            return hub._data
        except Exception as e:
            raise ApplicationError('cannot find a hub named: [%s]' % hub_name)


    def op_register(self,item,**kwargs):
        """

            register a satellite

            POST /platforms/<item>/register
                hub_name

        :param item:
        :param kwargs:
        :return:
        """
        data= self.request.json
        hub_name= data['name']
        try:
            hub= Hub.get(Hub.name==hub_name)
            #return hub._data
        except Exception as e:
            raise ApplicationError('cannot find a hub named: [%s]' % hub_name)

        # register the url of hub
        hub.register_host(self.request.remote_addr)
        data= hub._data
        return data

class Resource_oauth2(DispatchResource):
    """


    """
    collection= 'oauth2'

    def item_post_token(self,item,method,**kwargs):
        """
            create a token

            POST /oauth2/token

            input= dict ( client_id=, client_secret= , grant_type= "client_credentials" )


        """
        assert item == 'token'
        # open session
        data= self.request.json

        # try:
        #     self.backend.spec_schema_validate(data,'oauth2/token/post_request.json')
        # except Exception,e:
        #     raise InvalidUsage(e.message)

        assert data['grant_type'] == 'client_credentials' , "only supports grant_type='client_credentials"

        # check credentials for username
        user_name= data['client_id']

        # credential OK: create token
        token= Token.create(owner=user_name)
        token_data= dict(
            _id= token.get_id(),
            created= token.created.isoformat(),
            expires= token.expires.isoformat(),
            token= token.token,
            owner= token.owner
        )

        #return "POST /oauth2/token"
        return token_data

    def item_post_revoke(self,item,method,**kwarg):
        """
            revoke a token

            POST /oauth2/revoke


        """
        raise NotImplementedError()

        # assert item == 'revoke'
        #
        # # check session
        # access_token= self.check_oauth2(self.request)
        #
        # # retrieve session
        # session= self.backend.session_get_by_token(access_token)
        # session_id= session['id']
        #
        # # close session
        # self.backend.session_close(session_id)
        #
        # return {'message': 'session close'}




class Resource_device_sessions(Resource):
    """

    """
    collection= 'sessions'

    #
    # web interface
    #

    def collection_post(self,**kwargs):
        """
            create a pool of agents

            POST /restop/api/v1/sessions

            input :
                {
                    client_id,
                    client_secret,
                    grant_type= 'client_credentials',
                    members=
                        [
                            [ agent_alias, agent_category , lock_name , {} ],
                            [ ...                                  }
                        ]
                }
            #{ "client_id":"anonymous","client_secret":"anounymus","grant_type":"client_credentials","members":["sample_1","sample_2"]}


            output :
                {

                }


        :return:
        """
        # open create agents pool
        data= self.request.json or {}

        # create and load session
        session= Session.create(**data)
        session_id= session.get_id()
        session= Session.load(session_id)

        # start session on remote hubs and compute links
        response= self.create_hub_sessions(session)
        if response['result'] != 200:
            return response


        # prepare session response
        session_data= { 'links': {} ,'id': session.get_id(), 'token':session.token.token}
        for alias,link in session.links.items():
            session_data['links'][alias]= link
            session.logs.append("link to %s: %s" % (alias,link))

        #return session_data
        response= session.make_response(session_data)
        return response


    def collection_get(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        return "get sessions"

    def item_get(self,item ,**kwargs):
        """

            /_pool_sessions/<id>

            return session info

        :param kwargs:
        :return:
        """
        session=Session.load(item)
        hash= session.to_hash()
        data= session._data
        return repr(data)

    def create_hub_sessions(self,session):
        """
            create sub session on each hub of the session for each member

        :return:
        """

        # operation url
        base_url= self.request.base_url
        parts= base_url.split('/')
        session_url= '/'.join(parts[:-1])
        base_url= '/'.join(parts[:-3])

        # send create session for each hub ( or collection)
        counter=0
        errors= 0
        logs=[]
        links={}

        dispatch=session.dispatch

        # for each hub or collection
        for hub_name in dispatch.keys():
            configuration= {}
            counter += 1

            # get hub data
            hub= Hub.get(Hub.name==hub_name)
            hub_url= hub.hub_url
            hub_members= dispatch[hub_name]
            # send /hub/<collection>/-/open_session
            client= RestopClient(hub_url)
            #open_url = client.base_url + '/samples/-/open_session/'
            session_id= session.get_id()
            #hub_members= hub.members[:]
            #make configuration (aggregate profile and device data )
            for member_alias in hub_members:
                configuration[member_alias]={}
                device= Device.get(Device.name==member_alias)
                profile= device.get_profile()
                configuration[member_alias].update(profile.data)
                configuration[member_alias].update(device.data)
                continue
            data= { "session_id": session_id, "members":hub_members, "configuration": configuration }

            try:
                # create agents on hubs
                #response= client.post(open_url,data=data)
                response= client.operation_post(hub_name,'-','open_session',data=data)
            except TypeError as e:
                raise RuntimeError('bad json conversion on %s' % data)
            except Exception as  e:
                #raise RuntimeError("hub for collection [%s] not in service at [%s]" % (hub_name, hub_url))
                message= "hub for collection [%s] not in service at [%s]" % (hub_name, hub_url)
                raise ApplicationError(message=message,result= 400 , status_code=200)

            content_type= response.headers['content-type']
            if content_type == 'application/json':
                response_data= response.json()
                logs.extend(response_data.get('logs',[]))
            else:
                response_data= response.text
                logs.append(response_data)
                raise ApplicationError(response_data,500,logs)

            if logs:
                session.logs.extend(logs)

            if response.status_code == 200:
                # compute session links for each agent
                message= response_data['message']
                agent_ids= message['agents']
                for index,agent_id in enumerate(agent_ids):
                    link= base_url + "/hub/" + hub_name + '/' + str(agent_id)
                    session.links[hub_members[index]]= link
            else:
                # fail to create remote session
                logs= list(session.logs)
                response_body= dict(
                    message='fail to create remote session for hub %s' % hub_name,
                    result= 502 , logs=logs
                )
                if response_data.has_key('tb'):
                    # it is a critical response with traceback
                    for element in ['tb','exc_type','exc_value']:
                        response_body[element]= response_data[element]
                return response_body
                #raise ApplicationError("fail to create remote session for hub %s" % hub_name,502,logs=logs)
        # prepare response

        """
        {
            u'status': u'valid',
            u'grant_type': u'client_credentials',
            u'user_id': u'1',
            u'members': [[u'tv', u'tvbox_agents', {u'device_id': u'tv'}]],
            u'links': {
                u'tv': u'http://10.179.1.246/restop/api/v1/tvbox_agents/2'
                },
            u'created': u'2016-09-27T16:54:17.178243',
            u'expires': u'2016-09-27T17:54:17.178243',
            u'token': u'85ca93fe3872ad7bff200a1d5280582d',
            u'finished': None,
            u'agents': [
                u'tvbox_agents:2'
                ],
            u'client_id': u'anonymus',
            u'client_secret': u'',
            u'_id': u'Sessions:2',
            u'user_name': u'anonymus',
            u'id': u'2',
            u'log': u'session 2 created\ncreate agent: tv as http://10.179.1.246/restop/api/v1/tvbox_agents/2'
            }
        """

        response = session.make_response(True)
        return response
        #return dict( status= 200,message= 'OK', logs=logs)

    def op_start(self, item, **kwargs):
        """
            session start  POST /restop/api/v1/sessions/1/start
        :return:
        """
        # get session object
        try:
            session = Session.load(item)
        except KeyError:
            raise KeyError("no such session: %s" % item)

        # for each agent on each hub call agent start /prefix/hub/<hub_name>/<item>/start
        for alias, link in session.links.items():
            #print alias, link
            client= RestopClient('')
            url = link + "/start"
            try:
                response= client.post(url)
            except Exception as e:
                raise RuntimeError("cannot start agent at %s" % (link) )
            if response.status_code == 200:
                response_data = response.json()
                if response_data['logs']:
                    session.logs.extend(response_data['logs'])
                if response_data['message'] == True:
                    continue
            else:
                response_data = response.json()
                if response_data.get('logs',[]):
                    session.logs.extend(response_data['logs'])
                session.logs.append("failed to start agent,status is %s " % str(response.status_code))
                response= session.make_response(False,400)
                return response

        response = session.make_response(True)
        return response

    def op_stop(self, item, **kwargs):
        """
            call stop on each member of the session
        :param item: string , session id eg 1
        :return:
        """
        # get session object
        try:
            session = Session.load(item)
        except KeyError:
            raise ApplicationError("no such session: %s" % item,status_code=200)

        # for each agent on each hub call agent start /prefix/hub/<hub_name>/<item>/start
        for alias, link in session.links.items():
            #print alias, link
            client= RestopClient('')
            url = link + "/stop"
            try:
                response= client.post(url)
            except Exception as e:
                session.logs.append("cannot stop agent at %s" % (link) )
            if response.status_code == 200:
                response_data = response.json()
                if response_data['logs']:
                    session.logs.extend(response_data['logs'])
            else:
                session.logs.append("cannot stop agent at %s" % (link))

        # close remote session on all hubs
        dispatch= session.dispatch
        for hub_name, hub_members in dispatch.iteritems() :
            # for each hub call /restop/api/v1/<hub_name>/-/close_session
            # get hub data
            hub = Hub.get(Hub.name == hub_name)
            hub_url = hub.hub_url
            # send /hub/<hub_name>/-/close_session
            client = RestopClient(hub_url)
            payload = dict(session_id=session.get_id(), members=hub_members)
            try:
                # create agents on hubs
                #response= client.post(open_url,data=data)
                response= client.operation_post(hub_name,'-','close_session',data=payload)
            except TypeError as e:
                raise RuntimeError('bad json conversion on %s' % payload)
            except Exception as  e:
                #raise RuntimeError("hub for collection [%s] not in service at [%s]" % (hub_name, hub_url))
                message= "hub for collection [%s] not in service at [%s]" % (hub_name, hub_url)
                raise ApplicationError(message=message,result= 400 , status_code=200)

            if response.headers['content-type'] == 'application/json':
                data=response.json()
            else:
                data=response.text
            continue

        #message= dict (code= 200 , message= "OK" , logs= list(session.logs))
        r= session.close()
        response= session.make_response(message="OK")
        return response


    #s
    # primary interface
    #

    def build_dispatch(self,session,session_members):
        """

        :param members: list of session members
        :return:
        """
        dispatch= {}
        for hub in Hub.all():
            dispatch[hub.name]= []
        return dispatch

    def build_session_configuration(self,members):
        """

        :return:
        """

        return

    def create_item(self,item_data,**kwargs):
        """
            create a pool of agents
            input :{
                        client_id,
                        client_secret,
                        grant_type= 'client_credentials',
                        members=
                            [ [ agent_alias, agent_category , lock_name , {} ],
                              [ ...                                  }            ]
            }
        :return:
        """
        # open create agents pool
        data= item_data

        # check input
        #try:
        #    self.backend.spec_schema_validate(data,'agents/post_request.json')
        #except Exception,e:
        #    raise InvalidUsage(e.message)

        assert data['grant_type'] == 'client_credentials' , "only supports grant_type='client_credentials"

        user_name= data['client_id']

        # create session
        session_id= self.backend.session_new(user_name,**data)
        session_data= self.backend.session_get(session_id)

        self.backend.session_log(session_id,"session %s created" % session_id)
        # validate and add links to sessions
        self.validate(session_data)

        # hook to customize session data
        session_data= self.customize_item(session_data,session_id)

        # create agents
        agents= self.create_agents(session_data)

        # create session
        session_data.update( agents )
        rc= self.backend.item_put('Sessions',session_id,session_data)

        return self.item_get(session_id,**kwargs)

    def customize_item(self,session_data,session_id):
        return session_data

    def validate(self,session_data):
        """
            validate pool parameters
        :return:
        """
        assert session_data.has_key('members'), "data must have a members list"
        assert isinstance(session_data['members'],list), "members must be a list"
        #assert len(session_data['members']) > 0, "empty members list"


        return True
        # # handle members
        # agent_base_url= self.request.url.split('/')   # eg localhost:5000/restop/api/v1/agents
        # del agent_base_url[-1]
        # agent_base_url= '/'.join(agent_base_url)
        #
        # member_keys= []
        # member_links= {}
        # for member in session_data['members']:
        #     alias= member[0]
        #     category= member[1]
        #     lock_name= member[2 ]
        #     if len (member) >3:
        #         member_data= member[3]
        #     else:
        #         member_data= {}
        #
        #     # check there is a resource plugin for this category
        #     try:
        #         resource_class= Resource.select_plugin(category)
        #     except KeyError:
        #         # not plugin for this resource
        #         raise ApplicationError("no plugin for this collection: %s" % category)
        #     # create an agent entry
        #     agent_id= self.backend.identifier_new(collection=category)
        #     agent_data = dict( alias=alias,category=category,lock_name=lock_name,parameters=member_data)
        #     agent_data['session_id']= session_data['id']
        #
        #     rc= self.backend.item_new(category,agent_id,agent_data)
        #     agent_key= self.backend.item_key(category,agent_id)
        #     member_keys.append(agent_key)
        #     member_links[alias]= "%s/%s/%s" % ( agent_base_url,category,agent_id)
        #
        # return dict( agents= member_keys,links= member_links )

    def create_agents(self,session_data):
        """
            create agents in database

        :param session_data:
        :return:
        """
        return "create_agents"



class CollectionApidoc(Resource):
    """
         /<prefix>/apidoc   a resource to access auto documentation
    """
    collection= 'apidoc'

    def collection_post(self,**kwargs):
        """
            create a dummy1 item

        """
        raise NotImplementedError()
        # schema_path= "%s/post_request.json" % self.collection
        # data= self.request.json
        #
        # rc= self.backend.spec_schema_validate(data,schema_path)
        #
        # # create an id for this item in the collection
        # item_id= self.backend.identifier_new(self.collection)
        #
        #
        # infoUrl= self.request.url + '/' + item_id
        #
        # # create the item with data
        # r= self.backend.item_new(self.collection,item_id,data)
        #
        # return { 'code': 200, 'message': "OK", 'infoUrl': infoUrl}

    def collection_get(self,**kwargs):
        """
            return apidoc

            url= /prefix/apidoc

        """
        #data= self.request.json
        data=self.request.values
        message = {}

        apidoc= Apidoc.get(Apidoc.platform_name == 'default')

        if 'collection' in data:
            raise NotImplementedError()
            # # this is a request for a collection api
            # collection_name= data['collection']
            # try:
            #     plugin_class= Resource.select_plugin(collection_name)
            # except KeyError:
            #     raise ApplicationError("No plugin found for collection: %s" % collection_name)
            #
            # doc= plugin_class._autodoc()

        else:
            # this a request for the general interface ( key: apidoc:interface )

            doc= apidoc.export()

        #l= self.backend.collection_list(self.collection)
        return { 'code': 200, 'message': doc }


    def item_post(self,item,**kwargs):
        """

            update api doc with data

                { operations: [] , collection_operations:[] , platform_name='default'

            url /prefix/apidoc/<collection>

        :param item:
        :param kwargs:
        :return:
        """
        input= self.request.json
        platform_name =input.get('platform_name','default')
        apidoc= Apidoc.get(Apidoc.platform_name==platform_name)
        apidoc.update(input)

        return True





class AgentResource(Resource):
    """
        a base resource for an agent ( to be validated )

    """
    collection= "_agent"
    protected= ''

    _AgentClass= None

    @classmethod
    def _autodoc(cls,**kwargs):
        """

            generate autodocumentation
        :param kwargs:
        :return:
        """
        info = ScanMethods()
        info.add_docstring(cls)
        info.add_op_methods(cls)
        if cls._AgentClass:
            info.add_methods(cls._AgentClass)
        return info.result


    @property
    def model(self):
        return self.backend.select_plugin(self.collection)


    def configure_session(self,session_id,members,configuration):
        """

        :param session_id:
        :param members:
        :param configuration:
        :return:
        """
        return configuration

    def create_agent(self,alias,configuration=None,session_id=None):
        #
        agent=self.model.create(name=alias,parameters=configuration,session=session_id)
        return agent


    def collection_post(self, **kwargs):
        """
            create a sample object
        :return:
        """
        data = self.request.json or {}

        obj = self.model.create(**data)
        return True

    def item_get(self, item, **kwargs):
        """
        :param kwargs:
        :return:
        """
        obj = self.model.load(item)
        data = obj._data
        return repr(data)


    # def op_col_open_session(self, item, **kwargs):
    #     """
    #         open a local session
    #
    #         POST /hub/samples/-/open_session
    #
    #     :return:
    #     """
    #     #assert item == '-'
    #     agents= []
    #     data= self.request.json
    #     session_id= data['session_id']
    #     members= data['members']
    #     configuration=data['configuration']
    #     session_configuration= self.configure_session(session_id,members,configuration)
    #     # create agents
    #     for member_alias in members:
    #         agent=self.create_agent(member_alias,session_configuration[member_alias],session_id=session_id)
    #         agents.append(agent.get_id())
    #     # prepare  response
    #     data= { "agents": agents, "session_configuration":session_configuration}
    #
    #     return { 'result':200, 'message':data,'logs':['session open']}
    #
    #
    # def op_col_close_session(self, item, **kwargs):
    #     """
    #         close a local session
    #
    #         POST /hub/samples/-/close_session
    #
    #             parameters:  members
    #
    #     :return:
    #     """
    #     #assert item == '-'
    #     data= self.request.json
    #
    #     session_id= data['session_id']
    #     members= data['members']
    #
    #     message= "close session: %s , with members: %s" % (session_id,str(members))
    #
    #     return { 'result':200, 'message':data,'logs':[message]}

    def op_col_open_session(self, item, **kwargs):
        """
            open a local session

            POST /hub/samples/-/open_session

        :return:
        """
        #assert item == '-'
        agents= []
        data= self.request.json
        session_id= data['session_id']
        members= data['members']
        configuration=data['configuration']
        session_configuration= self.configure_session(session_id,members,configuration)

        # create local session
        local_session= LocalSession.create(session_id=session_id,data={'members':members,'configuration':configuration})

        # create agents
        for member_alias in members:
            agent=self.create_agent(member_alias,session_configuration[member_alias],session_id=session_id)
            agents.append(agent.get_id())
        # prepare  response
        data= { "agents": agents, "session_configuration":session_configuration}

        return { 'result':200, 'message':data,'logs':[]}

    def op_col_close_session(self, item, **kwargs):
        """
            close a local session

            POST /hub/samples/-/close_session

                parameters:  members

        :return:
        """
        #assert item == '-'
        data= self.request.json

        session_id= data['session_id']
        members= data['members']

        # load local session
        local_session=LocalSession.load(session_id)

        # for each agent of local session
        # call agent.stop


        rc= local_session.close()

        # local_session.status='closed'
        # local_session.save()
        #
        # message= "close session: %s , with members: %s" % (session_id,str(members))
        #
        # local_session.logs.append(message)

        response= local_session.make_response(rc)
        return response




    def op_dummy(self,item,**kwargs):
        """
            for test purpose
        :return: dictionary
        """
        return dict( result=200 , message="OK" , logs=['operation dummy called on item %s' % item])


    def _operation(self,collection,item,operation,**kwargs):
        """
            execute an operation on an item

            override standard dispatcher: search for a op_$operation method

        """
        # check if operation is a direct implementation = op_start , op_stop ...
        op= None
        if item == '-':
            # collection operation
            op_name = "op_col_" + operation
        else:
            # item operation
            op_name = "op_" + operation

        try:
            op= getattr(self,op_name)
        except AttributeError:
            pass
        if op :
            # call local operation op_*
            return op(item,**kwargs)

        # # check if operation is defined in Mobile
        # try:
        #     agent_func= getattr(self._AgentClass,operation)
        # except AttributeError:
        #     # operation does not exists on interface
        #     raise ApplicationError("invalid operation [%s] on item [%s]" % (operation,item) , 404 )

        # operation exists
        model = self.model.load(item)
        model.logs.append("agent [%s/%s] received [%s] command" % (self.model.collection,item,operation))
        agent = self._AgentClass(model)

        try:
            func= getattr(agent,operation)
        except AttributeError:
            # operation does not exists on interface
            raise ApplicationError("invalid operation [%s] on item [%s]" % (operation,item) , 404 )

        input_data= self.request.json
        if input_data:
            rc= func(**input_data)
        else:
            rc= func()
        #return rc
        return model.make_response(rc)
