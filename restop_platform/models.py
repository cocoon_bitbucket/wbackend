

import os
import binascii
from datetime import timedelta

from walrus import *
from wbackend.model import Model
from wbackend.graph import Graph
from restop_queues.models import Queue
import logging
log= logging.getLogger('platform')



class User(Model):

     collection= 'user'

     name = TextField(primary_key=True)
     dob = DateField(index=True)
     data = JSONField()


class Device(Model):

    collection= "device"

    name = TextField(index=True)
    profile= TextField(index=True)

    data= JSONField()

    #data= JSONField()

    @property
    def collection(self):
        """

        :return:
        """
        profile= self.get_profile()
        return profile.collection

    def get_profile(self):
        return Profile.get(Profile.name == self.profile)


    def get_site(self):
        graph = Graph(self.database, namespace='links')
        X = graph.v.X  # Create a variable placeholder
        results = graph.search({'o': self.get_hash_id(), 'p': 'has_device', 's': X})
        keys = list(results['X'])
        if len(keys):
            key= keys[0]
            site= Site.load(primary_key=key, convert_key=False)
        else:
            site=None
        return site

    def get_options(self):
        profile= self.get_profile()
        options= profile.data.get('options',{})
        return options

    def get_configuration(self):
        """
            merge own configuration with profile configuration
        :return: dict
        """
        configuration= {}
        profile = self.get_profile()
        configuration.update(profile.data)
        configuration.update(self.data)
        return configuration


class Token(Model):
    """


    """
    collection= 'token'
    owner= TextField(index=True)    # session:id:1
    token= TextField(index=True)        # 4fab4292e6588243fa5f0302f7a6b19d
    status= 'valid'
    created= DateTimeField (default= datetime.datetime.utcnow())
    expires= DateTimeField()
    revoked= BooleanField(default=0)
    expired= BooleanField(default=0)

    @classmethod
    def create(cls, owner,lenght=16,duration=3600*24*30*12):
        """
        Create a new token
        """
        token=cls.get_random_token_string(length=lenght)
        instance = cls(owner=owner,token=token)
        instance.expires = instance.created + timedelta(seconds=int(duration))
        instance.expired= False
        instance.save()
        return instance

    @classmethod
    def get_random_token_string(cls, length=16):
        """
            # generate a ramdom token
        """
        data = os.urandom(length)
        id = binascii.hexlify(data)
        return id

    def revoke(self):
        """

        :return:
        """
        self.status= 'revoked'
        self.save()

    def has_expired(self):
        status= datetime.datetime.utcnow() > self.expires
        if status:
            self.expired= True
        return status

class LocalSession(Model):
    """
        a local session on a hub

    """
    collection= 'local_session'

    # master session_id
    session_id= TextField(index=True)
    logs= ListField()
    log_offset = IntegerField(default=1)
    status = TextField(default='created')
    data= JSONField()

    @classmethod
    def create(cls, session_id,**kwargs):
        """
        Create a new local session
        """
        #members = []
        #if 'members' in kwargs:
        #    members = kwargs.pop('members')
        instance = cls(session_id=session_id, data=kwargs)
        instance.save()
        instance.logs.append('local session model created')
        return instance

    def close(self):
        """

        :return:
        """
        self.logs.append('local session closed')
        self.status= 'closed'
        self.save()
        return True

    def make_response(self,message,status=200,log_offset= 0):
        """

        :param status:
        :param message:
        :param logs:
        :return:
        """

        log_offset= int(log_offset) or  self.log_offset
        logs=self.logs[log_offset:]
        message= { 'result': int(status), 'message': message, 'logs': logs }
        self.log_offset= len(list(self.logs))
        self.save()
        return message


class Session(Model):
    """

    """
    collection = "session"

    client_id= TextField(default='anonymous')
    client_secret= TextField(default='anonymous')
    grant_type= TextField(default='client_credentials')
    members= ListField()

    platform= TextField(index=True,default='default')
    token_id= TextField(default='')
    created = DateField(index=True,default=datetime.datetime.utcnow())
    finished = DateField(default=datetime.datetime.utcnow())

    log_offset = IntegerField(default=1)
    status= TextField(default='created')
    active= BooleanField(default=1)

    collections = SetField()
    links= HashField()
    configuration= JSONField()

    events= ListField()
    logs= ListField()
    dispatch= JSONField()
    data = JSONField()

    @classmethod
    def create(cls, **kwargs):
        """
        Create a new token
        """
        members=[]
        if 'members' in kwargs:
            members= kwargs.pop('members')
        instance = cls(**kwargs)
        instance.save()
        instance.logs.append('session model created')
        instance.setup(members=members)
        return instance

    @classmethod
    def load(cls, primary_key, convert_key=True):
        """

        :param primary_key:
        :param convert_key:
        :return:
        """
        instance= super(Session, cls).load(primary_key, convert_key= convert_key)
        #instance._initial_log_index=0
        #if 'logs' in instance._fields:
        #    instance ._initial_log_index= len(instance.logs)
        return instance


    def make_response(self,message,status=200,log_offset= 0):
        """

        :param status:
        :param message:
        :param logs:
        :return:
        """

        log_offset= int(log_offset) or  self.log_offset
        logs=self.logs[log_offset:]
        message= { 'result': int(status), 'message': message, 'logs': logs }
        self.log_offset= len(list(self.logs))
        self.save()
        return message


    @property
    def token(self):
        t= Token.get(Token._id==self.token_id)
        return t

    def setup(self,members=None):
        """

        :param members:
        :return:
        """
        members=members or []
        self.setup_token()
        self.setup_members(members=members)
        self.events.append('session %s %s' % (self.get_id(), self.created))
        self.setup_dispatch()
        self.save()

    def setup_token(self,length=16):
        """

        :return:
        """
        token= Token.create(owner=self.get_hash_id())
        self.token_id= token.get_id()

    def setup_members(self,members=None):
        """

        :param members:
        :return:
        """
        members= members or []

        # setup members and collections
        for member_name in members:
            try:
                device= Device.get(Device.name==member_name)
            except ValueError as e:
                assert e.message == "Got 0 results, expected 1."
                raise ValueError("unknown device: %s" % member_name)
            except Exception as e:
                raise
            profile= device.get_profile()

            collection= profile.collection
            self.members.append(member_name)
            self.collections.add(collection)

        # setup configuration
        configuration={}
        # for collection_name in list(self.collections):
        #     device_class= Model.select_plugin(collection_name)
        #     configuration.update(device_class.session_configuration(self.members))
        #     continue

        self.configuration=configuration
        self.save()


    def setup_dispatch(self):
        """


        :return:
        """
        graph = Graph(self.database, namespace='links')
        dispatch={}
        # create a dispatch entry for each member of the collection
        for member in self.members[:]:
            device= Device.get(Device.name==member)
            X = graph.v.X
            results = graph.search(
                {'p': 'has_hub', 's': device.get_hash_id() , 'o': X},
                )
            hub_hash_id= results['X'].pop()
            hub= Hub.load(hub_hash_id,convert_key=False)
            if not dispatch.has_key(hub.name):
                dispatch[hub.name]= []
            dispatch[hub.name].append(member)
        self.dispatch=dispatch
        self.save()
        return dispatch


    def get_platform(self):
        return Platform.load(Session.platform)


    def close(self,status='finished'):
        """

        :param status:
        :return:
        """
        self.status= status
        self.active= False
        self.finished= datetime.datetime.utcnow()
        self.events.append('session %s %s' % (self.get_id(), self.finished))
        self.save()
        return



#class AgentModel(Model):
class AgentModel(Queue):
    """

    """
    collection= '_agent'

    #name= TextField(index=True)
    session= TextField(index=True)

    device= TextField(default='device')
    created = DateField(index=True, default=datetime.datetime.utcnow())

    #log_offset= IntegerField(default=1)

    #stdin= ListField()
    #stdout= ListField()
    logs = ListField()

    #parameters= JSONField(default={})
    state= JSONField(default={})


    @classmethod
    def create(cls, **kwargs):
        """
        Create a new token
        """
        instance = cls(**kwargs)
        instance.save()
        instance.logs.append('agent model created')
        instance.setup()
        instance.save()
        return instance


    @classmethod
    def load(cls, primary_key, convert_key=True):
        """

        :param primary_key:
        :param convert_key:
        :return:
        """
        instance= super(AgentModel, cls).load(primary_key, convert_key= convert_key)
        #instance._initial_log_index=0
        #if 'logs' in instance._fields:
        #    instance ._initial_log_index= len(instance.logs)
        return instance

    def setup(self):
        """

        :return:
        """
        pass

    def make_response(self,message,status=200,log_offset= 0):
        """

        :param status: 
        :param message:
        :param logs:
        :return:
        """

        log_offset= int(log_offset) or  self.log_offset
        logs=self.logs[log_offset:]
        message= { 'result': int(status), 'message': message, 'logs': logs }
        self.log_offset= len(list(self.logs))
        self.save()
        return message





    # def get_session(self):
    #     """
    #
    #     :return:
    #     """
    #     session= Session.load(Agent.session)
    #     return session
    #
    # def get_device(self):
    #     """
    #
    #     :return:
    #     """
    #     device= Device.load(Agent.device)
    #     return device
    #
    #
    # @classmethod
    # def get_collection_devices(cls, members, platform='default'):
    #     """
    #
    #     :members:list list of members or list of configuration members
    #     :return:
    #     """
    #     selected_devices = []
    #     for member_name in members:
    #         device = Device.get(Device.name == member_name)
    #         profile = device.get_profile()
    #         if profile.collection == cls.role:
    #             selected_devices.append(device)
    #     return selected_devices
    #
    #
    # # method to override
    # @classmethod
    # def session_configuration(cls, members, platform="default"):
    #     configuration= {}
    #     devices = cls.get_collection_devices(members, platform=platform)
    #     for device in devices:
    #         options= device.get_options()
    #         configuration[device.name]= options
    #     return configuration



#
# platform  entities
#

class Profile(Model):
    """

    """
    collection= 'profile'

    #name= TextField(primary_key=True)
    name = TextField(index=True)
    collection= TextField(index= True)
    data= JSONField()


class Enterprise(Model):
    """


    """
    collection= 'enterprise'

    name= TextField(index=True)
    data= JSONField()

    def get_sites(self):
        graph = Graph(self.database, namespace='links')
        X = graph.v.X  # Create a variable placeholder.
        results = graph.search({'s': self.get_hash_id(), 'p': 'HAS', 'o': X})
        sites=[]
        for key in results['X']:
            site= Site.load(primary_key=key,convert_key=False)
            sites.append(site)
        return sites

class Site(Model):

    collection= "site"

    name= TextField(index=True)
    enterprise= TextField(index=True)
    data=JSONField()

    def get_enterprise(self):
        return Enterprise.get(Enterprise.name == self.enterprise)


    def get_devices(self):
        """


        :return:
        """
        graph = Graph(self.database, namespace='links')
        X = graph.v.X  # Create a variable placeholder.
        results = graph.search({'s': self.get_hash_id(), 'p': 'has_device', 'o': X})
        devices=[]
        for device_key in results['X']:
            device= Device.load(primary_key=device_key,convert_key=False)
            devices.append(device)
        return devices

class Tag(Model):

    collection= "tag"

    name= TextField(index=True)
    value= TextField()

class FeatureAccessCode(Model):

    collection= 'feature_acess_code'

    name= TextField(index=True)
    code= TextField(index=True)
    description= TextField()


class Hub(Model):
    """

    """
    collection= 'hub'

    _url_base_port= 5000
    _url_base_prefix= '/restop/api/v1'

    name= TextField(index=True)
    #url_prefix = TextField(default='/restop/api/v1/hub')
    url_prefix = TextField(default='/restop/api/v1')
    host= TextField()
    port= IntegerField()
    redis_host= TextField(default='localhost')
    redis_port= IntegerField(default=6379)
    redis_db= IntegerField()
    master= TextField()

    registered_host= TextField(default='no-host')
    hub_url= TextField(default='no-url')
    # containers
    members= ListField()


    @classmethod
    def create(cls, name, indice, members ,**kwargs):
        """
        Create a new hub


        """
        members= members or None

        host=kwargs.get('host','localhost')
        port= cls._url_base_port + int(indice)
        url_prefix= kwargs.get('url_prefix',cls._url_base_prefix)

        redis_connection = cls.dabase_connection()

        redis_host= kwargs.get('redis_host',redis_connection['host'])
        redis_port= kwargs.get('redis_port',redis_connection['port'])
        redis_db= kwargs.get('redis_db',indice)


        master= kwargs.get('master','http://localhost:%d%s' % (cls._url_base_port,cls._url_base_prefix))

        instance = cls(
            name=name,host=host,port=port,url_prefix=url_prefix,
            redis_host=redis_host,redis_port=redis_port,redis_db=redis_db,
            master=master
        )
        instance.hub_url= instance.get_hub_url()
        instance.save()
        if members:
            for member in members:
                instance.members.append(member)
        return instance


    def get_host(self):
        """

        :return:
        """
        if self.registered_host == 'no-host':
            # has not been registered return host
            return self.host
        else:
            # has been registered
            return self.registered_host




    def get_hub_url(self):
        """
            return url of the hub

            "http://localhost:5001/restop/api/v1/hub"

        :return:
        """
        url= "http://%s:%s%s" % (self.get_host(), self.port,self.url_prefix)
        return url


    def register_host(self,host):
        """
            set registered_host
        :param host: string
        :return:
        """
        self.registered_host= host
        self.hub_url= self.get_hub_url()
        self.save()




class Platform(Model):
    """

    """
    collection= 'platform'

    _hub_mode= 'auto'
    #_hub_mode = 'yml'

    name= TextField(index=True,default='default')
    data= JSONField()
    hash= TextField()

    # list of all collections
    collections= SetField()

    def get_info(self):
        """

        :return:
        """
        info= self._data['data']['platform']
        return info


    def clear(self):
        """

        :return:
        """
        return


    def setup(self,master=True):
        """

        :return:
        """

        # setup links
        graph= Graph(self.database,namespace='links')


        # setup profiles
        profiles_data= self.data['profiles']
        for profile_name,profile_data in profiles_data.iteritems():
            #print profile_name, profiles_data
            # add collection to platform list
            collection = profile_data['collection']
            self.collections.add(collection)
            # create profile
            d= Profile.create(name=profile_name,collection=collection,data=profile_data)

        # setup enterprises
        data= self.data.get('enterprises',{})
        for name,value in data.iteritems():
            e= Enterprise.create(name=name,data=value)

        # setup site
        data= self.data.get('sites',{})
        for name,value in data.iteritems():
            site= Site.create(name=name,enterprise=value.get('enterprise','default'),data=value)
            enterprise=site.get_enterprise()
            # add links enterprise HAS site
            graph.store(enterprise.get_hash_id(),'HAS',site.get_hash_id())

        # setup devices
        devices_data= self.data['devices']
        for device_name,device_data in devices_data.iteritems():
            #print device_name, devices_data
            profile_name= device_data.get('profile','default')
            d= Device.create(name=device_name,
                             profile= profile_name,
                             data=device_data)

            # add link: device has_profile profile
            profile= Profile.get(Profile.name==profile_name)
            graph.store(d.get_hash_id(), 'has_profile', profile.get_hash_id())
            # add link: site has_device_device
            site_id= device_data.get('site',None)
            if site_id:
                site= Site.get(Site.name==site_id)
                graph.store(site.get_hash_id(),'has_device',d.get_hash_id())

        # setup tags
        data= self.data.get('destinations',{})
        for name,value in data.iteritems():
            e= Tag.create(name=name,value=value)

        # setup facs
        data = self.data.get('FeatureAccessCode', {})
        for name, value in data.iteritems():
            description= ""
            if len(value)>1:
                description= value[1]
            e = FeatureAccessCode.create(name=name,code=value[0],description=description)

        #
        # setup hubs
        #
        if master:
            if self._hub_mode == 'yml':
                # setup hubs form yml file
                platform= self.data['platform']
                hub_data= platform.get('hubs',[])
                if hub_data:
                    assert isinstance(hub_data,list) , "section platforms/hubs must be a list"
                    for indice,hub_element in enumerate(hub_data):
                        name= hub_element.pop('name')
                        if 'members' in hub_element:
                            members= hub_element.pop('members')
                        else:
                            members=[]

                        hub= Hub.create(name,indice+1,members= members,** hub_element)
            #
            else:
                # setup default hubs automaticaly:
                # one hub for each collection, containing all devices
                for index,collection_name in enumerate(sorted(self.collections)):
                    # for each device of this collection:
                    hub= Hub.create(collection_name,index+1,members=[])
                for device in Device.all():
                    hub_name= device.collection
                    hub= Hub.get(Hub.name==hub_name)
                    hub.members.append(device.name)
                    # create link  device - has_hub -> hub
                    graph.store(device.get_hash_id(),'has_hub',hub.get_hash_id())


                # update hub info with platform info
                platform= self.data['platform']
                hub_data= platform.get('hubs',[])
                if hub_data:
                    assert isinstance(hub_data,list) , "section platforms/hubs must be a list"
                    for hub_element in hub_data:
                        # for each hub entry
                        hub_name= hub_element.pop('name')
                        for topic in ['host','port']:
                            if topic in hub_element:
                                # overwrite topic
                                # find host
                                hub=  Hub.get(Hub.name==hub_name)
                                if hub:
                                    # update existing hub
                                    hub._data[topic]=hub_element[topic]
                                    hub.save()
                                else:
                                    # not implemented topic
                                    raise NotImplementedError('multi-hub not implemented')

        return





    # def setup(self):
    #     """

    #     :return:
    #     """

    #     # setup links
    #     graph= Graph(self.database,namespace='links')


    #     # setup profiles
    #     profiles_data= self.data['profiles']
    #     for profile_name,profile_data in profiles_data.iteritems():
    #         #print profile_name, profiles_data
    #         # add collection to platform list
    #         collection = profile_data['collection']
    #         self.collections.add(collection)
    #         # create profile
    #         d= Profile.create(name=profile_name,collection=collection,data=profile_data)

    #     # setup enterprises
    #     data= self.data.get('enterprises',{})
    #     for name,value in data.iteritems():
    #         e= Enterprise.create(name=name,data=value)

    #     # setup site
    #     data= self.data.get('sites',{})
    #     for name,value in data.iteritems():
    #         site= Site.create(name=name,enterprise=value.get('enterprise','default'),data=value)
    #         enterprise=site.get_enterprise()
    #         # add links enterprise HAS site
    #         graph.store(enterprise.get_hash_id(),'HAS',site.get_hash_id())

    #     # setup devices
    #     devices_data= self.data['devices']
    #     for device_name,device_data in devices_data.iteritems():
    #         #print device_name, devices_data
    #         profile_name= device_data.get('profile','default')
    #         d= Device.create(name=device_name,
    #                          profile= profile_name,
    #                          data=device_data)

    #         # add link: device has_profile profile
    #         profile= Profile.get(Profile.name==profile_name)
    #         graph.store(d.get_hash_id(), 'has_profile', profile.get_hash_id())
    #         # add link: site has_device_device
    #         site_id= device_data.get('site',None)
    #         if site_id:
    #             site= Site.get(Site.name==site_id)
    #             graph.store(site.get_hash_id(),'has_device',d.get_hash_id())

    #     # setup tags
    #     data= self.data.get('destinations',{})
    #     for name,value in data.iteritems():
    #         e= Tag.create(name=name,value=value)

    #     # setup facs
    #     data = self.data.get('FeatureAccessCode', {})
    #     for name, value in data.iteritems():
    #         description= ""
    #         if len(value)>1:
    #             description= value[1]
    #         e = FeatureAccessCode.create(name=name,code=value[0],description=description)

    #     #
    #     # setup hubs
    #     #
    #     if self._hub_mode == 'yml':
    #         # setup hubs form yml file
    #         platform= self.data['platform']
    #         hub_data= platform.get('hubs',[])
    #         if hub_data:
    #             assert isinstance(hub_data,list) , "section platforms/hubs must be a list"
    #             for indice,hub_element in enumerate(hub_data):
    #                 name= hub_element.pop('name')
    #                 if 'members' in hub_element:
    #                     members= hub_element.pop('members')
    #                 else:
    #                     members=[]

    #                 hub= Hub.create(name,indice+1,members= members,** hub_element)
    #     #
    #     else:
    #         # setup default hubs automaticaly:
    #         # one hub for each collection, containing all devices
    #         for index,collection_name in enumerate(sorted(self.collections)):
    #             # for each device of this collection:
    #             hub= Hub.create(collection_name,index+1,members=[])
    #         for device in Device.all():
    #             hub_name= device.collection
    #             hub= Hub.get(Hub.name==hub_name)
    #             hub.members.append(device.name)
    #             # create link  device - has_hub -> hub
    #             graph.store(device.get_hash_id(),'has_hub',hub.get_hash_id())


    #         # update hub info with platform info
    #         platform= self.data['platform']
    #         hub_data= platform.get('hubs',[])
    #         if hub_data:
    #             assert isinstance(hub_data,list) , "section platforms/hubs must be a list"
    #             for hub_element in hub_data:
    #                 # for each hub entry
    #                 hub_name= hub_element.pop('name')
    #                 for topic in ['host','port']:
    #                     if topic in hub_element:
    #                         # overwrite topic
    #                         # find host
    #                         hub=  Hub.get(Hub.name==hub_name)
    #                         if hub:
    #                             # update existing hub
    #                             hub._data[topic]=hub_element[topic]
    #                             hub.save()
    #                         else:
    #                             # not implemented topic
    #                             raise NotImplementedError('multi-hub not implemented')


    #     return


class Apidoc(Model):
    """


    """
    #name= TextField(index=True,default='default')
    platform_name= TextField(index=True,default='defaut')

    collections= SetField()
    session_collection = TextField(default='sessions')
    collection_operations = SetField()
    operations= SetField()

    keywords= HashField()

    @classmethod
    def create(cls,  session_collection='sessions',collections=None,collection_operations=None,operations=None,
               collection=None,keywords=None,**kwargs):
        """

        :param session_collection:
        :param collections:
        :param collection_operations:
        :param kwargs:
        :return:
        """
        collections=collections or []
        operations= operations or []
        collection_operations= collection_operations or []

        instance = cls( session_collection=session_collection, **kwargs)
        instance.save()
        for collection in collections:
            instance.collections.add(collection)
        for operation in operations:
            instance.operations.add(operation)
        for operation in collection_operations:
            instance.collection_operations.add(operation)
        if collection is not None:
            instance.keywords[collection]=keywords
        return instance


    @classmethod
    def create_or_update(cls,platform_name='default',**kwargs):
        """


        :param platform_name:
        :param session_collection:
        :param collections:
        :param collection_operations:
        :param operations:
        :param kwargs:
        :return:
        """

        try:
            # find existing apidoc for this platform
            api= Apidoc.get(Apidoc.platform_name==platform_name)
            # found: update it
            api.update(kwargs)
            if kwargs.get('collection',None):
                api.keywords[kwargs['collection']]=kwargs.get('keywords','')
        except ValueError, e:
            # create it
            api= Apidoc.create(platform_name=platform_name,**kwargs)
        return api

    def export(self):
        """

        :return:
        """
        apidoc = {}
        apidoc['platform_name']=self.platform_name
        apidoc['session_collection'] = self.session_collection

        apidoc['collections'] = list(self.collections)
        apidoc['collection_operations'] = list(self.collection_operations)
        apidoc['operations'] = list(self.operations)
        return apidoc


    def update(self,data=None):
        """

        :param data:
        :return:
        """
        if data:
            operations = data.get('operations', [])
            collection_operations = data.get('collection_operations', [])
            for operation in operations:
                self.operations.add(operation)
            for collection_operation in collection_operations:
                self.collection_operations.add(collection_operation)

    @classmethod
    def from_autodoc(cls,autodoc,collections,platform_name):
        """

        :param doc:
        :return:
        """
        data = {'operations': [], 'collection_operations': [], 'collections': collections}
        for operation in autodoc['item_methods'].keys():
            op = operation.replace('op_', '')
            data['operations'].append(op)
        for operation in autodoc['collection_methods'].keys():
            op = operation.replace('op_col_', '')
            data['collection_operations'].append(op)

        return data