
from walrus import Hash,List


# journal list
journal_redis_key= "dashboard:journal"

# main hash
dashboard_main_key= "dashboard:main"


class Dashboard(object):
    """



    """



    def __init__(self,redis_db):
        """

        :param redis_db:
        """
        self.database=redis_db

        self.main= Hash(redis_db,dashboard_main_key)

        self.journal=Journal(redis_db)



class Journal(object):
    """



    """
    def __init__(self,redis_db):
        """

        :param redis_db:
        """
        self.database=redis_db
        self.log= List(self.database,journal_redis_key)

    def write(self,message):
        """


        :param message:
        :return:
        """
        self.log.append(message)
