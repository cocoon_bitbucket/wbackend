

from walrus import List

# journal list
journal_redis_key= "dashboard:journal"


class Journal(object):
    """



    """
    def __init__(self,redis_db):
        """

        :param redis_db:
        """
        self.database=redis_db
        self.log= List(self.database,journal_redis_key)

    def write(self,message):
        """


        :param message:
        :return:
        """
        self.log.append(message)