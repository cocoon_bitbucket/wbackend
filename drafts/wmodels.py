from copy import deepcopy
import walrus
from walrus import Database,TextField,DateField

#from walrus.models import BaseModel, _with_metaclass,Field,AutoIncrementField,Query

from walrus.models import *
from walrus.models import _with_metaclass,_ScalarField,_ContainerField

import datetime


class PluginBaseModel(type):
    """

    """
    def __new__(cls, name, bases, attrs):

        #
        # walrus BaseModel code
        #
        if not bases:
            return super(PluginBaseModel, cls).__new__(cls, name, bases, attrs)

        # Declarative base juju.
        ignore = set()
        primary_key = None

        for key, value in attrs.items():
            if isinstance(value, Field) and value._primary_key:
                primary_key = (key, value)

        for base in bases:
            for key, value in base.__dict__.items():
                if key in attrs:
                    continue
                if isinstance(value, Field):
                    if value._primary_key and primary_key:
                        ignore.add(key)
                    else:
                        if value._primary_key:
                            primary_key = (key, value)
                        attrs[key] = deepcopy(value)

        if not primary_key:
            attrs['_id'] = AutoIncrementField()
            primary_key = ('_id', attrs['_id'])

        model_class = super(PluginBaseModel, cls).__new__(cls, name, bases, attrs)
        model_class._data = None

        defaults = {}
        fields = {}
        indexes = []
        for key, value in model_class.__dict__.items():
            if isinstance(value, Field) and key not in ignore:
                value.add_to_class(model_class, key)
                if value._index:
                    indexes.append(value)
                fields[key] = value
                if value._default:
                    defaults[key] = value._default

        model_class._defaults = defaults
        model_class._fields = fields
        model_class._indexes = indexes
        model_class._primary_key = primary_key[0]
        model_class._query = Query(model_class)

        #
        # add plugin system
        #
        if not hasattr(model_class, 'plugins'):
            # This branch only executes when processing the mount point itself.
            # So, since this is a new plugin type, not an implementation, this
            # class shouldn't be registered as a plugin. Instead, it sets up a
            # list where plugins can be registered later.
            model_class.plugins = []
        else:
            # This must be a plugin implementation, which should be registered.
            # Simply appending it to the list is all that's needed to keep
            # track of it later.
            model_class.plugins.append(model_class)


        return model_class

    #
    #  plugins methods
    #

    def get_plugins(cls, *args, **kwargs):
        return sorted([p(*args, **kwargs) for p in cls.plugins])


    def select_plugin(cls, role):
        """
            role is either user, workspace, run , report

        """
        for item in reversed(cls.plugins):
            if item.role == role:
                # warning in case of duplicates: return only the last one
                return item

        # not found: return None
        raise KeyError('no such backend plugin: %s' % role)


    def list_plugins(cls):
        """

        :return:
        """
        pls = [item for item in reversed(cls.plugins)]
        return pls


#
# same walrus Model but with PluginBaseModel metaclass
#
class Model(_with_metaclass(PluginBaseModel)):
    """
    A collection of fields to be stored in the database. Walrus
    stores model instance data in hashes keyed by a combination of
    model name and primary key value. Instance attributes are
    automatically converted to values suitable for storage in Redis
    (i.e., datetime becomes timestamp), and vice-versa.

    Additionally, model fields can be ``indexed``, which allows
    filtering. There are three types of indexes:

    * Absolute
    * Scalar
    * Full-text search

    Absolute indexes are used for values like strings or UUIDs and
    support only equality and inequality checks.

    Scalar indexes are for numeric values as well as datetimes,
    and support equality, inequality, and greater or less-than.

    The final type of index, FullText, can only be used with the
    :py:class:`TextField`. FullText indexes allow search using
    the ``match()`` method. For more info, see :ref:`fts`.
    """
    #: **Required**: the :py:class:`Database` instance to use to
    #: persist model data.
    database = None

    #: **Optional**: namespace to use for model data.
    namespace = None

    #: **Required**: character to use as a delimiter for indexes, default "."
    index_separator = ':'

    def __init__(self, *args, **kwargs):
        self._data = {}
        self._load_default_dict()
        for k, v in kwargs.items():
            setattr(self, k, v)
            # add non declared attributes
            if not k in self._fields:
                self._data[k]=v
        return

    def __repr__(self):
        return '<%s: %s>' % (type(self).__name__, self.get_id())

    def _load_default_dict(self):
        for field_name, default in self._defaults.items():
            if callable(default):
                default = default()
            setattr(self, field_name, default)

    def incr(self, field, incr_by=1):
        """
        Increment the value stored in the given field by the specified
        amount. Any indexes will be updated at the time ``incr()`` is
        called.

        :param Field field: A field instance.
        :param incr_by: An ``int`` or ``float``.

        Example:

        .. code-block:: python

            # Retrieve a page counter object for the given URL.
            page_count = PageCounter.get(PageCounter.url == url)

            # Update the hit count, persisting to the database and
            # updating secondary indexes in one go.
            page_count.incr(PageCounter.hits)
        """
        model_hash = self.to_hash()

        # Remove the value from the index.
        for index in field.get_indexes():
            index.remove(self)

        if isinstance(incr_by, int):
            new_val = model_hash.incr(field.name, incr_by)
        else:
            new_val = model_hash.incr_float(field.name, incr_by)
        setattr(self, field.name, new_val)

        # Re-index the new value.
        for index in field.get_indexes():
            index.save(self)

        return new_val

    def get_id(self):
        """
        Return the primary key for the model instance. If the
        model is unsaved, then this value will be ``None``.
        """
        try:
            return getattr(self, self._primary_key)
        except KeyError:
            return None

    def get_hash_id(self):
        return self._query.get_primary_hash_key(self.get_id())

    def _get_data_dict(self):
        data = {}
        for name, field in self._fields.items():
            if name in self._data:
                data[name] = field.db_value(self._data[name])
        # handle non declared attributes
        for name,value in self._data.iteritems():
            if not name in data:
                data[name]=value

        return data

    def to_hash(self):
        """
        Return a :py:class:`Hash` instance corresponding to the
        raw model data.
        """
        return self.database.Hash(self.get_hash_id())

    @classmethod
    def create(cls, **kwargs):
        """
        Create a new model instance and save it to the database.
        Values are passed in as keyword arguments.

        Example::

            user = User.create(first_name='Charlie', last_name='Leifer')
        """
        instance = cls(**kwargs)
        instance.save()
        return instance

    @classmethod
    def all(cls):
        """
        Return an iterator that successively yields saved model
        instances. Models are saved in an unordered :py:class:`Set`,
        so the iterator will return them in arbitrary order.

        Example::

            for note in Note.all():
                print note.content

        To return models in sorted order, see :py:meth:`Model.query`.
        Example returning all records, sorted newest to oldest::

            for note in Note.query(order_by=Note.timestamp.desc()):
                print note.timestamp, note.content
        """
        for result in cls._query.all_index():
            yield cls.load(result, convert_key=False)

    @classmethod
    def query(cls, expression=None, order_by=None):
        """
        Return model instances matching the given expression (if
        specified). Additionally, matching instances can be returned
        sorted by field value.

        Example::

            # Get administrators sorted by username.
            admin_users = User.query(
                (User.admin == True),
                order_by=User.username)

            # List blog entries newest to oldest.
            entries = Entry.query(order_by=Entry.timestamp.desc())

            # Perform a complex filter.
            values = StatData.query(
                (StatData.timestamp < datetime.date.today()) &
                ((StatData.type == 'pv') | (StatData.type == 'cv')))

        :param expression: A boolean expression to filter by.
        :param order_by: A field whose value should be used to
            sort returned instances.
        """
        if expression is not None:
            executor = Executor(cls.database)
            result = executor.execute(expression)
        else:
            result = cls._query.all_index()

        if order_by is not None:
            desc = False
            if isinstance(order_by, Desc):
                desc = True
                order_by = order_by.node

            alpha = not isinstance(order_by, _ScalarField)
            result = cls.database.sort(
                result.key,
                by='*->%s' % order_by.name,
                alpha=alpha,
                desc=desc)
        elif isinstance(result, ZSet):
            result = result.iterator(reverse=True)

        for hash_id in result:
            yield cls.load(hash_id, convert_key=False)

    @classmethod
    def query_delete(cls, expression=None):
        """
        Delete model instances matching the given expression (if
        specified). If no expression is provided, then all model instances
        will be deleted.

        :param expression: A boolean expression to filter by.
        """
        if expression is not None:
            executor = Executor(cls.database)
            result = executor.execute(expression)
        else:
            result = cls._query.all_index()

        for hash_id in result:
            cls.load(hash_id, convert_key=False).delete()

    @classmethod
    def get(cls, expression):
        """
        Retrieve the model instance matching the given expression.
        If the number of matching results is not equal to one, then
        a ``ValueError`` will be raised.

        :param expression: A boolean expression to filter by.
        :returns: The matching :py:class:`Model` instance.
        :raises: ``ValueError`` if result set size is not 1.
        """
        executor = Executor(cls.database)
        result = executor.execute(expression)
        if len(result) != 1:
            raise ValueError('Got %s results, expected 1.' % len(result))
        return cls.load(result._first_or_any(), convert_key=False)

    @classmethod
    def load(cls, primary_key, convert_key=True):
        """
        Retrieve a model instance by primary key.

        :param primary_key: The primary key of the model instance.
        :returns: Corresponding :py:class:`Model` instance.
        :raises: ``KeyError`` if object with given primary key does
            not exist.
        """
        if convert_key:
            primary_key = cls._query.get_primary_hash_key(primary_key)
        if not cls.database.hash_exists(primary_key):
            raise KeyError('Object not found.')
        raw_data = cls.database.hgetall(primary_key)
        data = {}
        for name, field in cls._fields.items():
            if isinstance(field, _ContainerField):
                continue
            elif name not in raw_data:
                data[name] = None
            else:
                data[name] = field.python_value(raw_data[name])
        # add non registerd fields
        for name,value in raw_data.iteritems():
            if not name in data:
                data[name]= value

        return cls(**data)

    @classmethod
    def count(cls):
        """
        Return the number of objects in the given collection.
        """
        return len(cls._query.all_index())

    def delete(self, for_update=False):
        """
        Delete the given model instance.
        """
        hash_key = self.get_hash_id()
        try:
            original_instance = self.load(hash_key, convert_key=False)
        except KeyError:
            return

        # Remove from the `all` index.
        all_index = self._query.all_index()
        all_index.remove(hash_key)

        # Remove from the secondary indexes.
        for field in self._indexes:
            for index in field.get_indexes():
                index.remove(original_instance)

        if not for_update:
            for field in self._fields.values():
                if isinstance(field, _ContainerField):
                    field._delete(self)

        # Remove the object itself.
        self.database.delete(hash_key)

    def save(self):
        """
        Save the given model instance. If the model does not have
        a primary key value, Walrus will call the primary key field's
        ``generate_key()`` method to attempt to generate a suitable
        value.
        """
        pk_field = self._fields[self._primary_key]
        if not self._data.get(self._primary_key):
            setattr(self, self._primary_key, pk_field._generate_key())
            require_delete = False
        else:
            require_delete = True

        if require_delete:
            self.delete(for_update=True)

        data = self._get_data_dict()
        hash_obj = self.to_hash()
        hash_obj.clear()
        hash_obj.update(data)

        all_index = self._query.all_index()
        all_index.add(self.get_hash_id())

        for field in self._indexes:
            for index in field.get_indexes():
                index.save(self)



if __name__=="__main__":


    db = Database(host='localhost', port=6379, db=0)


    db.flushall()

    class Item(Model):

        role= "item"
        index_separator = ':'
        database= db

        data = JSONField()

    class User(Model):

         role= 'user'
         index_separator = ':'

         database = db
         name = TextField(primary_key=True)
         dob = DateField(index=True)
         data = JSONField()


    class Device(Model):

        role= "device"
        namespace = 'ptf1'
        database = db


        name = TextField(index=True)
        myset= ZSetField()

        params= JSONField()

        #data= JSONField()

    class RQueue(Model):
        """

        """
        role= "queue"
        database = db

        name= TextField(index=True)

        stdin=  ListField()
        stdout= ListField()



    Item.create(data= {'name':'wild'})
    Item.create(data= {'name':'wild2'})



    User.create(name='Charlie', dob=datetime.date(1983, 1, 1))
    #<User: Charlie>

    d=Device.create(name='tv',other='wild',params= {'p1':'p1','p2':'p2'})
    d.myset.add('hello',0)

    d2= Device.load('1')

    d2.params={'p0':'p0','p2':'p2'}
    d2.save()

    d3= Device.load('1')

    names_dobs = [
         ('Huey', datetime.date(2011, 6, 1)),
         ('Zaizee', datetime.date(2012, 5, 1)),
         ('Mickey', datetime.date(2007, 8, 1)),
    ]


    User_model= Model.select_plugin('user')

    for name, dob in names_dobs:
         User_model.create(name=name, dob=dob, data={'bidon':"wild"})


    q= RQueue.create( name='queue_1')
    q.save()

    q.stdin.append('hello')

    q1= RQueue.query(RQueue.name=='queue_1')
    q1=q1.next()

    print 'Done'



