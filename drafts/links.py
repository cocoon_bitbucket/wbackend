"""


    ISA   n:1

    HAS    1:n

    LINKED  n:n

"""
from walrus import *
from wbackend.graph import Graph


from wmodels import Model



db= Database(host='localhost', port=6379, db=0)
db.flushall()

# Begin by instantiating a `Graph` object.
graph = Graph(db,namespace='links')


class RawItem(Model):
    role = "item"
    index_separator = ':'
    database = db

    data = JSONField()


class Item(Model):
    role = "item"
    index_separator = ':'
    database = db


    name = TextField(index=True)



class User(Model):
    role = 'user'
    index_separator = ':'

    database = db
    name = TextField(index=True)
    dob = DateField(index=True)
    data = JSONField()


class Session(Model):
    """


    """


    role = 'user'
    index_separator = ':'

    database = db


    start= DateTimeField(index=True,default=datetime.datetime.now())
    status= TextField(index=True, default='created')

    agents= ListField()

    log= ListField()


class Device(Model):
    role = "device"
    #namespace = 'ptf1'
    database = db

    name = TextField(index=True)
    myset = ZSetField()

    params = JSONField()

    # data= JSONField()


class Agent(Item):
    """

    """

    session=TextField(index=True)

    stdin= ListField()
    stdout= ListField()
    status= TextField(default='created',index=True)


    def get_session(self):
        return Session.load(self.session)


class RQueue(Model):
    """

    """
    role = "queue"
    database = db

    name = TextField(index=True)

    stdin = ListField()
    stdout = ListField()



i=Item.create(name='toto')
i2= Item.create(name='titi')


u= User.create(name='Alice', dob=datetime.date(1983, 1, 1))
u1= User.create(name='Bob', dob=datetime.date(1985, 2, 2))


d = Device.create(name='tv', other='wild', params={'p1': 'p1', 'p2': 'p2'})
d2 = Device.create(name='lb', other='livebox', params={'p1': 'p1', 'p2': 'p2'})



graph.store_many(
    [('user:id:1', 'has_device', 'device:id:1'),
    ('user:id:2', 'has_device', 'devicde:id:2')]


)

d= Device.load('1')

q = RQueue.create(name='queue_1')
q.save()

q.stdin.append('hello')
q.stdout.append('bye')

q2 = RQueue.create(name='queue_2')
q2.save()

q2.stdin.append('hello')
q2.stdout.append('bye')

q1 = RQueue.query(RQueue.name == 'queue_1')
q1 = q1.next()


graph.store_many(
    [('user:id:1', 'HAS', 'rqueue:id:1'),
    ('user:id:2', 'HAS', 'rqueue:id:2')]
)


r= lambda n: n== 'Bob'
u1=User.get(User.name=='Bob')


s1= Session.create()
s1.save()

s1.log.append("starting session %s" % s1.get_id())


a1= Agent.create(name='tv', session=s1.get_id())
a1.save()

a2= Agent.create(name='lb', session=s1.get_id())
a2.save()


s2= Session.create()
s2.save()

s2.log.append("starting session %s" % s2.get_id())


a1= Agent.create(name='tv', session=s2.get_id())
a1.save()

a2= Agent.create(name='lb', session=s2.get_id())
a2.save()

print a2.to_hash()
print s2.to_hash()


l= list(Agent.query(Agent.name=='tv'))
print l




print "Done"


