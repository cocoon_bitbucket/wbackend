# graph
from walrus import Database

db= Database(host='localhost', port=6379, db=0)


from walrus.graph import Graph
# Begin by instantiating a `Graph` object.
graph = Graph(db,namespace='ptf1')

# Store my friends.
# "charlie" is subject, "friends" is predicate, "huey" is object.
graph.store('charlie', 'friends', 'huey')

# Can also store multiple relationships at once.
graph.store_many(
    [('charlie', 'friends', 'zaizee'),
    ('charlie', 'friends', 'nuggie')])

# Store where people live.
graph.store_many(
    [('huey', 'lives', 'Kansas'),
    ('zaizee', 'lives', 'Missouri'),
    ('nuggie', 'lives', 'Kansas'),
    ('mickey', 'lives', 'Kansas')])

# We are now ready to search. We'll use a variable (X) to indicate
# the value we're interested in.
X = graph.v.X  # Create a variable placeholder.

# In the first clause we indicate we are searching for my friends.
# In the second clause, we only want those friends who also live
# in Kansas.
results = graph.search(
    {'s': 'charlie', 'p': 'friends', 'o': X},
    {'s': X, 'p': 'lives', 'o': 'Kansas'})

print results
assert list(results['X']) == ['huey', 'nuggie']

print "Done"