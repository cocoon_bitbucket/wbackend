import walrus
from walrus import *
db = Database(host='localhost', port=6379, db=0)


db.flushall()



# simple access dict
db['walrus'] = 'tusk'
print db['walrus']

t= db['walrus']
assert t == 'tusk'

assert db.get('non-existant') is None


# hash
h = db.Hash('charlie')
h.update(name='Charlie', favorite_cat='Huey')



# list
l = db.List('names')
l.extend(['charlie', 'huey', 'mickey', 'zaizee'])


print l[:2]
#['charlie', 'huey']
print l[-2:]
#['mickey', 'zaizee']
print l.pop()
#'zaizee'
l.prepend('scout')
#4L
len(l)
#4


# sets

s1 = db.Set('s1')
s2 = db.Set('s2')
s1.add(*range(5))
#5
s2.add(*range(3, 8))
#5

print (s1 | s2)
#{'0', '1', '2', '3', '4', '5', '6', '7'}
print (s1 & s2)
#{'3', '4'}
print(s1 - s2)
#{'0', '1', '2'}

#r= s1 -= s2


print (s1.members())
#{'0', '1', '2'}

print(len(s1))
#3


# sorted sets

z1 = db.ZSet('z1')
z1.add('charlie', 31, 'huey', 3, 'mickey', 6, 'zaizee', 2.5)
#4
z1['huey'] = 3.5


print(z1[:'mickey'])  # Who is younger than Mickey?
#['zaizee', 'huey']

print(z1[-2:])  # Who are the two oldest people?
#['mickey', 'charlie']

print(z1[-2:, True])  # Who are the two oldest, and what are their ages?
#[('mickey', 6.0), ('charlie', 31.0)]


# arrays

a = db.Array('arr')
a.extend(['foo', 'bar', 'baz', 'nugget'])
a[-1] = 'nize'
print list(a)
#['foo', 'bar', 'baz', 'nize']
print(a.pop(2))
#'baz'



# auto complete


database = Database()
ac = database.autocomplete()



phrases = [
     'the walrus and the carpenter',
     'walrus tusks',
     'the eye of the walrus']

for phrase in phrases:
     ac.store(phrase)


print list(ac.search('wal'))
#['the walrus and the carpenter',
# 'walrus tusks',
# 'the eye of the walrus']

print list(ac.search('wal car'))
#['the walrus and the carpenter']

ac.remove('walrus tusks')

ac.exists('the walrus and the carpenter')



# cache
from walrus import *
db = Database()
cache = db.cache()

cache.set('foo', 'bar', 2)  # Set foo=bar, expiring in 10s.
print(cache.get('foo'))
#'bar'

time.sleep(2)
print cache.get('foo') is None
#True


@cache.cached(timeout=2)
def get_time():
     return datetime.datetime.now()

print get_time()  # First call, return value cached.
#2015-01-07 18:26:42.730638

print get_time()  # Hits the cache.
#2015-01-07 18:26:42.730638

time.sleep(2)  # Wait for cache to expire then call again.
print get_time()
#2015-01-07 18:26:53.529011



class Clock(object):
     @cache.cached_property()
     def now(self):
         return datetime.datetime.now()

clock= Clock()
print clock.now
#2015-01-12 21:10:34.335755

print clock.now
#2015-01-12 21:10:34.335755


# asynchronus cache

import time

@cache.cache_async()
def get_now(seed=None):
     print 'About to sleep for 5 seconds.'
     time.sleep(10)
     return datetime.datetime.now()



result = get_now()
#About to sleep for 5 seconds.
print result
#<function _get_value at 0x7fe3a4685de8>


try:
    result(block=False)
except Exception ,e:
    pass



