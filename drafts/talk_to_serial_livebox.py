import time

from wbackend.model import Database
from restop_queues import QueueClient



QUEUE_HUB_HOST= "192.168.1.21"
QUEUE_HUB_KEY= "queuehub:id:default"


#QUEUE_NAME= "livebox_serial"
QUEUE_NAME= "usb2"



redis_db= Database(host= QUEUE_HUB_HOST)


# create hub client
hub_client = QueueClient(QUEUE_HUB_KEY, redis_db=redis_db)

# ask hub to start echo server
hub_client.send('start_adapter', dict(name=QUEUE_NAME, run_mode='thread'))
time.sleep(2)
sattus,queue_key = hub_client.receive()

# start echo queue client
queue_client = QueueClient(queue_key)

# test it
queue_client.open()
queue_client.write('hello\n')
time.sleep(2)
line = queue_client.readline()
assert line == 'hello'
queue_client.close()

# shutdown echo server
queue_client.exit()

time.sleep(2)
