import os
import threading
from robot import libdoc

import logging



class_pattern='''\
# -*- coding: utf-8 -*-

class %s(object):
    """
        %s
    """

'''

method_pattern='''
    def %s(self, %s **kwargs):
        """
        %s
        """
'''


def gen_robotfrmawork_doc(name, outfile,filetemp="Dummy.py", version="1.0"):
    """


    """
    def wrapper():
        libdoc.libdoc_cli(['--version', version, '--name', name, filetemp, outfile])
    return wrapper

class DocGenerator(object):
    """



    """
    filetemp= 'Dummy.py'

    def __init__(self,name,device_resource,version='1.0'):
        """


        :param name:
        :param device_resource:
        """
        self.name=name
        self.device_resource=device_resource
        self.version=version
        self.data= self.device_resource._autodoc()
        self.outfile= "%s.html" % name
        self.filetemp= "%s.py" % name

    def setup(self):
        """


        :return:
        """

        lines= []
        lines.extend((class_pattern % (self.name,self.data['doc'])).split('\n'))

        for operation, data in self.collection_operations():
            op_lines = method_pattern % (operation, '', self.docstring_lines(data['docstring']))
            lines.extend(op_lines.split('\n'))

        for operation,data in self.operations():
            op_lines= method_pattern %  (operation,'alias,',self.docstring_lines(data['docstring']))
            lines.extend(op_lines.split('\n'))

        self.lines= lines

        # write Dummy.py file
        with open(self.filetemp, "wb") as fout:
            fout.write("\n".join(lines))

        # generate html doc
        current_dir = os.path.dirname(os.path.abspath(__file__))
        os.environ['PYTHONPATH'] = current_dir



        #gen_robotfrmawork_doc(self.name,self.outfile,self.filetemp,self.version)

        t = threading.Thread(
            name='gen_doc',
            target=gen_robotfrmawork_doc(self.name,self.outfile,self.filetemp,self.version))
        t.start()
        t.join()

        #libdoc.libdoc_cli(['--version', self.version, '--name', self.name, self.filetemp, self.outfile])

        return lines

    def collection_operations(self):
        """


        :return:
        """
        for operation in sorted(self.data['collection_methods'].keys()):
            op = operation.replace('op_col_', '')
            yield op,self.data['collection_methods'][operation]

    def operations(self):
        """


        :return:
        """
        for operation in sorted(self.data['item_methods'].keys()):
            op = operation.replace('op_', '')
            yield op,self.data['item_methods'][operation]

    def docstring_lines(self,data,indent= 8 ):
        """

        :param data:
        :param indent:
        :return:
        """
        lines=[]
        if data:
            for line in data.split('\n'):
                if ":param " in line:
                    pass
                lines.append(" " * indent + line)
                lines.append('')
        return "\n".join(lines)

