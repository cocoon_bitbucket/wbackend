
from restop_platform.models import Apidoc

collections= ["root", "oauth2", "files", "apidoc", "platforms", "phone_sessions", 'syprunner_agents',
               'droydrunner_agents',
               "livebox_agents", "tvbox_agents"]

# store api doc
apidoc={}
apidoc['collections']= collections
apidoc['session_collection']= 'phone_sessions'
apidoc['collection_operations']=['adb_devices','get_platform_configuration','ptf_get_user_data','get_sip_address']
apidoc['operations'] = []

operations=set()
# add droydrunner keywords
operations.update(set(['click_by_index', 'native_drag_to', 'ui_object', 'dump', 'fling_backward_horizontally', 'get_object', 'click_on_object', 'scroll_forward_vertically', 'clear_text', 'pinch_in', 'long_click_by_index', 'pinch_out', 'register_press_watcher', 'swipe_right', 'native_gesture', 'fling_forward_horizontally', 'execute_adb_shell_command', 'set_text', 'scroll_to_end_horizontally', 'click_linear_by_text', 'scroll_to_horizontally', 'click', 'swipe_down', 'native_fling', 'open_notification', 'scroll_to_horizontal', 'start_test_agent', 'execute_adb_command', 'native_operation', 'swipe_left', 'scroll_backward_vertically', 'flash', 'fling_forward_vertically', 'unfreeze_screen_rotation', 'wait_for_object_exists', 'type', 'call', 'wait_until_object_gone', 'get_count', 'get_info_of_object', 'scroll_to_vertically', 'scroll_to_vertical', 'set_screen_orientation', 'get_object_data', 'scroll_to_beginning_vertically', 'get_device_info', 'screenshot', 'adb_devices', 'turn_off_screen', 'click_tab_by_text', 'long_click', 'scroll_to_end_vertically', 'open_quick_settings', 'freeze_screen_rotation', 'register_click_watcher', 'swipe_by_coordinates', 'drag_by_coordinates', 'press', 'scroll_backward_horizontally', 'get_child', 'fling_backward_vertically', 'press_home', 'scroll_to_beginning_horizontally', 'wait_until_gone', 'get_count_of_object', 'flatten_ui_objet', 'wait_for_exists', 'list_all_watchers', 'native_scroll', 'click_at_coordinates', 'wait_idle', 'turn_on_screen', 'set_object_text', 'get_info_by_index', 'remove_watchers', 'snapshot', 'scroll_forward_horizontally', 'install', 'stop_test_agent', 'get_sibling', 'swipe_up', 'wait_update', 'get_screen_orientation', 'uninstall']))
# add stb keywords
operations.update(set(['serial_synch','serial_watch','serial_expect','send_key','stb_send_dump','stb_status',
                       'page_get_info_from_live_banner','menu_select','scheduler_start','scheduler_read',
                       'page_action_select','page_get_list','page_get_label_from_list_focus','page_find_in_list',
                       'stb_toolbox_list','stb_toolbox_select','scheduler_init','scheduler_sync','scheduler_close','stb_get_lineup,stb_pcb_cli,stb_info'
                       ]))

apidoc['operations']= list(operations)

#backend.item_new('apidoc','interface',data=apidoc)

api= Apidoc.create(platform_name='default',
                   session_collection='sessions',
                   collections=collections,
                   collection_operations=apidoc['collection_operations'])

print "Done"