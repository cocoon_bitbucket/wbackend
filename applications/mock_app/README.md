mock_app
========

overview
--------
a mock application with

*  an http_server  on localhost:8080
* a console emulator (alpine)  on restop adapter mock:id:serial


use it
------

python mock_starter.py

in a browser

GET localhost:8080/custom
return custom

# post a ls command
POST /mock/console/stdin
body: 'ls -l'

# read result
GET /mock/console/stdout


# read logs
GET /mock/console/logs
