
from wbackend.models import Database , Model

def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)
    return db


def test_first():
    """


    :return:
    """
    db= get_new_db()

    print db.connection_pool.connection_kwargs['host']
    print db.connection_pool.connection_kwargs['port']
    print db.connection_pool.connection_kwargs['db']


    return


if __name__=="__main__":


    #
    test_first()

    print "Done."
