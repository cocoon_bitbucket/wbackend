

from wbackend.model import Database,Model
from applications.samples.models import Samples
from applications.samples.resources import Resource_samples


def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)
    return db

def test_model():
    """

    :return:
    """
    db= get_new_db()

    sample= Samples.create(name='sample_1',session=1)

    return

def test_apidoc():
    """


    :return:
    """
    autodoc = Resource_samples._autodoc()
    return


if __name__=="__main__":

    test_apidoc()
    test_model()
