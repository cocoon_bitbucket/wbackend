__author__ = 'cocoon'
"""

    /hub/samples/-/open_session    { session=1,  members= [] , configuration= {} }
                                  return urls of agents
    /hub/samples/-/close_session

    /hub/samples/1/start
    /hub/samples/1/stop

"""

from flask import request

from restop.application import get_application
from wbackend.model import Model


from restop.collection import GenericCollectionWithOperationApi

from models import DEVICE_NAME

# import resources
import resources




#
# create flask app
#

# get a flask application
app = get_application(__name__,with_error_handler=True)

app.config.from_pyfile('config.cfg')

redis_url= app.config.get('RESTOP_REDIS_URL',"")

# backend= Backend.new(redis_url,
#                      workspace_base=app.config['WORKSPACE_ROOT'],
#                      spec_base= app.config["SPEC_ROOT"],
#                      config=app.config)


#
app.config['backend'] = Model

#
# create collection blueprint
#
blueprint_name='restop_sample'
#url_prefix= '/restop/api/v1'

#url_prefix= app.config['RESTOP_URL_PREFIX'] + '/hub'
url_prefix= app.config['RESTOP_URL_PREFIX']

collections= [ "root", DEVICE_NAME]


app.config['collections']= {}
app.config['collections'][blueprint_name]= collections


my_blueprint= GenericCollectionWithOperationApi.create_blueprint(blueprint_name,__name__,url_prefix=url_prefix,
                collections= collections)

# register restop blueprint
app.register_blueprint(my_blueprint,url_prefix='')

# # store api doc
# apidoc={}
# apidoc['collections']= collections
# apidoc['session_collection']= 'sessions'
# apidoc['collection_operations']=['']
# apidoc['operations'] = []
#
# operations=set()
# # add droydrunner keywords
# #operations.update(set(['click_by_index', 'native_drag_to', 'ui_object', 'dump', 'fling_backward_horizontally', 'get_object', 'click_on_object', 'scroll_forward_vertically', 'clear_text', 'pinch_in', 'long_click_by_index', 'pinch_out', 'register_press_watcher', 'swipe_right', 'native_gesture', 'fling_forward_horizontally', 'execute_adb_shell_command', 'set_text', 'scroll_to_end_horizontally', 'click_linear_by_text', 'scroll_to_horizontally', 'click', 'swipe_down', 'native_fling', 'open_notification', 'scroll_to_horizontal', 'start_test_agent', 'execute_adb_command', 'native_operation', 'swipe_left', 'scroll_backward_vertically', 'flash', 'fling_forward_vertically', 'unfreeze_screen_rotation', 'wait_for_object_exists', 'type', 'call', 'wait_until_object_gone', 'get_count', 'get_info_of_object', 'scroll_to_vertically', 'scroll_to_vertical', 'set_screen_orientation', 'get_object_data', 'scroll_to_beginning_vertically', 'get_device_info', 'screenshot', 'adb_devices', 'turn_off_screen', 'click_tab_by_text', 'long_click', 'scroll_to_end_vertically', 'open_quick_settings', 'freeze_screen_rotation', 'register_click_watcher', 'swipe_by_coordinates', 'drag_by_coordinates', 'press', 'scroll_backward_horizontally', 'get_child', 'fling_backward_vertically', 'press_home', 'scroll_to_beginning_horizontally', 'wait_until_gone', 'get_count_of_object', 'flatten_ui_objet', 'wait_for_exists', 'list_all_watchers', 'native_scroll', 'click_at_coordinates', 'wait_idle', 'turn_on_screen', 'set_object_text', 'get_info_by_index', 'remove_watchers', 'snapshot', 'scroll_forward_horizontally', 'install', 'stop_test_agent', 'get_sibling', 'swipe_up', 'wait_update', 'get_screen_orientation', 'uninstall']))
# # add stb keywords
# # operations.update(set(['serial_synch','serial_watch','serial_expect','send_key','stb_send_dump','stb_status',
# #                        'page_get_info_from_live_banner','menu_select','scheduler_start','scheduler_read',
# #                        'page_action_select','page_get_list','page_get_label_from_list_focus','page_find_in_list',
# #                        'stb_toolbox_list','stb_toolbox_select','scheduler_init','scheduler_sync','scheduler_close','stb_get_lineup,stb_pcb_cli,stb_info'
# #                        ]))
#
# apidoc['operations']= list(operations)
# #backend.item_new('apidoc','interface',data=apidoc)


#
#  urls
#

@app.route('/')
def index():
    """

    :return:
    """
    root_url=  "%s%s" % (request.base_url[:-1],app.config['RESTOP_URL_PREFIX']+'/hub')
    return 'hello from restop server: try <a href="%s">%s</a>' % (root_url,root_url)



def start_server(host=None,port=None,debug=True,threaded=True,**kwargs):
    """


    :param kwargs:
    :return:
    """

    host= host or app.config['RESTOP_HOST']
    port= port or int(app.config['RESTOP_PORT'])

    app.run(host=host, port=port, debug=debug, threaded=threaded)



if __name__=="__main__":

    from yaml import load
    import time
    from restop_platform.models import *

    import logging
    logging.basicConfig(level=logging.DEBUG)


    # ...
    db = Database(host='localhost', port=6379, db=1)
    Model.bind(database=db,namespace=None)

    try:
        p= Platform.get(Platform.name=='default')
    except Exception,e:
        # does not exists create it
        db.flushdb()
        stream = file('../../tests/platform.yml')
        data = load(stream)
        p = Platform.create(name='default', data=data)
        p.setup()


    p = Platform.get(Platform.name == 'default')


    start_server(host='0.0.0.0', port=5001, debug=True, threaded=True)
