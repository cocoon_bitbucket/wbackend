import os
import time

from restop_adapters.adapter import exit_command
from restop_adapters.adapter import ThreadedAdapter as AgentAdapter

dir_path = os.path.dirname(os.path.realpath(__file__))
executable= os.path.join(dir_path,"implementation","term.py")

agent_command= "python -u %s" % executable


class Agent(object):
    """



    """
    def __init__(self,agent_model,**kwargs):
        """

        :param agent_model:
        :param kwargs:
        """
        self.model= agent_model
        self.agent_id= self.model.get_hash_id()
        self.parameters= kwargs

    def start(self):
        """
            start the slave process term.py

        :return:
        """

        adapter = AgentAdapter(self.agent_id, agent_command,redis_db=self.model.database)

        started = adapter.run_process()
        assert started == None ,"failed to start process , return code is %s" % str(started)
        adapter.start()

        return True


    def stop(self):
        """
            stop the fake terminal

        :return:
        """
        self.model.stdin.append(exit_command)
        time.sleep(2)

        return True



    def ping(self):
        """

        :return:
        """
        self.model.stdin.append('ping\n')
        return True

    def read(self):
        """

        :return:
        """
        received= None
        if len(self.model.stdout):
            received = self.model.stdout.popleft()
            self.model.logs.append('received: %s' % received)
        #return received
        return received



