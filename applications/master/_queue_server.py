"""

    a Hub server

    sample usage

    in master:

        hub= QueueServer()
        hub. start()

    in remote
        # create hub client
        hub_queue_key = 'queue:id:default'
        hub_client= QueueClient(hub_queue_key,redis_db=db)

        # ask hub to start echo server
        hub_client.send('start_adapter', dict(name='echo',run_mode='thread'))
        time.sleep(2)
        queue_key= hub_client.receive()

        # start echo queue client
        queue_client= QueueClient(queue_key)

        # test it
        queue_client.open()
        queue_client.write('hello\n')
        time.sleep(2)
        line= queue_client.readline()
        assert line == 'hello'
        queue_client.close()

        # shutdown echo server
        queue_client.exit()

        time.sleep(2)


"""
from restop_queues import Queue,QueueHub
from restop_queues.controllers.hub import ProcessQueueHubServer

# import connectors plugins
from restop_queues.connectors import EchoConnector
from restop_queues.connectors.process_connector import ProcessConnector
from restop_queues.connectors.queue_connector import QueueConnector
from restop_queues.connectors.telnet_connector import TelnetConnector
from restop_usb.connectors import SerialConnector



loop_serial= "loop://?logging=debug"
mock_serial= "mock://?logging=debug"

queue_config= {
    'echo': {
        'class':'EchoConnector'
        },
    'usb0': {
        # emulate device serial port console
        'class': 'ProcessConnector',
        'command_line': 'ash\n'
    },
    'serial': {
        'class':'SerialConnector',
        'device':loop_serial,
        'bauds':115200
    },
    'telnet': {
        'class': 'TelnetConnector',
        'host': 'localhost',
    },
    'shell': {
        'class': 'ProcessConnector',
        'command_line': 'bash\n'
    },
    'queue':{
        'class': "QueueConnector",
        'queue_key': 'queue:id:destination',
        'host': 'localhost',
        'port': 6379,
        'db':0
    }
}



class QueueServer(object):
    """

    """
    name= "default"


    def __init__(self,config=None):
        """

        """
        self.config= config or queue_config
        self.create_queues()
        return


    def create_queues(self):
        """

        :return:
        """
        # create queues
        for name, kwargs in self.config.iteritems():
            queue = Queue(name=name, parameters=kwargs)
            queue.save()

        # create hub queue
        QueueHub.create(name=self.name)
        return


    def start(self):
        """
            start hub server
        :return:
        """
        # create hub
        hub_model = QueueHub.load(self.name)
        hub = ProcessQueueHubServer(hub_model)
        hub.start()
        return

