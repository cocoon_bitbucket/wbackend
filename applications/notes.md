
uiautomator
===========


start the adb server on the machine where device are connected (on all interfaces)

     adb -a nodaemon server
     
on same machine launch adb command with -H x.x.x.x option

    adb -H 127.0.0.1 -P 5037 devices

on another machine

    adb -H 172.16.3.123 -P 5037 devices
    

from a docker container

    docker run --rm -ti sorccu/adb adb -H 172.16.3.123 -P 5037 devices
    
    
 