import inspect
from inspect  import getmembers, ismethod

from pprint import pprint




class ScanMethods(object):
    """

    """
    def __init__(self):
        """

        :return:
        """
        self.state={
            'doc': "",
            'item_methods': {},
            'collection_methods':{}
        }

    def add_docstring(self,cls):
        """


        :param docstring:
        :return:
        """
        self.state['doc']= inspect.getdoc(cls)

    def add_op_methods(self,cls):
        """

        :param cls:
        :return:
        """
        operations= ((name,member)  for name,member in getmembers(cls,ismethod) )

        for name,member in operations:
            if name.startswith('op_col_'):
                #insert_point= self.state['collection_methods']
                self.state['collection_methods'][name]={}
                self.state['collection_methods'][name]['docstring']= inspect.getdoc(member)
                self.state['collection_methods'][name]['argspec']= inspect.getargspec(member)._asdict()

            elif name.startswith('op_'):
                #insert_point= self.state['item_methods']
                self.state['item_methods'][name]={}
                self.state['item_methods'][name]['docstring']= inspect.getdoc(member)
                self.state['item_methods'][name]['argspec']= inspect.getargspec(member)._asdict()

            else:
                continue
            #insert_point[name]= { 'docstring': inspect.getdoc(member)}
            #insert_point[name] = {'argspec': inspect.getargspec(member)._asdict()}

    def add_methods(self,cls,exclude=None,):
        """

        :param cls:
        :return:
        """
        operations= ((name,member)  for name,member in getmembers(cls,ismethod) )

        for name,member in operations:
            if not name.startswith('_'):
                #insert_point= self.state['item_methods']
                self.state['item_methods'][name]={}
                self.state['item_methods'][name]['docstring']= inspect.getdoc(member)
                self.state['item_methods'][name]['argspec']= inspect.getargspec(member)._asdict()

            else:
                continue
            #insert_point[name]= { 'docstring': inspect.getdoc(member)}
            #insert_point[name] = {'argspec': inspect.getargspec(member)._asdict()}


    @property
    def result(self):
        """

        :return:
        """
        return self.state


    def pprint(self):
        """


        :return:
        """
        pprint(self.state)




