__author__ = 'cocoon'


import sys
import traceback
from flask import Flask ,jsonify ,Response,json

class ApplicationResponse(dict):
    """

    """


class ApplicationError(Exception):
    status_code = 500

    def __init__(self, message ,result=500, logs=None, status_code=None):
        Exception.__init__(self)
        self.message = message
        self.result= int(result) or 500
        self.logs= logs or []

        self.status_code= status_code or self.status_code

        self.trace_back= {}

        #self.payload={}
        #self.payload['code']= self.status_code
        #self.payload['message']= self.message

        # update with custom payload
        #payload = dict(payload or ())
        #self.payload.update(payload)

    def to_dict(self):
        rv = dict(message=self.message,result=self.result,logs=self.logs)
        if self.trace_back:
            rv.update(self.trace_back)
        return rv


class InvalidUsage(ApplicationError):
    status_code = 400


class CriticalError(ApplicationError):
    status_code = 500

    def __init__(self, message, result=None, logs=None, status_code=None):
        ApplicationError.__init__(self, message=message ,result=result, logs=logs, status_code=status_code)

        # add traceback info
        exc_type, exc_value, tb = sys.exc_info()

        self.trace_back['tb']= traceback.extract_tb(tb)
        self.trace_back['exc_value']= exc_value.message
        self.trace_back['exc_type']= str(exc_type)



# get a flask application
def get_application(name,with_error_handler=True):
    """

        get a flask application
            with optional error handler

    :return:
    """

    app= Flask(name)


    if  with_error_handler:
        # plug facets error Handlers to the app

        @app.errorhandler(ApplicationError)
        def handle_application_error(error):
            #response = jsonify(error.to_dict())
            data= error.to_dict()
            response=Response(json.dumps(data),status=500,content_type='application/json')
            response.status_code = error.status_code
            return response

        @app.errorhandler(InvalidUsage)
        def handle_invalid_usage(error):
            response = jsonify(error.to_dict())
            response.status_code = error.status_code
            return response

        @app.errorhandler(CriticalError)
        def handle_critical_error(error):
            response = jsonify(error.to_dict())
            response.status_code = error.status_code
            return response

        # @app.errorhandler(Exception)
        # def handle_wild_error(error):
        #     # catch all
        #     message= error.message or error.args[1]
        #     err= CriticalError( message or "wild exception")
        #     response = jsonify(err.to_dict())
        #     response.status_code = 200
        #     return response



    return app



def start( app, host="0.0.0.0" , port=5000 , debug=True):
    """

    :return:
    """

    app.debug = debug

    #app.run(host="127.0.0.1",port = 5001)
    app.run(host=host,port = port)