DeviceProxy highlights



DeviceProxy (restop) is a test framework for devices 
* physical  like
  * a livebox
  * an android phone

* and virtual devices like a sip softphone.

DeviceProxy is build around Robotframework but can be used with python directly.


features
========

* ability to write test scenario with multiple devices of different types
* a web server and an api to be able to access remotely the test platform
* a python client to ease interaction with server
* a robot framework plugin 


in a nutshell
==============

you write tests in robotframework scripting language ( or python )
involving multiple devices that can interact with each others


each of the devices supported by the framework is referred in a platform configuration file : platform.yml

platform.yml
    devices:
      Alias:
        profile: default
        id: alias
      Livebox1:
        profile: livebox
      	id: livebox_1
      AndroidPhone1:
        profile: android
        id: 12ERfke2345
        username: +33699999
      Alice:
        profile: ims
        id: +331999999999
        registrar: sip:ims.fr
        proxy: 10....:5060
      tv:
        profile: tvbox
        peer_address: 192.168.1.12
        peer_port: 8080


I can write a robot framework test where AndroidPhone1 call the livebox (simplified example)

    open session  Livebox1  AndroidPhone1

    livebox check firmware  version=1.2.33444555
    call user  AndroidPhone1  Livebox1
    detect call  Livebox1

    close session

the scenario begin with the keyword [open session] followed by a list of all device alliases involved in the scenario
the scenario end with a [close session] keyword

each script line is made of a keyword , a device alias and optionals key/value parameters



client / server architecture

allows running tests from a machine outside the platform with a light configuration.



a simple scenario with only one device

    open session  tv 
    send_key  tv  VOD
    ...
    
    close session




availables agents
=================

droydrunner: android device 
syprunner:   a voip softphone based on pjsip stack


agents under developement
=========================
orange_livebox
orange_tvbox













