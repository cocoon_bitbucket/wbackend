

from restop.plugins.resources import PoolAgentPipeResource
from devices.pjsip_device.server import get_pjterm_path



class Resource_pjsip_agents(PoolAgentPipeResource):
    """
        proxy to adapter


    """
    collection= 'pjsip_agents'


    #
    # generic agent interface
    #

    def op_start(self,item,**kwarg):
        """

        :param item:
        :param kwarg:
        :return:
        """
        # retrieve agent data
        agent_data=self.backend.item_get(self.collection,item)
        command_line= agent_data['parameters']['command_line']

        if 'fake' in command_line:
            # ask a fake terminal
            pjterm= get_pjterm_path('fake')
            command_line= 'python %s' % pjterm


        # start an adapter
        agent_key= self.backend.item_key(self.collection,item)
        rc= self.backend.adapters_new(agent_key,command_line)

        return {}


