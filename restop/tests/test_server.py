__author__ = 'cocoon'



from flask import blueprints

from restop.application import get_application, start

#from backend.native_backend import NativeBackend as Backend
from backend.redis_backend import RedisBackend as Backend

# import plugin resources
import resources


from restop.collection import GenericCollectionWithOperationApi

#from restop.collection import CollectionWithOperationApi

# class Examples(CollectionWithOperationApi):
#     """
#
#
#     """
#     name= 'examples'
#
# class Samples(CollectionWithOperationApi):
#     """
#
#
#     """
#     name= 'samples'


# get a flask application
app = get_application(__name__,with_error_handler=True)


#app.config['WORKSPACE_ROOT']= "workspace"
#app.config['SPEC_ROOT']= "../raml-spec"

app.config.from_pyfile(('config.cfg'))

#app.config['collections']= ["dummy1","dummy2", "oauth2","files","agents","pjsip_agents","droyd_agents"]
#app.config['workspace_root']= "workspace"

url_prefix= '/restop/api/v1'
collections= ["dummy1","dummy2", "oauth2","files","agents","pjsip_agents","droyd_agents"]


blueprint_name='restop_agents'
app.config['collections']= {}
app.config['collections'][blueprint_name]= collections




backend= Backend("",app.config['WORKSPACE_ROOT'],spec_base= app.config["SPEC_ROOT"],config=app.config)

# for debug
backend.db.flushdb()

app.config['backend'] = backend



# my_app= blueprints.Blueprint('restop',__name__,url_prefix= '/restop/api/v1')
# #my_app= blueprints.Blueprint('restop',__name__,url_prefix= '')
# #names= ["dummy1","dummy2", "oauth2","files","agents","pjsip_agents","droyd_agents"]
# generic= GenericCollectionWithOperationApi.create(my_app)
# #app.register_blueprint(my_app,url_prefix= '/restop/api/v1')
# app.register_blueprint(my_app,url_prefix= '')



my_blueprint= GenericCollectionWithOperationApi.create_blueprint(blueprint_name,__name__,url_prefix='/restop/api/v1',
                collections=[ "root", "oauth2","files","agents","pjsip_agents","droyd_agents"] )
app.register_blueprint(my_blueprint,url_prefix='')


# interface project for generix collection
# import resources
## create blueprints
# my_blueprint= Collection.create( name,import_name, url_prefix, collections= [] )
##                                 restop,__name__, '/restop/api/v1', [ 'root', 'oauth2','files','agents',...]
## register blueprint
# app.register(my_blueprint)


@app.route('/')
def index():
    """

    :return:
    """
    return "hello from restop server: try /restop/api/v1"




if __name__=="__main__":

    start(app)


