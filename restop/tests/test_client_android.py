import  time
from copy import deepcopy
import requests
from restop.client import RestopClient


base_url= "http://localhost:5000/restop/api/v1"



def test_agentspool():


    data= {
        'client_id': 'anonymus',
        'client_secret': '*',
        'grant_type': 'client_credentials',
        'members': [
            [ 'userA', 'pjsip_agents', 'Alice' ,{'command_line': "python term.py"} ],
            [ 'userB', 'droyd_agents', '95d03672',{} ],
            [ 'userC', 'droyd_agents', '0a9b2e63',{} ],
        ]

    }

    data_with_inexistent_collection= deepcopy(data)
    data_with_inexistent_collection['members'].append(
        [ 'userD', 'unknown_agents', 'Charlie' ,{} ],
    )


    client= RestopClient(base_url)
    url= client.url_collection('agents')


    # create agents pool
    r= client.post(url,data=data)
    assert r.status_code == 200
    session_data= r.json()
    assert len(session_data) > len(data)

    # execute operation dummy on an agent
    url= session_data['links']['userA']
    url_dummy= url + '/' + 'dummy'
    r= client.post(url_dummy,data= dict(p1='p1'))
    assert r.status_code==200


    # start agents
    session_id= session_data['id']
    url = client.url_operation('agents',session_id,'start')
    r= client.post(url)
    assert r.status_code==200
    time.sleep(1)


    # send ping command to userA pjsip agent
    url= session_data['links']['userA']
    url_send= url + '/' + 'send'
    r= client.post(url_send,data= dict(message='ping\n'))
    assert r.status_code==200

    # read
    url= session_data['links']['userA']
    url_read= url + '/' + 'read'
    r= client.post(url_read)
    assert r.status_code==200
    assert r.content == u'"pong\\n"'



    # test droydrunner adb_devices: list devices
    url= session_data['links']['userB']
    url_op= url + '/' + 'adb_devices'
    r= client.post(url_op)
    assert r.status_code==200
    data= r.json()



    # test droydrunner get_device_info
    url= session_data['links']['userB']
    url_op= url + '/' + 'get_device_info'
    r= client.post(url_op)
    assert r.status_code==200
    data= r.json()
    assert data.has_key('productName')


    # test droydrunner press key= 'home'
    url= session_data['links']['userB']
    url_op= url + '/' + 'press'
    data= {'key':'home'}
    r= client.post(url_op,data=data)
    assert r.status_code==200
    data= r.json()
    assert data==False or data == True


    time.sleep(1)
    # stop agents
    session_id= session_data['id']
    url = client.url_operation('agents',session_id,'stop')
    r= client.post(url)
    assert r.status_code==200



    # create pool with inexistant collection
    url= client.url_collection('agents')
    r= client.post(url,data=data_with_inexistent_collection)
    assert r.status_code == 500
    assert r.json()['message'] == "no plugin for this collection: unknown_agents"

    return



if __name__=="__main__":


    test_agentspool()
    print "Done."