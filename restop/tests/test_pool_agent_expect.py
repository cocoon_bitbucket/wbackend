import  time
from copy import deepcopy
import requests
from restop.client import RestopClient


base_url= "http://localhost:5000/restop/api/v1"


def test_expect():


    data= {
        'client_id': 'anonymus',
        'client_secret': '*',
        'grant_type': 'client_credentials',
        'members': [
            [ 'userA', 'pjsip_agents', 'Alice' ,{'command_line': "python term.py"} ],
            [ 'userB', 'pjsip_agents', 'Bob' ,{'command_line': "python term.py"} ],
        ]

    }

    client= RestopClient(base_url)


    # create agents pool
    url= client.url_collection('agents')
    r= client.post(url,data=data)
    assert r.status_code == 200
    session_data= r.json()
    assert len(session_data) > len(data)

    # start agents
    session_id= session_data['id']
    url = client.url_operation('agents',session_id,'start')
    r= client.post(url)
    assert r.status_code==200
    time.sleep(1)


    # send ping command to userA pjsip agent
    url= session_data['links']['userA']
    url_send= url + '/' + 'send'
    r= client.post(url_send,data= dict(message='ping\n'))
    assert r.status_code==200

    # read
    url= session_data['links']['userA']
    url_read= url + '/' + 'read'
    r= client.post(url_read)
    assert r.status_code==200
    assert r.content == u'"pong\\n"'
    data= r.json()
    assert data == "pong\n"



    # test send echo command
    url= session_data['links']['userA']
    url_send= url + '/' + 'send'
    url_read= url + '/' + 'read'


    text= 'echo this is my line\n'
    r= client.post(url_send,data= dict(message= text))
    assert r.status_code==200


    # read
    time.sleep(0.1)
    r= client.post(url_read)
    assert r.status_code==200
    data= r.json()
    assert data == "this is my line\n"



    # add text to test expect command
    url_expect= url + '/' + 'expect'

    text= [
        'searching a key', 'in secret place' , 'avoiding cancelers' , 'within a given time' , 'this is the End'
    ]
    for line in text:
        r= client.post(url_send,data= dict(message= 'echo %s\n' % line) )
        assert r.status_code == 200

    # test expect secret
    r= client.post(url_expect, data= dict(pattern=' secret ',timeout=300))
    assert r.status_code==200
    data= r.json()

    assert '=== found ' in data[-1]


    # test expect 'the End' cancel on cancelers
    r= client.post(url_expect, data= dict(
        pattern=' the End ',
        cancel_on= 'cancelers',
        timeout=300))
    assert r.status_code==200
    data= r.json()

    assert '=== canceled ' in data[-1]


    # test expect 'the End' cancel on cancelers
    r= client.post(url_expect, data= dict(
        pattern=' NO MATCH ',
        cancel_on= 'cancelers',
        timeout=3))
    assert r.status_code==200
    data= r.json()

    assert '=== timeout ' in data[-1]




    # stop agents
    time.sleep(1)
    session_id= session_data['id']
    url = client.url_operation('agents',session_id,'stop')
    r= client.post(url)
    assert r.status_code==200


    return


if __name__=="__main__":

    #
    test_expect()


    print "Done."
