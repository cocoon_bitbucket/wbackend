__author__ = 'cocoon'

import os
import binascii
import json
import time
import re

#from restop import resource

#import restop.resource

from restop.resource import Resource
from restop.application import ApplicationError,CriticalError,InvalidUsage

from restop.plugins.resources import RootResource, CollectionResource,DispatchResource
from restop.plugins.resources import PoolSessionResource,PoolAgentResource,PoolAgentPipeResource


from devices.android_device.server.mobiles import Mobile,adb_devices

from pjsip_server_agent import Resource_pjsip_agents



schema_base= '../raml-spec/schemas'
def load_schema(name):
    """

    :param name:
    :return:
    """
    filename= schema_base + '/' + name

    schema = open(filename,"r").read()
    schema= json.loads(schema)

    return schema

   # If no exception is raised by validate(), the instance is valid.
   # validate({"code" : 200, "message" : "OK"}, schema)

# def random_id():
#     """
#
#     :return:
#     """
#     data = os.urandom(16)
#     id= binascii.hexlify(data)
#     return id

class Root_Resource(RootResource):
    """

    """



class Resource_dummy1(Resource):
    """

    """
    collection= 'dummy1'


    def collection_post(self,**kwargs):
        """
            create a dummy1 item

        """
        data= self.request.json

        rc= self.backend.spec_schema_validate(data,'Dummy1-create-request.json')

        # create an id for this item in the collection
        item_id= self.backend.identifier_new(self.collection)
        infoUrl= self.request.url + '/' + item_id
        data['id'] = item_id

        # create the item with data
        r= self.backend.item_new(self.collection,item_id,data)

        return { 'code': 200, 'message': "OK", 'infoUrl': infoUrl}

    def collection_get(self,**kwargs):
        """
            list items in collection

        """
        collection= self.collection
        l= self.backend.collection_list(collection)
        return list(l)

    def item_get(self,**kwargs):
        """

        """
        item= kwargs['item']
        item_data= self.backend.item_get(self.collection,item)

        if not item_data :
            raise KeyError("item Not Found")

        return item_data

    def item_delete(self,**kwargs):
        """

        """
        item= kwargs['item']

        backend= self.parameters['backend']

        rc= backend.item_delete(self.collection,item)

        return { 'code': 200, 'message': "OK" }


    def op_open(self,item,operation,**kwargs):
        """

        """
        raise SyntaxError('unexpected error')


class Resource_dummy2(CollectionResource):
    """



    """
    collection= 'dummy2'


    def op_open(self,item,operation,**kwargs):
        """

        """
        return {}



class Resource_agents(PoolSessionResource):
    """

    """
    collection='agents'


# class Resource_pjsip_agents(PoolAgentPipeResource):
#     """
#         proxy to adapter
#
#
#     """
#     collection= 'pjsip_agents'



class Resource_droyd_agents(PoolAgentResource):
    """

    """
    collection= 'droyd_agents'
    protected= ''

    #
    # generic agent interface
    #
    def check_auth(self):
        return True

    def op_start(self,item,**kwarg):
        """

        :param item:
        :param kwarg:
        :return:
        """

        return {}

    def op_stop(self,item,**kwargs):
        """

        :return:
        """
        return {}

    def _operation(self,collection,item,operation,**kwargs):
        """
            execute an operation on an item

            standard dispatcher: search for a op_$operation method

        """
        # check if operation is a direct implementation = op_start , op_stop ...
        op= None
        try:
            op_name= "op_" + operation
            op= getattr(self,op_name)
        except AttributeError:
            pass
        if op :
            # call local operation op_*
            return op(item,**kwargs)

        # check if operation is defined in Mobile
        try:
            mobile_func= getattr(Mobile,operation)
        except AttributeError:
            # operation does not exists on interface
            raise ApplicationError("invalid operation [%s] on item [%s]" % (operation,item) , 404 )

        # operation exists
        mobile=self.device(item)
        func= getattr(mobile,operation)
        input_data= self.request.json
        if input_data:
            rc= func(**input_data)
        else:
            rc= func()
        return rc


    #
    #  specific interface
    #
    def device(self,agent_id):
        """
            load a uiautomator device for the agent

        :param agent_id: str  , userA ,
        :return:
        """
        agent_data= self.backend.item_get(self.collection,agent_id)
        serial= agent_data['lock_name']
        m= Mobile(serial)
        return m


    # def op_get_device_info(self,item):
    #     """
    #
    #     :return:
    #     """
    #     m= self.device(item)
    #     rc= m.get_device_info()
    #     return rc

    def op_adb_devices(self,item='-'):
        """

            list all connected devices

        :param item:
        :return:
        """
        # item should be '-'
        devices= adb_devices()
        return devices