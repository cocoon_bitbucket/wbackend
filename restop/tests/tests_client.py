
import  time
from copy import deepcopy
import requests
from restop.client import RestopClient


base_url= "http://localhost:5000/restop/api/v1"

headers= { 'Content-type': "application/json"}


def test_base():

    dummy1_data= dict( user='Alice', id='azerty', topic='phone')

    token_query = dict( client_id='anonymus', client_secret="", grant_type="client_credentials")

    client= RestopClient(base_url)

    r= client.item_new('dummy1', dummy1_data)
    assert r.status_code == 200
    data= r.json()
    # get the created item
    item_url= data['infoUrl']


    r= client.get( item_url )
    assert r.status_code == 200
    data= r.json()
    item_id= data['id']
    #{u'topic': u'phone', u'id': u'1', u'user': u'Alice'}
    assert data['topic'] ==  'phone'

   # test unexpected error
    url= client.url_operation('dummy1',item_id,'open')
    r= client.post(url)
    assert r.status_code == 500
    data= r.json()
    assert data.has_key('tb')


    r= client.delete(item_url)

    assert r.status_code == 200
    print "item deleted"

    r= client.get(item_url)
    assert r.status_code == 404

    dummy_data= dict( user='Bob', id='qwerty', topic='phone')

    client= RestopClient(base_url)

    r= client.item_new('dummy2', dummy_data)

    assert r.status_code == 200

    data= r.json()

    # get the created item
    item_url= data['infoUrl']

    r= client.get( item_url )
    assert r.status_code == 200

    data= r.json()
    assert data == {u'topic': u'phone', u'id': u'qwerty', u'user': u'Bob'}

    r= client.delete(item_url)
    assert r.status_code == 200
    print "item deleted"

    r= client.get(item_url)
    assert r.status_code == 404

    url= base_url+'/oauth2/token/'

    data= client.default_token_query.copy()
    data.update( dict( client_id='Alice',dummy='dummy'))

    r= client.post(url,data=data)
    assert r.status_code== 200

    session_data= r.json()
    assert session_data['user_name'] == 'Alice'

    access_token= session_data['token']
    assert len(access_token) == 32

    # oauth2 revoke
    authorization_headers= headers
    headers['Authorization']= "Bearer %s" % access_token
    url = base_url+'/oauth2/revoke/'
    r = requests.post(url,headers= authorization_headers)
    assert r.status_code == 200
    data= r.json()
    assert data['message'] == 'session close'


def test_files():
    """

    :return:
    """
    client= RestopClient(base_url)

    # create token
    session_response= client.oauth2_get_token()
    assert session_response.status_code == 200
    session_data= session_response.json()
    session_id = session_data['id']
    access_token = session_data['token']

    # upload file
    r= client.upload_file('samples/test.txt', "test.txt", access_token=access_token)
    assert r.status_code == 200

    # download file
    r= client.download_file('test.txt', access_token)
    assert r.status_code == 200
    assert r.content== 'this is a test content\n'

    # get touch operation template
    r= client.operation_get('files', 'test.txt', 'touch', access_token=access_token)
    assert r.status_code== 200
    rc= r.json()
    assert rc['message'] == 'syntax: touch()'

    # execute touch operation on file
    r= client.operation_post('files', 'test.txt', 'touch', access_token=access_token)
    assert r.status_code == 200
    rc= r.json()
    assert rc['message'] == "dummy touch executed on test.txt"

    # execute a non existent operation
    r= client.operation_post('files', 'test.txt', 'bad_operation', access_token=access_token)
    assert r.status_code == 404
    rc= r.json()
    assert rc['message'] == 'invalid operation [bad_operation] on item [test.txt]'

    # download inexistent file
    r= client.download_file('inexistant.txt', access_token=access_token)
    assert r.status_code == 404
    data=r.json()
    assert data['message']== u'no such a file: inexistant.txt'

    # test unzip
    source= './samples/Archive.zip'
    # upload zip
    r= client.upload_file(source, "Archive.zip", access_token=access_token)
    assert r.status_code == 200

    # unzip file
    r= client.operation_post('files', 'Archive.zip', 'unzip', access_token=access_token)
    assert r.status_code == 200

    # revoke token
    r= client.oauth2_revoke(access_token)
    assert r.status_code == 200
    assert r.json() == {u'message': u'session close'}

    # download file on a closed session
    r= client.download_file('test.txt', access_token)
    assert r.status_code == 403
    rc = r.json()
    assert rc['message'] == 'session invalidated'

    return

def test_agentspool():


    data= {
        'client_id': 'anonymus',
        'client_secret': '*',
        'grant_type': 'client_credentials',
        'members': [
            [ 'userA', 'pjsip_agents', 'Alice' ,{'command_line': "python term.py"} ],
            [ 'userB', 'droyd_agents', '95d03672',{} ],
            [ 'userC', 'droyd_agents', '0a9b2e63',{} ],
        ]

    }

    data_with_inexistent_collection= deepcopy(data)
    data_with_inexistent_collection['members'].append(
        [ 'userC', 'unknown_agents', 'Charlie' ,{} ],
    )


    client= RestopClient(base_url)
    url= client.url_collection('agents')


    # create agents pool
    r= client.post(url,data=data)
    assert r.status_code == 200
    session_data= r.json()
    assert len(session_data) > len(data)

    # execute operation dummy on an agent
    url= session_data['links']['userA']
    url_dummy= url + '/' + 'dummy'
    r= client.post(url_dummy,data= dict(p1='p1'))
    assert r.status_code==200


    # start agents
    session_id= session_data['id']
    url = client.url_operation('agents',session_id,'start')
    r= client.post(url)
    assert r.status_code==200
    time.sleep(1)


    # send ping command to userA pjsip agent
    url= session_data['links']['userA']
    url_send= url + '/' + 'send'
    r= client.post(url_send,data= dict(message='ping\n'))
    assert r.status_code==200

    # read
    url= session_data['links']['userA']
    url_read= url + '/' + 'read'
    r= client.post(url_read)
    assert r.status_code==200
    assert r.content == u'"pong\\n"'

    time.sleep(1)
    # stop agents
    session_id= session_data['id']
    url = client.url_operation('agents',session_id,'stop')
    r= client.post(url)
    assert r.status_code==200


    # create pool with inexistant collection
    url= client.url_collection('agents')
    r= client.post(url,data=data_with_inexistent_collection)
    assert r.status_code == 500
    assert r.json()['message'] == "no plugin for this collection: unknown_agents"

    return



if __name__=="__main__":

    #

    test_base()
    test_files()
    test_agentspool()

    print "Done."
