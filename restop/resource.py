import inspect

from flask import request
from jsonschema import validate,ValidationError

from restop.application import ApplicationError,CriticalError,InvalidUsage
from restop.tools import ScanMethods


class PluginResourceMount(type):
    """
        The base of the plugin system
    """
    def __init__(cls, name, bases, attrs):
        if not hasattr(cls, 'plugins'):
            # This branch only executes when processing the mount point itself.
            # So, since this is a new plugin type, not an implementation, this
            # class shouldn't be registered as a plugin. Instead, it sets up a
            # list where plugins can be registered later.
            cls.plugins = []
        else:
            # This must be a plugin implementation, which should be registered.
            # Simply appending it to the list is all that's needed to keep
            # track of it later.
            cls.plugins.append(cls)

    def get_plugins(cls, *args, **kwargs):
        return sorted([p(*args, **kwargs) for p in cls.plugins])

    def select_plugin(cls,collection):
        """
            role is either user, workspace, run , report

        """
        for item in reversed(cls.plugins):
            if item.collection == collection:
                # warning in case of duplicates: return only the last one
                return item

        # not found: return None
        raise KeyError('no such resource plugin: %s' % collection)



class Resource(object):
    """
        the base class for each resources
    """
    __metaclass__ = PluginResourceMount

    #role= None
    #version= None
    collection= None

    def __init__(self,**kwargs):
        """

        """
        self.parameters= kwargs
        self.session= None

    # convenient functions
    @property
    def request(self):
        return self.parameters['request']

    @property
    def backend(self):
        return self.parameters['backend']

    def validate_json(self,data,schema):
        """
            validate a json record towards a json schema

        :param data: dict of data to validate
        :param schema: dict of a json schema
        :return:
        """
        try:
            validate(data,schema)
        except ValidationError as e:
            raise ValueError(e.message)
        return True

    #
    # collection methods
    #

    def collection_post(self):
        """
            create an item

        """
        raise NotImplementedError

    def collection_get(self,**kwargs):
        """

            return list of items
        """
        raise NotImplementedError


    #
    # item methods
    #

    def item_get(self,**kwargs):
        """
            get the value of an item
        """
        raise NotImplementedError

    def item_put(self,**kwargs):
        """
            set the value of an item

        """
        raise NotImplementedError

    def item_delete(self,**kwargs):
        """
            delete an item

        """
        raise NotImplementedError

    #
    # standard operation method dispatcher
    #
    def _operation(self,collection,item,operation,**kwargs):
        """
            execute an operation on an item

            standard dispatcher:

                search for a op_$operation method

                or an op_col_$operation method if item is '-'

                /<collection>/-/<operation> :

        """
        if item == '-' :
            # search for collection operation
            op_name= "op_col_" + operation
        else:
            # search for item operation
            op_name= "op_" + operation
        try:
            op= getattr(self,op_name)
        except AttributeError:
            # operation not defined
            #raise KeyError('no such operation')
            if item == '-':
                raise ApplicationError("invalid operation [%s] on collection [%s]" % (operation,collection) , 404 )
            else:
                raise ApplicationError("invalid operation [%s] on item [%s]" % (operation,item) , 404 )

        # call operation
        return op(item,**kwargs)



    def check_oauth2(self,request):
        """

        :param headers:
        :return:
        """
        access_token= None
        prefix= "Bearer "
        try:
            authorization= request.headers['Authorization']
            assert authorization.startswith(prefix)

            access_token= authorization[len(prefix):].strip()
            return True

            # session= self.backend.session_get_by_token(access_token)
            # session_id= session['id']
            #
            # # check expires
            # if self.backend.session_has_expired(session_id):
            #     raise ApplicationError("session has expired",403)
            # # check valid
            # if not self.backend.session_is_valid(session_id):
            #     raise ApplicationError("session invalidated",403)

        except KeyError:
            # no authorization
            raise ApplicationError("needs an access_token",403)

        except AssertionError:
            # bad format
            raise ApplicationError("bad format for Authorization",403)

        return access_token

    # introspect
    @classmethod
    def _autodoc(cls,**kwargs):
        """

            generate autodocumentation
        :param kwargs:
        :return:
        """
        info = ScanMethods()
        info.add_docstring(cls)
        info.add_op_methods(cls)
        return info.result


class JsonResource(Resource):
    """



    """
    def _collection_post(self,**kwargs):
        raise NotImplementedError

    def collection_post(self,**kwargs):
        """
            create an item

        """
        try:
            input= self.request.json or {}
            return self._collection_post(**input)
        except Exception as e:
            raise e

    #
    # json  operation method dispatcher
    #
    def _operation(self,collection,item,operation,**kwargs):
        """
            execute an operation on an item

            json dispatcher= standard dispatcher + resolve entry parameters:

                search for a op_$operation method

                or an op_col_$operation method if item is '-'

                /<collection>/-/<operation> :

        """
        if item == '-' :
            # search for collection operation
            op_name= "op_col_" + operation
        else:
            # search for item operation
            op_name= "op_" + operation
        try:
            op= getattr(self,op_name)
        except AttributeError:
            # operation not defined
            #raise KeyError('no such operation')
            if item == '-':
                raise ApplicationError("invalid operation [%s] on collection [%s]" % (operation,collection) , 404 )
            else:
                raise ApplicationError("invalid operation [%s] on item [%s]" % (operation,item) , 404 )

        # call operation
        input= request.json or {}

        # check parameters ?
        #rc= self.backend.spec_schema_validate(data,schema_path)

        # check item exists
        if not self.backend.item_exists(self.collection,item):
            return self.response("item not found: [%s/%s]" % (self.collection,item),404)

        # item_data= self.backend.item_get(self.collection,item)
        # if item_data is None:
        #     # 404 if not found
        #     return self.response("item not found: [%s:%s]" % (self.collection,item),404)

        # call operation on item with all parameters as kwargs
        try:
            if item == '-':
                data,status,logs = op(**input)
            else:
                data,status,logs = op(item,**input)
            code= status or 200
        except Exception as e:
            code= 500
            logs= e.message
            data = "operation failed"

        return self.response(data,code=code,logs=logs)


    def response(self,message,code=200,logs=None):
            """

            """
            logs= logs or []
            res= dict (message= message,result=code,logs=logs)
            return res
