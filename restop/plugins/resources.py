
import time
import re
from flask import current_app,make_response,request

import resource
from restop.application import ApplicationError, InvalidUsage, CriticalError
import restop.resource
from restop.resource import Resource,PluginResourceMount



#
# base resource types
#

class RootResource(Resource):
    """
        the root resource  / or /prefix/
    """
    collection='root'

    def root_get(self,**kwargs):
        """

            list all resources handled by the blueprint

        :param kwargs:
        :return:
        """
        blueprint_name= request.blueprint
        return current_app.config['collections'][blueprint_name]

    def root_post(self):
        """

        :return:
        """
        return NotImplementedError


class CollectionResource(Resource):
    """
        a classic collection/item/operation resource
    """
    collection= 'default'

    def collection_post(self,**kwargs):
        """
            create a dummy1 item

        """
        schema_path= "%s/post_request.json" % self.collection
        data= self.request.json

        rc= self.backend.spec_schema_validate(data,schema_path)

        # create an id for this item in the collection
        item_id= self.backend.identifier_new(self.collection)


        infoUrl= self.request.url + '/' + item_id

        # create the item with data
        r= self.backend.item_new(self.collection,item_id,data)

        return { 'code': 200, 'message': "OK", 'infoUrl': infoUrl}

    def collection_get(self,**kwargs):
        """
            list items in collection

        """
        l= self.backend.collection_list(self.collection)
        return list(l)

    def item_get(self,**kwargs):
        """

        """
        item= kwargs['item']

        item_data= self.backend.item_get(self.collection,item)

        if not item_data :
            raise KeyError("item Not Found")

        return item_data

    def item_delete(self,**kwargs):
        """

        """
        item= kwargs['item']

        rc= self.backend.item_delete(self.collection,item)

        return { 'code': 200, 'message': "OK" }


class DispatchResource(Resource):
    """
        a collection with hard coded items




    """
    collection= 'dispatch'

    def _item_dispatch(self,item,method='get',**kwargs):
        """

        :param item:
        :return:
        """
        method= method.lower()
        try:
            # compute func name , like item_get_<item>
            func_name= "item_%s_%s" % (method,item)
            func= getattr(self, func_name)
        except AttributeError:
            # operation not defined
            raise KeyError('no such item: %s' % item)

        # call operation
        try:
            return func(item,method,**kwargs)
        except ValueError ,e:
            raise InvalidUsage(e.message,400)
        except AssertionError, e:
            raise InvalidUsage(e.message,400)

    def item_get(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        item= kwargs.pop('item')
        method= self.request.method
        return self._item_dispatch(item,method,**kwargs)

    def item_post(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        item= kwargs.pop('item')
        method= self.request.method
        return self._item_dispatch(item,method,**kwargs)

    def item_put(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        item= kwargs.pop('item')
        method= self.request.method
        return self._item_dispatch(item,method,**kwargs)

    def item_delete(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        item= kwargs.pop('item')
        method= self.request.method
        return self._item_dispatch(item,method,**kwargs)

    def collection_get(self,**kwargs):
        """
            list all items available to dispatch

        :param kwargs:
        :return:
        """
        raise NotImplementedError

#
# authentification
#

class Resource_oauth2(DispatchResource):
    """


    """
    collection= 'oauth2'

    def item_post_token(self,item,method,**kwargs):
        """

        """
        assert item == 'token'
        # open session
        data= self.request.json

        try:
            self.backend.spec_schema_validate(data,'oauth2/token/post_request.json')
        except Exception,e:
            raise InvalidUsage(e.message)

        assert data['grant_type'] == 'client_credentials' , "only supports grant_type='client_credentials"

        user_name= data['client_id']

        session_id= self.backend.session_new(user_name,**data)
        session_data= self.backend.session_get(session_id)

        session_data['code']= 200
        session_data['message'] = 'session open'

        return session_data

    def item_post_revoke(self,item,method,**kwarg):
        """
        """
        assert item == 'revoke'

        # check session
        access_token= self.check_oauth2(self.request)

        # retrieve session
        session= self.backend.session_get_by_token(access_token)
        session_id= session['id']

        # close session
        self.backend.session_close(session_id)

        return {'message': 'session close'}


#
# filesystem
#
class FilesystemResource(Resource):
    """

            upload and download files

            files are stored under a workspace directory
            and access is protected by an access_token

    """
    collection= 'files'
    protected= 'oauth2'

    def check_auth(self):
        """

            check auth and return session
        :return:
        """
        access_token= self.check_oauth2(self.request)
        session_data= self.backend.session_get_by_token(access_token)
        return session_data

    def collection_get(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        raise NotImplementedError

    def collection_post(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        # for test
        raise NotImplementedError

    def item_get(self,**kwargs):
        """
            download a file

        :param kwargs:
        :return:
        """
        filename= kwargs['item']

        #access_token= self.check_oauth2(self.request)
        data= self.download_file(session=self.session,filename=filename)
        response= make_response(data,200)

        return response

    def item_post(self,**kwargs):
        """
            upload a file

        :param kwargs:
        :return:
        """
        filename= kwargs['item']

        #access_token= self.check_oauth2(self.request)
        #access_token= self.session['token']

        file = self.request.files['file']

        rc= self.upload_file(file,session=self.session,filename=filename)

        if rc== True:
            return { 'code': 200, 'message': "OK"}
        else:
            raise ApplicationError("upload failed",500)

    def item_put(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        raise NotImplementedError

    def item_delete(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        raise NotImplementedError


    def upload_file(self,file,session,filename=None):
        """

        :param file: file object
        :param access_token:ame
        :return:
        """
        filename= filename or "default.log"
        session_id= session['id']
        data= file.read()
        rc= self.backend.workspace_write(session_id,data,filename=filename)
        return rc

    def download_file(self,session,filename):
        """

        :param file: file object
        :param access_token:ame
        :return:
        """
        session_id= session['id']
        try:
            data= self.backend.workspace_read(session_id,filename)
        except IOError, e:
            raise ApplicationError("no such a file: %s" % filename,404)
        except Exception,e:
            raise CriticalError('error while downloading file %s filename' % filename,404)
        return data

    def op_touch(self,item,**kwargs):
        """
            execute a system touch on a file
        :param kwargs:
        :return:
        """
        method= self.request.method

        if method == 'POST':
            # execute operation touch on item
            session_id= self.session['id']
            rc= self.backend.workspace_touch(session_id,item)
            if rc:
                return { 'code': 200 , 'message':'dummy touch executed on %s' % item}
            else:
                raise ApplicationError("touch failed", 500)
        elif method== 'GET':
            # ask for touch template
            return { 'code': 200 , 'message': 'syntax: touch()'}

        return {}

    def op_unzip(self,item,**kwargs):
        """
            execute a system touch on a file
        :param kwargs:
        :return:
        """
        method= self.request.method

        if method == 'POST':
            # execute operation  on item
            #access_token= self.check_oauth2(self.request)
            #session= self.backend.session_get_by_token(access_token)
            session_id= self.session['id']

            rc= self.backend.workspace_unzip(session_id,item)
            if rc :
                return { 'code': 200 , 'message':'file unzipped: %s' % item}
            else:
                raise ApplicationError("cannot unzip file" ,500)

        elif method== 'GET':
            # ask for touch template
            return { 'code': 200 , 'message': 'syntax: touch()'}

        return {}


#
#    pools
#

class PoolSessionResource(CollectionResource):
    """

    """
    collection= '_pool_sessions'

    #
    # web interface
    #

    def collection_post(self,**kwargs):
        """
            create a pool of agents


            input :
                {
                    client_id,
                    client_secret,
                    grant_type= 'client_credentials',
                    members=
                        [
                            [ agent_alias, agent_category , lock_name , {} ],
                            [ ...                                  }
                        ]
                }
        :return:
        """
        # open create agents pool
        data= self.request.json
        result= self.create_item(data)
        session_id= result['id']
        result['log']= "\n".join(self.backend.session_read_log(session_id))
        return result


    def item_get(self,item ,**kwargs):
        """

            /_pool_sessions/<id>

            return session info

        :param kwargs:
        :return:
        """
        session_data= self.backend.session_get(item)
        return session_data

    def op_start(self,item,**kwargs):
        """

        :return:
        """
        session_id=item
        self.backend.session_log(session_id,"start session")
        agents_count= 0
        agents_started= 0
        session_data= self.backend.item_get('Sessions',item)
        agents= session_data['agents']
        for agent_key in agents:
            agent_collection,agent_id= agent_key.split(':')
            agent_class= Resource.select_plugin(agent_collection)
            agent= agent_class(backend=self.backend,request=request)
            r= agent.op_start(agent_id,**kwargs)
            if r:
                agents_started+= 1
            agents_count +=1
            continue
        result= {'agents_count': agents_count, 'agent_started': agents_started}
        result['log']= "\n".join(self.backend.session_read_log(session_id))
        return result

    def op_stop(self,item,**kwargs):
        """

        :return:
        """
        session_id = item
        self.backend.session_log(session_id, "stop session")
        result= dict( result='204', message='Done', logs=[])
        session_data= self.backend.item_get('Sessions',item)
        agents= session_data['agents']
        for agent_key in agents:
            agent_collection,agent_id= agent_key.split(':')
            agent_class= Resource.select_plugin(agent_collection)
            agent= agent_class(backend=self.backend,request=self.request)
            r= agent.op_stop(agent_id)
            result['logs'].extend(r['logs'])
            continue
        self.backend.session_close(session_id)
        return result


    #
    # primary interface
    #

    def build_session_configuration(self,members):
        """

        :return:
        """

        return

    def create_item(self,item_data,**kwargs):
        """
            create a pool of agents
            input :{
                        client_id,
                        client_secret,
                        grant_type= 'client_credentials',
                        members=
                            [ [ agent_alias, agent_category , lock_name , {} ],
                              [ ...                                  }            ]
            }
        :return:
        """
        # open create agents pool
        data= item_data

        # check input
        #try:
        #    self.backend.spec_schema_validate(data,'agents/post_request.json')
        #except Exception,e:
        #    raise InvalidUsage(e.message)

        assert data['grant_type'] == 'client_credentials' , "only supports grant_type='client_credentials"

        user_name= data['client_id']

        # create session
        session_id= self.backend.session_new(user_name,**data)
        session_data= self.backend.session_get(session_id)

        self.backend.session_log(session_id,"session %s created" % session_id)
        # validate and add links to sessions
        self.validate(session_data)

        # hook to customize session data
        session_data= self.customize_item(session_data,session_id)

        # create agents
        agents= self.create_agents(session_data)

        # create session
        session_data.update( agents )
        rc= self.backend.item_put('Sessions',session_id,session_data)

        return self.item_get(session_id,**kwargs)

    def customize_item(self,session_data,session_id):
        return session_data

    def validate(self,session_data):
        """
            validate pool parameters
        :return:
        """
        assert session_data.has_key('members'), "data must have a members list"
        assert isinstance(session_data['members'],list), "members must be a list"
        #assert len(session_data['members']) > 0, "empty members list"


        return True
        # # handle members
        # agent_base_url= self.request.url.split('/')   # eg localhost:5000/restop/api/v1/agents
        # del agent_base_url[-1]
        # agent_base_url= '/'.join(agent_base_url)
        #
        # member_keys= []
        # member_links= {}
        # for member in session_data['members']:
        #     alias= member[0]
        #     category= member[1]
        #     lock_name= member[2 ]
        #     if len (member) >3:
        #         member_data= member[3]
        #     else:
        #         member_data= {}
        #
        #     # check there is a resource plugin for this category
        #     try:
        #         resource_class= Resource.select_plugin(category)
        #     except KeyError:
        #         # not plugin for this resource
        #         raise ApplicationError("no plugin for this collection: %s" % category)
        #     # create an agent entry
        #     agent_id= self.backend.identifier_new(collection=category)
        #     agent_data = dict( alias=alias,category=category,lock_name=lock_name,parameters=member_data)
        #     agent_data['session_id']= session_data['id']
        #
        #     rc= self.backend.item_new(category,agent_id,agent_data)
        #     agent_key= self.backend.item_key(category,agent_id)
        #     member_keys.append(agent_key)
        #     member_links[alias]= "%s/%s/%s" % ( agent_base_url,category,agent_id)
        #
        # return dict( agents= member_keys,links= member_links )

    def create_agents(self,session_data):
        """
            create agents in database

        :param session_data:
        :return:
        """
       # handle members
        session_id= session_data['id']

        agent_base_url= self.request.url.split('/')   # eg localhost:5000/restop/api/v1/agents
        del agent_base_url[-1]
        agent_base_url= '/'.join(agent_base_url)

        member_keys= []
        member_links= {}
        for indice,member in enumerate(session_data['members']):
            alias= member[0]
            category= member[1]
            lock_name= 'device_id'
            if len (member) >2:
                member_data= member[2]
            else:
                member_data= {}

            # check there is a resource plugin for this category
            try:
                resource_class= Resource.select_plugin(category)
            except KeyError:
                # not plugin for this resource
                raise ApplicationError("no plugin for this collection: %s" % category)
            # create an agent entry
            agent_indice= indice
            agent_id= self.backend.identifier_new(collection=category)
            agent_data = dict( alias=alias,category=category,lock_name=lock_name,
                               agent_indice=agent_indice,parameters=member_data)
            agent_data['session_id']= session_data['id']

            self.backend.session_log(session_id,'create agent: %s as %s/%s/%s' % (alias,agent_base_url,category,agent_id) )
            rc= self.backend.item_new(category,agent_id,agent_data)
            agent_key= self.backend.item_key(category,agent_id)
            member_keys.append(agent_key)
            member_links[alias]= "%s/%s/%s" % ( agent_base_url,category,agent_id)

        return dict( agents= member_keys,links= member_links )



class PoolAgentResource(CollectionResource):
    """

    """
    collection="_pool_agents"

    protected= 'oauth2'

    def op_dummy(self,item,**kwargs):
        """

        """
        return { 'message': "operation dummy on %s agent %s" % (self.collection,item) }

    def op_start(self,item,**kwarg):
        """
        :return:
        """
        return {}

    def op_stop(self,item,**kwargs):
        """

        :param item:
        :return:
        """
        return {}

    @classmethod
    def get_collection_session_configuration(cls,session,backend,**kwargs):
        """


        :return:
        """
        agents={}

        # setup droydrunner agents
        for alias,collection,device_data in session.iter_alias(collection=cls.collection):
            entry= { 'alias': alias, 'collection': collection }
            device_id= device_data['id']
            entry['parameters']= { 'device_id': device_id}
            agents[alias]= entry
        return agents

    def get_agent_object(self,alias,**kwargs):
        raise NotImplementedError


    # dialog with proxy agent
    def op_send(self,item ,**kwargs):
        """

        :param msg:
        :return:
        """
        data= self.request.json
        message= data['message']

        rc= self.send(item,message)
        return rc

        #agent_key= self.backend.item_key(self.collection,item)
        #rc= self.backend.adapters_send(agent_key,message)
        #return {}

    def op_read(self,item,**kwargs):
        """

        :return:
        """
        data= self.read(item)

        #agent_key= self.backend.item_key(self.collection,item)
        #data = self.backend.adapters_read(agent_key)
        return data

    def op_expect(self,item , **kwargs):
        """
            read agent stdin until pattern is match or timeout reached or one of cancel_on pattern matches

        :param item:
        :param kwargs:   pattern,timeout=3,cancel_on=None
        :return:
        """
        data=self.request.json
        rc= self.expect(item,data,**kwargs)
        return rc


    def send(self,item,message,**kwargs):
        """

        :param item:
        :param kwargs:
        :return:
        """
        raise NotImplementedError


    def read(self,item,**kwargs):
        """

        :param item:
        :param kwargs:
        :return:
        """
        raise NotImplementedError


    def expect(self,item ,data, **kwargs):
        """
            read agent input until pattern is match or timeout reached or one of cancel_on pattern matches

        :param item:
        :param data: dict   ( pattern="*" , timeout=3, cancel_on=[] )
        :param kwargs:   pattern,timeout=3,cancel_on=None
        :return:
        """

        trace_enabled= True

        pattern= data.get('pattern','*')
        timeout= int(data.get('timeout',3))
        cancel_on= data.get('cancel_on',[])
        if isinstance(cancel_on,basestring):
            cancel_on= [cancel_on]

        # compute search pattern with ignore case
        search_pattern= re.compile(pattern,re.I)
        # compute optional cancel patterns
        cancel_patterns= []
        for p in cancel_on:
            r= re.compile(p)
            cancel_patterns.append(r)

        buffer=[]
        # take timestamp
        start_time = time.strftime("%X") + ".000"
        t0= time.time()
        limit= t0 + timeout
        t= t0
        while True:

            # check timeout
            t= time.time()
            if t > limit:
                buffer.append(self.trace("timeout reached"))
                break

            # read the line
            line = self.read(item)
            if line is None:
                time.sleep(0.4)
                continue
            buffer.append(line)

            # Search for expected text
            if search_pattern.search(line) != None:
                # we found the expected line , return it
                buffer.append(self.trace("found %s" % pattern))
                # end the read loop
                break

            # search for cancel patterns
            found_cancel = False
            if cancel_patterns:
                for index,search_cancel in enumerate(cancel_patterns):
                    if search_cancel.search(line):
                        # we found a cancel pattern
                        buffer.append(self.trace('canceled %s' % cancel_on[index] ))
                        found_cancel= True
                        break
            if found_cancel:
                break
        return buffer


    def trace(self, s):
        """

        :param s: string , message to trace
        :return:
        """
        now = time.time()
        fmt = "================ " + s + " ==================" + " [at t=%(time)03d]"
        return fmt








class PoolAgentPipeResource(PoolAgentResource):
    """
        pipe agent
            send command via stdin of process
            receive data via stdout of process


    """
    collection= '_pool_agentpipes'


    #
    # generic agent interface
    #

    def op_start(self,item,**kwarg):
        """

        :param item:
        :param kwarg:
        :return:
        """
        # retrieve agent data
        agent_data=self.backend.item_get(self.collection,item)
        command_line= agent_data['parameters']['command_line']

        # start an adapter
        agent_key= self.backend.item_key(self.collection,item)
        rc= self.backend.adapters_new(agent_key,command_line)

        return {}


    def op_stop(self,item,**kwargs):
        """

        :return:
        """
        # stop the adapter
        agent_key= self.backend.item_key(self.collection,item)
        rc= self.backend.adapters_stop(agent_key)
        return {}


    #
    # specific agent interface
    #

    def op_send(self,item ,**kwargs):
        """

        :param msg:
        :return:
        """
        data= self.request.json
        message= data['message']

        rc= self.send(item,message)
        return rc

        #agent_key= self.backend.item_key(self.collection,item)
        #rc= self.backend.adapters_send(agent_key,message)
        #return {}

    def op_read(self,item,**kwargs):
        """

        :return:
        """
        data= self.read(item)

        #agent_key= self.backend.item_key(self.collection,item)
        #data = self.backend.adapters_read(agent_key)
        return data

    def op_expect(self,item , **kwargs):
        """
            read agent stdin until pattern is match or timeout reached or one of cancel_on pattern matches

        :param item:
        :param kwargs:   pattern,timeout=3,cancel_on=None
        :return:
        """
        data=self.request.json
        rc= self.expect(item,data,**kwargs)
        return rc

    #
    # local interface
    #

    def send(self,item,message,**kwargs):
        """

        :param item:
        :param kwargs:
        :return:
        """
        agent_key= self.backend.item_key(self.collection,item)
        rc= self.backend.adapters_send(agent_key,message)
        return rc


    def read(self,item,**kwargs):
        """

        :param item:
        :param kwargs:
        :return:
        """
        agent_key= self.backend.item_key(self.collection,item)
        data = self.backend.adapters_read(agent_key)
        return data


    def expect(self,item ,data, **kwargs):
        """
            read agent stdin until pattern is match or timeout reached or one of cancel_on pattern matches

        :param item:
        :param kwargs:   pattern,timeout=3,cancel_on=None
        :return:
        """
        #data=self.request.json

        trace_enabled= True

        pattern= data.get('pattern','*')
        timeout= int(data.get('timeout',3))
        cancel_on= data.get('cancel_on',[])
        if isinstance(cancel_on,basestring):
            cancel_on= [cancel_on]

        agent_key= self.backend.item_key(self.collection,item)
        # compute search pattern with ignore case
        search_pattern= re.compile(pattern,re.I)
        # compute optional cancel patterns
        cancel_patterns= []
        for p in cancel_on:
            r= re.compile(p)
            cancel_patterns.append(r)

        buffer=[]
        # take timestamp
        start_time = time.strftime("%X") + ".000"
        t0= time.time()
        limit= t0 + timeout
        t= t0
        while True:

            # check timeout
            t= time.time()
            if t > limit:
                buffer.append(self.trace("timeout reached"))
                break

            # read the line
            line = self.backend.adapters_read(agent_key)
            if line is None:
                time.sleep(0.4)
                continue
            buffer.append(line)

            # Search for expected text
            if search_pattern.search(line) != None:
                # we found the expected line , return it
                buffer.append(self.trace("found %s" % pattern))
                # end the read loop
                break

            # search for cancel patterns
            found_cancel = False
            if cancel_patterns:
                for index,search_cancel in enumerate(cancel_patterns):
                    if search_cancel.search(line):
                        # we found a cancel pattern
                        buffer.append(self.trace('canceled %s' % cancel_on[index] ))
                        found_cancel= True
                        break
            if found_cancel:
                break
        return buffer


    #
    # scpecific
    #
    def trace(self, s):
        """

        :param s:
        :return:
        """
        now = time.time()
        fmt = "================ " + s + " ==================" + " [at t=%(time)03d]"
        return fmt



class CollectionApidoc(Resource):
    """
         /<prefix>/apidoc   a resource to access auto documentation
    """
    collection= 'apidoc'

    def collection_post(self,**kwargs):
        """
            create a dummy1 item

        """
        schema_path= "%s/post_request.json" % self.collection
        data= self.request.json

        rc= self.backend.spec_schema_validate(data,schema_path)

        # create an id for this item in the collection
        item_id= self.backend.identifier_new(self.collection)


        infoUrl= self.request.url + '/' + item_id

        # create the item with data
        r= self.backend.item_new(self.collection,item_id,data)

        return { 'code': 200, 'message': "OK", 'infoUrl': infoUrl}

    def collection_get(self,**kwargs):
        """
            list items in collection

        """
        #data= self.request.json
        data=self.request.values
        message = {}


        if 'collection' in data:

            # this is a request for a collection api
            collection_name= data['collection']
            try:
                plugin_class= Resource.select_plugin(collection_name)
            except KeyError:
                raise ApplicationError("No plugin found for collection: %s" % collection_name)

            doc= plugin_class._autodoc()

        else:
            # this a request for the general interface ( key: apidoc:interface )
            doc= self.backend.item_get('apidoc','interface')

        #l= self.backend.collection_list(self.collection)
        return { 'code': 200, 'message': doc }

