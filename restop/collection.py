__author__ = 'cocoon'


import json
from flask.views import View

from flask import request, Response

from flask import current_app

from flask import blueprints

from restop.application import InvalidUsage, ApplicationError,CriticalError


# register plugins
import resource


class GenericCollectionWithOperationApi(View):
    """

        <collection>/<item>/<operation>

        POST <collection>/  :  create an item
        GET <collection>    :  list items

        GET <collection>/<item>:  get item representation
        PUT <collection>/<item>/ : set item value
        DELETE <collection>/<item>  : delete item

        POST <collection>/<item>/<operation>:  execute operation on item
        GET <collection>/<item>/<operation> : get a template for the operation

        POST <collection>/-/<operation>:  execute operation on collection
        GET <collection>/-/<operation> : get a template for the collection operation


        introspect

        GET /prefix/    : list all collections
        GET /prefix/-   : list all collection operation
        GET /prefix/-/- : list all item operations


        targets methods on plugin
        -------------------------

        root_get:  get list of collections or resources

        collection_post  create item in collection
        collection_get   list item
        collection_*

        item_get        get item representation
        item_put        set item value
        item_delete     delete item
        item_*

        _operation     : dispatcher for operations


    """
    methods = ['GET','POST','DELETE','PUT','HEAD','PATCH','OPTIONS']


    def __init__(self,*args,**kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        View.__init__(self)
        self.config = kwargs


    @classmethod
    def create(cls,app,*collections):
        """

        :param cls:
        :param app: instance of flask app
        :param name: str  eg sessions
        :param url: str eg /sessions ,default to / + name
        :return:
        """

        cls.check_plugins(*collections)

        #view_name= 'generic'
        view_name= app.name


        # create view
        view  = cls.as_view(view_name)

        # attach collections info to view
        #view.collections= collections


        # register view urls with app
        url = app.url_prefix or ('/')
        if not url.startswith('/'):
            url= '/' + url
        if not url.endswith('/'):
            url = url + '/'
        for add in [ '','<collection>', '/','<item>', '/' , '<operation>' , '/']:
            url += add
            app.add_url_rule(url, view_func=view)
            continue


    @classmethod
    def create_blueprint(cls,name,import_name,url_prefix,collections=None,**parameters):
        """

        :param app:
        :return:
        """
        # create a blueprint
        bp= blueprints.Blueprint(name,import_name,url_prefix=url_prefix,**parameters)

        #bp.config={}
        #bp.config['url_prefix']= url_prefix
        #bp.config['collections']= collections


        # create the view
        cls.create(bp,*collections)



        return bp

    @classmethod
    def resource_plugin(cls,collection_name):
        """

            get the plugin class for this collection

        :param collection_name:
        :return: return the plugin class for this resource
        """
        try:
            plugin_class= resource.Resource.select_plugin(collection_name)
            return plugin_class
        except KeyError:
            raise ApplicationError("No plugin found for collection: %s" % collection_name)

    @classmethod
    def check_plugins(cls,*collections):
        """
            check plugin presence
        :param collection:
        :return:
        """
        for collection in collections:
            plugin_class= cls.resource_plugin(collection)
        return


    def collection_list(self):
        """

        :return:  list of collections (or plugin names) handle by the blueprint
        """
        config= current_app.config
        blueprint_name= request.blueprint
        return config['collections'][blueprint_name]

    def dispatch_request(self,*args,**kwargs):
        """

            dispatch the request to the resource to right plugin / plugin method

        :return:
        """

        # fetch collection list
        config= current_app.config
        self.collections= self.collection_list()


        # evaluate request url mode : ( root , collection , item , operation )
        if kwargs.has_key('operation'):
            mode= 'operation'
        elif kwargs.has_key('item'):
            mode= 'item'
        elif kwargs.has_key('collection'):
            mode= 'collection'
        else:
            mode= 'root'

        # evaluate target plugin method name
        if mode== 'operation':
            plugin_method_name= '_operation'
        else:
            # method name : root_get, collection_post, item_delete ...
            plugin_method_name= "%s_%s" % ( mode , request.method.lower())

        # evaluate plugin name ( resource name)
        if mode == 'root':
            plugin_name= 'root'
        else:
            plugin_name= kwargs['collection']
            # check collection is supported
            if not plugin_name in self.collections:
                #
                return self.error_response(404,message="collection not found: %s" % plugin_name  )

        # check plugin or resource exists
        plugin_class= self.resource_plugin(plugin_name)
        # mount plugin
        self.plugin= plugin_class(request=request,backend= config['backend'],**kwargs)

        # check target plugin method exists
        try:
            plugin_method= getattr(self.plugin,plugin_method_name)
        except AttributeError:
            # the target plugin method does not exists
            return self.error_response(405,message="method  %s not implemented" % plugin_method_name )

        # check auth if necessary
        if hasattr(self.plugin,'check_auth'):
            self.plugin.session= self.plugin.check_auth()
        else:
            self.plugin.session= None

        #
        # run method on plugin (collection_post, item_get _operation ...)
        #
        try:
            rc= plugin_method(**kwargs)
        except NotImplementedError:
            #return self.error_response(405,message="%s method not implemented" % mode  )
            raise ApplicationError("%s method not implemented" % mode ,405,status_code=405)

        except (ValueError,RuntimeError,AssertionError,KeyError) as e:
            result= 500
            try:
                error_message= e.message
            except:
                error_message= 'no message'
            message= "%s : %s" % (str(type(e)),error_message )
            logs= []
            raise CriticalError(message,result=result,logs=logs)
            pass

        except (ApplicationError,CriticalError) :
            raise

        except Exception as e:
            result = 500
            try:
                error_message = e.message
            except:
                error_message = 'no message'
            message = "%s : %s" % (str(type(e)), error_message)
            logs = []
            raise CriticalError(message, result=result, logs=logs)
            pass

        return self.response(rc)



    def response(self,data,status=200,logs=None):
        """

        :param status_code:
        :param data:
        :return:
        """
        logs= logs or []
        if isinstance(data,Response):
            # already a complete response , send it back
            return data
        else:
            # assume it is a json response to serialize
            data= json.dumps(data)
            r = Response(data,status=status,content_type='application/json')
            return r

    def error_response(self,code,message="",**kwargs):
        """

        :param code:
        :param kwargs:
        :return:
        """
        data= kwargs
        data['code'] = code
        if message:
            data['message']= message
        return self.response(data,code)
