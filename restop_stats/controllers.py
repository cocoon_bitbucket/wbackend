import time
from jinja2 import Template
from restop_graph import TraceParser,TraceDriver
from restop_queues.adapters import GenericAdapter


TRACE_FILENAME = "/tmp/trace-scheduler.txt"
TRACE_SCRIPTNAME = "/tmp/trace-scheduler"


script_pattern='''\
#!/bin/ash
while true; do
  {% for job in jobs %}echo ">->" \`date -u +"%Y-%m-%dT%H:%M:%SZ"\` {{ job.name }};{{ job.cmd }};echo ">-<"
  {% endfor %}
  sleep {{ delay }}
done
'''


def shell_escape(line):
    """
        prefix characters like ( ` " ) with back slash to avoid shell interpretation

    :param line:
    :return:
    """
    line= line.replace('"' , '\\"')
    #line= line.replace('`', '\\`')
    return line


class StatScheduler(GenericAdapter):
    """

        a command queue connected to a Queueconnector

    """
    # run mode: one of None,thread,process
    _run_mode = 'thread'

    collector_config_file = 'collectors.ini'
    collector_config = {}


    def __init__(self, model, log=None, run_mode=None,
                 with_curl=None,
                 alias=None,
                 feedback_url= None,
                 jobs=None,
                 period=None,
                 sync_period= None,
                 **kwargs):
        """
        :param model:
        :param log:
        :param run_mode:
        :param kwargs:
        """
        super(StatScheduler,self).__init__(model,log=log,run_mode=run_mode,**kwargs)

        self.alias= alias or self.model.name
        self.feedback_url = feedback_url or self.model.feedback_url

        jobs = jobs or []
        self.jobs = jobs

        if with_curl is None:
            with_curl= self.model.with_curl
        self.with_curl = with_curl

        self.trace_filename= TRACE_FILENAME
        self.trace_driver = TraceDriver(TRACE_FILENAME)

        self.freeze= False
        self.period = period or self.model.period
        self.sync_period = sync_period or self.model.sync_period
        self.t0 = time.time()
        self.t1 = self.t0
        self.t2 = self.t0

        # self.check()
        self.metrics = ""

        self.scheduler_on = False
        self.set_status('created')
        self.log.debug("scheduler %s created: scheduler_on is %s" % (self.agent_id, self.scheduler_on))
        return

    def writenl(self,line):
        """

        :return:
        """
        self.connector.write(line + b'\n')


    def next_scheduled_sync(self):
        """


        :return:
        """
        need_sync= False
        self.t2 = time.time()
        if (self.t2 - self.t1) > self.sync_period:
            # schedule a synchronization
            need_sync= True
            self.t1 = self.t2
        else:
            # no synchro
            pass
        return need_sync


    def transfer_in(self):
        """
            read commands from stdin
            execute commands
            return result via stdout

        :return:
        """
        if not self.freeze:
            # read stdin and transfer to stdout
            if len(self.model.stdin):
                line = self.model.stdin.popleft()
                # interpret command   cmd <json arguments>
                if ' ' in line:
                    command, arguments = line.split(' ', 1)
                else:
                    command = line
                    arguments = ""
                response = self.execute_command(command, arguments=arguments)
                self.model.stdout.append(response)


    def transfer_out(self,group=False):
        """
            check synchro delay (sync_period )
              run job_sync if needed


        :return:
        """
        #schedule a synchronization
        if not self.freeze:
            if self.scheduler_on :
                need_sync =self.next_scheduled_sync()
                if need_sync:
                        self.log.debug("auto scheduler job_sync period:%d" % self.sync_period)
                        self.job_sync()
                        self.log.debug("auto scheduler job_sync done.")
                        #pass

    #
    #
    #
    def get_by_curl(self,filename,url):
        """
            send curl command to stb

        :param filename:
        :param url:
        :return:
        """
        # rename scheduler file
        cmd_line= "mv -f %s %s.bak" %(filename,filename)
        r= self.writenl(cmd_line)

        # send via curl
        cmd_line = "curl -i -X POST --data-binary @%s.bak %s" % (filename,url)

        #self.log.debug("send command to stb serial: %s" % cmd_line)
        #r= self.target_client.send(cmd_line)
        r= self.writenl(cmd_line)

        return r

    def get_by_redis(self, filename, redis_key, redis_host, redis_port=6379):
        """

        :param filename:
        :param redis_key:
        :return:
        """

        cmd_line = 'while read line; do echo -e "rpush %s \\"${line}\\"" | nc %s %s; done  < %s >/dev/null' % (
            redis_key, redis_host, redis_port, filename
        )
        self.log.debug("send command to stb serial: %s" % cmd_line)
        #r = self.target_client.send(cmd_line)
        r = self.writenl(cmd_line)

        return r


    #
    # commands
    #
    def dummy(self,option=None):
        """

        :param option:
        :return:
        """
        return option

    def check(self):
        """

            check jobs are referenced

        :return:
        """
        for job in self.jobs:
            command_lines = self.trace_driver.gen_command_lines(job)
        return True

    @classmethod
    def check_metrics(cls,metrics=None):
        """ check metrics are referenced in catalog
        :return:
        """
        metrics= metrics or []
        for metric in metrics:
            trace_driver = TraceDriver('dummy')
            command = trace_driver.get_collector_command(metric)
        return True


    def job_init(self,metrics=""):
        """

        :metrics: string metrics names separated with space eg: df cpu
        :return:
        """
        metrics= metrics.strip()
        # save metrics for restart
        self.metrics= metrics
        self.log.debug("start the scheduler with metrics: %s" % metrics)

        # parse metric list
        if ' ' in  metrics:
            jobs= metrics.split(' ')
        else:
            jobs= [metrics]
        self.jobs= [ job.strip() for job in jobs]

        # stop the scheduler if it is still running
        self.job_stop()
        # send command to re-init scheduler file
        self.job_raz()
        jobs= []
        for name in self.jobs:
            cmd = TraceDriver.get_collector_command(name)
            jobs.append(dict(name=name,cmd=cmd))

        t = Template(script_pattern)
        command_lines= t.render(delay=self.period,jobs=jobs)

        script_path= TRACE_SCRIPTNAME
        #self.target_client.send("rm -f %s" % script_path)
        self.writenl("rm -f %s" % script_path)

        for l in command_lines.split('\n'):
            line = shell_escape(l)
            line = 'echo  "%s" >> %s' % (line, script_path)
            #r = self.target_client.send(line)
            r = self.writenl(line)

        time.sleep(5)
        #self.job_raz()
        self.job_start()

        self.log.debug("scheduler initialized")
        return True




    def job_raz(self):
        """
            send command to erase the scheduler file
        :return:
        """
        command_lines = self.trace_driver.gen_start_trace()
        command_string = ";".join(command_lines)
        self.writenl(command_string)
        self.log.debug("scheduler reset")
        return True


    def job_sync(self,restart=True):
        """
            synchronize target queue ( empty it )
        :return:
        """
        # curl -i -X POST -F data=@/tmp/trace-scheduler-1.txt https://192.168.1.21/test
        # while read line; do echo -e "rpush logs \"${line}\"" | nc 192.168.1.21 6379; done  < trace-scheduler-1.txt >/dev/null
        #redis_line= 'while read line; do echo -e "rpush logs \"${line}\"" | nc 192.168.1.21 6379; done  < trace-scheduler-1.txt >/dev/null'

        #trace_filename= '/tmp/trace-scheduler-1.txt'
        #feedback_url = 'https://192.168.1.21/feedback/stats/tv'

        # redis_line = 'while read line; do echo -e "rpush %s \\"${line}\\"" | nc %s %s; done  < %s >/dev/null' % (
        #     redis_log_key, redis_host,redis_port,trace_filename
        # )
        #
        # log.debug("sync scheduler file:%s , redis_key:%s" % (trace_filename,redis_log_key))

        # stop scheduling
        self.job_stop()

        if self.with_curl:
            # send curl command
            self.log.debug("send stats via curl from %s to %s" % (self.trace_filename, self.feedback_url))
            r= self.get_by_curl(self.trace_filename,self.feedback_url)
            time.sleep(5)
        else:
            # no curl on platform get via tty
            self.log.debug("send stats via console from %s to queue %s" % (self.trace_filename, self.connector.name))
            rc= self.get_by_console()
            time.sleep(15)
            #self.log.error('cannot send stat: no other methods than curl yet implemented')

        if restart:
            self.log.debug("restart scheduler")
            self.job_raz()
            self.job_start()
        return



    def job_stop(self):
        """
            stop the scheduler:  killall ask
        :return:
        """
        self.scheduler_on = False
        self.set_status('stopping...')
        self.log.debug("scheduler stopping...")
        self.writenl('killall ash')
        time.sleep(6)
        self.set_status('stopped')
        self.log.debug("scheduler stopped")
        return True

    def job_start(self):
        """
            start the scheduler

        :return:
        """
        self.scheduler_on = True
        #r= self.target_client.send('ash /tmp/trace-scheduler >> %s &' % self.trace_filename)
        cmd= 'ash /tmp/trace-scheduler >> %s &' % self.trace_filename
        r = self.writenl(cmd)
        self.set_status('running')
        self.log.debug("scheduler started")
        return r



    def job_close(self,clear=False):
        self.job_sync(restart=False)

        if clear:
            self.job_raz()
            self.log.debug("scheduler file cleared")
        self.set_status('closed')
        return True


    def exit(self):
        """

        :return:
        """
        # kill the scheduler
        self.log.debug('close scheduler')
        self.job_close()
        time.sleep(1)

        return super(StatScheduler,self).exit()

    #
    # tools
    #

    @classmethod
    def push_to_graphite(self, content, device='device', configfile='collectors.ini'):
        """

        :content: string  see format in restop_graph.busybox_stats.TraceParser
        :return:
        """
        parser = TraceParser(content, device=device, configfile=configfile)
        parser.push_all_to_graphite()
        return len(parser._commands)

    #
    #  get by console
    #


    def get_by_console(self):
        """

            send command to dump the /tmp/scheduler file to console

            read console stdout and put it in scheduler stdout

        :param filename:
        :param redis_db:
        :return:
        """
        # stop input/output
        self.freeze= True

        # send dump command to busybox  (cat /tmp/trace-scheduler.txt)
        rc= self._job_dump_trace()

        # wait for the busybox writing to stdout
        time.sleep(5)
        # read busybox stdout and transfer to scheduler stdout
        data= self._job_read_trace()
        if data:
            self.log.debug('get_by_console: send data to graphite (len: %d)' % len(data))
            nb_metrics = 0
            try:
                parser= TraceParser(data,
                                            device= self.alias,
                                            configfile=  self.collector_config_file,
                                            config= self.collector_config,
                                            log=self.log)
                if parser.push_all_to_graphite():
                        nb_metrics= len(parser._commands)

            except Exception,e:
                self.log.error('get_by_console,%s' % str(e))
                nb_metrics=0
            self.log.debug("number of metrics sent to graphite: %d" % nb_metrics)
        else:
            self.log.warning("get_by_console: no data to push to graphite")

        # reset sync counter
        self.t1 = time.time()
        # restart input/output
        self.freeze= False
        return True

    def _flush(self):
        """
            flush stdout to queue
        :return:
        """
        line= self.connector.readline()
        while  line:
            line=self.connector.readline()
        return


    def _job_dump_trace(self):
        """
            send command to target queue to dump trace file


        :return:
        """
        # send dump trace file to target client
        time.sleep(1)
        self._flush()
        command_lines= self.trace_driver.gen_dump_command()
        command_string= ";".join(command_lines)
        self.writenl(command_string)


    def _job_read_trace(self):
        """
            read lines from target queue and store them to self stdout queue
        :return:
        """
        stop = False
        top= False
        data=[]
        self.set_status('collecting')
        self.log.debug('collect trace-scheduler data')
        while ( not stop):
            # read line from target queue
            line= self.connector.readline()
            if not line:
                self.log.debug('collect  done')
                break
            if line.startswith('>-['):
                # start of trace file detected
                top= True
                self.log.debug('start of trace detected')
            elif line.startswith('>-]'):
                top = False
                self.log.debug('end of trace detected')
                data.append(line)
            if top:
                # store line to self stdout queue
                data.append(line)
            else:
                # store line to log
                self.model.stdlog.append(line)
        self.set_status('collected')
        self.log.debug('collect trace-scheduler done')
        return data