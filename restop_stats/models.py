from walrus import *
from restop_queues.models import Queue,QueueHub




class StatSchedulerQueue(QueueHub):
    """


    """
    collection='statschedulerqueue'

    with_curl = BooleanField(default=True)
    feedback_url = TextField(default='http://localhost/feedback_url')
    period= IntegerField(default=10)
    sync_period= IntegerField(default=60)

    collector_init= TextField(default='./collectors.ini')
    collector_host= TextField(default='127.0.0.1')
    collector_port= IntegerField(default=2003)

    # name of the destination queue sink: -> queue:id:sink
    #connector= TextField(default='sink')

    @classmethod
    def create(cls, name='statscheduler',
               # with_curl=True,
               # feedback_url= 'http://localhost:5000/feedback_url',
               # period= 10,
               # sync_period= 60,
               parameters=None, **kwargs):
        """

        :param name:
        :param parameters:
        :param kwargs:
        :return:
        """
        queue= super(StatSchedulerQueue,cls).create(name=name,parameters=parameters,**kwargs)
        # for field in ('with_curl', 'feedback_url'):
        #     try:
        #         setattr(queue,field,parameters[field])
        #     except KeyError:
        #         pass
        return queue



