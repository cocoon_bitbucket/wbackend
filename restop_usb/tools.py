from wbackend.models import Model
from .models import UsbQueueHub
from .controllers import ProcessUsbQueueHubServer

# Model must be bound first

def start_usb_hub(data=None):
    """

        _loop:
          device: 'loop://?logging=debug'
        tv:
          device: /dev/ttyUSB0
          bauds: 115200

    :param data:
    :return:
    """

    assert Model.database, "Model must be bound"

    data= data or {}
    hub_model= UsbQueueHub.create()
    server= ProcessUsbQueueHubServer(hub_model)
    if not '_loop' in data.keys():
        server.create_queue_server('_loop',device='loop://?logging=debug')
    for name, values in data.iteritems():
        server.create_queue_server(name,**values)

    server.start()
    return server