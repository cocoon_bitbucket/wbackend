import serial
from serial.serialutil import to_bytes
import logging


from restop_queues.adapters import AbstractConnector

loop_serial= "loop://?logging=debug"
mock_serial= "mock://?logging=debug"



class SerialConnector(AbstractConnector):
    """




    """

    def __init__(self,name, device=loop_serial,bauds=115200,log=None,**kwargs):
        """

        :param device:
        :param bauds:
        """
        super(SerialConnector,self).__init__(name,log=log,**kwargs)
        self.device= device
        self.bauds= bauds

        self._driver=None


    def open(self, **kwargs):
        """
            start serial port

        """

        device= self.device
        bauds= self.bauds
        # create serial port
        if device.startswith('loop://'):
            # dummy loop serial
            self.log.debug('serial connector: start pyserial loop(%s)' % (device))
            self._driver=serial.serial_for_url(device)
        # elif device.startswith('mock://'):
        #     # dummy mock serial
        #     self.log.debug('serial server: start mock adapter(%s)' % (device))
        #     self.proc=MockProcess(device[7:],redis_db=self.database)
        else:
            self.log.debug('serial connector: opening serial port with  pyserial (%s|%s)' % (device,bauds))
            self._driver=serial.Serial(
                port=device,baudrate=bauds,bytesize=serial.EIGHTBITS,parity= serial.PARITY_NONE, stopbits=1,
                timeout=1
            )
            self.log.debug('serial connector: port open')

        self._driver._timeout=0
        #self._driver._timeout = 1


        return True


    def close(self):
        """
            kill slave process
        """
        self.log.debug('serial connector: closing port for %s' % str(self._driver))
        if self._driver:
            self._driver.close()
            del self._driver
            self._driver=None
        self.log.debug("serial connector: port closed")
        return
    close=close

    def write(self, line,**kwargs):
        """
            write line to serial port

        :param line:
        :return:
        """
        # line= to_bytes(line)
        if self._driver:
            self._driver.write(line)
            self._driver.flush()
        else:
            self.log.error('serial connector: no driver')


    # def read(self, **kwargs):
    #     """
    #
    #     :param channel_name:
    #     :return:
    #     """
    #     if self._driver:
    #         try:
    #             if self._driver.in_waiting:
    #                 lines = self._driver.readlines()
    #                 # lines = self.hook_on_readlines(lines)
    #                 return "".join(lines)
    #         except Exception, e:
    #             self.log.error('serial connector: readlines failed %s' % e)
    #             raise e
    #     else:
    #         self.log.error('serial connector: no connector')
    #     return None

    def readline(self,size=-1, **kwargs):
        """

        :param size:
        :param kwargs:
        :return:
        """
        if self._driver:
            line= self._driver.readline()
            return line
        else:
            self.log.error('serial connector: no connector')
        return None
