__author__ = 'cocoon'
"""

    GET /usbhub/queues/   list of usbqueues

    GET /usbHub/redis    redis config



"""
import os
import json
from flask import Flask

from flask.config import Config

from wbackend.model import Model, Database
from restop_usb.models import UsbQueueHub
from restop_usb.controllers import ProcessUsbQueueHubServer


import logging
logging.basicConfig(level=logging.DEBUG)



root_path= dir_path = os.path.dirname(os.path.realpath(__file__))


# get config from local config.cfg
config= Config(root_path)
config.from_pyfile('config.py')

#master_url= config['RESTOP_MASTER_URL']
#hub_name= config['RESTOP_HUB_NAME']
#platform_name= config.get('RESTOP_PLATFORM_NAME','default')

# create client to master
#master= RestopClient(base_url=master_url)
#config= yaml.load(file('config.yml','r'))


redis_host= config.get('REDIS_HOST','localhost')
redis_port= config.get('REDIS_PORT',6379)
redis_db= config.get('REDIS_DB',0)

redis_flushdb= config.get('REDIS_FLUSHDB',False)

logging.info('set redis database and bind model')
db = Database(host=redis_host, port=redis_port, db=redis_db)

if redis_flushdb:
    db.flushdb()

Model.bind(database=db, namespace=None)
database= Database(host=redis_host,port=redis_port,db=redis_db)


logging.info("create hub queue")
hub_queue= UsbQueueHub.create(name='default')

logging.info("create hub process")
hub_server= ProcessUsbQueueHubServer(hub_queue)

logging.info("create usb queues if any")
if 'QUEUES' in config:
    for name,value in config['QUEUES'].iteritems():

        logging.info("create usb queue %s with %s" % (name,value))

        hub_server.create_queue_server(
            name,
            device= value.get('device'),
            bauds= value.get('bauds',115200)
        )

        continue



app = Flask('usbhub')


@app.route('/')
def index():
    """

    :return:
    """
    return 'hello from USB HUB'

@app.route('/usbhub/config')
def url_config():
    """

    :return:
    """
    return json.dumps(config)



@app.route('/usbhub/queues')
def usbqueues():
    """

    :return:
    """
    queues =config.get('QUEUES',{})
    return json.dumps(queues)




logging.info("start USB Hub server")
hub_server.start()

logging.info("start USB Hub http server")
app.debug = False
app.run(host=config['APP_HOST'], port=config['APP_PORT'])


