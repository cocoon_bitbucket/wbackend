from walrus import *
from wbackend.graph import Graph
from restop_queues.models import Queue,QueueHub




class UsbQueue(Queue):
    """
        a local session on a hub

    """
    collection = 'usbqueue'

    # name = TextField(primary_key=True)
    #
    device = TextField(index=True)
    bauds = IntegerField(default=115200)

    @classmethod
    def create(cls, name,parameters=None, **kwargs):
        """

        :param name:
        :param parameters:
        :param kwargs:
        :return:
        """
        parameters= parameters or {}
        obj=cls(name=name,parameters=parameters, **kwargs)
        obj.device=parameters['device']
        if 'bauds' in parameters:
            obj.bauds= int(parameters['bauds'])
        obj.save()
        return obj


class UsbQueueHub(QueueHub):
    """
        an Usb queue controller

    """
    collection= 'usbqueuehub'
    _default_name= 'default'


    # @classmethod
    # def create(cls, name=None,parameters=None,**kwargs):
    #     """
    #     Create a new local session
    #     """
    #     #members = []
    #     #if 'members' in kwargs:
    #     #    members = kwargs.pop('members')
    #
    #     name= name or  cls._default_name
    #     parameters= parameters or {}
    #     instance = cls(name=name, parameters=parameters,**kwargs)
    #     instance.save()
    #     instance.stdlog.append('Usb Controller model created')
    #     #instance.create_queues()
    #     return instance
    #
    # @classmethod
    # def load(cls, primary_key=None, convert_key=True):
    #     primary_key= primary_key or cls._default_name
    #     return super(UsbQueueHub,cls).load(primary_key,convert_key=convert_key)



    # def create_queues(self,data):
    #     """
    #     :param data: dictinaries
    #     :return:
    #     """
    #     for name,value in data.iteritems():
    #         q= UsbQueue(name=name,**value)
    #         self.logs.append('create_queue %s with %s' % (name,str(value)))
    #         q.save()
    #     return



