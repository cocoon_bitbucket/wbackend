
APP_HOST= '0.0.0.0'
APP_PORT= 5017

REDIS_HOST= 'localhost'
REDIS_PORT= 6379
REDIS_DB= 0
REDIS_FLUSHDB= False

BAUDS= 115200
DEVICES= [
  "/dev/ttyUSB0",
  "/dev/ttyUSB1",
  "/dev/ttyUSB2",
  "/dev/ttyUSB3"
]

QUEUES= {
  '_loop': {
    'device': "loop://?logging=debug"
  },
  '_mock': {
    'device': "loop://?logging=debug"
  },
  'tv': {
    'device': "/dev/ttyUSB0",
    'bauds': 115200
  }
}
