
import time
import json

from restop_queues import QueueServerBase
from restop_queues.adapters import AbstractAdapter

from .models import UsbQueue
from .connectors import SerialConnector

loop_serial= "loop://?logging=debug"
mock_serial= "mock://?logging=debug"


#
# usb or serial adapter queue
#

class UsbQueueServerBase(AbstractAdapter):
    """

        a process to handle serial or usb queues

    """
    def __init__(self,model,log=None,**kwargs):
        """

        :param model: instance of UsbQueue
        :param log:
        :param kwargs:
        """
        super(UsbQueueServerBase,self).__init__(model,log=log,**kwargs)




    def open_connector(self):
        """
            start serial port

        """
        name= self.model.get_hash_id()
        self.connector=SerialConnector(
            name,log=self.log,device=self.model.device,bauds=self.model.bauds)
        self.connector.open()

        return True


    # def stop_connector(self):
    #     """
    #         kill slave process
    #     """
    #     if self.connector:
    #         self.connector.stop()
    #
    #
    # def write_connector(self, line):
    #     """
    #         write line to serial port
    #
    #     :param line:
    #     :return:
    #     """
    #     if self.connector:
    #         self.connector.write(line)
    #     else:
    #         self.log.error('no connector')
    #
    #
    # def read_connector(self):
    #     """
    #
    #     :param channel_name:
    #     :return:
    #     """
    #     if self.connector:
    #         return self.connector.read()
    #     else:
    #         return ""


class ThreadedUsbQueueServer(UsbQueueServerBase):
    """

    """
    _run_mode= 'thread'



class ProcessUsbQueueServer(UsbQueueServerBase):
    """
        a threading version of adapter

    """
    _run_mode= 'process'


#
#  serial Hub
#



class UsbQueueHubServerBase(QueueServerBase):
    """

        a process to handle a set of usb queue servers

    """

    def __init__(self, model, log=None, **kwargs):
        """

        :param model:
        :param log:
        :param kwargs:
        """
        super(UsbQueueHubServerBase, self).__init__(model, log=log, **kwargs)

    def run_once(self):
        """
                echo mode
        :return:
        """
        self.keep_alive()

        # read stdin and transfer to stdout
        if len(self.model.stdin):
            line = self.model.stdin.popleft()
            # interpret command   cmd <json arguments>
            if ' ' in line:
                command, arguments= line.split(' ',1)
            else:
                command= line
                arguments= ""
            response= self.execute_command(command,arguments=arguments)
            self.model.stdout.append(response)

        self.handle_signal()
        return

    def start_queue_server(self,name,run_mode= 'thread'):
        """
            start a thread to handle a queue

            usbqueue:id:<name>

        :name: string , name of the queue ( tv, _loop) etc...
        :return:
        """
        queue_model = UsbQueue.load(name)
        agent_id= queue_model.get_hash_id()
        queue_server= UsbQueueServerBase(queue_model,log=self.log)
        queue_server._run_mode= run_mode
        queue_server.start()

        #self.model.add_queue(queue_server)
        return agent_id

    def create_queue_server(self,name,device,bauds=115200):
        """
            start a thread to handle a queue

            usbqueue:id:<name>

        :name: string , name of the queue ( tv, _loop) etc...
        :return: queue hash id
        """
        queue = UsbQueue.create(name,parameters=dict(device=device,bauds=bauds))
        return queue.get_hash_id()

    def clear_queue(self,name):
        """
            reset all channels of queue ( stdin,stdout,stdlog )
        :param name:
        :return:
        """
        try:
            queue = UsbQueue.load(name)
            rc= queue.clear()
            rc= True
        except Exception,e:
            # not found
            rc= False
        return rc




    def execute_command(self,command,arguments=None):
        """

        :param command: string,   q:tv ,
        :param content:
        :return:
        """
        command= command.strip('\n').strip()
        try:
            kwargs= json.loads(arguments)
        except:
            kwargs= {}
        self.log.debug('received command: %s %s' % (command,arguments))
        response=""
        try:
            func= getattr(self,command)
            response= func(**kwargs)
        except AttributeError:
            # not implemented
            response= "command %s not implemented" % command
            self.log.debug(response)

        except Exception,e:
            response= "execution raised: %s" % e
            self.log.error(response)

        response= json.dumps(response)
        self.log.debug('response is %s' % response)
        return response

    # def send_to_queue(self,queue_name,content):
    #     """
    #
    #     :param queue:
    #     :param content:
    #     :return:
    #     """
    #     if queue_name in self.queue_index.keys() :
    #         queue= self.queue_index[queue_name]
    #         #rc= queue.proc.write(queue_name)
    #         rc= queue._stdin.append(content)
    #     else:
    #         # queue not active
    #         raise KeyError('no such queue or inactive: %s' % queue_name)
    #     return




    def run(self):
        """


        :return:
        """
        while not self.Terminated:

            self.run_once()

            time.sleep(1)



class ThreadedUsbQueueHubServer(UsbQueueHubServerBase):
    """
        a process version of adapter

    """
    _run_mode = 'thread'



class ProcessUsbQueueHubServer(UsbQueueHubServerBase):
    """
        a process version of adapter

    """
    _run_mode = 'process'

