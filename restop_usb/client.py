import time
import json
from restop_queues.client import QueueClient

from . import controller_collection, queue_collection

default_redis_url="redis://localhost:6379/0"



class UsbControllerClient(QueueClient):
    """



    """
    def __init__(self,name='default',log=None,redis_db=None,**kwargs):
        """

        """
        agent_id= "%s:id:%s" % (controller_collection,name)
        super(UsbControllerClient,self).__init__(agent_id,log=log,redis_db=redis_db,**kwargs)

        return

    def get_adapter_for_queue(self,queue_name):
        """
            return a client adapter for this queue

        :param queue_name:
        :return:
        """
        agent_id= '%s:id:%s' % (queue_collection,queue_name)
        # usbqueue:id:_loop
        adapter= QueueClient(agent_id,redis_db=self.database)
        return adapter

    def start_queue_server(self,name):
        """

        :param name:
        :return: a client adapter to that queue
        """
        command= "start_queue_server %s" % json.dumps(dict(name=name))
        self.write(command)
        time.sleep(3)
        response= self.readline()
        agent_id= json.loads(response)

        adapter= QueueClient(agent_id,redis_db=self.database)
        return adapter

    def create_queue_server(self,name,device,bauds=115200):
        """

        :param device:
        :param bauds:
        :return:
        """
        command = "create_queue_server %s" % json.dumps(dict(name=name,device=device,bauds=bauds))
        self.write(command)
        time.sleep(2)
        response = self.readline()
        agent_id= json.loads(response)
        return agent_id


    def clear_queue(self,name):
        """

        :param name: string name of the queue ( _loop, tv ...)
        :return:
        """
        command = "clear_queue %s" % json.dumps(dict(name=name))
        self.write(command)
        time.sleep(2)
        response = self.readline()
        result= json.loads(response)
        return result


    # def send_to_queue(self,queue_name='_loop',content=""):
    #     """
    #
    #     :param queue_name:
    #     :param content:
    #     :return:
    #     """
    #     command="!q:%s!%s" % (queue_name,content)
    #     self.write(command)

    # def read_from_queue(self,queue_name='_loop'):
    #     """
    #
    #     :param queue_name:
    #     :param content:
    #     :return:
    #     """
    #     command="read:%s" % (queue_name)
    #     self.send(command)