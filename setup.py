#!/usr/bin/env python

from distutils.core import setup

setup(name='wbackend',
      version='1.0',
      description='restop core framework',
      author='Laurent Tordjman',
      author_email='laurent.tordjman@orange.com',
      url='',
      packages=[
          'applications', 'applications.master', 'applications.samples','applications.mock_app',
          'restop','restop.plugins',
          'restop-client',
          'restop_adapters',
          'restop_apidoc',
          'restop_graph', 'restop_graph.gdiamond',
          'restop_platform',
          'restop_queues', 'restop_queues.apps','restop_queues.connectors','restop_queues.controllers',
          'restop_queues.tutorial.usb_queues',
          'restop_stats',
          'restop_trace',
          'restop_usb',
          'wbackend', 'wbackend.redislog'
      ],
     )
