from restop_adapters.client import ClientAdapter
from restop_tests.mockdevice.console import MockConsole


MOCK_CONSOLE_KEY= "mock:id:serial"

def test_basic():
    """

    :return:
    """
    agent_id= MOCK_CONSOLE_KEY
    command_line= ''

    client= ClientAdapter(agent_id)
    console= MockConsole(agent_id,command_line)


    client.send('ls -l')

    console.transfer_in()

    console.transfer_out()

    r0= client.read()
    assert r0 == 'ls -l\n'

    r1= client.read()
    assert 'total' in r1
    r2= client.read()
    assert len(r2)


    return

if __name__=="__main__":


    test_basic()

    print "Done"