
import requests

from restop_tests.mockserver.mockclient import MockClient

#path= imp.find_module('os')
#parts= path[1].split('/')
#parts= parts[:-1]
#lib_path= "/".join(parts[:-1])

#print lib_path


def get_mock_client(host='localhost',port=5016):
    """


    :return:
    """
    client= MockClient(host=host,port=port)
    client.start_mockserver(threaded=True)
    return client




def test_mockserver():

    c= get_mock_client()


    r= c.check_mockserver()

    r= c.create_mock_url('/my/url', 'POST', status=200, data= {'name':'name'})


    r= requests.post('http://localhost:5016/my/url',json={'new':'new'})
    assert r.status_code==200
    data=r.json()
    assert data['name'] == 'name'

    r= c.mock_url_usage(clear=False)
    assert r['my/url']['POST']['new'] == 'new'


    r = c.mock_url_usage(url='/my/url', method='POST', clear=True)
    assert r['new'] == 'new'

    r= c.mock_url_usage(url='/my/url')
    assert len(r) == 0

    #client.start_mockserver()

    c.shutdown_mockserver()

    return



test_mockserver()
print "Done"