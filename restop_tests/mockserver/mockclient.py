
import os
import requests
import subprocess
import json
import time

try:
    from restop_tests.mockserver.mockserver import ThreadedMockserver
except ImportError:
    ThreadedMockserver=None



dir_path = os.path.dirname(os.path.realpath(__file__))

class MockClient(object):
    """


    Set MockServer
    Start Mockserver
    Check Mockserver
    Shutdown Mockserver
    Clear Mockserver
    Create Mock Url



    """
    def __init__(self,host='localhost',port=5016,log=None,mockserver=None):
        """

        """
        self.set_mockserver(host=host,port=port,log=log,mockserver=mockserver)
        #self._host= 'localhost'
        #self._port= 5000
        #self._url = "http://%s:%s" % (self._host, self._port)
        self._pid= None
        #self._log='mockserver.log'
        #self._mockserver= 'mockserver.py'

        self.rs = requests.session()
        self.rs.headers.update({'content-type': 'application/json'})

    def set_mockserver(self,host='localhost',port=5016,log=None,mockserver=None):
        """


        :param host:
        :param port:
        :return:
        """
        self._host = host
        self._port = port
        self._url = "http://%s:%s" % (host, port)
        self._mockserver= mockserver or "mockserver.py"
        self._log=log or "mockserver.log"


    def start_mockserver(self,host=None,port=None,log=None,debug=False,mockserver=None,threaded=False):
        """
            [Documentation]  start a mockserver
            Start Process   ${mockserver_executable}  ${mockserver_host}  ${mockserver_port}
            builtin.sleep  2
        """
        host=host or self._host
        port=port or self._port
        log= log or self._log

        if threaded:
            # start server into a separate thread
            if ThreadedMockserver:
                server= ThreadedMockserver(host,port,debug=debug)
                server.start()
                self._pid=0
            else:
                # no mockserver installed
                raise RuntimeError('threaded mode not available. (cannot import mockserver)')
        else:
            #start a server into a process
            mockserver=  mockserver or self._mockserver
            FOUT = open(log, 'w')
            self._pid = subprocess.Popen([mockserver, host, str(port)],stdout=FOUT,stderr=FOUT,shell=True).pid
            time.sleep(2)
        return

    def check_mockserver(self,uri='/api', session='default'):
        """

        :param session:
        :return:

          [Documentation]  check mockserver is on line
          [Arguments]  ${session}=${default_mocksession}

          ${resp}=  get request  ${session}  uri=/api
          log many  ${resp}
          Should Be Equal As Strings  ${resp.status_code}  200

        """
        url= self._url + uri
        r= self.rs.get(url,timeout= 2)
        assert r.status_code == 200


    def shutdown_mockserver(self,session="default"):

        """
            [Documentation]  shutdown a running mockserver via /api/shutdown
            [Arguments]  ${session}=${default_mocksession}

            Post Request  ${session}  uri=/api/shutdown
            builtin.sleep  2
        """

        url = self._url + '/api/shutdown'
        r = self.rs.post(url)
        assert r.status_code == 200
        time.sleep(2)


    def clear_mockserver(self,session="default"):

        """
            [Documentation]  clear a running mockserver via /api/clear
            [Arguments]  ${session}=${default_mocksession}

            Post Request  ${session}  uri=/api/clear
            builtin.sleep  2
        """
        url = self._url + '/api/clear'
        r = self.rs.post(url)
        assert r.status_code == 200
        time.sleep(2)

    def mock_url_usage(self, url = None, method=None,clear= True,session="default"):
        """
            [Documentation]  shutdown a running mockserver via /api/shutdown
            [Arguments]  ${session}=${default_mocksession}

            Post Request  ${session}  uri=/api/clear
            builtin.sleep  2
        """
        api_url = self._url + '/api/usage'
        data= dict( url= url,method=method,clear=clear)
        r = self.rs.post(api_url,json=data)
        assert r.status_code == 200
        return r.json()


    def create_mock_url(self,url,method,data=None,status=200,content_type='application/json',session="default"):
        """
          [Documentation]  create a mock url on mockserver via it s api
          [Arguments]  ${url}  ${method}  ${data}  ${status}=200   ${session}=${default_mocksession}


          ${request_data}=  create dictionary  url=${url}  method=${method}  status=${status}  content=${data}
          log many  ${request_data}
          #${json_data}=  ${request_data.json()}

          ${json_data}=  Stringify Json  ${request_data}

          ${headers}=  create dictionary  content-type=application/json

          ${resp}=  post request  ${session}  uri=/api/url  data=${json_data}  headers=${headers}
          Should Be Equal As Strings  ${resp.status_code}  200
        """
        api_url = self._url + '/api/url'
        payload= dict( url= url, method= method,status=status,content=data,content_type=content_type)
        r = self.rs.post(api_url, json=payload )
        assert r.status_code == 200

mockclient=MockClient


if __name__=="__main__":

    c= MockClient()

    r= c.check_mockserver()
    r= c.create_mock_url('/my/url', 'POST', status=200, data= {'name':'name'})

    r= requests.post('http://localhost:5000/my/url',json={'new':'new'})

    r= c.mock_url_usage(clear=False)
    r = c.mock_url_usage(url='/my/url', method='POST', clear=True)
    r= c.mock_url_usage(url='/my/url')





    print r