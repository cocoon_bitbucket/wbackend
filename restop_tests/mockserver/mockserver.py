#!/usr/bin/env python
__version__= 0.1

from flask import Flask
from flask import request,Response,json,abort,jsonify

from flask import Blueprint

import threading


topography= {}
url_usages={}

# topography= {
#
#     'my/first/url': {
#         'GET': {
#             'content': { 'name':'my_first_url'}
#         }
#     },
#     'my/second/url': {
#         'GET': {
#             'content': {'name': 'my_second_url'},
#             'content-type': "application/json"
#         },
#         'POST': {
#             'content': {'name': 'my_second_url'},
#             'status': 201
#         },
#     }
#
# }


mock = Blueprint('mock', __name__)



all_methods= ['GET', 'POST','PUT','PATCH','DELETE','HEAD','OPTIONS']


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()



@mock.route('/api')
def hello_world():
    return 'MockServer api version: %s' % str(__version__)

@mock.route('/api/shutdown', methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Server shutting down...'

@mock.route('/api/clear', methods=['POST'])
def clear():
    global topography, url_usages
    topography= {}
    url_usages= {}
    return 'Server is clear'

@mock.route('/api/url', methods=['POST'])
def new_url():
    """

        create an url

        parameters: {
            url: ''
            method: ''
            content: {}
        }

    :return:
    """
    parameters= request.json

    try:
        url=parameters['url']
        method=parameters['method']
        content=parameters.get('content','')
        status= parameters.get('status',200)
        content_type= parameters.get('content_type','application/json')
    except KeyError,e:
        abort(400,e.message)

    if url.startswith('/'):
        url=url[1:]

    if not parameters['url'] in topography:
        topography[url]= {}

    if not method in topography[url]:
        topography[url][method]={}

    # set url content
    topography[url][method]['content']= content
    topography[url][method]['status'] = status
    topography[url][method]['content_type'] = content_type

    return 'url created'


#
# requesting mock url usage
#
@mock.route('/api/usage', methods=['GET','POST'])
def url_usage():
    """

    :return:
    """
    global url_usages
    if request.content_type == 'application/json':
        parameters = request.json
    else:
        parameters=request.args

    url= parameters.get('url',None)
    method= parameters.get('method',None)
    clear= parameters.get('clear',False)

    result={}
    if url:
        if url.startswith('/'):
            # remove starting /
            url=url[1:]
        # return usage for one url
        if method:
            # return usage for one url , one method
            try:
                result= url_usages[url][method]
                if clear:
                    del url_usages[url][method]
            except KeyError:
                pass
        else:
            # return usage for one url , all methods
            try:
                result= url_usages[url].copy()
                if clear:
                    del url_usages[url]
            except KeyError:
                pass
    else:
        # usage for all url
        result= url_usages.copy()
        if clear:
            url_usages={}

    response= jsonify(result)
    return response



#
# serving dynamic urls
#
@mock.route('/', defaults={'path': ''}, methods=all_methods)
@mock.route('/<path:path>' , methods=all_methods)
def catch_all(path):
    """


    :param path:
    :return:
    """
    method= request.method

    try:
        # search url in data
        url_data= topography[path]
    except KeyError:
        # 404
        abort(404)

    try:
        # search method for this url
        method_data= url_data[method]
    except KeyError:
        # 405 not allowed
        abort(405)

    try:
        content_type= method_data['content_type']
    except KeyError:
        content_type='application/json'

    try:
        status= method_data['status']
    except KeyError:
        status= 200

    content= method_data['content']

    if content_type == 'application/json':
        content= content or {}
        data=json.dumps(content)
    else:
        data= str(content)

    resp= Response(status=status,content_type=content_type)
    resp.data= data

    register_url_usage(path,request)

    #return 'You want path: %s' % path
    return resp



def register_url_usage(path,request):
    """


    :param request:
    :return:
    """
    if not path in url_usages:
        url_usages[path]={}
    if not request.method in url_usages[path]:
        url_usages[path][request.method] = {}
    # store usage
    content_type= topography[path][request.method]['content_type']
    if content_type == "application/json":
        url_usages[path][request.method] = request.json
    else:
        url_usages[path][request.method] = request.data
    return

def get_app():

    # create flask app
    app = Flask(__name__)
    app.config.update(
        SECRET_KEY='guess what'
    )


    # register blueprints
    app.register_blueprint(mock)


    @app.route('/')
    def index():

        return "dummy server"

    return app


def start_app(host='localhost',port=5016,debug=True, use_reloader=True):
    """

    :return:
    """
    app= get_app()
    app.run(host=host, port=port, debug=debug,use_reloader=use_reloader)

    return


class ThreadedMockserver(threading.Thread):
    """
        a threading version of mockserver

    """
    def __init__(self,host='localhost',port=5016,debug=False,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        self.host=host
        self.port=port
        self.debug=debug
        self.Terminated=False
        self.setDaemon(True)

    def run(self):
        """

        :return:
        """
        start_app(host=self.host,port=self.port,debug=self.debug,use_reloader=False)



if __name__=='__main__':

    import sys

    host="localhost"
    port=5000
    debug=False


    if len(sys.argv) > 1:
        host= sys.argv[1]
    if len(sys.argv) > 2:
        port= int(sys.argv[2])
    if len(sys.argv) > 3:
        debug= True

    app = get_app()
    app.run(host=host,port=port,debug=debug)

