import flask
from flask import request,jsonify
from flask.config import Config
from walrus import Database
from wbackend.model import Model
from restop_tests.mockserver.mockserver import get_app
from queue_server import QueueServer


platform_name= 'default'

# get config from local config.cfg
config= Config('./')
config.from_pyfile('config.cfg')

http_host= '0.0.0.0'
http_port= config['MOCK_HTTP_PORT']
http_debug= True

redis_host= config['MOCK_REDIS_HOST']
redis_port= config['MOCK_REDIS_PORT']
redis_db= config['MOCK_REDIS_DB']


db = Database(host=redis_host, port=redis_port, db=redis_db)
Model.bind(db)

if config.get('MOCK_REDIS_FLUSH_DB ',False):
    db.flushdb()


app = get_app()
app.config.update(config)

app.config['redis_db']= db

@app.route('/feedback_url')
def feedback_url():
    """

    :return:
    """
    print "receive data from feedback_url: %s"
    data= request.get_data()
    print "data length=%d" % len(data)
    with open('./feedback.txt' ,'w') as fh:
        fh.write(data)
    return ""


@app.route('/mock/console/<queue>', methods=['POST','GET'])
def mock_console_queue(queue,message=None):
    """

    """
    app= flask.current_app
    console= app.config['console']
    if request.method == 'GET':
        # read mock:id:serial
        #message= "GET /custom/serial/stdin"
        message = console.show(queue)
        #message= str(message) or ""
        response= jsonify(message)
    else:

        #message= "/mock/console/<queue>: wrong method"
        message= request.data
        rc= console.send(message)
        response= jsonify(True)

    return response


if __name__=="__main__":

    # start the queue server
    qserver= QueueServer(app.config['QUEUES'])
    qserver.start()

    #start http_debug server
    app.run(host=http_host ,port=http_port ,debug=http_debug,threaded=True)

    print "Done"